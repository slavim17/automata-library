project(alt-libgraph_algo VERSION ${CMAKE_PROJECT_VERSION})
find_package(LibXml2 REQUIRED)
alt_library(alib2graph_algo
	DEPENDS alib2graph_data alib2xml alib2common alib2abstraction alib2measure alib2std
	TEST_DEPENDS LibXml2::LibXml2
)

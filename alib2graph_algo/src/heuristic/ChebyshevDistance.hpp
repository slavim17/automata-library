// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <cmath>
#include <alib/pair>
#include <alib/functional>

namespace graph {

namespace heuristic {

class ChebyshevDistance {
// ---------------------------------------------------------------------------------------------------------------------

 public:
  template<typename TCoordinate>
  static TCoordinate chebyshevDistance(const ext::pair<TCoordinate, TCoordinate> &goal,
                                       const ext::pair<TCoordinate, TCoordinate> &node);

// ---------------------------------------------------------------------------------------------------------------------

  template<typename TCoordinate>
  static std::function<TCoordinate(const ext::pair<TCoordinate, TCoordinate> &,
                                   const ext::pair<TCoordinate, TCoordinate> &)> chebyshevDistanceFunction();

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================

template<typename TCoordinate>
TCoordinate ChebyshevDistance::chebyshevDistance(const ext::pair<TCoordinate, TCoordinate> &goal,
                                                 const ext::pair<TCoordinate, TCoordinate> &node) {
  return std::max(node.first - goal.first, node.second - goal.second);
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate>
std::function<TCoordinate(const ext::pair<TCoordinate, TCoordinate> &,
                          const ext::pair<TCoordinate, TCoordinate> &)> ChebyshevDistance::chebyshevDistanceFunction() {
  return chebyshevDistance < TCoordinate >;
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace heuristic

} // namespace graph


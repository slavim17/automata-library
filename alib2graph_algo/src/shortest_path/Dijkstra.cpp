// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "Dijkstra.hpp"

#include <registration/AlgoRegistration.hpp>

namespace graph::shortest_path {

class DijkstraBidirectional {};

} // namespace graph::shortest_path

namespace {

// ---------------------------------------------------------------------------------------------------------------------
// uni-directional

auto Dijkstra1 = registration::AbstractRegister<graph::shortest_path::Dijkstra,
                                                ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                const graph::WeightedUndirectedGraph<> &,
                                                const DefaultNodeType &,
                                                const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathRegistration);

auto Dijkstra2 = registration::AbstractRegister<graph::shortest_path::Dijkstra,
                                                ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                const graph::WeightedUndirectedMultiGraph<> &,
                                                const DefaultNodeType &,
                                                const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathRegistration);

auto Dijkstra3 = registration::AbstractRegister<graph::shortest_path::Dijkstra,
                                                ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                const graph::WeightedDirectedGraph<> &,
                                                const DefaultNodeType &,
                                                const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathRegistration);

auto Dijkstra4 = registration::AbstractRegister<graph::shortest_path::Dijkstra,
                                                ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                const graph::WeightedDirectedMultiGraph<> &,
                                                const DefaultNodeType &,
                                                const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathRegistration);

auto Dijkstra5 = registration::AbstractRegister<graph::shortest_path::Dijkstra,
                                                ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                const graph::WeightedMixedGraph<> &,
                                                const DefaultNodeType &,
                                                const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathRegistration);

auto Dijkstra6 = registration::AbstractRegister<graph::shortest_path::Dijkstra,
                                                ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                const graph::WeightedMixedMultiGraph<> &,
                                                const DefaultNodeType &,
                                                const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathRegistration);

auto DijkstraGrid1 = registration::AbstractRegister<graph::shortest_path::Dijkstra,
                                                    ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                              DefaultWeightType>,
                                                    const grid::WeightedSquareGrid4<> &,
                                                    const DefaultSquareGridNodeType &,
                                                    const DefaultSquareGridNodeType &>(graph::shortest_path::Dijkstra::findPathRegistration);

auto DijkstraGrid2 = registration::AbstractRegister<graph::shortest_path::Dijkstra,
                                                    ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                              DefaultWeightType>,
                                                    const grid::WeightedSquareGrid8<> &,
                                                    const DefaultSquareGridNodeType &,
                                                    const DefaultSquareGridNodeType &>(graph::shortest_path::Dijkstra::findPathRegistration);

// ---------------------------------------------------------------------------------------------------------------------
// bidirectional


auto DijkstraBidirectional1 = registration::AbstractRegister<graph::shortest_path::DijkstraBidirectional,
                                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                             const graph::WeightedUndirectedGraph<> &,
                                                             const DefaultNodeType &,
                                                             const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathBidirectionalRegistration);

auto DijkstraBidirectional2 = registration::AbstractRegister<graph::shortest_path::DijkstraBidirectional,
                                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                             const graph::WeightedUndirectedMultiGraph<> &,
                                                             const DefaultNodeType &,
                                                             const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathBidirectionalRegistration);

auto DijkstraBidirectional3 = registration::AbstractRegister<graph::shortest_path::DijkstraBidirectional,
                                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                             const graph::WeightedDirectedGraph<> &,
                                                             const DefaultNodeType &,
                                                             const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathBidirectionalRegistration);

auto DijkstraBidirectional4 = registration::AbstractRegister<graph::shortest_path::DijkstraBidirectional,
                                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                             const graph::WeightedDirectedMultiGraph<> &,
                                                             const DefaultNodeType &,
                                                             const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathBidirectionalRegistration);

auto DijkstraBidirectional5 = registration::AbstractRegister<graph::shortest_path::DijkstraBidirectional,
                                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                             const graph::WeightedMixedGraph<> &,
                                                             const DefaultNodeType &,
                                                             const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathBidirectionalRegistration);

auto DijkstraBidirectional6 = registration::AbstractRegister<graph::shortest_path::DijkstraBidirectional,
                                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                             const graph::WeightedMixedMultiGraph<> &,
                                                             const DefaultNodeType &,
                                                             const DefaultNodeType &>(graph::shortest_path::Dijkstra::findPathBidirectionalRegistration);

auto DijkstraGridBidirectional1 = registration::AbstractRegister<graph::shortest_path::DijkstraBidirectional,
                                                                 ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                                           DefaultWeightType>,
                                                                 const grid::WeightedSquareGrid4<> &,
                                                                 const DefaultSquareGridNodeType &,
                                                                 const DefaultSquareGridNodeType &>(graph::shortest_path::Dijkstra::findPathBidirectionalRegistration);

auto DijkstraGridbidirectional2 = registration::AbstractRegister<graph::shortest_path::DijkstraBidirectional,
                                                                 ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                                           DefaultWeightType>,
                                                                 const grid::WeightedSquareGrid8<> &,
                                                                 const DefaultSquareGridNodeType &,
                                                                 const DefaultSquareGridNodeType &>(graph::shortest_path::Dijkstra::findPathBidirectionalRegistration);

// ---------------------------------------------------------------------------------------------------------------------
}

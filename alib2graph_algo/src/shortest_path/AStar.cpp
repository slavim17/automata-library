// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "AStar.hpp"

#include <registration/AlgoRegistration.hpp>

namespace graph::shortest_path {

class AStarBidirectional {};

} // namespace graph::shortest_path

namespace {

// ---------------------------------------------------------------------------------------------------------------------
// uni-directional

auto AStar1 = registration::AbstractRegister<graph::shortest_path::AStar,
                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                             const graph::WeightedUndirectedGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &,
                                             std::function<DefaultWeightType(const DefaultNodeType &,
                                                                             const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathRegistration);

auto AStar2 = registration::AbstractRegister<graph::shortest_path::AStar,
                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                             const graph::WeightedUndirectedMultiGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &,
                                             std::function<DefaultWeightType(const DefaultNodeType &,
                                                                             const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathRegistration);

auto AStar3 = registration::AbstractRegister<graph::shortest_path::AStar,
                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                             const graph::WeightedDirectedGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &,
                                             std::function<DefaultWeightType(const DefaultNodeType &,
                                                                             const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathRegistration);

auto AStar4 = registration::AbstractRegister<graph::shortest_path::AStar,
                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                             const graph::WeightedDirectedMultiGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &,
                                             std::function<DefaultWeightType(const DefaultNodeType &,
                                                                             const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathRegistration);

auto AStar5 = registration::AbstractRegister<graph::shortest_path::AStar,
                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                             const graph::WeightedMixedGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &,
                                             std::function<DefaultWeightType(const DefaultNodeType &,
                                                                             const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathRegistration);

auto AStar6 = registration::AbstractRegister<graph::shortest_path::AStar,
                                             ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                             const graph::WeightedMixedMultiGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &,
                                             std::function<DefaultWeightType(const DefaultNodeType &,
                                                                             const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathRegistration);

auto AStarGrid1 = registration::AbstractRegister<graph::shortest_path::AStar,
                                                 ext::pair<ext::vector<DefaultSquareGridNodeType>, DefaultWeightType>,
                                                 const grid::WeightedSquareGrid4<> &,
                                                 const DefaultSquareGridNodeType &,
                                                 const DefaultSquareGridNodeType &,
                                                 std::function<DefaultWeightType(const DefaultSquareGridNodeType &,
                                                                                 const DefaultSquareGridNodeType &)> >(
    graph::shortest_path::AStar::findPathRegistration);

auto AStarGrid2 = registration::AbstractRegister<graph::shortest_path::AStar,
                                                 ext::pair<ext::vector<DefaultSquareGridNodeType>, DefaultWeightType>,
                                                 const grid::WeightedSquareGrid8<> &,
                                                 const DefaultSquareGridNodeType &,
                                                 const DefaultSquareGridNodeType &,
                                                 std::function<DefaultWeightType(const DefaultSquareGridNodeType &,
                                                                                 const DefaultSquareGridNodeType &)> >(
    graph::shortest_path::AStar::findPathRegistration);

// ---------------------------------------------------------------------------------------------------------------------
// bidirectional

auto AStarBidirectional1 = registration::AbstractRegister<graph::shortest_path::AStarBidirectional,
                                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                          const graph::WeightedUndirectedGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &,
                                                          std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                          const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathBidirectionalRegistration);

auto AStarBidirectional2 = registration::AbstractRegister<graph::shortest_path::AStarBidirectional,
                                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                          const graph::WeightedUndirectedMultiGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &,
                                                          std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                          const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathBidirectionalRegistration);

auto AStarBidirectional3 = registration::AbstractRegister<graph::shortest_path::AStarBidirectional,
                                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                          const graph::WeightedDirectedGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &,
                                                          std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                          const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathBidirectionalRegistration);

auto AStarBidirectional4 = registration::AbstractRegister<graph::shortest_path::AStarBidirectional,
                                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                          const graph::WeightedDirectedMultiGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &,
                                                          std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                          const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathBidirectionalRegistration);

auto AStarBidirectional5 = registration::AbstractRegister<graph::shortest_path::AStarBidirectional,
                                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                          const graph::WeightedMixedGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &,
                                                          std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                          const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathBidirectionalRegistration);

auto AStarBidirectional6 = registration::AbstractRegister<graph::shortest_path::AStarBidirectional,
                                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                                          const graph::WeightedMixedMultiGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &,
                                                          std::function<DefaultWeightType(const DefaultNodeType &,
                                                                                          const DefaultNodeType &)> >(
    graph::shortest_path::AStar::findPathBidirectionalRegistration);

auto AStarGridBidirectional1 = registration::AbstractRegister<graph::shortest_path::AStarBidirectional,
                                                              ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                                        DefaultWeightType>,
                                                              const grid::WeightedSquareGrid4<> &,
                                                              const DefaultSquareGridNodeType &,
                                                              const DefaultSquareGridNodeType &,
                                                              std::function<DefaultWeightType(const DefaultSquareGridNodeType &,
                                                                                              const DefaultSquareGridNodeType &)> >(
    graph::shortest_path::AStar::findPathBidirectionalRegistration);

auto AStarGridBidirectional2 = registration::AbstractRegister<graph::shortest_path::AStarBidirectional,
                                                              ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                                        DefaultWeightType>,
                                                              const grid::WeightedSquareGrid8<> &,
                                                              const DefaultSquareGridNodeType &,
                                                              const DefaultSquareGridNodeType &,
                                                              std::function<DefaultWeightType(const DefaultSquareGridNodeType &,
                                                                                              const DefaultSquareGridNodeType &)> >(
    graph::shortest_path::AStar::findPathBidirectionalRegistration);

// ---------------------------------------------------------------------------------------------------------------------
}


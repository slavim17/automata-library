// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "IDDFS.hpp"

#include <registration/AlgoRegistration.hpp>
#include <graph/GraphClasses.hpp>
#include <grid/GridClasses.hpp>

namespace graph::traverse {
class IDDFSBidirectional {};
} // namespace graph::traverse

namespace {

// ---------------------------------------------------------------------------------------------------------------------
// Normal Graph uni-directional

auto IDDFS1 = registration::AbstractRegister<graph::traverse::IDDFS,
                                             ext::vector<DefaultNodeType>,
                                             const graph::UndirectedGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto IDDFS2 = registration::AbstractRegister<graph::traverse::IDDFS,
                                             ext::vector<DefaultNodeType>,
                                             const graph::UndirectedMultiGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto IDDFS3 = registration::AbstractRegister<graph::traverse::IDDFS,
                                             ext::vector<DefaultNodeType>,
                                             const graph::DirectedGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto IDDFS4 = registration::AbstractRegister<graph::traverse::IDDFS,
                                             ext::vector<DefaultNodeType>,
                                             const graph::DirectedMultiGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto IDDFS5 = registration::AbstractRegister<graph::traverse::IDDFS,
                                             ext::vector<DefaultNodeType>,
                                             const graph::MixedGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto IDDFS6 = registration::AbstractRegister<graph::traverse::IDDFS,
                                             ext::vector<DefaultNodeType>,
                                             const graph::MixedMultiGraph<> &,
                                             const DefaultNodeType &,
                                             const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto IDDFSGrid1 = registration::AbstractRegister<graph::traverse::IDDFS,
                                                 ext::vector<DefaultSquareGridNodeType>,
                                                 const grid::SquareGrid4<> &,
                                                 const DefaultSquareGridNodeType &,
                                                 const DefaultSquareGridNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto IDDFSGrid2 = registration::AbstractRegister<graph::traverse::IDDFS,
                                                 ext::vector<DefaultSquareGridNodeType>,
                                                 const grid::SquareGrid8<> &,
                                                 const DefaultSquareGridNodeType &,
                                                 const DefaultSquareGridNodeType &>(graph::traverse::IDDFS::findPathRegistration);

// ---------------------------------------------------------------------------------------------------------------------
// Normal Graph bidirectional

auto IDDFSBidirectional1 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                          ext::vector<DefaultNodeType>,
                                                          const graph::UndirectedGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto IDDFSBidirectional2 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                          ext::vector<DefaultNodeType>,
                                                          const graph::UndirectedMultiGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto IDDFSBidirectional3 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                          ext::vector<DefaultNodeType>,
                                                          const graph::DirectedGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto IDDFSBidirectional4 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                          ext::vector<DefaultNodeType>,
                                                          const graph::DirectedMultiGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto IDDFSBidirectional5 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                          ext::vector<DefaultNodeType>,
                                                          const graph::MixedGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto IDDFSBidirectional6 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                          ext::vector<DefaultNodeType>,
                                                          const graph::MixedMultiGraph<> &,
                                                          const DefaultNodeType &,
                                                          const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto IDDFSGridBidirectional1 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                              ext::vector<DefaultSquareGridNodeType>,
                                                              const grid::SquareGrid4<> &,
                                                              const DefaultSquareGridNodeType &,
                                                              const DefaultSquareGridNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto IDDFSGridbidirectional2 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                              ext::vector<DefaultSquareGridNodeType>,
                                                              const grid::SquareGrid8<> &,
                                                              const DefaultSquareGridNodeType &,
                                                              const DefaultSquareGridNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

// ---------------------------------------------------------------------------------------------------------------------
// Weighted Graph uni-directional

auto WIDDFS1 = registration::AbstractRegister<graph::traverse::IDDFS,
                                              ext::vector<DefaultNodeType>,
                                              const graph::WeightedUndirectedGraph<> &,
                                              const DefaultNodeType &,
                                              const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto WIDDFS2 = registration::AbstractRegister<graph::traverse::IDDFS,
                                              ext::vector<DefaultNodeType>,
                                              const graph::WeightedUndirectedMultiGraph<> &,
                                              const DefaultNodeType &,
                                              const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto WIDDFS3 = registration::AbstractRegister<graph::traverse::IDDFS,
                                              ext::vector<DefaultNodeType>,
                                              const graph::WeightedDirectedGraph<> &,
                                              const DefaultNodeType &,
                                              const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto WIDDFS4 = registration::AbstractRegister<graph::traverse::IDDFS,
                                              ext::vector<DefaultNodeType>,
                                              const graph::WeightedDirectedMultiGraph<> &,
                                              const DefaultNodeType &,
                                              const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto WIDDFS5 = registration::AbstractRegister<graph::traverse::IDDFS,
                                              ext::vector<DefaultNodeType>,
                                              const graph::WeightedMixedGraph<> &,
                                              const DefaultNodeType &,
                                              const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto WIDDFS6 = registration::AbstractRegister<graph::traverse::IDDFS,
                                              ext::vector<DefaultNodeType>,
                                              const graph::WeightedMixedMultiGraph<> &,
                                              const DefaultNodeType &,
                                              const DefaultNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto WIDDFSGrid1 = registration::AbstractRegister<graph::traverse::IDDFS,
                                                  ext::vector<DefaultSquareGridNodeType>,
                                                  const grid::WeightedSquareGrid4<> &,
                                                  const DefaultSquareGridNodeType &,
                                                  const DefaultSquareGridNodeType &>(graph::traverse::IDDFS::findPathRegistration);

auto WIDDFSGrid2 = registration::AbstractRegister<graph::traverse::IDDFS,
                                                  ext::vector<DefaultSquareGridNodeType>,
                                                  const grid::WeightedSquareGrid8<> &,
                                                  const DefaultSquareGridNodeType &,
                                                  const DefaultSquareGridNodeType &>(graph::traverse::IDDFS::findPathRegistration);

// ---------------------------------------------------------------------------------------------------------------------
// Weighted Graph bidirectional

auto WIDDFSBidirectional1 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                           ext::vector<DefaultNodeType>,
                                                           const graph::WeightedUndirectedGraph<> &,
                                                           const DefaultNodeType &,
                                                           const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto WIDDFSBidirectional2 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                           ext::vector<DefaultNodeType>,
                                                           const graph::WeightedUndirectedMultiGraph<> &,
                                                           const DefaultNodeType &,
                                                           const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto WIDDFSBidirectional3 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                           ext::vector<DefaultNodeType>,
                                                           const graph::WeightedDirectedGraph<> &,
                                                           const DefaultNodeType &,
                                                           const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto WIDDFSBidirectional4 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                           ext::vector<DefaultNodeType>,
                                                           const graph::WeightedDirectedMultiGraph<> &,
                                                           const DefaultNodeType &,
                                                           const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto WIDDFSBidirectional5 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                           ext::vector<DefaultNodeType>,
                                                           const graph::WeightedMixedGraph<> &,
                                                           const DefaultNodeType &,
                                                           const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto WIDDFSBidirectional6 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                           ext::vector<DefaultNodeType>,
                                                           const graph::WeightedMixedMultiGraph<> &,
                                                           const DefaultNodeType &,
                                                           const DefaultNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto WIDDFSGridBidirectional1 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                               ext::vector<DefaultSquareGridNodeType>,
                                                               const grid::WeightedSquareGrid4<> &,
                                                               const DefaultSquareGridNodeType &,
                                                               const DefaultSquareGridNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

auto WIDDFSGridbidirectional2 = registration::AbstractRegister<graph::traverse::IDDFSBidirectional,
                                                               ext::vector<DefaultSquareGridNodeType>,
                                                               const grid::WeightedSquareGrid8<> &,
                                                               const DefaultSquareGridNodeType &,
                                                               const DefaultSquareGridNodeType &>(graph::traverse::IDDFS::findPathBidirectionalRegistration);

// ---------------------------------------------------------------------------------------------------------------------

}

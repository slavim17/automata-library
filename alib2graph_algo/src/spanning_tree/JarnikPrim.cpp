// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "JarnikPrim.hpp"

#include <registration/AlgoRegistration.hpp>

namespace {

// ---------------------------------------------------------------------------------------------------------------------
// uni-directional

auto JarnikPrim1 = registration::AbstractRegister<graph::spanning_tree::JarnikPrim,
                                                  graph::WeightedUndirectedGraph<>,
                                                  const graph::WeightedUndirectedGraph<> &,
                                                  const DefaultNodeType &>(graph::spanning_tree::JarnikPrim::findSpanningTree);
}

project(alt-libelgo VERSION ${CMAKE_PROJECT_VERSION})
find_package(LibXml2 REQUIRED)
alt_library(alib2elgo
	DEPENDS alib2algo alib2std alib2common alib2data alib2xml LibXml2::LibXml2
	TEST_DEPENDS LibXml2::LibXml2
)

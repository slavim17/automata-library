#include "UselessStatesRemover.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto UselessStatesRemoverEpsilonNFA = registration::AbstractRegister < automaton::simplify::efficient::UselessStatesRemover, automaton::EpsilonNFA < >, const automaton::EpsilonNFA < > & > ( automaton::simplify::efficient::UselessStatesRemover::remove );
auto UselessStatesRemoverNFA = registration::AbstractRegister < automaton::simplify::efficient::UselessStatesRemover, automaton::NFA < > , const automaton::NFA < > & > ( automaton::simplify::efficient::UselessStatesRemover::remove );
auto UselessStatesRemoverCompactNFA = registration::AbstractRegister < automaton::simplify::efficient::UselessStatesRemover, automaton::CompactNFA < >, const automaton::CompactNFA < > & > ( automaton::simplify::efficient::UselessStatesRemover::remove );
auto UselessStatesRemoverExtendedNFA = registration::AbstractRegister < automaton::simplify::efficient::UselessStatesRemover, automaton::ExtendedNFA < >, const automaton::ExtendedNFA < > & > ( automaton::simplify::efficient::UselessStatesRemover::remove );
auto UselessStatesRemoverDFA = registration::AbstractRegister < automaton::simplify::efficient::UselessStatesRemover, automaton::DFA < >, const automaton::DFA < > & > ( automaton::simplify::efficient::UselessStatesRemover::remove );
auto UselessStatesRemoverMultiInitialStateNFA = registration::AbstractRegister < automaton::simplify::efficient::UselessStatesRemover, automaton::MultiInitialStateNFA < >, const automaton::MultiInitialStateNFA < > & > ( automaton::simplify::efficient::UselessStatesRemover::remove );

} /* namespace */

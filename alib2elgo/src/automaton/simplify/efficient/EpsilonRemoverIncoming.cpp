#include "EpsilonRemoverIncoming.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto EpsilonRemoverIncomingDFA = registration::AbstractRegister < automaton::simplify::efficient::EpsilonRemoverIncoming, automaton::DFA < >, const automaton::DFA < > & > ( automaton::simplify::efficient::EpsilonRemoverIncoming::remove );
auto EpsilonRemoverIncomingMultiInitialStateNFA = registration::AbstractRegister < automaton::simplify::efficient::EpsilonRemoverIncoming, automaton::MultiInitialStateNFA < >, const automaton::MultiInitialStateNFA < > & > ( automaton::simplify::efficient::EpsilonRemoverIncoming::remove );
auto EpsilonRemoverIncomingNFA = registration::AbstractRegister < automaton::simplify::efficient::EpsilonRemoverIncoming, automaton::NFA < >, const automaton::NFA < > & > ( automaton::simplify::efficient::EpsilonRemoverIncoming::remove );
auto EpsilonRemoverIncomingEpsilonNFA = registration::AbstractRegister < automaton::simplify::efficient::EpsilonRemoverIncoming, automaton::NFA < >, const automaton::EpsilonNFA < > & > ( automaton::simplify::efficient::EpsilonRemoverIncoming::remove );

} /* namespace */

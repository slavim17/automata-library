#include <catch2/catch.hpp>

#include <list>

#include "automaton/simplify/Trim.h"
#include "automaton/FSM/DFA.h"


TEST_CASE ( "Trim automaton", "[unit][automaton][elgo]" ) {
	automaton::DFA < std::string, int > automaton(1);

	automaton.addState(1);
	automaton.addState(2);
	automaton.addState(3);
	automaton.addInputSymbol(std::string("a"));
	automaton.addInputSymbol(std::string("b"));

	automaton.addTransition(1, std::string("a"), 2);
	automaton.addTransition(2, std::string("b"), 1);
	automaton.addTransition(3, std::string("b"), 1);

	automaton.addFinalState(1);

	automaton::DFA < std::string, int > trimed = automaton::simplify::Trim::trim(automaton);

	CHECK ( trimed.getStates().size() == 2 );
}

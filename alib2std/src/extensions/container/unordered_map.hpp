/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <unordered_map>

#include <ext/ostream>

#include <extensions/range.hpp>

namespace ext {

/**
 * \brief
 * Class extending the unordered_map class from the standard library. Original reason is to allow printing of the container with overloaded operator <<.
 *
 * The class mimics the behavior of the unordered_map from the standard library.
 *
 * \tparam T the type of keys inside the unordered_map
 * \tparam R the type of values inside the unordered_map
 * \tparam Hash the hasher type used to order keys
 * \tparam KeyEqual the comparator of keys
 * \tparam Alloc the allocator of values of type T
 */
template < class T, class R, class Hash = std::hash < T >, class KeyEqual = std::equal_to < T >, class Alloc = std::allocator < std::pair < const T, R > > >
class unordered_map : public std::unordered_map < T, R, Hash, KeyEqual, Alloc > {
public:
	/**
	 * Inherit constructors of the standard unordered_map
	 */
	using std::unordered_map < T, R, Hash, KeyEqual, Alloc >::unordered_map; // NOLINT(modernize-use-equals-default)

	/**
	 * Inherit operator = of the standard unordered_map
	 */
	using std::unordered_map < T, R, Hash, KeyEqual, Alloc >::operator =;
#ifndef __clang__

	/**
	 * Default constructor needed by g++ since it is not inherited
	 */
	unordered_map ( ) = default;

	/**
	 * Copy constructor needed by g++ since it is not inherited
	 */
	unordered_map ( const unordered_map & other ) = default;

	/**
	 * Move constructor needed by g++ since it is not inherited
	 */
	unordered_map ( unordered_map && other ) = default;

	/**
	 * Copy operator = needed by g++ since it is not inherited
	 */
	unordered_map & operator = ( unordered_map && other ) = default;

	/**
	 * Move operator = needed by g++ since it is not inherited
	 */
	unordered_map & operator = ( const unordered_map & other ) = default;
#endif
	/**
	 * Constructor from range of values.
	 *
	 * \tparam Iterator the type of range iterator
	 *
	 * \param range the source range
	 */
	template < class Iterator >
	unordered_map ( const ext::iterator_range < Iterator > & range ) : unordered_map ( range.begin ( ), range.end ( ) ) {
	}

	/**
	 * \brief
	 * The iterator type is inheried.
	 */
	using iterator = typename std::unordered_map<T, R, Hash, KeyEqual, Alloc>::iterator;

	/**
	 * \brief
	 * Inherit all insert methods of the standard unordered_map.
	 */
	using std::unordered_map< T, R, Hash, KeyEqual, Alloc >::insert;

	/**
	 * \brief
	 * Insert variant with explicit key and value parameters.
	 *
	 * \param key the key
	 * \param value the value
	 *
	 * \return pair of iterator to inserted key-value pair and true if the value was inserted or false if the key already exited
	 */
	std::pair < iterator, bool > insert ( const T & key, const R & value ) {
		return insert ( std::make_pair ( key, value ) );
	}

	/**
	 * \brief
	 * Insert variant with explicit key and value parameters.
	 *
	 * \param key the key
	 * \param value the value
	 *
	 * \return pair of iterator to inserted key-value pair and true if the value was inserted or false if the key already exited
	 */
	std::pair < iterator, bool > insert ( const T & key, R && value ) {
		return insert ( std::make_pair ( key, std::move ( value ) ) );
	}

	/**
	 * \brief
	 * Insert variant with explicit key and value parameters.
	 *
	 * \param key the key
	 * \param value the value
	 *
	 * \return pair of iterator to inserted key-value pair and true if the value was inserted or false if the key already exited
	 */
	std::pair < iterator, bool > insert ( T && key, const R & value ) {
		return insert ( std::make_pair ( std::move ( key ), value ) );
	}

	/**
	 * \brief
	 * Insert variant with explicit key and value parameters.
	 *
	 * \param key the key
	 * \param value the value
	 *
	 * \return pair of iterator to inserted key-value pair and true if the value was inserted or false if the key already exited
	 */
	std::pair < iterator, bool > insert ( T && key, R && value ) {
		return insert ( std::make_pair ( std::move ( key ), std::move ( value ) ) );
	}

	/**
	 * \brief
	 * Inherited behavior of begin for non-const instance.
	 *
	 * \return iterator the first element of unordered_map
	 */
	auto begin ( ) & {
		return std::unordered_map < T, R, Hash, KeyEqual, Alloc >::begin ( );
	}

	/**
	 * \brief
	 * Inherited behavior of begin for const instance.
	 *
	 * \return const_iterator the first element of unordered_map
	 */
	auto begin ( ) const & {
		return std::unordered_map < T, R, Hash, KeyEqual, Alloc >::begin ( );
	}

	/**
	 * \brief
	 * New variant of begin for rvalues.
	 *
	 * \return move_iterator the first element of unordered_map
	 */
	auto begin ( ) && {
		return make_map_move_iterator < T, R > ( this->begin ( ) );
	}

	/**
	 * \brief
	 * Inherited behavior of end for non-const instance.
	 *
	 * \return iterator to one after the last element of unordered_map
	 */
	auto end ( ) & {
		return std::unordered_map < T, R, Hash, KeyEqual, Alloc >::end ( );
	}

	/**
	 * \brief
	 * Inherited behavior of end for const instance.
	 *
	 * \return const_iterator to one after the last element of unordered_map
	 */
	auto end ( ) const & {
		return std::unordered_map < T, R, Hash, KeyEqual, Alloc >::end ( );
	}

	/**
	 * \brief
	 * New variant of end for rvalues.
	 *
	 * \return move_iterator to one after the last element of unordered_map
	 */
	auto end ( ) && {
		return make_map_move_iterator < T, R > ( this->end ( ) );
	}

	/**
	 * \brief
	 * Make range of non-const begin to end iterators.
	 *
	 * \return full range over container values
	 */
	auto range ( ) & {
		auto endIter = end ( );
		auto beginIter = begin ( );
		return ext::iterator_range < decltype ( endIter ) > ( beginIter, endIter );
	}

	/**
	 * \brief
	 * Make range of non-const begin to end iterators.
	 *
	 * \return full range over container values
	 */
	auto range ( ) const & {
		auto endIter = end ( );
		auto beginIter = begin ( );
		return ext::iterator_range < decltype ( endIter ) > ( beginIter, endIter );
	}

	/**
	 * \brief
	 * Make range of move begin to end iterators.
	 *
	 * \return full range over container values
	 */
	auto range ( ) && {
		auto endIter = std::move ( * this ).end ( );
		auto beginIter = std::move ( * this ).begin ( );
		return ext::iterator_range < decltype ( endIter ) > ( beginIter, endIter );
	}
};

/**
 * \brief
 * Operator to print the unordered_map to the output stream.
 *
 * \param out the output stream
 * \param unordered_map the unordered_map to print
 *
 * \tparam T the type of keys inside the unordered_map
 * \tparam R the type of values inside the unordered_map
 * \tparam Ts ... remaining unimportant template parameters of the unordered_map
 *
 * \return the output stream from the \p out
 */
template< class T, class R, class ... Ts >
ext::ostream& operator<<(ext::ostream& out, const ext::unordered_map<T, R, Ts ... >& unordered_map) {
	out << "{";

	bool first = true;
	for(const std::pair<const T, R>& item : unordered_map) {
		if(!first) out << ", ";
		first = false;
		out << "(" << item.first << ", " << item.second << ")";
	}

	out << "}";
	return out;
}

} /* namespace ext */

/**
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * https://gist.github.com/tibordp/6909880
 */

#pragma once

#include <optional>

#include <ext/ostream>

namespace ext {

/**
 * \brief
 * Operator to print the optional to the output stream.
 *
 * \param out the output stream
 * \param optional the optional to print
 *
 * \tparam T the type of value inside the optional
 *
 * \return the output stream from the \p out
 */
template< class T >
ext::ostream & operator << ( ext::ostream & out, const std::optional < T > & optional ) {
	if ( ! optional )
		return out << "void";
	else
		return out << optional.value ( );
}

} /* namespace ext */

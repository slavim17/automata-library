/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <istream>
#include <concepts>
#include <memory>

namespace ext {

class istream {
	std::unique_ptr < std::istream > m_os;

public:
	istream ( std::basic_streambuf < char > * sb );

	virtual ~istream ( ) noexcept;

protected:
	istream ( istream && rhs ) noexcept;

	istream & operator = ( istream && rhs ) noexcept;

public:
	template < class T >
	requires requires { std::declval < std::istream > ( ) << std::declval < T > ( ); }
	friend istream & operator >> ( istream & os, const T & val ) {
		static_cast < std::istream & > ( os ) >> val;
		return os;
	}

	friend istream & operator >> ( istream & os, std::istream & ( * fn ) ( std::istream & ) );

	friend istream & operator >> ( istream & os, std::ios_base & ( * fn ) ( std::ios_base & ) );

protected:
	operator std::istream & ( ) &;

	operator const std::istream & ( ) const &;

public:
	int peek ( );

	int get ( );

	istream & get ( char & ch );

	istream & unget ( );

	istream & putback ( char ch );

	istream & read ( char * s, std::streamsize count );

	std::streampos tellg ( );

	istream & seekg ( std::streampos pos );

	istream & seekg ( std::streamoff off, std::ios_base::seekdir dir );

	istream & sync ( );

	std::streambuf * rdbuf ( ) const;

	std::streambuf * rdbuf ( std::streambuf * buf );

	void setstate ( std::ios_base::iostate state );

	void clear ( std::ios_base::iostate state = std::ios_base::goodbit );

	bool good ( ) const;

	bool fail ( ) const;

	bool eof ( ) const;

	operator bool ( ) const;
};

/**
 * \brief
 * Read into a constant string. The implementation tests whether exact same sequence represented as \p str is in the \p in stream. In case it is, it reads it. In case it is not the stream is not advanced and fail bit is set.
 *
 * \param in the input stream
 * \param str the string to test
 *
 * \return the in stream either with fail bit set if str is not in the stream or advanced by reading str.
 */
ext::istream & operator >> ( ext::istream & in, const std::string & str );

} /* namespace ext */


#include "istream.h"

namespace ext {

istream::istream ( std::basic_streambuf < char > * sb ) : m_os ( std::make_unique < std::istream > ( sb ) ) {
}

istream::~istream ( ) noexcept = default;

istream::istream ( istream && rhs ) noexcept : m_os ( std::move ( rhs.m_os ) ) {
}

istream & istream::operator = ( istream && rhs ) noexcept {
	m_os = std::move ( rhs.m_os );
	return * this;
}

istream & operator >> ( istream & os, std::istream & ( * fn ) ( std::istream & ) ) {
	static_cast < std::istream & > ( os ) >> fn;
	return os;
}

istream & operator >> ( istream & os, std::ios_base & ( * fn ) ( std::ios_base & ) ) {
	static_cast < std::istream & > ( os ) >> fn;
	return os;
}

istream::operator std::istream & ( ) & {
	return * m_os;
}

istream::operator const std::istream & ( ) const & {
	return * m_os;
}

int istream::peek ( ) {
	return m_os->peek ( );
}

int istream::get ( ) {
	return m_os->get ( );
}

istream & istream::get ( char & ch ) {
	m_os->get ( ch );
	return * this;
}

istream & istream::unget ( ) {
	m_os->unget ( );
	return * this;
}

istream & istream::putback ( char ch ) {
	m_os->putback ( ch );
	return * this;
}

istream & istream::read ( char * s, std::streamsize count ) {
	m_os->read ( s, count );
	return * this;
}

std::streampos istream::tellg ( ) {
	return m_os->tellg ( );
}

istream & istream::seekg ( std::streampos pos ) {
	m_os->seekg ( pos );
	return * this;
}

istream & istream::seekg ( std::streamoff off, std::ios_base::seekdir dir ) {
	m_os->seekg ( off, dir );
	return * this;
}

istream & istream::sync ( ) {
	m_os->sync ( );
	return * this;
}

std::streambuf * istream::rdbuf ( ) const {
	return m_os->rdbuf ( );
}

std::streambuf * istream::rdbuf ( std::streambuf * buf ) {
	return m_os->rdbuf ( buf );
}

void istream::setstate ( std::ios_base::iostate state ) {
	m_os->setstate ( state );
}

void istream::clear ( std::ios_base::iostate state ) {
	m_os->clear ( state );
}

bool istream::good ( ) const {
	return m_os->good ( );
}

bool istream::fail ( ) const {
	return m_os->fail ( );
}

bool istream::eof ( ) const {
	return m_os->eof ( );
}

istream::operator bool ( ) const {
	return m_os->operator bool ( );
}

ext::istream & oprr ( ext::istream & in, const std::string & str, bool start ) {
	if ( str.empty ( ) ) return in;

	char c_str = str[0];
	int c_in  = in.peek ( );
	in.get ( );

	if ( c_in == EOF ) {
		in.clear ( std::ios::failbit );
		return in;
	}

	if ( in.good ( ) ) {
		if ( start && ( ( c_in == ' ' ) || ( c_in == '\n' ) || ( c_in == '\t' ) ) )
			oprr ( in, str, start );
		else if ( c_str == c_in )
			oprr ( in, str.substr ( 1 ), false );
		else
			in.clear ( std::ios::failbit );
	}

	if ( in.fail ( ) ) {
		in.clear ( );
		in.putback ( static_cast < char > ( c_in ) );
		in.clear ( std::ios::failbit );
	}

	return in;
}

ext::istream & operator >>( ext::istream & in, const std::string & str ) {
	return oprr ( in, str, true );
}

} /* namespace ext */

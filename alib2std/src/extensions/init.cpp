#include "init.h"
#include "random.hpp"
#include "iostream.hpp"

namespace ext {

ofdstream cmeasure ( CMEASURE_FD, CERR_FD );

istream cin ( std::cin.rdbuf ( ) );

ostream cout ( std::cout.rdbuf ( ) );

ostream cerr ( std::cerr.rdbuf ( ) );

ostream clog ( std::clog.rdbuf ( ) );

Init::Init ( ) : clog_fdaccessor ( CLOG_FD, CERR_FD ), clog_fdstreambuf ( clog_fdaccessor.get_fd ( ) ) {
	std::clog.rdbuf ( & clog_fdstreambuf );
	std::clog.clear ( );

	ext::clog.rdbuf ( std::clog.rdbuf ( ) );
	ext::clog.clear ( );

	ext::random_devices::getSemirandom ( ).seed ( ext::random_devices::getRandom ( ) ( ) );
}

Init::~Init ( ) {
	std::clog.flush ( );
	ext::clog.flush ( );
}

Init relinkio;

} /* namespace ext */

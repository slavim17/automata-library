/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "fdstream.hpp"
#include "iostream.hpp"

namespace ext {

/**
 * \brief
 * Class responsible for initialisation of std extensions and standard library changes.
 *
 * Includes seeding of semirandom device and redirection of clog stream
 */
class Init {
	/**
	 * \brief
	 * Instance of a class determining the used file descriptor for clog stream from prefered (if available) and fallback file descriptor.
	 */
	fdaccessor clog_fdaccessor;

	/**
	 * \brief
	 * Instance of a class representing buffered stream for clog stream designed to work on defined file descriptor instead of usual file name.
	 */
	fdstreambuf clog_fdstreambuf;
public:
	/**
	 * \brief
	 * Constructor of the initialisation class.
	 */
	Init ( );

	/**
	 * \brief
	 * Destructor of the initialisation class.
	 */
	~Init ( );
};

} /* namespace ext */


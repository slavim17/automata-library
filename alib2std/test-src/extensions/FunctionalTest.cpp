#include <testing.h>

#include <ext/functional>

TEST_CASE ( "Functional Test", "[unit][std][bits]" ) {
	SECTION ( "Less" ) {
		std::less < > test;
		CHECK ( test ( 1, 2 ) == true );
		CHECK ( test ( 1, 2. ) == true );

		CHECK_EXCLUSIVE_OR( ( test ( ext::poly_comp ( std::string ( "aa" ) ), 1 ) ), ( test ( ext::poly_comp ( 1 ), std::string ( "aa" ) ) ) );

		CHECK ( test ( ext::poly_comp ( 1 ), 2 ) == true );

		std::pair < int, std::string > value ( 1, "aa" );

		CHECK ( test ( ext::slice_comp ( 0 ), value ) == true );
		CHECK ( test ( ext::slice_comp ( 1 ), value ) == false );
	}
}

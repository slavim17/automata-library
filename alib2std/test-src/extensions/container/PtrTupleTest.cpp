#include <catch2/catch.hpp>
#include <ext/ptr_tuple>
#include <ext/sstream>

TEST_CASE ( "PtrTuple", "[unit][std][container]" ) {
	SECTION ( "Test call on nth" ) {
		ext::ptr_tuple < int, int, int, int, int > t = ext::make_ptr_tuple ( 1, 2, 3, 4, 5 );

		ext::ostringstream ss;
		ss << t;
		CAPTURE ( ss.str ( ) );
		CHECK ( ss.str ( ) == "(1, 2, 3, 4, 5)" );

		CHECK ( t == t );
		ext::ptr_tuple < int, int, int, int, int > r = ext::make_ptr_tuple ( 1, 2, 3, 4, 6 );
		auto res = t < r;
		CHECK ( res );
	}
}

#include <catch2/catch.hpp>
#include <exception>

#include <ext/managed_value>

TEST_CASE ( "ManagedValue", "[unit][std][container]" ) {
	SECTION ( "Test" ) {
		ext::managed_value < int > number ( 1 );
		number.addChangeCallback ( [ ] ( const int & value ) { if ( value >= 10 ) throw value; } );
		CHECK_NOTHROW ( number = 2 );
		CHECK_THROWS_AS ( number = 10, int );
	}
}

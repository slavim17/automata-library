#include <catch2/catch.hpp>
#include <ext/optional>
#include <sstream>

TEST_CASE ( "Optional", "[unit][std][container]" ) {
	SECTION ( "Ref test" ) {
		int a = 5;

		ext::optional_ref < int > empty;
		ext::optional_ref < int > a_ref ( a );
		CHECK ( a_ref.value ( ) == a );
		a_ref.value ( ) = 10;
		CHECK ( a == 10 );

		ext::optional_ref < int > a_ref2 ( a_ref );
		CHECK ( a_ref2 == a_ref );
	}
}


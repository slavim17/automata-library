#include <catch2/catch.hpp>

#include <ext/memory>

TEST_CASE ( "Memory", "[unit][std][bits]" ) {
	SECTION ( "Compare Pointers" ) {
		std::shared_ptr < int > i = std::make_shared < int > ( 1 );
		std::shared_ptr < int > j = i;
		std::shared_ptr < int > k = std::make_shared < int > ( 1 );
		std::shared_ptr < int > l;

		{
			auto ii = * i <=> * i == 0;
			CHECK ( ii );
			auto ij = * i <=> * j == 0;
			CHECK ( ij );
			auto ik = * i <=> * k == 0;
			CHECK ( ik );
		}

		{
			auto ik = i <=> k != 0;
			CHECK ( ik );
			auto jk = j <=> k != 0;
			CHECK ( jk );
			auto ji = j <=> i == 0;
			CHECK ( ji );

			auto il = i <=> l > 0;
			CHECK ( il );
			auto li = l <=> i < 0;
			CHECK ( li );
		}
	}
}

#include <catch2/catch.hpp>

#include <ext/random>

TEST_CASE ( "Random", "[unit][std][bits]" ) {
	SECTION ( "Test Random" ) {
		CAPTURE ( ext::random_devices::random(), ext::random_devices::semirandom() );

		ext::random_devices::semirandom.seed ( 100 );
		unsigned first = ext::random_devices::semirandom();
		ext::random_devices::semirandom.seed ( 100 );
		unsigned second = ext::random_devices::semirandom();

		CHECK( first == second );
	}
}


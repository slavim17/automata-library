/*
 * Author: Radovan Cerveny
 */

#include <sstream>
#include <tuple>
#include "MeasurementFrame.hpp"

namespace measurements {

MeasurementFrame::MeasurementFrame ( measurements::stealth_string frameName, measurements::Type frameType, unsigned frameParentIdx ) : name ( std::move ( frameName ) ), type ( frameType ), parentIdx ( frameParentIdx ), time ( ), memory ( ) {
}

std::ostream & operator <<( std::ostream & os, const MeasurementFrame & frame ) {

	std::stringstream ss;

	ss << "(TIME: " << frame.time << "), (MEM: " << frame.memory << "), (COUNTER: " << frame.counter << ")";

	os << "(" << frame.name << ", " << frame.type << ", " << frame.parentIdx << ", [";
	bool first = true;
	for ( unsigned subId : frame.subIdxs ) {
		if ( first )
			first = false;
		else
			os << ", ";
		os << subId;
	}
	os << "], " << ss.str ( ) << ")";
	return os;
}

MeasurementFrame MeasurementFrame::aggregate ( const std::vector < MeasurementFrame > & framesToAggregate ) {
	MeasurementFrame aggregatedFrame ( framesToAggregate[0].name, framesToAggregate[0].type, framesToAggregate[0].parentIdx );

	aggregatedFrame.subIdxs = framesToAggregate[0].subIdxs;

	aggregatedFrame.time = TimeDataFrame::aggregate ( framesToAggregate );
	aggregatedFrame.memory = MemoryDataFrame::aggregate ( framesToAggregate );
	aggregatedFrame.counter = CounterDataFrame::aggregate ( framesToAggregate );

	return aggregatedFrame;
}

}

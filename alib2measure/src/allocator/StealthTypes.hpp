/*
 * Author: Radovan Cerveny
 */

#pragma once

#include <string>
#include <vector>
#include <map>
#include "StealthAllocator.hpp"

namespace measurements {

using stealth_string = std::basic_string < char, std::char_traits < char >, stealth_allocator < char > >;

std::string to_string ( const stealth_string & );
std::string to_string ( stealth_string & );

stealth_string stealthStringFromString ( const std::string & );

template < typename T >
using stealth_vector = std::vector < T, stealth_allocator < T > >;

template < class Key, class T, class Compare = std::less < Key > >
using stealth_map = std::map < Key, T, Compare, stealth_allocator < std::pair < const Key, T > > >;
}


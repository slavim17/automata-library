#include "CompactSuffixAutomatonTerminatingSymbol.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>
#include <registration/XmlRegistration.hpp>

namespace {

auto DFAFromSuffixAutomaton = registration::CastRegister < automaton::CompactDFA < DefaultSymbolType, unsigned >, indexes::stringology::CompactSuffixAutomatonTerminatingSymbol < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < indexes::stringology::CompactSuffixAutomatonTerminatingSymbol < > > ( );

auto xmlWrite = registration::XmlWriterRegister < indexes::stringology::CompactSuffixAutomatonTerminatingSymbol < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::stringology::CompactSuffixAutomatonTerminatingSymbol < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::stringology::CompactSuffixAutomatonTerminatingSymbol < > > ( );

} /* namespace */

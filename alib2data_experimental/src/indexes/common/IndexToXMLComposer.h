#pragma once

#include <alib/deque>
#include <alib/set>
#include <sax/Token.h>
#include "../stringology/SuffixTrieNodeTerminatingSymbol.h"

namespace indexes {

/**
 * This class contains methods to print XML representation of indexes to the output stream.
 */
class IndexToXMLComposer {
public:
	static void composeAlphabet ( ext::deque < sax::Token > & out, const ext::set < DefaultSymbolType > & symbols );

	static void composeNode ( ext::deque < sax::Token > & out, const SuffixTrieNodeTerminatingSymbol & node );
};

} /* namespace indexes */


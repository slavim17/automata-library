#pragma once

#include <ostream>

#include <core/type_details_base.hpp>

namespace example {

/**
 * Example of a simple datatype
 */
class DataType {
	int m_a;

public:
	DataType ( int a ) : m_a ( a ) {
	}

	int getA ( ) const {
		return m_a;
	}

	/* For ValuePrinter (see cpp) */
	friend std::ostream & operator << ( std::ostream & out, const DataType & type ) {
		return out << "(DataType " << type.getA ( ) << ")";
	}
};

} /* namespace example */

namespace core {

template < >
struct type_details_retriever < example::DataType > {
	static std::unique_ptr < type_details_base > get ( );
};

} /* namespace core */

#include <ext/typeinfo>
#include <registration/ValuePrinterRegistration.hpp>
#include "DataType.h"

namespace core {

std::unique_ptr < type_details_base > type_details_retriever < example::DataType >::get ( ) {
	return std::make_unique < type_details_type > ( "example::DataType" );
}

} /* namespace core */

namespace {

// registration of a ValuePrinter. This "printer" is used for instance in AQL when the instance is returned
auto valuePrinter = registration::ValuePrinterRegister < example::DataType > ( );

}

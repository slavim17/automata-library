// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <ostream>
#include <alib/pair>
#include <object/Object.h>
#include <alib/tuple>

#include <edge/EdgeFeatures.hpp>
#include <edge/EdgeBase.hpp>

#include <core/type_details_base.hpp>

namespace edge {

template<typename TNode, typename TCapacity>
class CapacityEdge : public ext::pair<TNode, TNode>, public EdgeBase {
// ---------------------------------------------------------------------------------------------------------------------
 public:
  using node_type = TNode;
  using capacity_type = TCapacity;
  using normalized_type = WeightedEdge<>;

// ---------------------------------------------------------------------------------------------------------------------

 private:
  TCapacity m_capacity;

// =====================================================================================================================
// Constructor, Destructor, Operators

 public:
  explicit CapacityEdge(TNode _first, TNode _second, TCapacity capacity);

// ---------------------------------------------------------------------------------------------------------------------

  TCapacity capacity() const;
  void capacity(TCapacity &&capacity);

// =====================================================================================================================
// EdgeBase interface

	auto operator <=> (const CapacityEdge &other) const {
		return ext::tie(this->first, this->second, m_capacity) <=> ext::tie(other.first, other.second, other.m_capacity);
	}

	bool operator == (const CapacityEdge &other) const {
		return ext::tie(this->first, this->second, m_capacity) == ext::tie(other.first, other.second, other.m_capacity);
	}

  void operator>>(ext::ostream &ostream) const override;

// =====================================================================================================================

  virtual std::string name() const;

// ---------------------------------------------------------------------------------------------------------------------
};
// =====================================================================================================================

template<typename TNode, typename TCapacity>
CapacityEdge<TNode, TCapacity>::CapacityEdge(TNode _first, TNode _second, TCapacity capacity)
    : ext::pair<TNode, TNode>(_first, _second), m_capacity(capacity) {

}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode, typename TCapacity>
TCapacity CapacityEdge<TNode, TCapacity>::capacity() const {
  return m_capacity;
}

template<typename TNode, typename TCapacity>
void CapacityEdge<TNode, TCapacity>::capacity(TCapacity &&capacity) {
  m_capacity = std::forward<TCapacity>(capacity);
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode, typename TCapacity>
std::string CapacityEdge<TNode, TCapacity>::name() const {
  return "CapacityEdge";
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode, typename TCapacity>
void CapacityEdge<TNode, TCapacity>::operator>>(ext::ostream &ostream) const {
  ostream << "(" << name() << "(first=" << this->first << ", second=" << this->second << ", weight="
          << m_capacity << "))";
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace edge

// =====================================================================================================================

namespace core {

template < class TNode, class TCapacity >
struct type_details_retriever < edge::CapacityEdge < TNode, TCapacity > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < TNode >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < TCapacity >::get ( ) );
		return std::make_unique < type_details_template > ( "edge::CapacityEdge", std::move ( sub_types_vec ) );
	}
};

} /* namespace core */

// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <ostream>
#include <alib/pair>
#include <object/Object.h>
#include <alib/tuple>

#include <edge/EdgeFeatures.hpp>
#include <edge/EdgeBase.hpp>

#include <core/type_details_base.hpp>

namespace edge {

template<typename TNode, typename TWeight>
class WeightedEdge : public ext::pair<TNode, TNode>, public EdgeBase {
// ---------------------------------------------------------------------------------------------------------------------
 public:
  using node_type = TNode;
  using weight_type = TWeight;
  using normalized_type = WeightedEdge<>;

// ---------------------------------------------------------------------------------------------------------------------

 private:
  TWeight m_weight;

// =====================================================================================================================
// Constructor, Destructor, Operators

 public:
  explicit WeightedEdge(TNode _first, TNode _second, TWeight weight);

// ---------------------------------------------------------------------------------------------------------------------

  TWeight weight() const;
  void weight(TWeight &&weight);

// =====================================================================================================================
// EdgeBase interface

	auto operator <=> (const WeightedEdge &other) const {
		return ext::tie(this->first, this->second, m_weight) <=> ext::tie(other.first, other.second, other.m_weight);
	}

	bool operator == (const WeightedEdge &other) const {
		return ext::tie(this->first, this->second, m_weight) == ext::tie(other.first, other.second, other.m_weight);
	}

  void operator>>(ext::ostream &ostream) const override;

// =====================================================================================================================

  virtual std::string name() const;

// ---------------------------------------------------------------------------------------------------------------------
};
// =====================================================================================================================

template<typename TNode, typename TWeight>
WeightedEdge<TNode, TWeight>::WeightedEdge(TNode _first, TNode _second, TWeight weight)
    : ext::pair<TNode, TNode>(_first, _second), m_weight(weight) {

}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode, typename TWeight>
TWeight WeightedEdge<TNode, TWeight>::weight() const {
  return m_weight;
}

template<typename TNode, typename TWeight>
void WeightedEdge<TNode, TWeight>::weight(TWeight &&weight) {
  m_weight = std::forward<TWeight>(weight);
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode, typename TWeight>
std::string WeightedEdge<TNode, TWeight>::name() const {
  return "WeightedEdge";
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode, typename TWeight>
void WeightedEdge<TNode, TWeight>::operator>>(ext::ostream &ostream) const {
  ostream << "(" << name() << "(first=" << this->first << ", second=" << this->second << ", weight="
          << m_weight << "))";
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace edge

// =====================================================================================================================

namespace core {

template < class TNode, class TWeight >
struct type_details_retriever < edge::WeightedEdge < TNode, TWeight > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < TNode >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < TWeight >::get ( ) );
		return std::make_unique < type_details_template > ( "edge::WeightedEdge", std::move ( sub_types_vec ) );
	}
};

} /* namespace core */

// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <common/default_type/DefaultNodeType.hpp>
#include <common/default_type/DefaultWeightType.hpp>
#include <common/default_type/DefaultCapacityType.hpp>

namespace edge {

template<typename TNode = DefaultNodeType>
class Edge;

template<typename TNode = DefaultNodeType, typename TWeight = DefaultWeightType>
class WeightedEdge;

template<typename TNode = DefaultNodeType, typename TWeight = DefaultCapacityType>
class CapacityEdge;

} // namespace edge


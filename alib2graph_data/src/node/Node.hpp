// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <ostream>
#include <object/Object.h>

#include "NodeBase.hpp"
#include <core/type_util.hpp>
#include <core/type_details_base.hpp>

namespace node {

class Node : public NodeBase {
// ---------------------------------------------------------------------------------------------------------------------

 private:
  int m_id;

// =====================================================================================================================
// Constructor, Destructor, Operators

 public:
  explicit Node();
  explicit Node(int id);

// ---------------------------------------------------------------------------------------------------------------------

  auto operator <=> ( const Node &rhs) const { return m_id <=> rhs.m_id; }
  bool operator ==( const Node &rhs) const { return m_id == rhs.m_id; }

// ---------------------------------------------------------------------------------------------------------------------
// =====================================================================================================================
// NodeBase interface

  void operator>>(ext::ostream &ostream) const override;

// =====================================================================================================================

  std::string name() const;

// ---------------------------------------------------------------------------------------------------------------------

  int getId() const;

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================

} // namespace node

// =====================================================================================================================

namespace std {

template<>
struct hash<node::Node> {
  std::size_t operator()(const node::Node &k) const {
    return hash<int>()(k.getId());
  }
};
}

// =====================================================================================================================

namespace core {

template < >
struct type_util < node::Node > {
	static node::Node denormalize ( node::Node && arg );

	static node::Node normalize ( node::Node && arg );

	static std::unique_ptr < type_details_base > type ( const node::Node & arg );
};

template < >
struct type_details_retriever < node::Node > {
	static std::unique_ptr < type_details_base > get ( );
};

} /* namespace core */

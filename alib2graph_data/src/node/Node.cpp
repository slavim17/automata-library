// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "Node.hpp"
#include <registration/ValuePrinterRegistration.hpp>

#include <ext/typeinfo>
#include <object/AnyObject.h>
#include <object/ObjectFactory.h>

namespace node {

// ---------------------------------------------------------------------------------------------------------------------

Node::Node() : m_id(-1) {

}

Node::Node(int id) : m_id(id) {

}

// ---------------------------------------------------------------------------------------------------------------------

std::string Node::name() const {
  return "Node";
}

// ---------------------------------------------------------------------------------------------------------------------

void Node::operator>>(ext::ostream &ostream) const {
  ostream << "(" << name() << "(id=" << m_id << "))";

}

// ---------------------------------------------------------------------------------------------------------------------

int Node::getId() const {
  return m_id;
}

} // namespace node

// ---------------------------------------------------------------------------------------------------------------------

namespace core {

node::Node type_util < node::Node >::denormalize ( node::Node && arg ) {
	return std::move ( arg );
}

node::Node type_util < node::Node >::normalize ( node::Node && arg ) {
	return std::move ( arg );
}

std::unique_ptr < type_details_base > type_util < node::Node >::type ( const node::Node & ) {
	return std::make_unique < type_details_type > ( "node::Node" );
}

std::unique_ptr < type_details_base > type_details_retriever < node::Node >::get ( ) {
	return std::make_unique < type_details_type > ( "node::Node" );
}

} /* namespace core */

// =====================================================================================================================

namespace {

auto valuePrinter = registration::ValuePrinterRegister<node::Node>();

}
// ---------------------------------------------------------------------------------------------------------------------


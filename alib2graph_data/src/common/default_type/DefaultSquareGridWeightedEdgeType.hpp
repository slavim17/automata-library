// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/pair>
#include <edge/weighted/WeightedEdge.hpp>
#include "DefaultSquareGridNodeType.hpp"
#include "DefaultWeightType.hpp"

typedef edge::WeightedEdge<DefaultSquareGridNodeType, DefaultWeightType>
    DefaultSquareGridWeightedEdgeType;


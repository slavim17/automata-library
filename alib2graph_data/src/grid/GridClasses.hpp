// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include "square/SquareGrid4.hpp"
#include "square/SquareGrid8.hpp"
#include "square/WeightedSquareGrid4.hpp"
#include "square/WeightedSquareGrid8.hpp"


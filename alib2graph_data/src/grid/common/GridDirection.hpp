// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/set>

namespace grid {

// ---------------------------------------------------------------------------------------------------------------------

enum class SquareGridDirections {
  north_west,
  north,
  north_east,
  east,
  south_east,
  south,
  south_west,
  west,
  none,
};

const ext::set<SquareGridDirections> SQUARE_GRID_DIRECTIONS = {
    SquareGridDirections::north_west,
    SquareGridDirections::north,
    SquareGridDirections::north_east,
    SquareGridDirections::east,
    SquareGridDirections::south_east,
    SquareGridDirections::south,
    SquareGridDirections::south_west,
    SquareGridDirections::west,
};

// ---------------------------------------------------------------------------------------------------------------------

} // namespace grid

// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/pair>
#include <alib/tuple>

#include "SquareGrid.hpp"

namespace grid {

template<typename TCoordinate, typename TEdge>
class SquareGrid8 final : public SquareGrid<TCoordinate, TEdge> {
// ---------------------------------------------------------------------------------------------------------------------

 public:
  using coordinate_type = TCoordinate;
  using edge_type = TEdge;
  using node_type = ext::pair<TCoordinate, TCoordinate>;
  using direction_type = SquareGridDirections;

// =====================================================================================================================
// Constructor, Destructor, Operators
  explicit SquareGrid8(TCoordinate height, TCoordinate width);

// =====================================================================================================================
// GridBase interface
	auto operator <=> ( const SquareGrid8 & other ) const {
		return std::tie(this->m_obstacles, this->m_height, this->m_width) <=> std::tie(other.getObstacleList(), other.getHeight(), other.getWidth());
	}

	bool operator == ( const SquareGrid8 & other ) const {
		return std::tie(this->m_obstacles, this->m_height, this->m_width) == std::tie(other.getObstacleList(), other.getHeight(), other.getWidth());
	}

// =====================================================================================================================
// GridInterface interface

  bool isValidDirection(direction_type direction) const override;

// ---------------------------------------------------------------------------------------------------------------------

 protected:
  TEdge createEdge(const node_type &a, const node_type &b) const override;

// ---------------------------------------------------------------------------------------------------------------------
// =====================================================================================================================
// GraphInterface interface

 public:
  std::string name() const override;

// ---------------------------------------------------------------------------------------------------------------------

};

// =====================================================================================================================

template<typename TCoordinate, typename TEdge>
SquareGrid8<TCoordinate, TEdge>::SquareGrid8(TCoordinate height, TCoordinate width)
    : SquareGrid<TCoordinate, TEdge>(height, width) {
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate, typename TEdge>
TEdge SquareGrid8<TCoordinate, TEdge>::createEdge(const SquareGrid8::node_type &a,
                                                  const SquareGrid8::node_type &b) const {
  return TEdge(a, b);
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate, typename TEdge>
bool SquareGrid8<TCoordinate, TEdge>::isValidDirection(SquareGrid8::direction_type direction) const {
  return SQUARE_GRID_DIRECTIONS.find(direction) != SQUARE_GRID_DIRECTIONS.end();
}

// ---------------------------------------------------------------------------------------------------------------------

template<typename TCoordinate, typename TEdge>
std::string SquareGrid8<TCoordinate, TEdge>::name() const {
  return "SquareGrid8";
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace grid

// =====================================================================================================================

namespace core {

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */
/*template<typename TCoordinate, typename TEdge>
struct normalize < grid::SquareGrid8 < TCoordinate, TEdge > > {
  static grid::SquareGrid8<> eval(grid::SquareGrid8<TCoordinate, TEdge> &&value) {
    grid::SquareGrid8<> grid(value.getHeight(), value.getWidth());

    // Copy obstacles
    for (auto &&i: ext::make_mover(std::move(value).getObstacleList())) {
      DefaultSquareGridNodeType
          obstacle = graph::GraphNormalize::normalizeObstacle(std::move(i));

      grid.addObstacle(std::move(obstacle));
    }

    return grid;
  }
};*/

template<typename TCoordinate, typename TEdge>
struct type_details_retriever < grid::SquareGrid8 < TCoordinate, TEdge > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < TCoordinate >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < TEdge >::get ( ) );
		return std::make_unique < type_details_template > ( "grid::SquareGrid8", std::move ( sub_types_vec ) );
	}
};

} // namespace grid

// =====================================================================================================================

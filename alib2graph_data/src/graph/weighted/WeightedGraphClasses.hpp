// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <common/Normalize.hpp>
#include <core/type_details_base.hpp>
#include <common/default_type/DefaultNodeType.hpp>
#include <common/default_type/DefaultWeightedEdgeType.hpp>

#include <graph/undirected/UndirectedGraph.hpp>
#include <graph/undirected/UndirectedMultiGraph.hpp>
#include <graph/directed/DirectedGraph.hpp>
#include <graph/directed/DirectedMultiGraph.hpp>
#include <graph/mixed/MixedGraph.hpp>
#include <graph/mixed/MixedMultiGraph.hpp>

namespace graph {

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode = DefaultNodeType, typename TEdge = DefaultWeightedEdgeType>
class WeightedUndirectedGraph : public UndirectedGraph<TNode, TEdge> {
 public:
  using node_type = TNode;
  using edge_type = TEdge;
};

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode = DefaultNodeType, typename TEdge = DefaultWeightedEdgeType>
class WeightedUndirectedMultiGraph : public UndirectedMultiGraph<TNode, TEdge> {
 public:
  using node_type = TNode;
  using edge_type = TEdge;
};

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode = DefaultNodeType, typename TEdge = DefaultWeightedEdgeType>
class WeightedDirectedGraph : public DirectedGraph<TNode, TEdge> {
 public:
  using node_type = TNode;
  using edge_type = TEdge;
};

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode = DefaultNodeType, typename TEdge = DefaultWeightedEdgeType>
class WeightedDirectedMultiGraph : public DirectedMultiGraph<TNode, TEdge> {
 public:
  using node_type = TNode;
  using edge_type = TEdge;
};

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode = DefaultNodeType, typename TEdge = DefaultWeightedEdgeType>
class WeightedMixedGraph : public MixedGraph<TNode, TEdge> {
 public:
  using node_type = TNode;
  using edge_type = TEdge;
};

// ---------------------------------------------------------------------------------------------------------------------

template<typename TNode = DefaultNodeType, typename TEdge = DefaultWeightedEdgeType>
class WeightedMixedMultiGraph : public MixedMultiGraph<TNode, TEdge> {
 public:
  using node_type = TNode;
  using edge_type = TEdge;
};

// ---------------------------------------------------------------------------------------------------------------------
}

// =====================================================================================================================

namespace core {

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */
/*template<typename TNode, typename TEdge>
struct normalize < graph::WeightedUndirectedGraph < TNode, TEdge > > {
  static graph::WeightedUndirectedGraph<> eval(graph::WeightedUndirectedGraph<TNode, TEdge> &&value) {
    graph::WeightedUndirectedGraph<> graph;

    for (auto &&i: value.getAdjacencyList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultWeightedEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    return graph;
  }
};*/

template < class TNode, class TEdge >
struct type_details_retriever < graph::WeightedUndirectedGraph < TNode, TEdge > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < TNode >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < TEdge >::get ( ) );
		return std::make_unique < type_details_template > ( "graph::WeightedUndirectedGraph", std::move ( sub_types_vec ) );
	}
};

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */
/*template<typename TNode, typename TEdge>
struct normalize < graph::WeightedUndirectedMultiGraph < TNode, TEdge > > {
  static graph::WeightedUndirectedMultiGraph<> eval(graph::WeightedUndirectedMultiGraph<TNode, TEdge> &&value) {
    graph::WeightedUndirectedMultiGraph<> graph;

    for (auto &&i: value.getAdjacencyList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultWeightedEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    return graph;
  }
};*/

template < class TNode, class TEdge >
struct type_details_retriever < graph::WeightedUndirectedMultiGraph < TNode, TEdge > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < TNode >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < TEdge >::get ( ) );
		return std::make_unique < type_details_template > ( "graph::WeightedUndirectedMultiGraph", std::move ( sub_types_vec ) );
	}
};

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */
/*template<typename TNode, typename TEdge>
struct normalize < graph::WeightedDirectedGraph < TNode, TEdge > > {
  static graph::WeightedDirectedGraph<> eval(graph::WeightedDirectedGraph<TNode, TEdge> &&value) {
    graph::WeightedDirectedGraph<> graph;

    // Create successor
    for (auto &&i: value.getSuccessorList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    // Create predecessors
    for (auto &&i: value.getPredecessorList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    return graph;
  }
};*/

template < class TNode, class TEdge >
struct type_details_retriever < graph::WeightedDirectedGraph < TNode, TEdge > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < TNode >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < TEdge >::get ( ) );
		return std::make_unique < type_details_template > ( "graph::WeightedDirectedGraph", std::move ( sub_types_vec ) );
	}
};

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */
/*template<typename TNode, typename TEdge>
struct normalize < graph::WeightedDirectedMultiGraph < TNode, TEdge > > {
  static graph::WeightedDirectedMultiGraph<> eval(graph::WeightedDirectedMultiGraph<TNode, TEdge> &&value) {
    graph::WeightedDirectedMultiGraph<> graph;

    // Create successor
    for (auto &&i: value.getSuccessorList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    // Create predecessors
    for (auto &&i: value.getPredecessorList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    return graph;
  }
};*/

template < class TNode, class TEdge >
struct type_details_retriever < graph::WeightedDirectedMultiGraph < TNode, TEdge > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < TNode >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < TEdge >::get ( ) );
		return std::make_unique < type_details_template > ( "graph::WeightedDirectedMultiGraph", std::move ( sub_types_vec ) );
	}
};

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */
/*template<typename TNode, typename TEdge>
struct normalize < graph::WeightedMixedGraph < TNode, TEdge > > {
  static graph::WeightedMixedGraph<> eval(graph::WeightedMixedGraph<TNode, TEdge> &&value) {
    graph::WeightedMixedGraph<> graph;

    // Create edges
    for (auto &&i: value.getAdjacencyList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    // Create successor
    for (auto &&i: value.getSuccessorList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    // Create predecessors
    for (auto &&i: value.getPredecessorList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    return graph;
  }
};*/

template < class TNode, class TEdge >
struct type_details_retriever < graph::WeightedMixedGraph < TNode, TEdge > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < TNode >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < TEdge >::get ( ) );
		return std::make_unique < type_details_template > ( "graph::WeightedMixedGraph", std::move ( sub_types_vec ) );
	}
};

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols and states.
 *
 * \returns new instance of the graph with default template parameters or unmodified instance if the template parameters were already the default ones
 */
/*template<typename TNode, typename TEdge>
struct normalize < graph::WeightedMixedMultiGraph < TNode, TEdge > > {
  static graph::WeightedMixedMultiGraph<> eval(graph::WeightedMixedMultiGraph<TNode, TEdge> &&value) {
    graph::WeightedMixedMultiGraph<> graph;

    // Create edges
    for (auto &&i: value.getAdjacencyList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    // Create successor
    for (auto &&i: value.getSuccessorList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    // Create predecessors
    for (auto &&i: value.getPredecessorList()) {
      DefaultNodeType first = graph::GraphNormalize::normalizeNode(std::move(i.first));
      for (auto &&j: i.second) {
        DefaultNodeType second = graph::GraphNormalize::normalizeNode(std::move(j.first));
        DefaultEdgeType edge = graph::GraphNormalize::normalizeWeightedEdge(std::move(j.second));

        graph.addNode(first);
        graph.addNode(second);
        graph.addEdge(edge);
      }
    }

    return graph;
  }
};*/

template < class TNode, class TEdge >
struct type_details_retriever < graph::WeightedMixedMultiGraph < TNode, TEdge > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < TNode >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < TEdge >::get ( ) );
		return std::make_unique < type_details_template > ( "graph::WeightedMixedMultiGraph", std::move ( sub_types_vec ) );
	}
};

// ---------------------------------------------------------------------------------------------------------------------

}

// =====================================================================================================================

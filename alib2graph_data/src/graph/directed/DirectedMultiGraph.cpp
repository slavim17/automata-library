// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "DirectedMultiGraph.hpp"
#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister<graph::DirectedMultiGraph<> >();

}

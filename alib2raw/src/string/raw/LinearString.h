#pragma once

#include <string/LinearString.h>
#include <core/rawApi.hpp>

#include <iterator>

namespace core {

template < class SymbolType >
struct rawApi < string::LinearString < SymbolType > > {
	static string::LinearString < SymbolType > parse ( ext::istream & input );
	static void compose ( ext::ostream & output, const string::LinearString < SymbolType > & string );
};

template < class SymbolType >
string::LinearString < SymbolType > rawApi < string::LinearString < SymbolType > >::parse ( ext::istream & input ) {
	std::istreambuf_iterator < char > input_iter ( input.rdbuf ( ) );
	input >> std::noskipws;

	std::string data ( input_iter, std::istreambuf_iterator < char > ( ) );
	return string::LinearString < SymbolType > { data };
}

template < class SymbolType >
void rawApi < string::LinearString < SymbolType > >::compose ( ext::ostream & output, const string::LinearString < SymbolType > & string ) {
	for(const SymbolType & symbol : string.getContent()) {
		output << ext::to_string ( symbol );
	}
}

} /* namespace core */


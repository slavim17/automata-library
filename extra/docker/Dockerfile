# -------------------------------------------------------------------------------------------------
# build stage

FROM gitlab.fit.cvut.cz:5000/algorithms-library-toolkit/infrastructure/ci-docker-images/alpine:3.13 AS build

ADD . /build
WORKDIR /build

RUN apk add --update \
	curl bash g++ ninja cmake git \
	libexecinfo-dev libxml2-dev tclap-dev readline-dev

RUN mkdir -p build && cd build && cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/build/install \
		-GNinja \
		.. && \
	ninja -j $(grep -c processor /proc/cpuinfo) && \
	ctest -j $(grep -c processor /proc/cpuinfo) --output-on-failure && \
	ninja install

# -------------------------------------------------------------------------------------------------
# deploy stage

FROM gitlab.fit.cvut.cz:5000/algorithms-library-toolkit/infrastructure/ci-docker-images/alpine:3.13 AS deploy
LABEL maintainer="peckato1@fit.cvut.cz"

COPY --from=build /build/install /usr

RUN apk add --update \
	bash libstdc++ \
	libexecinfo libxml2 tclap readline graphviz && \
	mv /usr/lib64/* /usr/lib


CMD /usr/bin/aql2

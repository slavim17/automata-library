#include "WriteFile.h"
#include <registration/AlgoRegistration.hpp>
#include <factory/XmlDataFactory.hpp>

namespace xml::builtin {

void WriteFile::write ( const std::string & filename, const object::Object & data ) {
	return factory::XmlDataFactory::toFile ( data, filename );
}

auto WriteFileString = registration::AbstractRegister < WriteFile, void, const std::string &, const object::Object & > ( WriteFile::write, "filename", "data" ).setDocumentation (
"Writes some string into a file.\n\
\n\
@param filename the name of written file\n\
@param data the content of the file" );

} /* namespace xml::builtin */

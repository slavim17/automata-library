#include "UnsignedLong.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

unsigned long xmlApi < unsigned long >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	unsigned long data = ext::from_string < unsigned long > ( sax::FromXMLParserHelper::popTokenData ( input, sax::Token::TokenType::CHARACTER ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return data;
}

bool xmlApi < unsigned long >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < unsigned long >::xmlTagName ( ) {
	return "UnsignedLong";
}

void xmlApi < unsigned long >::compose ( ext::deque < sax::Token > & output, unsigned long data ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( ext::to_string ( data ), sax::Token::TokenType::CHARACTER );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < unsigned long > ( );
auto xmlRead = registration::XmlReaderRegister < unsigned long > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, unsigned long > ( );

} /* namespace */

#include "String.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

std::string xmlApi < std::string >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	std::string data;
	if ( sax::FromXMLParserHelper::isTokenType ( input, sax::Token::TokenType::CHARACTER ) )
		data = sax::FromXMLParserHelper::popTokenData ( input, sax::Token::TokenType::CHARACTER );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return data;
}

bool xmlApi < std::string >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < std::string >::xmlTagName ( ) {
	return "String";
}

void xmlApi < std::string >::compose ( ext::deque < sax::Token > & output, const std::string & data) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( data, sax::Token::TokenType::CHARACTER );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < std::string > ( );
auto xmlRead = registration::XmlReaderRegister < std::string > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, std::string > ( );

} /* namespace */

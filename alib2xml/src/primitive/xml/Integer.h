#pragma once

#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < int > {
	static int parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, int data );
};

} /* namespace core */


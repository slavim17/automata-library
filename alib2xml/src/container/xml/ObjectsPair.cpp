#include "ObjectsPair.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < ext::pair < object::Object, object::Object > > ( );
auto xmlRead = registration::XmlReaderRegister < ext::pair < object::Object, object::Object > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, ext::pair < object::Object, object::Object > > ( );

} /* namespace */

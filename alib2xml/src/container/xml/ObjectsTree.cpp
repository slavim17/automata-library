#include "ObjectsTree.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < ext::tree < object::Object > > ( );
auto xmlRead = registration::XmlReaderRegister < ext::tree < object::Object > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, ext::tree < object::Object > > ( );

} /* namespace */

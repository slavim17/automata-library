#pragma once

#include <alib/multimap>
#include <core/xmlApi.hpp>
#include <container/xml/ObjectsPair.h>

namespace core {

template < typename T, typename R >
struct xmlApi < ext::multimap < T, R > > {
	static ext::multimap < T, R > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const ext::multimap < T, R > & input );
};

template < typename T, typename R >
ext::multimap < T, R > xmlApi < ext::multimap < T, R > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );

	ext::multimap < T, R > multimap;

	while ( sax::FromXMLParserHelper::isTokenType ( input, sax::Token::TokenType::START_ELEMENT ) )
		multimap.insert ( core::xmlApi < ext::pair < T, R > >::parse ( input ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return multimap;
}

template < typename T, typename R >
bool xmlApi < ext::multimap < T, R > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < typename T, typename R >
std::string xmlApi < ext::multimap < T, R > >::xmlTagName ( ) {
	return "Multimap";
}

template < typename T, typename R >
void xmlApi < ext::multimap < T, R > >::compose ( ext::deque < sax::Token > & output, const ext::multimap < T, R > & input ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );

	for ( const std::pair < const T, R > & item : input )
		core::xmlApi < std::pair < const T, R > >::compose ( output, item );

	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */


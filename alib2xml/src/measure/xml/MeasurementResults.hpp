/*
 * Author: Radovan Cerveny
 */

#pragma once

#include <measurements/MeasurementResults.hpp>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < measurements::MeasurementResults > {
	static measurements::MeasurementResults parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const measurements::MeasurementResults & input );

private:
	static std::string MEASUREMENT_RESULTS_TAG;

	static std::string MEASUREMENT_FRAME_TAG;
	static std::string MEASUREMENT_FRAME_NAME_TAG;
	static std::string MEASUREMENT_FRAME_TYPE_TAG;

	static std::string MEASUREMENT_SUBFRAMES_TAG;

	 // TimeDataFrame
	static std::string TIME_DATA_FRAME_TAG;
	static std::string TIME_DATA_FRAME_DURATION_TAG;
	static std::string TIME_DATA_FRAME_IN_FRAME_DURATION_TAG;

	 // MemoryDataFrame
	static std::string MEMORY_DATA_FRAME_TAG;
	static std::string MEMORY_DATA_FRAME_START_HEAP_USAGE_TAG;
	static std::string MEMORY_DATA_FRAME_END_HEAP_USAGE_TAG;
	static std::string MEMORY_DATA_FRAME_HIGH_WATERMARK_TAG;
	static std::string MEMORY_DATA_FRAME_IN_FRAME_HIGH_WATERMARK_TAG;

	 // CounterDataFrame
	static std::string COUNTER_DATA_FRAME_TAG;
	static std::string COUNTER_DATA_FRAME_COUNTERS_TAG;
	static std::string COUNTER_DATA_FRAME_IN_FRAME_COUNTERS_TAG;
	static std::string COUNTER_DATA_FRAME_COUNTER_TAG;
	static std::string COUNTER_DATA_FRAME_COUNTER_NAME_TAG;
	static std::string COUNTER_DATA_FRAME_COUNTER_VALUE_TAG;

	static void composeMeasurementFrames ( ext::deque < sax::Token > &, unsigned, const measurements::stealth_vector < measurements::MeasurementFrame > & );
	static void composeTimeDataFrame ( ext::deque < sax::Token > &, unsigned, const measurements::stealth_vector < measurements::MeasurementFrame > & );
	static void composeMemoryDataFrame ( ext::deque < sax::Token > &, unsigned, const measurements::stealth_vector < measurements::MeasurementFrame > & );
	static void composeCounterDataFrame ( ext::deque < sax::Token > &, unsigned, const measurements::stealth_vector < measurements::MeasurementFrame > & );

	static void parseRootMeasurementFrame ( measurements::MeasurementResults &, ext::deque < sax::Token >::iterator & );
	static void parseSubframes ( unsigned, measurements::MeasurementResults &, ext::deque < sax::Token >::iterator & );
	static void parseMeasurementFrame ( unsigned, measurements::MeasurementResults &, ext::deque < sax::Token >::iterator & );
	static void parseTimeDataFrame ( measurements::MeasurementFrame &, ext::deque < sax::Token >::iterator & );
	static void parseMemoryDataFrame ( measurements::MeasurementFrame &, ext::deque < sax::Token >::iterator & );
	static void parseCounterDataFrame ( measurements::MeasurementFrame &, ext::deque < sax::Token >::iterator & );
	static void parseCounterDataFrameCounters ( const std::string &, measurements::stealth_map < measurements::stealth_string, measurements::CounterHint::value_type > &, ext::deque < sax::Token >::iterator & );

	template < typename T >
	static T valueTypeFromString ( const std::string & );

};

} /* namespace core */


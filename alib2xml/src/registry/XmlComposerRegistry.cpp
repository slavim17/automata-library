#include <registry/XmlComposerRegistry.hpp>
#include <exception/CommonException.h>

namespace abstraction {

ext::map < std::string, std::unique_ptr < XmlComposerRegistry::Entry > > & XmlComposerRegistry::getEntries ( ) {
	static ext::map < std::string, std::unique_ptr < Entry > > fileWriters;
	return fileWriters;
}

void XmlComposerRegistry::unregisterXmlComposer ( const std::string & param ) {
	if ( getEntries ( ).erase ( param ) == 0u )
		throw std::invalid_argument ( "Entry " + param + " not registered." );
}

void XmlComposerRegistry::registerXmlComposer ( std::string param, std::unique_ptr < Entry > entry ) {
	auto iter = getEntries ( ).insert ( std::make_pair ( std::move ( param ), std::move ( entry ) ) );
	if ( ! iter.second )
		throw std::invalid_argument ( "Entry " + iter.first->first + " already registered." );
}

std::unique_ptr < abstraction::OperationAbstraction > XmlComposerRegistry::getAbstraction ( const std::string & param ) {
	auto res = getEntries ( ).find ( param );
	if ( res == getEntries ( ).end ( ) ) {
		for ( const auto & entry : getEntries ( ) ) {
			std::cout << entry.first;
		}
		throw exception::CommonException ( "Entry " + param + " not available." );
	}

	return res->second->getAbstraction ( );
}

ext::set < std::string > XmlComposerRegistry::listGroup ( const std::string & group ) {
	ext::set < std::string > res;

	for ( const std::pair < const std::string, std::unique_ptr < Entry > > & entry : getEntries ( ) )
		if ( entry.first.find ( group ) == 0 ) //found at the begining
			res.insert ( entry.first );

	return res;
}

ext::set < std::string > XmlComposerRegistry::list ( ) {
	ext::set < std::string > res;

	for ( const std::pair < const std::string, std::unique_ptr < Entry > > & entry : getEntries ( ) )
		res.insert ( entry.first );

	return res;
}

} /* namespace abstraction */

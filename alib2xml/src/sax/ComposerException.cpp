#include "ComposerException.h"

namespace sax {

ComposerException::ComposerException(const Token& expected, const Token& read) : CommonException("Composer Exception: Expected: " + expected.getData() + " Read: " + read.getData()), m_expected(expected), m_read(read) {
}

} /* namespace sax */

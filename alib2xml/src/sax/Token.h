#pragma once

#include <alib/string>
#include <ostream>

#include <core/type_details_base.hpp>
#include <core/type_util.hpp>
#include <core/type_details_base.hpp>

namespace sax {

/**
 * Represents part of parsed XML. Can be start of tag, end of tag,
 * tag attribute or array of characters.
 */
class Token {
public:

	enum class TokenType {
		START_ELEMENT, END_ELEMENT, START_ATTRIBUTE, END_ATTRIBUTE, CHARACTER
	};

private:
	std::string data;
	TokenType type;

public:
	Token ( std::string, TokenType );

	/**
	 * @return name of the tag or characters read
	 */
	const std::string & getData ( ) const &;

	/**
	 * @return name of the tag or characters read
	 */
	std::string && getData ( ) &&;

	/**
	 * @return type of the token - star of the tag, end of the tag, attribute
	 * of the tag or characters
	 */
	TokenType getType() const;

	bool operator==(const Token& other) const;

	auto operator<=>(const Token& other) const {
		return std::tie ( type, data ) <=> std::tie ( other.type, other.data );
	}

	friend std::ostream& operator<<(std::ostream& os, const Token& token);
};

} /* namespace sax */

namespace core {

template < >
struct type_util < sax::Token > {
	static sax::Token denormalize ( sax::Token && arg );

	static sax::Token normalize ( sax::Token && arg );

	static std::unique_ptr < type_details_base > type ( const sax::Token & arg );
};

template < >
struct type_details_retriever < sax::Token > {
	static std::unique_ptr < type_details_base > get ( );
};

} /* namespace core */

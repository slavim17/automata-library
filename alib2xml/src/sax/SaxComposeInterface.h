#pragma once

#include <libxml/xmlwriter.h>
#include <alib/deque>
#include "Token.h"

namespace sax {

/**
 * This class performs composing of XML Tokens to file, string, or stream. Contains callback
 * method for libxml SAX composer.
 */
class SaxComposeInterface {
	static void xmlSAXUserCompose(xmlTextWriterPtr writer, const ext::deque<Token>& in);
public:
	/**
	 * Composes the XML to a string.
	 * @param xmlOut resulting XML
	 * @param in list of xml tokens
	 * @throws CommonException when an error occurs (e.g. xml tokens incorrectly nested)
	 */
	static void composeMemory(std::string& xmlOut, const ext::deque<Token>& in);

	/**
	 * Composes the XML to a string.
	 * @param in list of xml tokens
	 * @return resulting XML
	 * @throws CommonException when an error occurs (e.g. xml tokens incorrectly nested)
	 */
	static std::string composeMemory ( const ext::deque < Token > & in );

	/**
	 * Composes the XML to a file.
	 * @param filename resulting XML file destination
	 * @param in list of xml tokens
	 * @throws CommonException when an error occurs (e.g. xml tokens incorrectly nested)
	 */
	static void composeFile(const std::string& filename, const ext::deque<Token>& in);

	/**
	 * Composes the XML to a stdout.
	 * @param in list of xml tokens
	 * @throws CommonException when an error occurs (e.g. xml tokens incorrectly nested)
	 */
	static void composeStdout(const ext::deque<Token>& in);
	
	/**
	 * Composes the XML to a stream.
	 * @param out resulting XML stream destination
	 * @param in list of xml tokens
	 * @throws CommonException when an error occurs (e.g. xml tokens incorrectly nested)
	 */
	static void composeStream(ext::ostream& out, const ext::deque<Token>& in);
};

} /* namespace sax */



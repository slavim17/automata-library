#include "InducedEquivalence.h"
#include <registration/AlgoRegistration.hpp>

#include <object/Object.h>

namespace {

auto InducedEquivalenceObject = registration::AbstractRegister < relation::InducedEquivalence, ext::set < ext::set < object::Object > >, const ext::set < ext::pair < object::Object, object::Object > > & > ( relation::InducedEquivalence::inducedEquivalence, "relation" ).setDocumentation (
"Creates a set of equivalent objects from a symetric relation.\n\
\n\
@param  automaton which states are to be partitioned.\n\
@return induced equivalence set." );

} /* namespace */

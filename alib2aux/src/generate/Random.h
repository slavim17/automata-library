#pragma once

#include <ext/random>

namespace generate {

template < class T >
class RandomFactory {
public:
	static T randomNumber ( );

	/** @brief Returns random number within range lo (inclusive) and hi (exclusive) */
	static T randomNumber ( const T& lo, const T& hi );
};

template < class T >
T RandomFactory < T >::randomNumber ( ) {
	return ext::random_devices::semirandom ( );
}

template < class T >
T RandomFactory < T >::randomNumber ( const T& lo, const T& hi ) {

	return ext::random_devices::semirandom ( lo, hi );
}

} /* namespace generate */


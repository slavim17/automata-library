#include "Random.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RandomInt = registration::AbstractRegister < generate::RandomFactory < int >, int > ( generate::RandomFactory < int >::randomNumber );
auto RandomIntRange = registration::AbstractRegister < generate::RandomFactory < int >, int, const int&, const int& > ( generate::RandomFactory < int >::randomNumber );
auto RandomUnsigned = registration::AbstractRegister < generate::RandomFactory < unsigned >, unsigned > ( generate::RandomFactory < unsigned >::randomNumber );
auto RandomUnsignedRange = registration::AbstractRegister < generate::RandomFactory < unsigned >, unsigned, const unsigned&, const unsigned& > ( generate::RandomFactory < unsigned >::randomNumber );

} /* namespace */

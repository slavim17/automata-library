#include <object/Object.h>
#include <registration/AlgoRegistration.hpp>

namespace compare {

class IsSame {
public:
	static bool isSame ( const object::Object & a, const object::Object & b ) {
		return a == b;
	}
};

} /* namespace compare */

namespace {

auto IsSame = registration::AbstractRegister < compare::IsSame, bool, const object::Object &, const object::Object & > ( compare::IsSame::isSame );

} /* namespace */

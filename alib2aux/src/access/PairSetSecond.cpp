#include <object/Object.h>
#include "PairSetSecond.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto pairSetSecondObjectObject = registration::AbstractRegister < dataAccess::PairSetSecond, ext::set < object::Object >, const ext::set < ext::pair < object::Object, object::Object > > & > ( dataAccess::PairSetSecond::access );

} /* namespace */

#include "DotConverter.h"
#include "DotConverterRTEPart.hxx"
#include <registration/AlgoRegistration.hpp>

namespace {

auto DotConverterEpsilonNFA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::EpsilonNFA < > & > ( convert::DotConverter::convert );
auto DotConverterMultiInitialStateNFA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::MultiInitialStateNFA < > & > ( convert::DotConverter::convert );
auto DotConverterNFA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::NFA < > & > ( convert::DotConverter::convert );
auto DotConverterDFA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::DFA < > & > ( convert::DotConverter::convert );
auto DotConverterExtendedNFA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::ExtendedNFA < > & > ( convert::DotConverter::convert );
auto DotConverterCompactNFA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::CompactNFA < > & > ( convert::DotConverter::convert );
auto DotConverterNFTA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::NFTA < > & > ( convert::DotConverter::convert );
auto DotConverterEpsilonNFTA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::EpsilonNFTA < > & > ( convert::DotConverter::convert );
auto DotConverterUnorderedNFTA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::UnorderedNFTA < > & > ( convert::DotConverter::convert );
auto DotConverterDFTA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::DFTA < > & > ( convert::DotConverter::convert );
auto DotConverterArcFactoredNondeterministicZAutomaton = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::ArcFactoredNondeterministicZAutomaton < > & > ( convert::DotConverter::convert );
auto DotConverterNondeterministicZAutomaton = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::NondeterministicZAutomaton < > & > ( convert::DotConverter::convert );
auto DotConverterDPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::DPDA < > & > ( convert::DotConverter::convert );
auto DotConverterSinglePopDPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::SinglePopDPDA < > & > ( convert::DotConverter::convert );
auto DotConverterInputDrivenDPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::InputDrivenDPDA < > & > ( convert::DotConverter::convert );
auto DotConverterInputDrivenNPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::InputDrivenNPDA < > & > ( convert::DotConverter::convert );
auto DotConverterVisiblyPushdownDPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::VisiblyPushdownDPDA < > & > ( convert::DotConverter::convert );
auto DotConverterVisiblyPushdownNPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::VisiblyPushdownNPDA < > & > ( convert::DotConverter::convert );
auto DotConverterRealTimeHeightDeterministicDPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::RealTimeHeightDeterministicDPDA < > & > ( convert::DotConverter::convert );
auto DotConverterRealTimeHeightDeterministicNPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::RealTimeHeightDeterministicNPDA < > & > ( convert::DotConverter::convert );
auto DotConverterNPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::NPDA < > & > ( convert::DotConverter::convert );
auto DotConverterSinglePopNPDA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::SinglePopNPDA < > & > ( convert::DotConverter::convert );
auto DotConverterOneTapeDTM = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::OneTapeDTM < > & > ( convert::DotConverter::convert );
auto DotConverterExtendedNFTA = registration::AbstractRegister < convert::DotConverter, std::string, const automaton::ExtendedNFTA < > & > ( convert::DotConverter::convert );
auto DotConverterFormalRTE = registration::AbstractRegister < convert::DotConverter, std::string, const rte::FormalRTE < > & > ( convert::DotConverter::convert );
auto DotConverterRankedTree = registration::AbstractRegister < convert::DotConverter, std::string, const tree::RankedTree < > & > ( convert::DotConverter::convert );

} /* namespace */

#include "Gap.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

Gap::Gap() = default;

ext::ostream & operator << ( ext::ostream & out, const Gap & ) {
	return out << "(Gap)";
}

} /* namespace alphabet */

namespace core {

alphabet::Gap type_util < alphabet::Gap >::denormalize ( alphabet::Gap && arg ) {
	return arg;
}

alphabet::Gap type_util < alphabet::Gap >::normalize ( alphabet::Gap && arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < alphabet::Gap >::type ( const alphabet::Gap & ) {
	return std::make_unique < type_details_type > ( "alphabet::Gap" );
}

std::unique_ptr < type_details_base > type_details_retriever < alphabet::Gap >::get ( ) {
	return std::make_unique < type_details_type > ( "alphabet::Gap" );
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::Gap > ( );

} /* namespace */

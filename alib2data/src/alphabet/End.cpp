#include "End.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

End::End() = default;

ext::ostream & operator << ( ext::ostream & out, const End & ) {
	return out << "(End)";
}

} /* namespace alphabet */

namespace core {

alphabet::End type_util < alphabet::End >::denormalize ( alphabet::End && arg ) {
	return arg;
}

alphabet::End type_util < alphabet::End >::normalize ( alphabet::End && arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < alphabet::End >::type ( const alphabet::End & ) {
	return std::make_unique < type_details_type > ( "alphabet::End" );
}

std::unique_ptr < type_details_base > type_details_retriever < alphabet::End >::get ( ) {
	return std::make_unique < type_details_type > ( "alphabet::End" );
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::End > ( );

} /* namespace */

#include "NonlinearVariable.h"
#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::NonlinearVariable < > > ( );

} /* namespace */

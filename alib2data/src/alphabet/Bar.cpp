#include "Bar.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

Bar::Bar() = default;

ext::ostream & operator << ( ext::ostream & out, const Bar & ) {
	return out << "(Bar symbol)";
}

} /* namespace alphabet */

namespace core {

alphabet::Bar type_util < alphabet::Bar >::denormalize ( alphabet::Bar && arg ) {
	return arg;
}

alphabet::Bar type_util < alphabet::Bar >::normalize ( alphabet::Bar && arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < alphabet::Bar >::type ( const alphabet::Bar & ) {
	return std::make_unique < type_details_type > ( "alphabet::Bar" );
}

std::unique_ptr < type_details_base > type_details_retriever < alphabet::Bar >::get ( ) {
	return std::make_unique < type_details_type > ( "alphabet::Bar" );
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::Bar > ( );

} /* namespace */

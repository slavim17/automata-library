#pragma once

#include <alphabet/Start.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < alphabet::Start > {
	static alphabet::Start parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::Start & data );
};

} /* namespace core */


#pragma once

#include <alphabet/Gap.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < alphabet::Gap > {
	static alphabet::Gap parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::Gap & data );
};

} /* namespace core */

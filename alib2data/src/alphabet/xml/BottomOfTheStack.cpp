#include "BottomOfTheStack.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::BottomOfTheStack xmlApi < alphabet::BottomOfTheStack >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::BottomOfTheStack ( );
}

bool xmlApi < alphabet::BottomOfTheStack >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::BottomOfTheStack >::xmlTagName ( ) {
	return "BottomOfTheStack";
}

void xmlApi < alphabet::BottomOfTheStack >::compose ( ext::deque < sax::Token > & output, const alphabet::BottomOfTheStack & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::BottomOfTheStack > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::BottomOfTheStack > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::BottomOfTheStack > ( );

} /* namespace */

#include "Wildcard.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::Wildcard xmlApi < alphabet::Wildcard >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::Wildcard ( );
}

bool xmlApi < alphabet::Wildcard >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::Wildcard >::xmlTagName ( ) {
	return "Wildcard";
}

void xmlApi < alphabet::Wildcard >::compose ( ext::deque < sax::Token > & output, const alphabet::Wildcard & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::Wildcard > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::Wildcard > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::Wildcard > ( );

} /* namespace */

#include "NonlinearVariable.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::NonlinearVariable < > > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::NonlinearVariable < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::NonlinearVariable < > > ( );

} /* namespace */

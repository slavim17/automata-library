#pragma once

#include <alphabet/Initial.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < alphabet::Initial > {
	static alphabet::Initial parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::Initial & data );
};

} /* namespace core */


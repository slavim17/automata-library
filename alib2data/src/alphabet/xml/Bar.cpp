#include "Bar.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::Bar xmlApi < alphabet::Bar >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return alphabet::Bar ( );
}

bool xmlApi < alphabet::Bar >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < alphabet::Bar >::xmlTagName ( ) {
	return "Bar";
}

void xmlApi < alphabet::Bar >::compose ( ext::deque < sax::Token > & output, const alphabet::Bar & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < alphabet::Bar > ( );
auto xmlRead = registration::XmlReaderRegister < alphabet::Bar > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, alphabet::Bar > ( );

} /* namespace */

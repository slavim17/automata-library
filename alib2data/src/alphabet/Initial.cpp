#include "Initial.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

Initial::Initial() = default;

ext::ostream & operator << ( ext::ostream & out, const Initial & ) {
	return out << "(Initial)";
}

} /* namespace alphabet */

namespace core {

alphabet::Initial type_util < alphabet::Initial >::denormalize ( alphabet::Initial && arg ) {
	return arg;
}

alphabet::Initial type_util < alphabet::Initial >::normalize ( alphabet::Initial && arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < alphabet::Initial >::type ( const alphabet::Initial & ) {
	return std::make_unique < type_details_type > ( "alphabet::Initial" );
}

std::unique_ptr < type_details_base > type_details_retriever < alphabet::Initial >::get ( ) {
	return std::make_unique < type_details_type > ( "alphabet::Initial" );
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::Initial > ( );

} /* namespace */

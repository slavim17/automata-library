#include "Blank.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

Blank::Blank() = default;

ext::ostream & operator << ( ext::ostream & out, const Blank & ) {
	return out << "(Blank symbol)";
}

} /* namespace alphabet */

namespace core {

alphabet::Blank type_util < alphabet::Blank >::denormalize ( alphabet::Blank && arg ) {
	return arg;
}

alphabet::Blank type_util < alphabet::Blank >::normalize ( alphabet::Blank && arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < alphabet::Blank >::type ( const alphabet::Blank & ) {
	return std::make_unique < type_details_type > ( "alphabet::Blank" );
}

std::unique_ptr < type_details_base > type_details_retriever < alphabet::Blank >::get ( ) {
	return std::make_unique < type_details_type > ( "alphabet::Blank" );
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::Blank > ( );

} /* namespace */

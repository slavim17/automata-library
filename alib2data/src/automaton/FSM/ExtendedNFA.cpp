#include "ExtendedNFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::ExtendedNFA < >;
template class abstraction::ValueHolder < automaton::ExtendedNFA < > >;
template const automaton::ExtendedNFA < > & abstraction::retrieveValue < const automaton::ExtendedNFA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::ExtendedNFA < > & >;
template class registration::NormalizationRegisterImpl < automaton::ExtendedNFA < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::ExtendedNFA < > > ( );

auto extendedNFAFromDFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::DFA < > > ( );
auto extendedNFAFromNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::NFA < > > ( );
auto extendedNFAFromMultiInitialStateNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::MultiInitialStateNFA < > > ( );
auto extendedNFAFromEpsilonNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::EpsilonNFA < > > ( );
auto extendedNFAFromMultiInitialStateEpsilonNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::MultiInitialStateEpsilonNFA < > > ( );
auto extendedNFAFromCompactNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::CompactNFA < > > ( );

} /* namespace */

/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ostream>

#include <alib/map>
#include <alib/set>

#include <ext/algorithm>
#include <ext/typeinfo>

#include <core/modules.hpp>
#include <common/createUnique.hpp>
#include <object/ObjectFactory.h>

#include <common/DefaultStateType.h>
#include <common/DefaultSymbolType.h>

#include <label/InitialStateLabel.h>

#include <automaton/AutomatonException.h>

#include <core/type_util.hpp>
#include <core/type_details_base.hpp>
#include <alphabet/common/SymbolNormalize.h>
#include <automaton/common/AutomatonNormalize.h>
#include <alphabet/common/SymbolDenormalize.h>
#include <automaton/common/AutomatonDenormalize.h>

#include "DFA.h"

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace automaton {

/**
 * \brief
 * Compact deterministic finite automaton. Accepts regular languages. The automaton has a list of symbols on transitions.

 * \details
 * Definition is classical definition of finite automata.
 * A = (Q, T, I, \delta, F),
 * Q (States) = nonempty finite set of states,
 * T (TerminalAlphabet) = finite set of terminal symbols - having this empty won't let automaton do much though,
 * I (InitialState) = initial state,
 * \delta = transition function of the form A \times a -> P(Q), where A \in Q, a \in T*, and P(Q) is a powerset of states,
 * F (FinalStates) = set of final states
 *
 * \tparam SymbolTypeT used for the terminal alphabet
 * \tparam StateTypeT used to the states, and the initial state of the automaton.
 */
template < class SymbolTypeT = DefaultSymbolType, class StateTypeT = DefaultStateType >
class CompactDFA final : public core::Components < CompactDFA < SymbolTypeT, StateTypeT >, ext::set < SymbolTypeT >, module::Set, component::InputAlphabet, ext::set < StateTypeT >, module::Set, std::tuple < component::States, component::FinalStates >, StateTypeT, module::Value, component::InitialState > {
public:
	typedef SymbolTypeT SymbolType;
	typedef StateTypeT StateType;

private:
	/**
	 * Transition function as mapping from a state times a list of input symbols on the left hand side to a set of states.
	 */
	ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType > transitions;

public:
	/**
	 * \brief Creates a new instance of the automaton with a concrete initial state.
	 *
	 * \param initialState the initial state of the automaton
	 */
	explicit CompactDFA ( StateType initialState );

	/**
	 * \brief Creates a new instance of the automaton with a concrete set of states, input alphabet, initial state, and a set of final states.
	 *
	 * \param states the initial set of states of the automaton
	 * \param inputAlphabet the initial input alphabet
	 * \param initialState the initial state of the automaton
	 * \param finalStates the initial set of final states of the automaton
	 */
	explicit CompactDFA ( ext::set < StateType > states, ext::set < SymbolType > inputAlphabet, StateType initialState, ext::set < StateType > finalStates );

	/*
	 * \brief Creates a new instance of the automaton based on the Deterministic finite automaton.
	 *
	 * \param other the Deterministic finite automaton
	 */
	explicit CompactDFA ( const DFA < SymbolType, StateType > & other );

	/**
	 * Getter of the initial state.
	 *
	 * \returns the initial state of the automaton
	 */
	const StateType & getInitialState ( ) const & {
		return this->template accessComponent < component::InitialState > ( ).get ( );
	}

	/**
	 * Getter of the initial state.
	 *
	 * \returns the initial state of the automaton
	 */
	StateType && getInitialState ( ) && {
		return std::move ( this->template accessComponent < component::InitialState > ( ).get ( ) );
	}

	/**
	 * Setter of the initial state.
	 *
	 * \param state new initial state of the automaton
	 *
	 * \returns true if the initial state was indeed changed
	 */
	bool setInitialState ( StateType state ) {
		return this->template accessComponent < component::InitialState > ( ).set ( std::move ( state ) );
	}

	/**
	 * Getter of states.
	 *
	 * \returns the states of the automaton
	 */
	const ext::set < StateType > & getStates ( ) const & {
		return this->template accessComponent < component::States > ( ).get ( );
	}

	/**
	 * Getter of states.
	 *
	 * \returns the states of the automaton
	 */
	ext::set < StateType > && getStates ( ) && {
		return std::move ( this->template accessComponent < component::States > ( ).get ( ) );
	}

	/**
	 * Adder of a state.
	 *
	 * \param state the new state to be added to a set of states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addState ( StateType state ) {
		return this->template accessComponent < component::States > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of states.
	 *
	 * \param states completely new set of states
	 */
	void setStates ( ext::set < StateType > states ) {
		this->template accessComponent < component::States > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of a state.
	 *
	 * \param state a state to be removed from a set of states
	 *
	 * \returns true if the state was indeed removed
	 */
	void removeState ( const StateType & state ) {
		this->template accessComponent < component::States > ( ).remove ( state );
	}

	/**
	 * Getter of final states.
	 *
	 * \returns the final states of the automaton
	 */
	const ext::set < StateType > & getFinalStates ( ) const & {
		return this->template accessComponent < component::FinalStates > ( ).get ( );
	}

	/**
	 * Getter of final states.
	 *
	 * \returns the final states of the automaton
	 */
	ext::set < StateType > && getFinalStates ( ) && {
		return std::move ( this->template accessComponent < component::FinalStates > ( ).get ( ) );
	}

	/**
	 * Adder of a final state.
	 *
	 * \param state the new state to be added to a set of final states
	 *
	 * \returns true if the state was indeed added
	 */
	bool addFinalState ( StateType state ) {
		return this->template accessComponent < component::FinalStates > ( ).add ( std::move ( state ) );
	}

	/**
	 * Setter of final states.
	 *
	 * \param states completely new set of final states
	 */
	void setFinalStates ( ext::set < StateType > states ) {
		this->template accessComponent < component::FinalStates > ( ).set ( std::move ( states ) );
	}

	/**
	 * Remover of a final state.
	 *
	 * \param state a state to be removed from a set of final states
	 *
	 * \returns true if the state was indeed removed
	 */
	void removeFinalState ( const StateType & state ) {
		this->template accessComponent < component::FinalStates > ( ).remove ( state );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	const ext::set < SymbolType > & getInputAlphabet ( ) const & {
		return this->template accessComponent < component::InputAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the input alphabet.
	 *
	 * \returns the input alphabet of the automaton
	 */
	ext::set < SymbolType > && getInputAlphabet ( ) && {
		return std::move ( this->template accessComponent < component::InputAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of a input symbol.
	 *
	 * \param symbol the new symbol to be added to an input alphabet
	 *
	 * \returns true if the symbol was indeed added
	 */
	bool addInputSymbol ( SymbolType symbol ) {
		return this->template accessComponent < component::InputAlphabet > ( ).add ( std::move ( symbol ) );
	}

	/**
	 * Adder of input symbols.
	 *
	 * \param symbols new symbols to be added to an input alphabet
	 */
	void addInputSymbols ( ext::set < SymbolType > symbols ) {
		this->template accessComponent < component::InputAlphabet > ( ).add ( std::move ( symbols ) );
	}

	/**
	 * Setter of input alphabet.
	 *
	 * \param symbols completely new input alphabet
	 */
	void setInputAlphabet ( ext::set < SymbolType > symbols ) {
		this->template accessComponent < component::InputAlphabet > ( ).set ( std::move ( symbols ) );
	}

	/**
	 * Remover of an input symbol.
	 *
	 * \param symbol a symbol to be removed from an input alphabet
	 *
	 * \returns true if the symbol was indeed removed
	 */
	void removeInputSymbol ( const SymbolType & symbol ) {
		this->template accessComponent < component::InputAlphabet > ( ).remove ( symbol );
	}

	/**
	 * \brief Add a transition to the automaton.
	 *
	 * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in T*
	 *
	 * \param current the source state (A)
	 * \param input the list of input symbols (a)
	 * \param next the target state (B)
	 *
	 * \throws AutomatonException when transition contains state or symbol not present in the automaton components
	 *
	 * \returns true if the transition was indeed added
	 */
	bool addTransition ( StateType from, ext::vector < SymbolType > input, StateType to );

	/**
	 * \brief Removes a transition from the automaton.
	 *
	 * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in T*
	 *
	 * \param current the source state (A)
	 * \param input the list of input symbols (a)
	 * \param next the target state (B)
	 *
	 * \returns true if the transition was indeed removed
	 */
	bool removeTransition ( const StateType & from, const ext::vector < SymbolType > & input, const StateType & to );

	/**
	 * Get the transition function of the automaton in its natural form.
	 *
	 * \returns transition function of the automaton
	 */
	const ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType > & getTransitions ( ) const &;

	/**
	 * Get the transition function of the automaton in its natural form.
	 *
	 * \returns transition function of the automaton
	 */
	ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType > && getTransitions ( ) &&;

	/**
	 * Get a subset of the transition function of the automaton, with the source state fixed in the transitions natural representation.
	 *
	 * \param from filter the transition function based on this state as a source state
	 *
	 * \returns a subset of the transition function of the automaton with the source state fixed
	 */
	ext::iterator_range < typename ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType >::const_iterator > getTransitionsFromState ( const StateType & from ) const;

	/**
	 * Get a subset of the transition function of the automaton, with the target state fixed in the transitions natural representation.
	 *
	 * \param to filter the transition function based on this state as a source state
	 *
	 * \returns a subset of the transition function of the automaton with the target state fixed
	 */
	ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType > getTransitionsToState ( const StateType & to ) const;

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const CompactDFA & other ) const {
		return std::tie ( getStates ( ), getInputAlphabet ( ), getInitialState ( ), getFinalStates ( ), transitions ) <=> std::tie ( other.getStates ( ), other.getInputAlphabet ( ), other.getInitialState ( ), other.getFinalStates ( ), other.getTransitions ( ) );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const CompactDFA & other ) const {
		return std::tie ( getStates ( ), getInputAlphabet ( ), getInitialState ( ), getFinalStates ( ), transitions ) == std::tie ( other.getStates ( ), other.getInputAlphabet ( ), other.getInitialState ( ), other.getFinalStates ( ), other.getTransitions ( ) );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const CompactDFA & instance ) {
		return out << "(CompactDFA"
			   << " states = " << instance.getStates ( )
			   << " inputAlphabet = " << instance.getInputAlphabet ( )
			   << " initialState = " << instance.getInitialState ( )
			   << " finalStates = " << instance.getFinalStates ( )
			   << " transitions = " << instance.getTransitions ( )
			   << ")";
	}
};

/**
 * Trait to detect whether the type parameter T is or is not CompactDFA. Derived from std::false_type.
 *
 * \tparam T the tested type parameter
 */
template < class T >
class isCompactDFA_impl : public std::false_type {};

/**
 * Trait to detect whether the type parameter T is or is not CompactDFA. Derived from std::true_type.
 *
 * Specialisation for CompactDFA.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton
 * \tparam StateType used for the terminal alphabet of the automaton
 */
template < class SymbolType, class StateType >
class isCompactDFA_impl < CompactDFA < SymbolType, StateType > > : public std::true_type {};

/**
 * Constexpr true if the type parameter T is CompactDFA, false otherwise.
 *
 * \tparam T the tested type parameter
 */
template < class T >
constexpr bool isCompactDFA = isCompactDFA_impl < T > { };

template < class SymbolType, class StateType >
CompactDFA < SymbolType, StateType >::CompactDFA ( ext::set < StateType > states, ext::set < SymbolType > inputAlphabet, StateType initialState, ext::set < StateType > finalStates ) : core::Components < CompactDFA, ext::set < SymbolType >, module::Set, component::InputAlphabet, ext::set < StateType >, module::Set, std::tuple < component::States, component::FinalStates >, StateType, module::Value, component::InitialState > ( std::move ( inputAlphabet ), std::move ( states ), std::move ( finalStates ), std::move ( initialState ) ) {
}

template < class SymbolType, class StateType >
CompactDFA < SymbolType, StateType >::CompactDFA ( StateType initialState ) : CompactDFA ( ext::set < StateType > { initialState }, ext::set < SymbolType > { }, initialState, ext::set < StateType > { } ) {
}

template < class SymbolType, class StateType >
CompactDFA < SymbolType, StateType >::CompactDFA ( const DFA < SymbolType, StateType > & other ) : CompactDFA ( other.getStates ( ), other.getInputAlphabet ( ), other.getInitialState ( ), other.getFinalStates ( ) ) {
	for ( const auto & transition : other.getTransitions ( ) )
		transitions.insert ( ext::make_pair ( transition.first.first, ext::vector < SymbolType > { transition.first.second } ), transition.second );
}

template < class SymbolType, class StateType >
bool CompactDFA < SymbolType, StateType >::addTransition ( StateType from, ext::vector < SymbolType > input, StateType to ) {
	if ( ! getStates ( ).contains ( from ) )
		throw AutomatonException ( "State \"" + ext::to_string ( from ) + "\" doesn't exist." );

	ext::set < SymbolType > inputStringAlphabet ( input.begin ( ), input.end ( ) );

	 // Transition regexp's alphabet must be subset of automaton's alphabet
	if ( ! std::includes ( getInputAlphabet ( ).begin ( ), getInputAlphabet ( ).end ( ), inputStringAlphabet.begin ( ), inputStringAlphabet.end ( ) ) )
		throw AutomatonException ( "Input string is over different alphabet than automaton" );

	if ( ! getStates ( ).contains ( to ) )
		throw AutomatonException ( "State \"" + ext::to_string ( to ) + "\" doesn't exist." );

	ext::pair < StateType, ext::vector < SymbolType > > key = ext::make_pair ( std::move ( from ), std::move ( input ) );
	if ( transitions.find ( key ) != transitions.end ( ) )
		return false;

	auto transitionsFromState = getTransitionsFromState ( key.first );
	if ( key.second.empty ( ) && ! transitionsFromState.empty ( ) )
		throw AutomatonException ( "Epsilon transition from state \"" + ext::to_string ( key.first ) + "\" coflicts already existent transitions." );

	for ( const std::pair < const ext::pair < StateType, ext::vector < SymbolType > >, StateType > & transition : getTransitionsFromState ( key.first ) )
		if ( transition.first.second.empty ( ) || transition.first.second [ 0 ] == key.second [ 0 ] )
			throw AutomatonException ( "Transition from state \"" + ext::to_string ( key.first ) + "\" reading symbol \"" + ext::to_string ( key.second ) + "\" already exists. Targeto state was \"" + ext::to_string ( to ) + "\"." );

	transitions.insert ( std::move ( key ), std::move ( to ) );
	return true;
}

template < class SymbolType, class StateType >
bool CompactDFA < SymbolType, StateType >::removeTransition ( const StateType & from, const ext::vector < SymbolType > & input, const StateType & to ) {
	ext::pair < StateType, ext::vector < SymbolType > > key = ext::make_pair ( from, input );

	auto iter = transitions.find ( key );

	if ( iter == transitions.end ( ) )
		return false;

	if ( iter->second != to )
		return false;

	transitions.erase ( iter );
	return true;
}

template < class SymbolType, class StateType >
const ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType > & CompactDFA < SymbolType, StateType >::getTransitions ( ) const & {
	return transitions;
}

template < class SymbolType, class StateType >
ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType > && CompactDFA < SymbolType, StateType >::getTransitions ( ) && {
	return std::move ( transitions );
}

template<class SymbolType, class StateType >
ext::iterator_range < typename ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType >::const_iterator > CompactDFA < SymbolType, StateType >::getTransitionsFromState ( const StateType & from ) const {
	if ( ! getStates ( ).contains ( from ) )
		throw AutomatonException ( "State \"" + ext::to_string ( from ) + "\" doesn't exist" );

	auto lower = transitions.lower_bound ( ext::slice_comp ( from ) );
	auto upper = transitions.upper_bound ( ext::slice_comp ( from ) );

	return ext::make_iterator_range ( lower, upper );
}

template < class SymbolType, class StateType >
ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType > CompactDFA < SymbolType, StateType >::getTransitionsToState ( const StateType & to ) const {
	if ( ! getStates ( ).contains ( to ) )
		throw AutomatonException ( "State \"" + ext::to_string ( to ) + "\" doesn't exist" );

	ext::map < ext::pair < StateType, ext::vector < SymbolType > >, StateType > transitionsToState;

	for ( const std::pair < const ext::pair < StateType, ext::vector < SymbolType > >, StateType > & transition : transitions )
		if ( transition.second == to )
			transitionsToState.insert ( transition );

	return transitionsToState;
}

} /* namespace automaton */

namespace core {

/**
 * Helper class specifying constraints for the automaton's internal input alphabet component.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class SymbolType, class StateType >
class SetConstraint< automaton::CompactDFA < SymbolType, StateType >, SymbolType, component::InputAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const automaton::CompactDFA < SymbolType, StateType > & automaton, const SymbolType & symbol ) {
		for ( const std::pair < const ext::pair < StateType, ext::vector < SymbolType > >, StateType > & transition : automaton.getTransitions ( ) ) {
			ext::set < SymbolType > alphabet ( transition.first.second.begin ( ), transition.first.second.end ( ) );
			if ( alphabet.contains ( symbol ) )
				return true;
		}

		return false;
	}

	/**
	 * Returns true as all symbols are possibly available to be elements of the input alphabet.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested state
	 *
	 * \returns true
	 */
	static bool available ( const automaton::CompactDFA < SymbolType, StateType > &, const SymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as input symbols.
	 *
	 * \param automaton the tested automaton
	 * \param symbol the tested state
	 */
	static void valid ( const automaton::CompactDFA < SymbolType, StateType > &, const SymbolType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal states component.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class SymbolType, class StateType >
class SetConstraint< automaton::CompactDFA < SymbolType, StateType >, StateType, component::States > {
public:
	/**
	 * Returns true if the state is still used in some transition of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is used, false othervise
	 */
	static bool used ( const automaton::CompactDFA < SymbolType, StateType > & automaton, const StateType & state ) {
		if ( automaton.getInitialState ( ) == state )
			return true;

		if ( automaton.getFinalStates ( ).contains ( state ) )
			return true;

		for ( const std::pair < const ext::pair < StateType, ext::vector < SymbolType > >, StateType > & t : automaton.getTransitions ( ) )
			if ( ( t.first.first == state ) || t.second == state )
				return true;

		return false;
	}

	/**
	 * Returns true as all states are possibly available to be elements of the states.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true
	 */
	static bool available ( const automaton::CompactDFA < SymbolType, StateType > &, const StateType & ) {
		return true;
	}

	/**
	 * All states are valid as a state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::CompactDFA < SymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal final states component.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class SymbolType, class StateType >
class SetConstraint< automaton::CompactDFA < SymbolType, StateType >, StateType, component::FinalStates > {
public:
	/**
	 * Returns false. Final state is only a mark that the automaton itself does require further.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns false
	 */
	static bool used ( const automaton::CompactDFA < SymbolType, StateType > &, const StateType & ) {
		return false;
	}

	/**
	 * Determines whether the state is available in the automaton's states set.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is already in the set of states of the automaton
	 */
	static bool available ( const automaton::CompactDFA < SymbolType, StateType > & automaton, const StateType & state ) {
		return automaton.template accessComponent < component::States > ( ).get ( ).contains ( state );
	}

	/**
	 * All states are valid as a final state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::CompactDFA < SymbolType, StateType > &, const StateType & ) {
	}
};

/**
 * Helper class specifying constraints for the automaton's internal initial state element.
 *
 * \tparam SymbolType used for the terminal alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template < class SymbolType, class StateType >
class ElementConstraint< automaton::CompactDFA < SymbolType, StateType >, StateType, component::InitialState > {
public:
	/**
	 * Determines whether the state is available in the automaton's states set.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 *
	 * \returns true if the state is already in the set of states of the automaton
	 */
	static bool available ( const automaton::CompactDFA < SymbolType, StateType > & automaton, const StateType & state ) {
		return automaton.template accessComponent < component::States > ( ).get ( ).contains ( state );
	}

	/**
	 * All states are valid as an initial state of the automaton.
	 *
	 * \param automaton the tested automaton
	 * \param state the tested state
	 */
	static void valid ( const automaton::CompactDFA < SymbolType, StateType > &, const StateType & ) {
	}
};

} /* namespace core */

namespace core {

template < class SymbolType, class StateType >
struct type_util < automaton::CompactDFA < SymbolType, StateType > > {
	static automaton::CompactDFA < SymbolType, StateType > denormalize ( automaton::CompactDFA < > && value ) {
		ext::set < SymbolType > alphabet = alphabet::SymbolDenormalize::denormalizeAlphabet < SymbolType > ( std::move ( value ).getInputAlphabet ( ) );
		ext::set < StateType > states = automaton::AutomatonDenormalize::denormalizeStates < StateType > ( std::move ( value ).getStates ( ) );
		StateType initialState = automaton::AutomatonDenormalize::denormalizeState < StateType > ( std::move ( value ).getInitialState ( ) );
		ext::set < StateType > finalStates = automaton::AutomatonDenormalize::denormalizeStates < StateType > ( std::move ( value ).getFinalStates ( ) );

		automaton::CompactDFA < SymbolType, StateType > res ( std::move ( states ), std::move ( alphabet ), std::move ( initialState ), std::move ( finalStates ) );

		for ( std::pair < ext::pair < DefaultStateType, ext::vector < DefaultSymbolType > >, DefaultStateType > && transition : ext::make_mover ( std::move ( value ).getTransitions ( ) ) ) {
			StateType from = automaton::AutomatonDenormalize::denormalizeState < StateType > ( std::move ( transition.first.first ) );
			ext::vector < SymbolType > input = alphabet::SymbolDenormalize::denormalizeSymbols < SymbolType > ( std::move ( transition.first.second ) );
			StateType target = automaton::AutomatonDenormalize::denormalizeState < StateType > ( std::move ( transition.second ) );

			res.addTransition ( std::move ( from ), std::move ( input ), std::move ( target ) );
		}

		return res;
	}

	static automaton::CompactDFA < > normalize ( automaton::CompactDFA < SymbolType, StateType > && value ) {
		ext::set < DefaultSymbolType > alphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getInputAlphabet ( ) );
		ext::set < DefaultStateType > states = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getStates ( ) );
		DefaultStateType initialState = automaton::AutomatonNormalize::normalizeState ( std::move ( value ).getInitialState ( ) );
		ext::set < DefaultStateType > finalStates = automaton::AutomatonNormalize::normalizeStates ( std::move ( value ).getFinalStates ( ) );

		automaton::CompactDFA < > res ( std::move ( states ), std::move ( alphabet ), std::move ( initialState ), std::move ( finalStates ) );

		for ( std::pair < ext::pair < StateType, ext::vector < SymbolType > >, StateType > && transition : ext::make_mover ( std::move ( value ).getTransitions ( ) ) ) {
			DefaultStateType from = automaton::AutomatonNormalize::normalizeState ( std::move ( transition.first.first ) );
			ext::vector < DefaultSymbolType > input = alphabet::SymbolNormalize::normalizeSymbols ( std::move ( transition.first.second ) );
			DefaultStateType target = automaton::AutomatonNormalize::normalizeState ( std::move ( transition.second ) );

			res.addTransition ( std::move ( from ), std::move ( input ), std::move ( target ) );
		}

		return res ;
	}

	static std::unique_ptr < type_details_base > type ( const automaton::CompactDFA < SymbolType, StateType > & arg ) {
		core::unique_ptr_set < type_details_base > subTypesSymbol;
		for ( const SymbolType & item : arg.getInputAlphabet ( ) )
			subTypesSymbol.insert ( type_util < SymbolType >::type ( item ) );

		core::unique_ptr_set < type_details_base > subTypesState;
		for ( const StateType & item : arg.getStates ( ) )
			subTypesState.insert ( type_util < StateType >::type ( item ) );

		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_variant_type::make_variant ( std::move ( subTypesSymbol ) ) );
		sub_types_vec.push_back ( type_details_variant_type::make_variant ( std::move ( subTypesState ) ) );
		return std::make_unique < type_details_template > ( "automaton::CompactDFA", std::move ( sub_types_vec ) );
	}
};

template < class SymbolType, class StateType >
struct type_details_retriever < automaton::CompactDFA < SymbolType, StateType > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < SymbolType >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < StateType >::get ( ) );
		return std::make_unique < type_details_template > ( "automaton::CompactDFA", std::move ( sub_types_vec ) );
	}
};

} /* namespace core */

extern template class automaton::CompactDFA < >;
extern template class abstraction::ValueHolder < automaton::CompactDFA < > >;
extern template const automaton::CompactDFA < > & abstraction::retrieveValue < const automaton::CompactDFA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
extern template class registration::DenormalizationRegisterImpl < const automaton::CompactDFA < > & >;
extern template class registration::NormalizationRegisterImpl < automaton::CompactDFA < > >;

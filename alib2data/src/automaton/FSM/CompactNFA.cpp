#include "CompactNFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class automaton::CompactNFA < >;
template class abstraction::ValueHolder < automaton::CompactNFA < > >;
template const automaton::CompactNFA < > & abstraction::retrieveValue < const automaton::CompactNFA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::CompactNFA < > & >;
template class registration::NormalizationRegisterImpl < automaton::CompactNFA < > >;

namespace {

auto components = registration::ComponentRegister < automaton::CompactNFA < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < automaton::CompactNFA < > > ( );

auto compactNFAFromDFA = registration::CastRegister < automaton::CompactNFA < >, automaton::DFA < > > ( );
auto compactNFAFromCompactDFA = registration::CastRegister < automaton::CompactNFA < >, automaton::CompactDFA < > > ( );
auto compactNFAFromNFA = registration::CastRegister < automaton::CompactNFA < >, automaton::NFA < > > ( );
auto compactNFAFromMultiInitialStateNFA = registration::CastRegister < automaton::CompactNFA < >, automaton::MultiInitialStateNFA < > > ( );
auto compactNFAFromEpsilonNFA = registration::CastRegister < automaton::CompactNFA < >, automaton::EpsilonNFA < > > ( );
auto compactNFAFromMultiInitialStateEpsilonNFA = registration::CastRegister < automaton::CompactNFA < >, automaton::MultiInitialStateEpsilonNFA < > > ( );

} /* namespace */

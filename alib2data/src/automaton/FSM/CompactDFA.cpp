#include "CompactDFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class automaton::CompactDFA < >;
template class abstraction::ValueHolder < automaton::CompactDFA < > >;
template const automaton::CompactDFA < > & abstraction::retrieveValue < const automaton::CompactDFA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::CompactDFA < > & >;
template class registration::NormalizationRegisterImpl < automaton::CompactDFA < > >;

namespace {

auto components = registration::ComponentRegister < automaton::CompactDFA < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < automaton::CompactDFA < > > ( );

auto compactNFAFromDFA = registration::CastRegister < automaton::CompactDFA < >, automaton::DFA < > > ( );

} /* namespace */

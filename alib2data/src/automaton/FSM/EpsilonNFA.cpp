#include "EpsilonNFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::EpsilonNFA < >;
template class abstraction::ValueHolder < automaton::EpsilonNFA < > >;
template const automaton::EpsilonNFA < > & abstraction::retrieveValue < const automaton::EpsilonNFA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::EpsilonNFA < > & >;
template class registration::NormalizationRegisterImpl < automaton::EpsilonNFA < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::EpsilonNFA < > > ( );

auto epsilonNFAFromDFA = registration::CastRegister < automaton::EpsilonNFA < >, automaton::DFA < > > ( );
auto epsilonNFAFromNFA = registration::CastRegister < automaton::EpsilonNFA < >, automaton::NFA < > > ( );
auto epsilonNFAFromMultiInitialStateNFA = registration::CastRegister < automaton::EpsilonNFA < >, automaton::MultiInitialStateNFA < > > ( );

} /* namespace */

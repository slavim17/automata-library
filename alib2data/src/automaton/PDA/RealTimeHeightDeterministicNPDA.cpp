#include "RealTimeHeightDeterministicNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::RealTimeHeightDeterministicNPDA < >;
template class abstraction::ValueHolder < automaton::RealTimeHeightDeterministicNPDA < > >;
template const automaton::RealTimeHeightDeterministicNPDA < > & abstraction::retrieveValue < const automaton::RealTimeHeightDeterministicNPDA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::RealTimeHeightDeterministicNPDA < > & >;
template class registration::NormalizationRegisterImpl < automaton::RealTimeHeightDeterministicNPDA < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::RealTimeHeightDeterministicNPDA < > > ( );

} /* namespace */

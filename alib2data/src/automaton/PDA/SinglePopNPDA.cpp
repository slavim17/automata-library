#include "SinglePopNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::SinglePopNPDA < >;
template class abstraction::ValueHolder < automaton::SinglePopNPDA < > >;
template const automaton::SinglePopNPDA < > & abstraction::retrieveValue < const automaton::SinglePopNPDA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::SinglePopNPDA < > & >;
template class registration::NormalizationRegisterImpl < automaton::SinglePopNPDA < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::SinglePopNPDA < > > ( );

} /* namespace */

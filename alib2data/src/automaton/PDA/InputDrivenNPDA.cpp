#include "InputDrivenNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::InputDrivenNPDA < >;
template class abstraction::ValueHolder < automaton::InputDrivenNPDA < > >;
template const automaton::InputDrivenNPDA < > & abstraction::retrieveValue < const automaton::InputDrivenNPDA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::InputDrivenNPDA < > & >;
template class registration::NormalizationRegisterImpl < automaton::InputDrivenNPDA < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::InputDrivenNPDA < > > ( );

} /* namespace */

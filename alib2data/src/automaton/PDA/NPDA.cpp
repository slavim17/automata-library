#include "NPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::NPDA < >;
template class abstraction::ValueHolder < automaton::NPDA < > >;
template const automaton::NPDA < > & abstraction::retrieveValue < const automaton::NPDA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::NPDA < > & >;
template class registration::NormalizationRegisterImpl < automaton::NPDA < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NPDA < > > ( );

} /* namespace */

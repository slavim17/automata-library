#include "OneTapeDTM.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::OneTapeDTM < >;
template class abstraction::ValueHolder < automaton::OneTapeDTM < > >;
template const automaton::OneTapeDTM < > & abstraction::retrieveValue < const automaton::OneTapeDTM < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::OneTapeDTM < > & >;
template class registration::NormalizationRegisterImpl < automaton::OneTapeDTM < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::OneTapeDTM < > > ( );

} /* namespace */

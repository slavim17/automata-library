#include "DFA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::DFA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::DFA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::DFA < > > ( );

} /* namespace */

#include "SinglePopDPDA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::SinglePopDPDA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::SinglePopDPDA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::SinglePopDPDA < > > ( );

} /* namespace */

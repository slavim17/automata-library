#include "InputDrivenDPDA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::InputDrivenDPDA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::InputDrivenDPDA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::InputDrivenDPDA < > > ( );

} /* namespace */

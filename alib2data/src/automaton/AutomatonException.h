#pragma once

#include <exception/CommonException.h>

namespace automaton {

/**
 * Exception thrown by an automaton, automaton parser or automaton printer.
 */
class AutomatonException: public exception::CommonException {
public:
	explicit AutomatonException(const std::string& cause);
};

} /* namespace automaton */


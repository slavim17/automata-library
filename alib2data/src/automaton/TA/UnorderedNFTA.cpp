#include "UnorderedNFTA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::UnorderedNFTA < >;
template class abstraction::ValueHolder < automaton::UnorderedNFTA < > >;
template const automaton::UnorderedNFTA < > & abstraction::retrieveValue < const automaton::UnorderedNFTA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::UnorderedNFTA < > & >;
template class registration::NormalizationRegisterImpl < automaton::UnorderedNFTA < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::UnorderedNFTA < > > ( );

auto NFTAFromDFTA = registration::CastRegister < automaton::UnorderedNFTA < >, automaton::UnorderedDFTA < > > ( );

} /* namespace */

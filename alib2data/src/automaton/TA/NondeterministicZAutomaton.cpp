#include "NondeterministicZAutomaton.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::NondeterministicZAutomaton < >;
template class abstraction::ValueHolder < automaton::NondeterministicZAutomaton < > >;
template const automaton::NondeterministicZAutomaton < > & abstraction::retrieveValue < const automaton::NondeterministicZAutomaton < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::NondeterministicZAutomaton < > & >;
template class registration::NormalizationRegisterImpl < automaton::NondeterministicZAutomaton < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NondeterministicZAutomaton < > > ( );

} /* namespace */

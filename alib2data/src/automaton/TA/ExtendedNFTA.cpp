#include "ExtendedNFTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::ExtendedNFTA < >;
template class abstraction::ValueHolder < automaton::ExtendedNFTA < > >;
template const automaton::ExtendedNFTA < > & abstraction::retrieveValue < const automaton::ExtendedNFTA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::ExtendedNFTA < > & >;
template class registration::NormalizationRegisterImpl < automaton::ExtendedNFTA < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::ExtendedNFTA < > > ( );

} /* namespace */

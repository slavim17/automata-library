#include "ArcFactoredDeterministicZAutomaton.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::ArcFactoredDeterministicZAutomaton < >;
template class abstraction::ValueHolder < automaton::ArcFactoredDeterministicZAutomaton < > >;
template const automaton::ArcFactoredDeterministicZAutomaton < > & abstraction::retrieveValue < const automaton::ArcFactoredDeterministicZAutomaton < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::ArcFactoredDeterministicZAutomaton < > & >;
template class registration::NormalizationRegisterImpl < automaton::ArcFactoredDeterministicZAutomaton < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::ArcFactoredDeterministicZAutomaton < > > ( );

} /* namespace */

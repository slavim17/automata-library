#include "NFTA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::NFTA < >;
template class abstraction::ValueHolder < automaton::NFTA < > >;
template const automaton::NFTA < > & abstraction::retrieveValue < const automaton::NFTA < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const automaton::NFTA < > & >;
template class registration::NormalizationRegisterImpl < automaton::NFTA < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NFTA < > > ( );

auto NFTAFromDFTA = registration::CastRegister < automaton::NFTA < >, automaton::DFTA < > > ( );

} /* namespace */

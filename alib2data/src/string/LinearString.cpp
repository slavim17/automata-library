#include "LinearString.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class string::LinearString < >;
template class abstraction::ValueHolder < string::LinearString < > >;
template const string::LinearString < > & abstraction::retrieveValue < const string::LinearString < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const string::LinearString < > & >;
template class registration::NormalizationRegisterImpl < string::LinearString < > >;

namespace {

auto components = registration::ComponentRegister < string::LinearString < > > ( );

auto LinearStringFromString = registration::CastRegister < string::LinearString < char >, std::string > ( );

auto valuePrinter = registration::ValuePrinterRegister < string::LinearString < > > ( );

} /* namespace */

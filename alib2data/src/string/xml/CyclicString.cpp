#include "CyclicString.h"

#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < string::CyclicString < > > ( );
auto xmlRead = registration::XmlReaderRegister < string::CyclicString < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, string::CyclicString < > > ( );

} /* namespace */

#pragma once

#include <string/CyclicString.h>
#include <core/xmlApi.hpp>

#include <string/xml/common/StringFromXmlParserCommon.h>
#include <string/xml/common/StringToXmlComposerCommon.h>

namespace core {

template < typename SymbolType >
struct xmlApi < string::CyclicString < SymbolType > > {
	static string::CyclicString < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const string::CyclicString < SymbolType > & input );
};

template < typename SymbolType >
string::CyclicString < SymbolType > xmlApi < string::CyclicString < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	ext::set<SymbolType> alphabet = string::StringFromXmlParserCommon::parseAlphabet < SymbolType > ( input );
	ext::vector<SymbolType> content = string::StringFromXmlParserCommon::parseContent < SymbolType > ( input );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return string::CyclicString < SymbolType > ( std::move ( alphabet ), std::move ( content ) );
}

template < typename SymbolType >
bool xmlApi < string::CyclicString < SymbolType > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < typename SymbolType >
std::string xmlApi < string::CyclicString < SymbolType > >::xmlTagName ( ) {
	return "CyclicString";
}

template < typename SymbolType >
void xmlApi < string::CyclicString < SymbolType > >::compose ( ext::deque < sax::Token > & output, const string::CyclicString < SymbolType > & input ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	string::StringToXmlComposerCommon::composeAlphabet ( output, input.getAlphabet ( ) );
	string::StringToXmlComposerCommon::composeContent ( output, input.getContent ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */


#pragma once

#include <rte/formal/FormalRTEElements.h>
#include <rte/formal/FormalRTEStructure.h>

#include <alphabet/common/SymbolNormalize.h>

namespace rte {

/**
 * Determines whether regular expression (or its subtree) describes an empty language (rte == \0)
 *
 */
class RTENormalizeVariant {
public:
	/**
	 * Determines whether regular expression is describes an empty language (rte == \0)
	 *
	 * \tparam DefaultSymbolType the type of symbol in the tested regular expression
	 *
	 * \param rte the rte to test
	 *
	 * \return true of the language described by the regular expression is empty
	 */
	template < class ... Types >
	static ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > normalize ( rte::FormalRTEElement < ext::variant < Types ... > > && rte );

	template < class ... Types >
	class Formal {
	public:
		static ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > visit ( rte::FormalRTEAlternation < ext::variant < Types ... > > && alternation);
		static ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > visit ( rte::FormalRTESubstitution < ext::variant < Types ... > > && substitution);
		static ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > visit ( rte::FormalRTEIteration < ext::variant < Types ... > > && iteration);
		static ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > visit ( rte::FormalRTESymbolAlphabet < ext::variant < Types ... > > && symbol);
		static ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > visit ( rte::FormalRTEEmpty < ext::variant < Types ... > > && empty);
		static ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > visit ( rte::FormalRTESymbolSubst < ext::variant < Types ... > > && symbol);
	};

};

// ----------------------------------------------------------------------------

template < class ... Types >
ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > RTENormalizeVariant::normalize ( rte::FormalRTEElement < ext::variant < Types ... > > && rte ) {
	return std::move ( rte ).template accept < ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > >, RTENormalizeVariant::Formal < Types ... > > ( );
}

// ----------------------------------------------------------------------------

template < class ... Types >
ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > RTENormalizeVariant::Formal < Types ... >::visit ( rte::FormalRTEAlternation < ext::variant < Types ... > > && alternation ) {
	return ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > (
			new FormalRTEAlternation < ext::variant < ext::second_type < Types, object::Object > ... > > (
				std::move ( * std::move ( std::move ( alternation ).getLeftElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > >, RTENormalizeVariant::Formal < Types ... > > ( ) ),
				std::move ( * std::move ( std::move ( alternation ).getRightElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > >, RTENormalizeVariant::Formal < Types ... > > ( ) )
			)
		);
}

template < class ... Types >
ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > RTENormalizeVariant::Formal < Types ... >::visit ( rte::FormalRTESubstitution < ext::variant < Types ... > > && substitution ) {
	FormalRTESymbolSubst < ext::variant < ext::second_type < Types, object::Object > ... > > subst ( alphabet::SymbolNormalize::normalizeRankedVariantSymbol ( std::move ( substitution ).getSubstitutionSymbol ( ).getSymbol ( ) ) );
	return ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > (
			new FormalRTESubstitution < ext::variant < ext::second_type < Types, object::Object > ... > > (
				std::move ( * std::move ( std::move ( substitution ).getLeftElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > >, RTENormalizeVariant::Formal < Types ... > > ( ) ),
				std::move ( * std::move ( std::move ( substitution ).getRightElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > >, RTENormalizeVariant::Formal < Types ... > > ( ) ),
				std::move ( subst )
			)
		);
}

template < class ... Types >
ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > RTENormalizeVariant::Formal < Types ... >::visit ( rte::FormalRTEIteration < ext::variant < Types ... > > && iteration ) {
	FormalRTESymbolSubst < ext::variant < ext::second_type < Types, object::Object > ... > > subst ( alphabet::SymbolNormalize::normalizeRankedVariantSymbol ( std::move ( iteration ).getSubstitutionSymbol ( ).getSymbol ( ) ) );
	return ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > (
			new FormalRTEIteration < ext::variant < ext::second_type < Types, object::Object > ... > > (
				std::move ( * std::move ( std::move ( iteration ).getElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > >, RTENormalizeVariant::Formal < Types ... > > ( ) ),
				std::move ( subst )
			)
		);
}

template < class ... Types >
ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > RTENormalizeVariant::Formal < Types ... >::visit ( rte::FormalRTESymbolAlphabet < ext::variant < Types ... > > && symbol ) {
	ext::ptr_vector < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > children;
	for ( FormalRTEElement < ext::variant < Types ... > > && element : ext::make_mover ( std::move ( symbol ).getChildren ( ) ) )
		children.push_back ( std::move ( * std::move ( element ).template accept < ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > >, RTENormalizeVariant::Formal < Types ... > > ( ) ) );

	return ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > (
			new FormalRTESymbolAlphabet < ext::variant < ext::second_type < Types, object::Object > ... > > (
				alphabet::SymbolNormalize::normalizeRankedVariantSymbol ( std::move ( symbol ).getSymbol ( ) ),
				std::move ( children )
			)
		);
}

template < class ... Types >
ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > RTENormalizeVariant::Formal < Types ... >::visit ( rte::FormalRTEEmpty < ext::variant < Types ... > > && ) {
	return ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > ( new FormalRTEEmpty < ext::variant < ext::second_type < Types, object::Object > ... > > ( ) );
}

template < class ... Types >
ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > RTENormalizeVariant::Formal < Types ... >::visit ( rte::FormalRTESymbolSubst < ext::variant < Types ... > > && symbol ) {
	FormalRTESymbolSubst < ext::variant < ext::second_type < Types, object::Object > ... > > res ( alphabet::SymbolNormalize::normalizeRankedVariantSymbol ( std::move ( symbol ).getSymbol ( ) ) );
	return ext::smart_ptr < FormalRTEElement < ext::variant < ext::second_type < Types, object::Object > ... > > > ( std::move ( res ).clone ( ) );
}

} /* namespace rte */

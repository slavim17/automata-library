#pragma once

#include <rte/formal/FormalRTEElements.h>
#include <rte/formal/FormalRTEStructure.h>

#include <alphabet/common/SymbolDenormalize.h>

namespace rte {

/**
 * Determines whether regular expression (or its subtree) describes an empty language (rte == \0)
 *
 */
class RTEDenormalize {
public:
	/**
	 * Determines whether regular expression is describes an empty language (rte == \0)
	 *
	 * \tparam SymbolType the type of symbol in the tested regular expression
	 *
	 * \param rte the rte to test
	 *
	 * \return true of the language described by the regular expression is empty
	 */
	template < class SymbolType >
	static ext::smart_ptr < FormalRTEElement < SymbolType > > denormalize ( rte::FormalRTEElement < DefaultSymbolType > && rte);

	template < class SymbolType >
	class Formal {
	public:
		static ext::smart_ptr < FormalRTEElement < SymbolType > > visit ( rte::FormalRTEAlternation < DefaultSymbolType > && alternation);
		static ext::smart_ptr < FormalRTEElement < SymbolType > > visit ( rte::FormalRTESubstitution < DefaultSymbolType > && substitution);
		static ext::smart_ptr < FormalRTEElement < SymbolType > > visit ( rte::FormalRTEIteration < DefaultSymbolType > && iteration);
		static ext::smart_ptr < FormalRTEElement < SymbolType > > visit ( rte::FormalRTESymbolAlphabet < DefaultSymbolType > && symbol);
		static ext::smart_ptr < FormalRTEElement < SymbolType > > visit ( rte::FormalRTEEmpty < DefaultSymbolType > && empty);
		static ext::smart_ptr < FormalRTEElement < SymbolType > > visit ( rte::FormalRTESymbolSubst < DefaultSymbolType > && symbol);
	};

};

// ----------------------------------------------------------------------------

template < class SymbolType >
ext::smart_ptr < FormalRTEElement < SymbolType > > RTEDenormalize::denormalize ( rte::FormalRTEElement < DefaultSymbolType > && rte) {
	return std::move ( rte ).template accept < ext::smart_ptr < FormalRTEElement < SymbolType > >, RTEDenormalize::Formal < SymbolType > > ( );
}

// ----------------------------------------------------------------------------

template < class SymbolType >
ext::smart_ptr < FormalRTEElement < SymbolType > > RTEDenormalize::Formal< SymbolType >::visit ( rte::FormalRTEAlternation < DefaultSymbolType > && alternation ) {
	return ext::smart_ptr < FormalRTEElement < SymbolType > > (
			new FormalRTEAlternation < SymbolType > (
				std::move ( * std::move ( std::move ( alternation ).getLeftElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < SymbolType > >, RTEDenormalize::Formal < SymbolType > > ( ) ),
				std::move ( * std::move ( std::move ( alternation ).getRightElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < SymbolType > >, RTEDenormalize::Formal < SymbolType > > ( ) )
			)
		);
}

template < class SymbolType >
ext::smart_ptr < FormalRTEElement < SymbolType > > RTEDenormalize::Formal< SymbolType >::visit ( rte::FormalRTESubstitution < DefaultSymbolType > && substitution ) {
	FormalRTESymbolSubst < DefaultSymbolType > subst ( alphabet::SymbolDenormalize::denormalizeRankedSymbol < SymbolType > ( std::move ( substitution ).getSubstitutionSymbol ( ).getSymbol ( ) ) );
	return ext::smart_ptr < FormalRTEElement < SymbolType > > (
			new FormalRTESubstitution < SymbolType > (
				std::move ( * std::move ( std::move ( substitution ).getLeftElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < SymbolType > >, RTEDenormalize::Formal < SymbolType > > ( ) ),
				std::move ( * std::move ( std::move ( substitution ).getRightElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < SymbolType > >, RTEDenormalize::Formal < SymbolType > > ( ) ),
				std::move ( subst )
			)
		);
}

template < class SymbolType >
ext::smart_ptr < FormalRTEElement < SymbolType > > RTEDenormalize::Formal< SymbolType >::visit ( rte::FormalRTEIteration < DefaultSymbolType > && iteration ) {
	FormalRTESymbolSubst < SymbolType > subst ( alphabet::SymbolDenormalize::denormalizeRankedSymbol < SymbolType > ( std::move ( iteration ).getSubstitutionSymbol ( ).getSymbol ( ) ) );
	return ext::smart_ptr < FormalRTEElement < SymbolType > > (
			new FormalRTEIteration < SymbolType > (
				std::move ( * std::move ( std::move ( iteration ).getElement ( ) ).template accept < ext::smart_ptr < FormalRTEElement < SymbolType > >, RTEDenormalize::Formal < SymbolType > > ( ) ),
				std::move ( subst )
			)
		);
}

template < class SymbolType >
ext::smart_ptr < FormalRTEElement < SymbolType > > RTEDenormalize::Formal< SymbolType >::visit ( rte::FormalRTESymbolAlphabet < DefaultSymbolType > && symbol ) {
	ext::ptr_vector < FormalRTEElement < SymbolType > > children;
	for ( FormalRTEElement < DefaultSymbolType > && element : ext::make_mover ( std::move ( symbol ).getChildren ( ) ) )
		children.push_back ( std::move ( * std::move ( element ).template accept < ext::smart_ptr < FormalRTEElement < SymbolType > >, RTEDenormalize::Formal < SymbolType > > ( ) ) );

	return ext::smart_ptr < FormalRTEElement < SymbolType > > (
			new FormalRTESymbolAlphabet < SymbolType > (
				alphabet::SymbolDenormalize::denormalizeRankedSymbol < SymbolType > ( std::move ( symbol ).getSymbol ( ) ),
				std::move ( children )
			)
		);
}

template < class SymbolType >
ext::smart_ptr < FormalRTEElement < SymbolType > > RTEDenormalize::Formal< SymbolType >::visit ( rte::FormalRTEEmpty < DefaultSymbolType > && ) {
	return ext::smart_ptr < FormalRTEElement < SymbolType > > ( new FormalRTEEmpty < SymbolType > ( ) );
}

template < class SymbolType >
ext::smart_ptr < FormalRTEElement < SymbolType > > RTEDenormalize::Formal< SymbolType >::visit ( rte::FormalRTESymbolSubst < DefaultSymbolType > && symbol ) {
	FormalRTESymbolSubst < SymbolType > res ( alphabet::SymbolDenormalize::denormalizeRankedSymbol < SymbolType > ( std::move ( symbol ).getSymbol ( ) ) );
	return ext::smart_ptr < FormalRTEElement < SymbolType > > ( std::move ( res ).clone ( ) );
}

} /* namespace rte */

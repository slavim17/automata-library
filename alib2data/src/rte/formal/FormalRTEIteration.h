/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "FormalRTEElement.h"
#include "FormalRTESymbolSubst.h"

#include <exception/CommonException.h>

#include <ext/utility>

namespace rte {

/**
 * \brief Represents the iteration operator in the regular tree expression. The node has exactly one child.
 *
 * The structure is derived from UnaryNode that provides the child node and its accessors.
 *
 * The node can be visited by the FormalRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class FormalRTEIteration : public ext::UnaryNode < FormalRTEElement < SymbolType > > {
	/**
	 * The substitution symbol of the node. The symbol will be substitued in left tree by right
	 */
	FormalRTESymbolSubst < SymbolType > m_substitutionSymbol;

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) const &
	 */
	void accept ( typename FormalRTEElement < SymbolType >::ConstVisitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &
	 */
	void accept ( typename FormalRTEElement < SymbolType >::Visitor & visitor ) & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &&
	 */
	void accept ( typename FormalRTEElement < SymbolType >::RValueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the substitution node with explicit tree to iterate and a symbol representing place of substitution
	 *
	 * \param element the tree to iterate
	 * \param substitutionSymbol the symbol representing the substitution place
	 */
	explicit FormalRTEIteration ( FormalRTEElement < SymbolType > && element, FormalRTESymbolSubst < SymbolType > substitutionSymbol );

	/**
	 * \brief Creates a new instance of the substitution node with explicit tree to iterate and a symbol representing place of substitution
	 *
	 * \param element the tree to iterate
	 * \param substitutionSymbol the symbol representing the substitution place
	 */
	explicit FormalRTEIteration ( const FormalRTEElement < SymbolType > & element, FormalRTESymbolSubst < SymbolType > substitutionSymbol );

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) const &
	 */
	FormalRTEIteration < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) &&
	 */
	FormalRTEIteration < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & ) const
	 */
	bool testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > &, ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	void computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > &, const ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	bool checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * Getter of the iterated tree
	 *
	 * \return the iterated tree
	 */
	const FormalRTEElement < SymbolType > & getElement ( ) const;

	/**
	 * Getter of the substitution symbol
	 *
	 * \return the substitution symbol
	 */
	const FormalRTESymbolSubst < SymbolType > & getSubstitutionSymbol ( ) const &;

	/**
	 * Getter of the iterated tree
	 *
	 * \return the iterated tree
	 */
	FormalRTEElement < SymbolType > & getElement ( );

	/**
	 * Getter of the substitution symbol
	 *
	 * \return the substitution symbol
	 */
	FormalRTESymbolSubst < SymbolType > && getSubstitutionSymbol ( ) &&;

	/**
	 * Setter of the iterated tree
	 *
	 * \param element the iterated tree
	 */
	void setElement ( const FormalRTEElement < SymbolType > & element );

	/**
	 * Setter of the iterated tree
	 *
	 * \param element the iterated tree
	 */
	void setElement ( FormalRTEElement < SymbolType > && element );

	/**
	 * Setter of the substitution symbol
	 *
	 * \param element the substitution symbol
	 */
	void setSubstitutionSymbol ( FormalRTESymbolSubst < SymbolType > symbol );

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator <=> ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> static_cast < decltype ( ( * this ) ) > ( other );

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FormalRTEIteration < SymbolType > & ) const;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator == ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	bool operator == ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == static_cast < decltype ( ( * this ) ) > ( other );

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FormalRTEIteration < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRTEElement < SymbolType > >::operator >> ( ext::ostream & ) const
	 */
	void operator >>( ext::ostream & out ) const override;
};

template < class SymbolType >
FormalRTEIteration < SymbolType >::FormalRTEIteration ( FormalRTEElement < SymbolType > && element, FormalRTESymbolSubst < SymbolType > substitutionSymbol ) : ext::UnaryNode < FormalRTEElement < SymbolType > > ( std::move ( element ) ), m_substitutionSymbol ( std::move ( substitutionSymbol ) ) {
}

template < class SymbolType >
FormalRTEIteration < SymbolType >::FormalRTEIteration ( const FormalRTEElement < SymbolType > & element, FormalRTESymbolSubst < SymbolType > substitutionSymbol ) : FormalRTEIteration ( ext::move_copy ( element ), std::move ( substitutionSymbol ) ) {
}

template < class SymbolType >
const FormalRTEElement < SymbolType > & FormalRTEIteration < SymbolType >::getElement ( ) const {
	return this->getChild ( );
}

template < class SymbolType >
FormalRTEElement < SymbolType > & FormalRTEIteration < SymbolType >::getElement ( ) {
	return this->getChild ( );
}

template < class SymbolType >
const FormalRTESymbolSubst < SymbolType > & FormalRTEIteration < SymbolType >::getSubstitutionSymbol ( ) const & {
	return m_substitutionSymbol;
}

template < class SymbolType >
FormalRTESymbolSubst < SymbolType > && FormalRTEIteration < SymbolType >::getSubstitutionSymbol ( ) && {
	return std::move ( m_substitutionSymbol );
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::setElement ( const FormalRTEElement < SymbolType > & element ) {
	setElement ( ext::move_copy ( element ) );
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::setElement ( FormalRTEElement < SymbolType > && element ) {
	this->setChild ( std::move ( element ) );
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::setSubstitutionSymbol ( FormalRTESymbolSubst < SymbolType > symbol ) {
	m_substitutionSymbol = std::move ( symbol );
}

template < class SymbolType >
FormalRTEIteration < SymbolType > * FormalRTEIteration < SymbolType >::clone ( ) const & {
	return new FormalRTEIteration ( * this );
}

template < class SymbolType >
FormalRTEIteration < SymbolType > * FormalRTEIteration < SymbolType >::clone ( ) && {
	return new FormalRTEIteration ( std::move ( * this ) );
}

template < class SymbolType >
std::strong_ordering FormalRTEIteration < SymbolType >::operator <=> ( const FormalRTEIteration < SymbolType > & other ) const {
	return getElement ( ) <=> other.getElement ( );
}

template < class SymbolType >
bool FormalRTEIteration < SymbolType >::operator == ( const FormalRTEIteration < SymbolType > & other ) const {
	return getElement ( ) == other.getElement ( );
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::operator >>( ext::ostream & out ) const {
	out << "(FormalRTEIteration " << m_substitutionSymbol << " " << getElement ( ) << ")";
}

template < class SymbolType >
bool FormalRTEIteration < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const {
	return this->getElement ( ).testSymbol ( symbol );
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	alphabetK.insert ( m_substitutionSymbol.getSymbol ( ) );
	this->getElement ( ).computeMinimalAlphabet ( alphabetF, alphabetK );
}

template < class SymbolType >
bool FormalRTEIteration < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	return alphabetK.count ( getSubstitutionSymbol ( ).getSymbol ( ) ) > 0 && getElement ( ).checkAlphabet ( alphabetF, alphabetK );
}

} /* namespace rte */

extern template class rte::FormalRTEIteration < DefaultSymbolType >;


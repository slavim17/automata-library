/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "FormalRTESymbol.h"

#include <exception/CommonException.h>

namespace rte {

/**
 * \brief Represents the substitution symbol in the regular tree expression. The node can't have any children.
 *
 * The structure is derived from NullaryNode.
 *
 * The node can be visited by the FormalRTEElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class FormalRTESymbolSubst : public FormalRTESymbol < SymbolType >, public ext::NullaryNode < FormalRTEElement < SymbolType > > {
	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) const &
	 */
	void accept ( typename FormalRTEElement < SymbolType >::ConstVisitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &
	 */
	void accept ( typename FormalRTEElement < SymbolType >::Visitor & visitor ) & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &&
	 */
	void accept ( typename FormalRTEElement < SymbolType >::RValueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the symbol node using the actual symbol to represent.
	 *
	 * \param symbol the value of the represented symbol
	 */
	explicit FormalRTESymbolSubst ( common::ranked_symbol < SymbolType > symbol );

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) const &
	 */
	FormalRTESymbolSubst < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) &&
	 */
	FormalRTESymbolSubst < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & ) const
	 */
	bool testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > &, ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	void computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > &, const ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	bool checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator <=> ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> static_cast < decltype ( ( * this ) ) > ( other );

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FormalRTESymbolSubst < SymbolType > & ) const;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator == ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	bool operator == ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == static_cast < decltype ( ( * this ) ) > ( other );

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FormalRTESymbolSubst < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRTEElement < SymbolType > >::operator >> ( ext::ostream & ) const
	 */
	void operator >>( ext::ostream & out ) const override;
};

template < class SymbolType >
FormalRTESymbolSubst < SymbolType >::FormalRTESymbolSubst ( common::ranked_symbol < SymbolType > symbol ) : FormalRTESymbol < SymbolType > ( symbol ) {
}

template < class SymbolType >
FormalRTESymbolSubst < SymbolType > * FormalRTESymbolSubst < SymbolType >::clone ( ) const & {
	return new FormalRTESymbolSubst ( * this );
}

template < class SymbolType >
FormalRTESymbolSubst < SymbolType > * FormalRTESymbolSubst < SymbolType >::clone ( ) && {
	return new FormalRTESymbolSubst ( std::move ( * this ) );
}

template < class SymbolType >
std::strong_ordering FormalRTESymbolSubst < SymbolType >::operator <=> ( const FormalRTESymbolSubst < SymbolType > & other ) const {
	return this->getSymbol ( ) <=> other.getSymbol ( );
}

template < class SymbolType >
bool FormalRTESymbolSubst < SymbolType >::operator == ( const FormalRTESymbolSubst < SymbolType > & other ) const {
	return this->getSymbol ( ) == other.getSymbol ( );
}

template < class SymbolType >
void FormalRTESymbolSubst < SymbolType >::operator >>( ext::ostream & out ) const {
	out << "(FormalRTESymbolSubst " << " symbol = " << this->getSymbol ( ) << "})";
}

template < class SymbolType >
bool FormalRTESymbolSubst < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const {
	return symbol == this->getSymbol ( );
}

template < class SymbolType >
void FormalRTESymbolSubst < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & /* alphabetF */, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	alphabetK.insert ( this->getSymbol ( ) );
}

template < class SymbolType >
bool FormalRTESymbolSubst < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & /* alphabetF */, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	return alphabetK.count ( this->getSymbol ( ) ) > 0;
}

} /* namespace rte */

extern template class rte::FormalRTESymbolSubst < DefaultSymbolType >;


/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "FormalRTESymbol.h"

#include <exception/CommonException.h>
#include <ext/utility>

namespace rte {

/**
 * \brief Represents the terminal symbol in the regular tree expression. The number of children must be the same as the arity of the symbol of the node.
 *
 * The structure is derived from VararyNode, which however does not check the number of children of the node.
 *
 * The node can be visited by the FormalRTEElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class FormalRTESymbolAlphabet : public FormalRTESymbol < SymbolType >, public ext::VararyNode < FormalRTEElement < SymbolType > > {
	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) const &
	 */
	void accept ( typename FormalRTEElement < SymbolType >::ConstVisitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &
	 */
	void accept ( typename FormalRTEElement < SymbolType >::Visitor & visitor ) & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &&
	 */
	void accept ( typename FormalRTEElement < SymbolType >::RValueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the symbol node using the actual symbol to represent.
	 *
	 * \param symbol the value of the represented symbol
	 */
	explicit FormalRTESymbolAlphabet ( common::ranked_symbol < SymbolType > symbol, ext::ptr_vector < FormalRTEElement < SymbolType > > children );

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) const &
	 */
	FormalRTESymbolAlphabet < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) &&
	 */
	FormalRTESymbolAlphabet < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & ) const
	 */
	bool testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > &, ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	void computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > &, const ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	bool checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * Getter of child nodes of the rte node
	 *
	 * \return child nodes
	 */
	const ext::ptr_vector < FormalRTEElement < SymbolType > > & getElements ( ) const;

	/**
	 * Getter of child nodes of the rte node
	 *
	 * \return child nodes
	 */
	const ext::ptr_vector < FormalRTEElement < SymbolType > > & getElements ( );

	/**
	 * Getter of child node on n-th position
	 *
	 * \return child node
	 */
	const FormalRTEElement < SymbolType > & getElement ( size_t index ) const;

	/**
	 * Getter of child node on n-th position
	 *
	 * \return child node
	 */
	FormalRTEElement < SymbolType > & getElement ( size_t index );

	/**
	 * Setter of the n-th child
	 *
	 * \param index the index of child to change
	 * \param element the iterated tree
	 */
	void setElement ( size_t index, const FormalRTEElement < SymbolType > & element );

	/**
	 * Setter of the n-th child
	 *
	 * \param index the index of child to change
	 * \param element the iterated tree
	 */
	void setElement ( size_t index, FormalRTEElement < SymbolType > && element );

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator <=> ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> static_cast < decltype ( ( * this ) ) > ( other );

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FormalRTESymbolAlphabet < SymbolType > & ) const;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::operator == ( const FormalRTEElement < SymbolType > & other ) const;
	 */
	bool operator == ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == static_cast < decltype ( ( * this ) ) > ( other );

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FormalRTESymbolAlphabet < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRTEElement < SymbolType > >::operator >> ( ext::ostream & ) const
	 */
	void operator >>( ext::ostream & out ) const override;
};

template < class SymbolType >
FormalRTESymbolAlphabet < SymbolType >::FormalRTESymbolAlphabet ( common::ranked_symbol < SymbolType > symbol, ext::ptr_vector < FormalRTEElement < SymbolType > > children ) : FormalRTESymbol < SymbolType > ( symbol ), ext::VararyNode < FormalRTEElement < SymbolType > > ( std::move ( children ) ) {
	if ( this->getChildren ( ).size ( ) != this->getSymbol ( ).getRank ( ) )
		throw exception::CommonException ( "Symbol's rank and number of children differ. Rank is " + ext::to_string ( this->getSymbol ( ).getRank ( ) ) + ", number of children is " + ext::to_string ( this->getChildren ( ).size ( ) ) + "." );

}

template < class SymbolType >
FormalRTESymbolAlphabet < SymbolType > * FormalRTESymbolAlphabet < SymbolType >::clone ( ) const & {
	return new FormalRTESymbolAlphabet ( * this );
}

template < class SymbolType >
FormalRTESymbolAlphabet < SymbolType > * FormalRTESymbolAlphabet < SymbolType >::clone ( ) && {
	return new FormalRTESymbolAlphabet ( std::move ( * this ) );
}

template < class SymbolType >
const ext::ptr_vector < FormalRTEElement < SymbolType > > & FormalRTESymbolAlphabet < SymbolType >::getElements ( ) const {
	return this->getChildren ( );
}

template < class SymbolType >
const ext::ptr_vector < FormalRTEElement < SymbolType > > & FormalRTESymbolAlphabet < SymbolType >::getElements ( ) {
	return this->getChildren ( );
}

template < class SymbolType >
const FormalRTEElement < SymbolType > & FormalRTESymbolAlphabet < SymbolType >::getElement ( size_t index ) const {
	return this->getChild ( index );
}

template < class SymbolType >
FormalRTEElement < SymbolType > & FormalRTESymbolAlphabet < SymbolType >::getElement ( size_t index ) {
	return this->getChild ( index );
}

template < class SymbolType >
void FormalRTESymbolAlphabet < SymbolType >::setElement ( size_t index, const FormalRTEElement < SymbolType > & element ) {
	setElement ( index, ext::move_copy ( element ) );
}

template < class SymbolType >
void FormalRTESymbolAlphabet < SymbolType >::setElement ( size_t index, FormalRTEElement < SymbolType > && element ) {
	this->setChild ( std::move ( element ), index );
}

template < class SymbolType >
std::strong_ordering FormalRTESymbolAlphabet < SymbolType >::operator <=> ( const FormalRTESymbolAlphabet < SymbolType > & other ) const {
	return std::tie ( this->getSymbol ( ), this->getChildren ( ) ) <=> std::tie ( other.getSymbol ( ), other.getChildren ( ) );
}

template < class SymbolType >
bool FormalRTESymbolAlphabet < SymbolType >::operator == ( const FormalRTESymbolAlphabet < SymbolType > & other ) const {
	return std::tie ( this->getSymbol ( ), this->getChildren ( ) ) == std::tie ( other.getSymbol ( ), other.getChildren ( ) );
}

template < class SymbolType >
void FormalRTESymbolAlphabet < SymbolType >::operator >>( ext::ostream & out ) const {
	out << "(FormalRTESymbolAlphabet " << " symbol = " << this->getSymbol ( ) << " children = " << this->getChildren ( ) << "})";
}

template < class SymbolType >
bool FormalRTESymbolAlphabet < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const {
	return symbol == this->getSymbol ( );
}

template < class SymbolType >
void FormalRTESymbolAlphabet < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	alphabetF.insert ( this->getSymbol ( ) );

	for ( const FormalRTEElement < SymbolType > & child : this->getElements ( ) )
		child.computeMinimalAlphabet ( alphabetF, alphabetK );
}

template < class SymbolType >
bool FormalRTESymbolAlphabet < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	return alphabetF.count ( this->getSymbol ( ) ) > 0 && std::all_of ( getElements ( ).begin ( ), getElements ( ).end ( ), [&] ( const FormalRTEElement < SymbolType > & e ) {
			return e.checkAlphabet ( alphabetF, alphabetK );
		} );
}

} /* namespace rte */

extern template class rte::FormalRTESymbolAlphabet < DefaultSymbolType >;


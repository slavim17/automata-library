#include "CompressedBitParallelTreeIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::arbology::CompressedBitParallelTreeIndex < > > ( );

} /* namespace */

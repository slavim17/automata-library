/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/iostream>

#include <alib/map>
#include <alib/set>
#include <alib/string>
#include <alib/bitset>

#include <common/DefaultSymbolType.h>

#include <core/modules.hpp>
#include <core/type_details_base.hpp>
#include <exception/CommonException.h>

// #include <alphabet/common/SymbolNormalize.h>

#include <string/LinearString.h>

namespace indexes {

namespace stringology {

/**
 * \brief Bit set string index. Stores a bit set for each symbol of the alphabet. The bit set of symbol a contains true on index i if symbol a is on i-th position in the indexed string. The class does not check whether the bit sets actually represent valid index.
 *
 * \tparam SymbolType type of symbols of indexed string
 */
template < class SymbolType = DefaultSymbolType, size_t BitmaskBitCount = 64 >
class BitSetIndex final : public core::Components < BitSetIndex < SymbolType >, ext::set < SymbolType >, module::Set, component::GeneralAlphabet > {
	/**
	 * Representation of bit sets for each symbol of the alphabet.
	 */
	ext::map < SymbolType, ext::bitset < BitmaskBitCount > > m_vectors;

	/**
	 * The original indexed string.
	 */
	string::LinearString < SymbolType > m_string;

public:
	/**
	 * Creates a new instance of the index with concrete bit sets and original indexed string.
	 *
	 * \param vectors the bit sets
	 * \param string the original indexed string
	 */
	explicit BitSetIndex ( ext::map < SymbolType, ext::bitset < BitmaskBitCount > > vectors, string::LinearString < SymbolType > string );

	/**
	 * Getter of the bit sets.
	 *
	 * @return bit sets
	 */
	const ext::map < SymbolType, ext::bitset < BitmaskBitCount > > & getData ( ) const &;

	/**
	 * Getter of the bit sets.
	 *
	 * @return bit sets
	 */
	ext::map < SymbolType, ext::bitset < BitmaskBitCount > > && getData ( ) &&;

	/**
	 * Getter of the original indexed string.
	 *
	 * @return the original indexed string
	 */
	const string::LinearString < SymbolType > & getString ( ) const &;

	/**
	 * Getter of the original indexed string.
	 *
	 * @return the original indexed string
	 */
	string::LinearString < SymbolType > && getString ( ) &&;

	/**
	 * Getter of the alphabet of the indexed string.
	 *
	 * \returns the alphabet of the indexed string
	 */
	const ext::set < SymbolType > & getAlphabet ( ) const & {
		return m_string.getAlphabet ( );
	}

	/**
	 * Getter of the alphabet of the indexed string.
	 *
	 * \returns the alphabet of the indexed string
	 */
	ext::set < SymbolType > && getAlphabet ( ) && {
		return m_string.getAlphabet ( );
	}

	/**
	 * Changes the bit vector for concrete symbol.
	 *
	 * \param symbol the changed symbol
	 * \param data the new bit vector
	 */
	void setBitVectorForSymbol ( SymbolType symbol, ext::bitset < BitmaskBitCount > data );

	/**
	 * Remover of a symbol from the alphabet of the indexed string. The symbol can be removed if it is not used in any of bit vector keys.
	 *
	 * \param symbol a symbol to remove.
	 */
	bool removeSymbolFromAlphabet ( const SymbolType & symbol ) {
		return m_string.removeSymbol ( symbol );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const BitSetIndex & other ) const {
		return std::tie ( getData ( ), getAlphabet ( ) ) <=> std::tie ( other.getData ( ), other.getAlphabet ( ) );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const BitSetIndex & other ) const {
		return std::tie ( getData ( ), getAlphabet ( ) ) == std::tie ( other.getData ( ), other.getAlphabet ( ) );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const BitSetIndex & instance ) {
		return out << "(BitSetIndex " << instance.m_vectors << ")";
	}
};

} /* namespace stringology */

} /* namespace indexes */

namespace indexes {

namespace stringology {

template < class SymbolType, size_t BitmaskBitCount >
BitSetIndex < SymbolType, BitmaskBitCount >::BitSetIndex ( ext::map < SymbolType, ext::bitset < BitmaskBitCount > > vectors, string::LinearString < SymbolType > string ) : m_vectors ( std::move ( vectors ) ), m_string ( std::move ( string ) ) {
}

template < class SymbolType, size_t BitmaskBitCount >
const ext::map < SymbolType, ext::bitset < BitmaskBitCount > > & BitSetIndex < SymbolType, BitmaskBitCount >::getData ( ) const & {
	return m_vectors;
}

template < class SymbolType, size_t BitmaskBitCount >
ext::map < SymbolType, ext::bitset < BitmaskBitCount > > && BitSetIndex < SymbolType, BitmaskBitCount >::getData ( ) && {
	return std::move ( m_vectors );
}

template < class SymbolType, size_t BitmaskBitCount >
const string::LinearString < SymbolType > & BitSetIndex < SymbolType, BitmaskBitCount >::getString ( ) const & {
	return m_string;
}

template < class SymbolType, size_t BitmaskBitCount >
string::LinearString < SymbolType > && BitSetIndex < SymbolType, BitmaskBitCount >::getString ( ) && {
	return std::move ( m_string );
}

template < class SymbolType, size_t BitmaskBitCount >
void BitSetIndex < SymbolType, BitmaskBitCount >::setBitVectorForSymbol ( SymbolType symbol, ext::bitset < BitmaskBitCount > data ) {
	this->m_vectors [ symbol ] = std::move ( data );
}

} /* namespace stringology */

} /* namespace indexes */

namespace core {

/*template < class SymbolType, size_t BitmaskBitCount >
struct normalize < indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > > {
	static indexes::stringology::BitSetIndex < DefaultSymbolType, BitmaskBitCount > eval ( indexes::stringology::BitSetIndex < SymbolType, BitmaskBitCount > && value ) {
		ext::map < DefaultSymbolType, ext::bitset < BitmaskBitCount > > vectors;
		for ( std::pair < SymbolType, ext::bitset < BitmaskBitCount > > && vector : ext::make_mover ( std::move ( value ).getData ( ) ) )
			vectors.insert ( std::make_pair ( alphabet::SymbolNormalize::normalizeSymbol ( std::move ( vector.first ) ), std::move ( vector.second ) ) );

		string::LinearString < DefaultSymbolType > string = normalize < string::LinearString < SymbolType > >::eval ( std::move ( value ).getString ( ) );

		return indexes::stringology::BitSetIndex < DefaultSymbolType, BitmaskBitCount > ( std::move ( vectors ), std::move ( string ) );
	}
};*/

template < class SymbolType >
struct type_details_retriever < indexes::stringology::BitSetIndex < SymbolType > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < SymbolType >::get ( ) );
		return std::make_unique < type_details_template > ( "indexes::stringology::BitSetIndex", std::move ( sub_types_vec ) );
	}
};

} /* namespace core */


/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/iostream>

#include <alib/set>
#include <alib/string>

#include <common/DefaultSymbolType.h>

#include <core/modules.hpp>
#include <exception/CommonException.h>

#include <common/SparseBoolVector.hpp>

#include <core/type_details_base.hpp>
//#include <alphabet/common/SymbolNormalize.h>

namespace indexes {

namespace stringology {

/**
 * \brief Compressed bit parallel string index. Stores a bit vector for each symbol of the alphabet. The bit vector of symbol a contains true on index i if symbol a is on i-th position in the indexed string. The class does not check whether the bit vectors actually represent valid index. The bit vectors are compressed with run length encoding packing runs of false values.
 *
 * \tparam SymbolType type of symbols of indexed string
 */
template < class SymbolType = DefaultSymbolType >
class CompressedBitParallelIndex final : public core::Components < CompressedBitParallelIndex < SymbolType >, ext::set < SymbolType >, module::Set, component::GeneralAlphabet > {
	/**
	 * Representation of compressed bit vectors for each symbol of the alphabet.
	 */
	ext::map < SymbolType, common::SparseBoolVector > m_vectors;

public:
	/**
	 * Creates a new instance of the index with concrete alphabet and bit vectors.
	 *
	 * \param alphabet the alphabet of indexed string
	 * \param vectors the compressed bit vectors
	 */
	explicit CompressedBitParallelIndex ( ext::set < SymbolType > alphabet, ext::map < SymbolType, common::SparseBoolVector > vectors );

	/**
	 * Getter of the compressed bit vectors.
	 *
	 * @return compressed bit vectors
	 */
	const ext::map < SymbolType, common::SparseBoolVector > & getData ( ) const &;

	/**
	 * Getter of the compressed bit vectors.
	 *
	 * @return compressed bit vectors
	 */
	ext::map < SymbolType, common::SparseBoolVector > && getData ( ) &&;

	/**
	 * Reconstructs the indexed string from bit vectors.
	 *
	 * @return the original indexed string
	 */
	ext::vector < SymbolType > getString ( ) const;

	/**
	 * Getter of the alphabet of the indexed string.
	 *
	 * \returns the alphabet of the indexed string
	 */
	const ext::set < SymbolType > & getAlphabet ( ) const & {
		return this->template accessComponent < component::GeneralAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the alphabet of the indexed string.
	 *
	 * \returns the alphabet of the indexed string
	 */
	ext::set < SymbolType > && getAlphabet ( ) && {
		return std::move ( this->template accessComponent < component::GeneralAlphabet > ( ).get ( ) );
	}

	/**
	 * Changes the bit vector for concrete symbol.
	 *
	 * \param symbol the changed symbol
	 * \param data the new bit vector
	 */
	void setCompressedBitVectorForSymbol ( SymbolType symbol, common::SparseBoolVector data );

	/**
	 * Remover of a symbol from the alphabet. The symbol can be removed if it is not used in any of bit vector keys.
	 *
	 * \param symbol a symbol to remove.
	 */
	bool removeSymbolFromAlphabet ( const SymbolType & symbol ) {
		return this->template accessComponent < component::GeneralAlphabet > ( ).remove ( symbol );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const CompressedBitParallelIndex & other ) const {
		return std::tie ( getData ( ), getAlphabet ( ) ) <=> std::tie ( other.getData ( ), other.getAlphabet ( ) );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	auto operator == ( const CompressedBitParallelIndex & other ) const {
		return std::tie ( getData ( ), getAlphabet ( ) ) == std::tie ( other.getData ( ), other.getAlphabet ( ) );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const CompressedBitParallelIndex & instance ) {
		return out << "(CompressedBitParallelIndex " << instance.m_vectors << ")";
	}
};

} /* namespace stringology */

} /* namespace indexes */

namespace indexes {

namespace stringology {

template < class SymbolType >
CompressedBitParallelIndex < SymbolType >::CompressedBitParallelIndex ( ext::set < SymbolType > alphabet, ext::map < SymbolType, common::SparseBoolVector > vectors ) : core::Components < CompressedBitParallelIndex, ext::set < SymbolType >, module::Set, component::GeneralAlphabet > ( std::move ( alphabet ) ), m_vectors ( std::move ( vectors ) ) {
}

template < class SymbolType >
const ext::map < SymbolType, common::SparseBoolVector > & CompressedBitParallelIndex < SymbolType >::getData ( ) const & {
	return m_vectors;
}

template < class SymbolType >
ext::map < SymbolType, common::SparseBoolVector > && CompressedBitParallelIndex < SymbolType >::getData ( ) && {
	return std::move ( m_vectors );
}

template < class SymbolType >
ext::vector < SymbolType > CompressedBitParallelIndex < SymbolType >::getString ( ) const {
	ext::vector < SymbolType > res;

	unsigned index = 0;
	do {
		for ( const std::pair < const SymbolType, common::SparseBoolVector > & compressedBitVector : m_vectors )
			if ( compressedBitVector.second.size ( ) > index && compressedBitVector.second [ index ] ) {
				res.push_back ( compressedBitVector.first );
				continue;
			}

	} while ( res.size ( ) == index ++ + 1 );

	return res;
}

template < class SymbolType >
void CompressedBitParallelIndex < SymbolType >::setCompressedBitVectorForSymbol ( SymbolType symbol, common::SparseBoolVector data ) {
	this->m_vectors [ symbol ] = std::move ( data );
}

} /* namespace stringology */

} /* namespace indexes */

namespace core {

/**
 * Helper class specifying constraints for the internal alphabet component of the index.
 *
 * \tparam SymbolType type of symbols of indexed string
 */
template < class SymbolType >
class SetConstraint < indexes::stringology::CompressedBitParallelIndex < SymbolType >, SymbolType, component::GeneralAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used as key in mapping symbol to bit vector.
	 *
	 * \param index the tested index
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const indexes::stringology::CompressedBitParallelIndex < SymbolType > & index, const SymbolType & symbol ) {
		const ext::map < SymbolType, common::SparseBoolVector > & content = index.getData ( );
		return content.find( symbol ) != content.end();
	}

	/**
	 * Returns true as all symbols are possibly available to be elements of the alphabet.
	 *
	 * \param index the tested index
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const indexes::stringology::CompressedBitParallelIndex < SymbolType > &, const SymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as symbols of the alphabet.
	 *
	 * \param index the tested index
	 * \param symbol the tested symbol
	 */
	static void valid ( const indexes::stringology::CompressedBitParallelIndex < SymbolType > &, const SymbolType & ) {
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the index with default template parameters or unmodified instance if the template parameters were already the default ones
 */
/*template < class SymbolType >
struct normalize < indexes::stringology::CompressedBitParallelIndex < SymbolType > > {
	static indexes::stringology::CompressedBitParallelIndex < > eval ( indexes::stringology::CompressedBitParallelIndex < SymbolType > && value ) {
		ext::set < DefaultSymbolType > alphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getAlphabet ( ) );

		ext::map < DefaultSymbolType, common::SparseBoolVector > vectors;
		for ( std::pair < SymbolType, common::SparseBoolVector > && vector : ext::make_mover ( std::move ( value ).getData ( ) ) )
			vectors.insert ( std::make_pair ( alphabet::SymbolNormalize::normalizeSymbol ( std::move ( vector.first ) ), std::move ( vector.second ) ) );

		return indexes::stringology::CompressedBitParallelIndex < > ( std::move ( alphabet ), std::move ( vectors ) );
	}
};*/

template < class SymbolType >
struct type_details_retriever < indexes::stringology::CompressedBitParallelIndex < SymbolType > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < SymbolType >::get ( ) );
		return std::make_unique < type_details_template > ( "indexes::stringology::CompressedBitParallelIndex", std::move ( sub_types_vec ) );
	}
};

} /* namespace core */


#include "SuffixTrie.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::stringology::SuffixTrie < > > ( );

} /* namespace */

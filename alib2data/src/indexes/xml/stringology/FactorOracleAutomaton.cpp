#include "FactorOracleAutomaton.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::stringology::FactorOracleAutomaton < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::stringology::FactorOracleAutomaton < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::stringology::FactorOracleAutomaton < > > ( );

} /* namespace */

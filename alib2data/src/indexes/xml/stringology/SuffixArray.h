/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <indexes/stringology/SuffixArray.h>

#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

#include <primitive/xml/Unsigned.h>
#include <container/xml/ObjectsVector.h>

#include <string/xml/LinearString.h>

namespace core {

template < class SymbolType >
struct xmlApi < indexes::stringology::SuffixArray < SymbolType > > {
	static indexes::stringology::SuffixArray < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const indexes::stringology::SuffixArray < SymbolType > & index );
};

template < class SymbolType >
indexes::stringology::SuffixArray < SymbolType > xmlApi < indexes::stringology::SuffixArray < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	ext::vector < unsigned > data = core::xmlApi < ext::vector < unsigned > >::parse ( input );
	string::LinearString < SymbolType > string = core::xmlApi < string::LinearString < SymbolType > >::parse ( input );
	indexes::stringology::SuffixArray < SymbolType > res ( std::move ( data ), std::move ( string ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return res;
}

template < class SymbolType >
bool xmlApi < indexes::stringology::SuffixArray < SymbolType > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < class SymbolType >
std::string xmlApi < indexes::stringology::SuffixArray < SymbolType > >::xmlTagName ( ) {
	return "SuffixArray";
}

template < class SymbolType >
void xmlApi < indexes::stringology::SuffixArray < SymbolType > >::compose ( ext::deque < sax::Token > & output, const indexes::stringology::SuffixArray < SymbolType > & index ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < ext::vector < unsigned > >::compose ( output, index.getData ( ) );
	core::xmlApi < string::LinearString < SymbolType > >::compose ( output, index.getString ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */


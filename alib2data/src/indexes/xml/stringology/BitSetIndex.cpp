#include "BitSetIndex.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::stringology::BitSetIndex < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::stringology::BitSetIndex < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::stringology::BitSetIndex < > > ( );

} /* namespace */

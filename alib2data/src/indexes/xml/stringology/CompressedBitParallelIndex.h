/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <indexes/stringology/CompressedBitParallelIndex.h>

#include <sax/FromXMLParserHelper.h>
#include <core/xmlApi.hpp>

#include <primitive/xml/Bool.h>
#include <container/xml/ObjectsSet.h>
#include <container/xml/ObjectsMap.h>

#include <common/xml/SparseBoolVector.hpp>

namespace core {

template < class SymbolType >
struct xmlApi < indexes::stringology::CompressedBitParallelIndex < SymbolType > > {
	static indexes::stringology::CompressedBitParallelIndex < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const indexes::stringology::CompressedBitParallelIndex < SymbolType > & index );
};

template < class SymbolType >
indexes::stringology::CompressedBitParallelIndex < SymbolType > xmlApi < indexes::stringology::CompressedBitParallelIndex < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	ext::set < SymbolType > alphabet = core::xmlApi < ext::set < SymbolType > >::parse ( input );
	ext::map < SymbolType, common::SparseBoolVector > data = core::xmlApi < ext::map < SymbolType, common::SparseBoolVector > >::parse ( input );
	indexes::stringology::CompressedBitParallelIndex < SymbolType > res ( std::move ( alphabet ), std::move ( data ) );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return res;
}

template < class SymbolType >
bool xmlApi < indexes::stringology::CompressedBitParallelIndex < SymbolType > >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

template < class SymbolType >
std::string xmlApi < indexes::stringology::CompressedBitParallelIndex < SymbolType > >::xmlTagName ( ) {
	return "CompressedBitParallelIndex";
}

template < class SymbolType >
void xmlApi < indexes::stringology::CompressedBitParallelIndex < SymbolType > >::compose ( ext::deque < sax::Token > & output, const indexes::stringology::CompressedBitParallelIndex < SymbolType > & index ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	core::xmlApi < ext::set < SymbolType > >::compose ( output, index.getAlphabet ( ) );
	core::xmlApi < ext::map < SymbolType, common::SparseBoolVector > >::compose ( output, index.getData ( ) );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */


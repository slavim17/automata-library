#include "NonlinearCompressedBitParallelTreeIndex.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::arbology::NonlinearCompressedBitParallelTreeIndex < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::arbology::NonlinearCompressedBitParallelTreeIndex < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::arbology::NonlinearCompressedBitParallelTreeIndex < > > ( );

} /* namespace */

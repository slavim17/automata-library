#include "LeftLG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::LeftLG < >;
template class abstraction::ValueHolder < grammar::LeftLG < > >;
template const grammar::LeftLG < > & abstraction::retrieveValue < const grammar::LeftLG < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const grammar::LeftLG < > & >;
template class registration::NormalizationRegisterImpl < grammar::LeftLG < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::LeftLG < > > ( );

} /* namespace */

#include "RightRG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::RightRG < >;
template class abstraction::ValueHolder < grammar::RightRG < > >;
template const grammar::RightRG < > & abstraction::retrieveValue < const grammar::RightRG < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const grammar::RightRG < > & >;
template class registration::NormalizationRegisterImpl < grammar::RightRG < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::RightRG < > > ( );

} /* namespace */

#include "NonContractingGrammar.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::NonContractingGrammar < >;
template class abstraction::ValueHolder < grammar::NonContractingGrammar < > >;
template const grammar::NonContractingGrammar < > & abstraction::retrieveValue < const grammar::NonContractingGrammar < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const grammar::NonContractingGrammar < > & >;
template class registration::NormalizationRegisterImpl < grammar::NonContractingGrammar < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::NonContractingGrammar < > > ( );

} /* namespace */

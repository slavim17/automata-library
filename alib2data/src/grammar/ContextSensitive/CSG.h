/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <ext/algorithm>

#include <alib/map>
#include <alib/set>
#include <alib/vector>

#include <core/modules.hpp>

#include <common/DefaultSymbolType.h>

#include <grammar/GrammarException.h>

#include <core/type_util.hpp>
#include <core/type_details_base.hpp>
#include <alphabet/common/SymbolNormalize.h>
#include <alphabet/common/SymbolDenormalize.h>
#include <grammar/common/GrammarNormalize.h>
#include <grammar/common/GrammarDenormalize.h>

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace grammar {

/**
 * \brief
 * Context sensitive grammar in Chomsky hierarchy or type 1 in Chomsky hierarchy. Generates context sensitive languages.

 * \details
 * Definition is similar to all common definitions of context sensitive grammars.
 * G = (N, T, P, S),
 * N (NonterminalAlphabet) = nonempty finite set of nonterminal symbols,
 * T (TerminalAlphabet) = finite set of terminal symbols - having this empty won't let grammar do much though,
 * P = set of production rules of the form \alpha A \beta -> \alpha B \beta, where A \in N, B \in ( N \cup T )+ and \alpha, \beta \in ( N \cup T )*,
 * S (InitialSymbol) = initial nonterminal symbol,
 *
 * \tparam SymbolType used for the terminal alphabet, the nonterminal alphabet, and the initial symbol of the grammar.
 */
template < class SymbolType = DefaultSymbolType >
class CSG final : public core::Components < CSG < SymbolType >, ext::set < SymbolType >, module::Set, std::tuple < component::TerminalAlphabet, component::NonterminalAlphabet >, SymbolType, module::Value, component::InitialSymbol > {
	/**
	 * Rules function as mapping from nonterminal symbol and contexts on the left hand side to a set of sequences of terminal and nonterminal symbols.
	 */
	ext::map < ext::tuple < ext::vector < SymbolType >, SymbolType, ext::vector < SymbolType > >, ext::set < ext::vector < SymbolType > > > rules;

	/**
	 * Boolean signaling whether grammar generates empty string or don't.
	 */
	bool generatesEpsilon;

public:
	/**
	 * \brief Creates a new instance of the grammar with a concrete initial symbol.
	 *
	 * \param initialSymbol the initial symbol of the grammar
	 */
	explicit CSG ( SymbolType initialSymbol );

	/**
	 * \brief Creates a new instance of the grammar with a concrete nonterminal alphabet, terminal alphabet and initial symbol.
	 *
	 * \param nonTerminalSymbols the initial nonterminal alphabet
	 * \param terminalSymbols the initial terminal alphabet
	 * \param initialSymbol the initial symbol of the grammar
	 */
	explicit CSG ( ext::set < SymbolType > nonterminalAlphabet, ext::set < SymbolType > terminalAlphabet, SymbolType initialSymbol );

	/**
	 * \brief Add a new rule of a grammar.
	 *
	 * \details The rule is in a form of \alpha A \beta -> \alpha B \beta, where A \in N, B \in ( N \cup T )+, and \alpha, \beta \in ( N \cup T )*.
	 *
	 * \param lContext the left context of the rule
	 * \param leftHandSide the left hand side of the rule
	 * \param rContext the right context of the rule
	 * \param rightHandSide the right hand side of the rule
	 *
	 * \returns true if the rule was indeed added, false othervise
	 */
	bool addRule ( ext::vector < SymbolType > lContext, SymbolType leftHandSide, ext::vector < SymbolType > rContext, ext::vector < SymbolType > rightHandSide );

	/**
	 * \brief Add new rules of a grammar.
	 *
	 * \details The rules are in form of \alpha A \beta -> \alpha B \beta | \alpha C \beta | ..., where A \in N, B, C ... \in ( N \cup T )+, and \alpha, \beta \in ( N \cup T )*.
	 *
	 * \param lContext the left context of the rule
	 * \param leftHandSide the left hand side of the rule
	 * \param rContext the right context of the rule
	 * \param rightHandSide a set of right hand sides of the rule
	 */
	void addRules ( ext::vector < SymbolType > lContext, SymbolType leftHandSide, ext::vector < SymbolType > rContext, ext::set < ext::vector < SymbolType > > rightHandSide );

	/**
	 * Get rules of the grammar.
	 *
	 * \returns rules of the grammar
	 */
	const ext::map < ext::tuple < ext::vector < SymbolType >, SymbolType, ext::vector < SymbolType > >, ext::set < ext::vector < SymbolType > > > & getRules ( ) const &;

	/**
	 * Get rules of the grammar.
	 *
	 * \returns rules of the grammar
	 */
	ext::map < ext::tuple < ext::vector < SymbolType >, SymbolType, ext::vector < SymbolType > >, ext::set < ext::vector < SymbolType > > > && getRules ( ) &&;

	/**
	 * Remove a rule of a grammar in form of \alpha A \beta -> \alpha B \beta, where A \in N, B \in (N \cup T)+, and \alpha, \beta \in ( N \cup T )*.
	 *
	 * \param lContext the left context of the rule
	 * \param leftHandSide the left hand side of the rule
	 * \param rContext the right context of the rule
	 * \param rightHandSide the right hand side of the rule
	 *
	 * \returns true if the rule was indeed removed, false othervise
	 */
	bool removeRule ( const ext::vector < SymbolType > & lContext, const SymbolType & leftHandSide, const ext::vector < SymbolType > & rContext, const ext::vector < SymbolType > & rightHandSide );

	/**
	 * Getter of initial symbol.
	 *
	 * \returns the initial symbol of the grammar
	 */
	const SymbolType & getInitialSymbol ( ) const & {
		return this->template accessComponent < component::InitialSymbol > ( ).get ( );
	}

	/**
	 * Getter of initial symbol.
	 *
	 * \returns the initial symbol of the grammar
	 */
	SymbolType && getInitialSymbol ( ) && {
		return std::move ( this->template accessComponent < component::InitialSymbol > ( ).get ( ) );
	}

	/**
	 * Setter of initial symbol.
	 *
	 * \param symbol new initial symbol of the grammar
	 *
	 * \returns true if the initial symbol was indeed changed
	 */
	bool setInitialSymbol ( SymbolType symbol ) {
		return this->template accessComponent < component::InitialSymbol > ( ).set ( std::move ( symbol ) );
	}

	/**
	 * Getter of nonterminal alphabet.
	 *
	 * \returns the nonterminal alphabet of the grammar
	 */
	const ext::set < SymbolType > & getNonterminalAlphabet ( ) const & {
		return this->template accessComponent < component::NonterminalAlphabet > ( ).get ( );
	}

	/**
	 * Getter of nonterminal alphabet.
	 *
	 * \returns the nonterminal alphabet of the grammar
	 */
	ext::set < SymbolType > && getNonterminalAlphabet ( ) && {
		return std::move ( this->template accessComponent < component::NonterminalAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of nonterminal symbol.
	 *
	 * \param symbol the new symbol to be added to nonterminal alphabet
	 *
	 * \returns true if the symbol was indeed added
	 */
	bool addNonterminalSymbol ( SymbolType symbol ) {
		return this->template accessComponent < component::NonterminalAlphabet > ( ).add ( std::move ( symbol ) );
	}

	/**
	 * Setter of nonterminal alphabet.
	 *
	 * \param symbols completely new nonterminal alphabet
	 */
	void setNonterminalAlphabet ( ext::set < SymbolType > symbols ) {
		this->template accessComponent < component::NonterminalAlphabet > ( ).set ( std::move ( symbols ) );
	}

	/**
	 * Getter of terminal alphabet.
	 *
	 * \returns the terminal alphabet of the grammar
	 */
	const ext::set < SymbolType > & getTerminalAlphabet ( ) const & {
		return this->template accessComponent < component::TerminalAlphabet > ( ).get ( );
	}

	/**
	 * Getter of terminal alphabet.
	 *
	 * \returns the terminal alphabet of the grammar
	 */
	ext::set < SymbolType > && getTerminalAlphabet ( ) && {
		return std::move ( this->template accessComponent < component::TerminalAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of terminal symbol.
	 *
	 * \param symbol the new symbol tuo be added to nonterminal alphabet
	 *
	 * \returns true if the symbol was indeed added
	 */
	bool addTerminalSymbol ( SymbolType symbol ) {
		return this->template accessComponent < component::TerminalAlphabet > ( ).add ( std::move ( symbol ) );
	}

	/**
	 * Setter of terminal alphabet.
	 *
	 * \param symbol completely new nontemrinal alphabet
	 */
	void setTerminalAlphabet ( ext::set < SymbolType > symbols ) {
		this->template accessComponent < component::TerminalAlphabet > ( ).set ( std::move ( symbols ) );
	}

	/**
	 * Sets sign representing that grammar generates or doesn't generate empty word.
	 *
	 * \param genEps sign representing the posibility of generating empty string from the grammar
	 */
	void setGeneratesEpsilon ( bool genEps );

	/**
	 * Gets sign representing that grammar generates or doesn't generate empty word.
	 *
	 * \returns sign representing the posibility of generating empty string from the grammar
	 */
	bool getGeneratesEpsilon ( ) const;

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const CSG & other ) const {
		return std::tie ( getTerminalAlphabet ( ), getNonterminalAlphabet ( ), getInitialSymbol ( ), rules ) <=> std::tie ( other.getTerminalAlphabet ( ), other.getNonterminalAlphabet ( ), other.getInitialSymbol ( ), other.rules );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const CSG & other ) const {
		return std::tie ( getTerminalAlphabet ( ), getNonterminalAlphabet ( ), getInitialSymbol ( ), rules ) == std::tie ( other.getTerminalAlphabet ( ), other.getNonterminalAlphabet ( ), other.getInitialSymbol ( ), other.rules );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const CSG & instance ) {
		return out << "(CSG"
			   << " nonterminalAlphabet = " << instance.getNonterminalAlphabet ( )
			   << " terminalAlphabet = " << instance.getTerminalAlphabet ( )
			   << " initialSymbol = " << instance.getInitialSymbol ( )
			   << " rules = " << instance.getRules ( )
			   << " generatesEpsilon = " << instance.getGeneratesEpsilon ( )
			   << ")";
	}
};

template < class SymbolType >
CSG < SymbolType >::CSG ( SymbolType initialSymbol ) : CSG ( ext::set < SymbolType > { initialSymbol }, ext::set < SymbolType > ( ), initialSymbol ) {
}

template < class SymbolType >
CSG < SymbolType >::CSG ( ext::set < SymbolType > nonterminalAlphabet, ext::set < SymbolType > terminalAlphabet, SymbolType initialSymbol ) : core::Components < CSG, ext::set < SymbolType >, module::Set, std::tuple < component::TerminalAlphabet, component::NonterminalAlphabet >, SymbolType, module::Value, component::InitialSymbol > ( std::move ( terminalAlphabet), std::move ( nonterminalAlphabet ), std::move ( initialSymbol ) ), generatesEpsilon ( false ) {
}

template < class SymbolType >
bool CSG < SymbolType >::addRule ( ext::vector < SymbolType > lContext, SymbolType leftHandSide, ext::vector < SymbolType > rContext, ext::vector < SymbolType > rightHandSide ) {
	for ( const SymbolType & symbol : lContext )
		if ( !getTerminalAlphabet ( ).count ( symbol ) && !getNonterminalAlphabet ( ).count ( symbol ) )
			throw GrammarException ( "Symbol \"" + ext::to_string ( symbol ) + "\" is not neither terminal nor nonterminal symbol" );

	if ( !getNonterminalAlphabet ( ).count ( leftHandSide ) )
		throw GrammarException ( "Rule must rewrite nonterminal symbol" );

	for ( const SymbolType & symbol : rContext )
		if ( !getTerminalAlphabet ( ).count ( symbol ) && !getNonterminalAlphabet ( ).count ( symbol ) )
			throw GrammarException ( "Symbol \"" + ext::to_string ( symbol ) + "\" is not neither terminal nor nonterminal symbol" );

	if ( rightHandSide.empty ( ) ) {
		throw GrammarException ( "Epsilon rule is not allowed" );
	} else {
		for ( const SymbolType & symbol : rightHandSide )
			if ( !getTerminalAlphabet ( ).count ( symbol ) && !getNonterminalAlphabet ( ).count ( symbol ) )
				throw GrammarException ( "Symbol \"" + ext::to_string ( symbol ) + "\" is not neither terminal nor nonterminal symbol" );
	}

	return rules [ ext::make_tuple ( std::move ( lContext ), std::move ( leftHandSide ), std::move ( rContext ) ) ].insert ( std::move ( rightHandSide ) ).second;
}

template < class SymbolType >
void CSG < SymbolType >::addRules ( ext::vector < SymbolType > lContext, SymbolType leftHandSide, ext::vector < SymbolType > rContext, ext::set < ext::vector < SymbolType > > rightHandSide ) {
	for ( const SymbolType & symbol : lContext )
		if ( !getTerminalAlphabet ( ).count ( symbol ) && !getNonterminalAlphabet ( ).count ( symbol ) )
			throw GrammarException ( "Symbol \"" + ext::to_string ( symbol ) + "\" is not neither terminal nor nonterminal symbol" );

	if ( !getNonterminalAlphabet ( ).count ( leftHandSide ) )
		throw GrammarException ( "Rule must rewrite nonterminal symbol" );

	for ( const SymbolType & symbol : rContext )
		if ( !getTerminalAlphabet ( ).count ( symbol ) && !getNonterminalAlphabet ( ).count ( symbol ) )
			throw GrammarException ( "Symbol \"" + ext::to_string ( symbol ) + "\" is not neither terminal nor nonterminal symbol" );

	for ( const ext::vector < SymbolType > & rhs : rightHandSide ) {
		if ( rhs.empty ( ) ) {
			throw GrammarException ( "Epsilon rule is not allowed" );
		} else {
			for ( const SymbolType & symbol : rhs )
				if ( !getTerminalAlphabet ( ).count ( symbol ) && !getNonterminalAlphabet ( ).count ( symbol ) )
					throw GrammarException ( "Symbol \"" + ext::to_string ( symbol ) + "\" is not neither terminal nor nonterminal symbol" );
		}
	}

	rules [ ext::make_tuple ( std::move ( lContext ), std::move ( leftHandSide ), std::move ( rContext ) ) ].insert ( ext::make_mover ( rightHandSide ).begin ( ), ext::make_mover ( rightHandSide ).end ( ) );
}

template < class SymbolType >
const ext::map < ext::tuple < ext::vector < SymbolType >, SymbolType, ext::vector < SymbolType > >, ext::set < ext::vector < SymbolType > > > & CSG < SymbolType >::getRules ( ) const & {
	return rules;
}

template < class SymbolType >
ext::map < ext::tuple < ext::vector < SymbolType >, SymbolType, ext::vector < SymbolType > >, ext::set < ext::vector < SymbolType > > > && CSG < SymbolType >::getRules ( ) && {
	return std::move ( rules );
}

template < class SymbolType >
bool CSG < SymbolType >::removeRule ( const ext::vector < SymbolType > & lContext, const SymbolType & leftHandSide, const ext::vector < SymbolType > & rContext, const ext::vector < SymbolType > & rightHandSide ) {
	return rules [ ext::make_tuple ( lContext, leftHandSide, rContext ) ].erase ( rightHandSide );
}

template < class SymbolType >
void CSG < SymbolType >::setGeneratesEpsilon ( bool genEps ) {
	generatesEpsilon = genEps;
}

template < class SymbolType >
bool CSG < SymbolType >::getGeneratesEpsilon ( ) const {
	return generatesEpsilon;
}

} /* namespace grammar */

namespace core {

/**
 * Helper class specifying constraints for the grammar's internal terminal alphabet component.
 *
 * \tparam SymbolType used for the terminal alphabet of the grammar.
 */
template < class SymbolType >
class SetConstraint< grammar::CSG < SymbolType >, SymbolType, component::TerminalAlphabet > {
public:
	/**
	 * Returns true if the terminal symbol is still used in some rule of the grammar.
	 *
	 * \param grammar the tested grammar
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const grammar::CSG < SymbolType > & grammar, const SymbolType & symbol ) {
		for ( const std::pair < const ext::tuple < ext::vector < SymbolType >, SymbolType, ext::vector < SymbolType > >, ext::set < ext::vector < SymbolType > > > & rule : grammar.getRules ( ) ) {
			for ( const SymbolType & lCont : std::get < 0 > ( rule.first ) )
				if ( lCont == symbol )
					return false;

			for ( const SymbolType & rCont : std::get < 2 > ( rule.first ) )
				if ( rCont == symbol )
					return false;

			for ( const ext::vector < SymbolType > & rhs : rule.second )
				if ( std::find ( rhs.begin ( ), rhs.end ( ), symbol ) != rhs.end ( ) )
					return false;

		}

		return false;
	}

	/**
	 * Returns true as all terminal symbols are possibly available to be terminal symbols.
	 *
	 * \param grammar the tested grammar
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const grammar::CSG < SymbolType > &, const SymbolType & ) {
		return true;
	}

	/**
	 * Throws runtime exception if the symbol requested to be terminal symbol is already in nonterminal alphabet.
	 *
	 * \param grammar the tested grammar
	 * \param symbol the tested symbol
	 *
	 * \throws grammar::GrammarException of the tested symbol is in nonterminal alphabet
	 */
	static void valid ( const grammar::CSG < SymbolType > & grammar, const SymbolType & symbol ) {
		if ( grammar.template accessComponent < component::NonterminalAlphabet > ( ).get ( ).count ( symbol ) )
			throw grammar::GrammarException ( "Symbol " + ext::to_string ( symbol ) + " cannot be in the terminal alphabet since it is already in the nonterminal alphabet." );
	}
};

/**
 * Helper class specifying constraints for the grammar's internal nonterminal alphabet component.
 *
 * \tparam SymbolType used for the nonterminal alphabet of the grammar.
 */
template < class SymbolType >
class SetConstraint< grammar::CSG < SymbolType >, SymbolType, component::NonterminalAlphabet > {
public:
	/**
	 * Returns true if the nonterminal symbol is still used in some rule of the grammar or if it is the initial symbol of the grammar.
	 *
	 * \param grammar the tested grammar
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const grammar::CSG < SymbolType > & grammar, const SymbolType & symbol ) {
		for ( const std::pair < const ext::tuple < ext::vector < SymbolType >, SymbolType, ext::vector < SymbolType > >, ext::set < ext::vector < SymbolType > > > & rule : grammar.getRules ( ) ) {
			for ( const SymbolType & lCont : std::get < 0 > ( rule.first ) )
				if ( lCont == symbol )
					return true;

			if ( std::get < 1 > ( rule.first ) == symbol )
				return true;

			for ( const SymbolType & rCont : std::get < 2 > ( rule.first ) )
				if ( rCont == symbol )
					return true;

			for ( const ext::vector < SymbolType > & rhs : rule.second )
				if ( std::find ( rhs.begin ( ), rhs.end ( ), symbol ) != rhs.end ( ) )
					return true;

		}

		return grammar.template accessComponent < component::InitialSymbol > ( ).get ( ) == symbol;
	}

	/**
	 * Returns true as all terminal symbols are possibly available to be nonterminal symbols.
	 *
	 * \param grammar the tested grammar
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const grammar::CSG < SymbolType > &, const SymbolType & ) {
		return true;
	}

	/**
	 * Throws runtime exception if the symbol requested to be nonterminal symbol is already in terminal alphabet.
	 *
	 * \param grammar the tested grammar
	 * \param symbol the tested symbol
	 *
	 * \throws grammar::GrammarException of the tested symbol is in nonterminal alphabet
	 */
	static void valid ( const grammar::CSG < SymbolType > & grammar, const SymbolType & symbol ) {
		if ( grammar.template accessComponent < component::TerminalAlphabet > ( ).get ( ).count ( symbol ) )
			throw grammar::GrammarException ( "Symbol " + ext::to_string ( symbol ) + " cannot be in the nonterminal alphabet since it is already in the terminal alphabet." );
	}
};

/**
 * Helper class specifying constraints for the grammar's internal initial symbol element.
 *
 * \tparam SymbolType used for the initial symbol of the grammar.
 */
template < class SymbolType >
class ElementConstraint< grammar::CSG < SymbolType >, SymbolType, component::InitialSymbol > {
public:
	/**
	 * Returns true if the symbol requested to be initial is available in nonterminal alphabet.
	 *
	 * \param grammar the tested grammar
	 * \param symbol the tested symbol
	 *
	 * \returns true if the tested symbol is in nonterminal alphabet
	 */
	static bool available ( const grammar::CSG < SymbolType > & grammar, const SymbolType & symbol ) {
		return grammar.template accessComponent < component::NonterminalAlphabet > ( ).get ( ).count ( symbol );
	}

	/**
	 * All symbols are valid as initial symbols.
	 *
	 * \param grammar the tested grammar
	 * \param symbol the tested symbol
	 */
	static void valid ( const grammar::CSG < SymbolType > &, const SymbolType & ) {
	}
};

template < class SymbolType >
struct type_util < grammar::CSG < SymbolType > > {
	static grammar::CSG < SymbolType > denormalize ( grammar::CSG < > && value ) {
		ext::set < SymbolType > nonterminals = alphabet::SymbolDenormalize::denormalizeAlphabet < SymbolType > ( std::move ( value ).getNonterminalAlphabet ( ) );
		ext::set < SymbolType > terminals = alphabet::SymbolDenormalize::denormalizeAlphabet < SymbolType > ( std::move ( value ).getTerminalAlphabet ( ) );
		SymbolType initialSymbol = alphabet::SymbolDenormalize::denormalizeSymbol < SymbolType > ( std::move ( value ).getInitialSymbol ( ) );

		grammar::CSG < SymbolType > res ( std::move ( nonterminals ), std::move ( terminals ), std::move ( initialSymbol ) );

		for ( std::pair < ext::tuple < ext::vector < DefaultSymbolType >, DefaultSymbolType, ext::vector < DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > > && rule : ext::make_mover ( std::move ( value ).getRules ( ) ) ) {
			ext::set < ext::vector < SymbolType > > rhs;
			for ( ext::vector < DefaultSymbolType > && target : ext::make_mover ( rule.second ) )
				rhs.insert ( alphabet::SymbolDenormalize::denormalizeSymbols < SymbolType > ( std::move ( target ) ) );

			ext::vector < SymbolType > lContext = alphabet::SymbolDenormalize::denormalizeSymbols < SymbolType > ( std::move ( std::get < 0 > ( rule.first ) ) );
			SymbolType lhs = alphabet::SymbolDenormalize::denormalizeSymbol < SymbolType > ( std::move ( std::get < 1 > ( rule.first ) ) );
			ext::vector < SymbolType > rContext = alphabet::SymbolDenormalize::denormalizeSymbols < SymbolType > ( std::move ( std::get < 2 > ( rule.first ) ) );

			res.addRules ( std::move ( lContext ), std::move ( lhs ), std::move ( rContext ), std::move ( rhs ) );
		}

		res.setGeneratesEpsilon ( value.getGeneratesEpsilon ( ) );

		return res;
	}

	static grammar::CSG < > normalize ( grammar::CSG < SymbolType > && value ) {
		ext::set < DefaultSymbolType > nonterminals = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getNonterminalAlphabet ( ) );
		ext::set < DefaultSymbolType > terminals = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getTerminalAlphabet ( ) );
		DefaultSymbolType initialSymbol = alphabet::SymbolNormalize::normalizeSymbol ( std::move ( value ).getInitialSymbol ( ) );

		grammar::CSG < > res ( std::move ( nonterminals ), std::move ( terminals ), std::move ( initialSymbol ) );

		for ( std::pair < ext::tuple < ext::vector < SymbolType >, SymbolType, ext::vector < SymbolType > >, ext::set < ext::vector < SymbolType > > > && rule : ext::make_mover ( std::move ( value ).getRules ( ) ) ) {
			ext::set < ext::vector < DefaultSymbolType > > rhs;
			for ( ext::vector < SymbolType > && target : ext::make_mover ( rule.second ) )
				rhs.insert ( alphabet::SymbolNormalize::normalizeSymbols ( std::move ( target ) ) );

			ext::vector < DefaultSymbolType > lContext = alphabet::SymbolNormalize::normalizeSymbols ( std::move ( std::get < 0 > ( rule.first ) ) );
			DefaultSymbolType lhs = alphabet::SymbolNormalize::normalizeSymbol ( std::move ( std::get < 1 > ( rule.first ) ) );
			ext::vector < DefaultSymbolType > rContext = alphabet::SymbolNormalize::normalizeSymbols ( std::move ( std::get < 2 > ( rule.first ) ) );

			res.addRules ( std::move ( lContext ), std::move ( lhs ), std::move ( rContext ), std::move ( rhs ) );
		}

		res.setGeneratesEpsilon ( value.getGeneratesEpsilon ( ) );

		return res;
	}

	static std::unique_ptr < type_details_base > type ( const grammar::CSG < SymbolType > & arg ) {
		core::unique_ptr_set < type_details_base > subTypesSymbol;
		for ( const SymbolType & item : arg.getTerminalAlphabet ( ) )
			subTypesSymbol.insert ( type_util < SymbolType >::type ( item ) );

		for ( const SymbolType & item : arg.getNonterminalAlphabet ( ) )
			subTypesSymbol.insert ( type_util < SymbolType >::type ( item ) );

		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_variant_type::make_variant ( std::move ( subTypesSymbol ) ) );
		return std::make_unique < type_details_template > ( "grammar::CSG", std::move ( sub_types_vec ) );
	}
};

template < class SymbolType >
struct type_details_retriever < grammar::CSG < SymbolType > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < SymbolType >::get ( ) );
		return std::make_unique < type_details_template > ( "grammar::CSG", std::move ( sub_types_vec ) );
	}
};

} /* namespace core */

extern template class grammar::CSG < >;
extern template class abstraction::ValueHolder < grammar::CSG < > >;
extern template const grammar::CSG < > & abstraction::retrieveValue < const grammar::CSG < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
extern template class registration::DenormalizationRegisterImpl < const grammar::CSG < > & >;
extern template class registration::NormalizationRegisterImpl < grammar::CSG < > >;

#include "CSG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::CSG < >;
template class abstraction::ValueHolder < grammar::CSG < > >;
template const grammar::CSG < > & abstraction::retrieveValue < const grammar::CSG < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const grammar::CSG < > & >;
template class registration::NormalizationRegisterImpl < grammar::CSG < > >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::CSG < > > ( );

} /* namespace */

#pragma once

#include <alib/vector>
#include <alib/tuple>
#include <alib/set>
#include <alib/variant>

#include <alphabet/common/SymbolDenormalize.h>

namespace grammar {

/**
 * This class contains methods to print XML representation of automata to the output stream.
 */
class GrammarDenormalize {
public:
	template < class FirstSymbolType, class SecondSymbolType >
	static ext::pair < FirstSymbolType, ext::vector < SecondSymbolType > > denormalizeRHS ( ext::pair < DefaultSymbolType, ext::vector < DefaultSymbolType > > && symbol );

	template < class FirstSymbolType, class SecondSymbolType, class ThirdSymbolType >
	static ext::variant < FirstSymbolType, ext::pair < SecondSymbolType, ThirdSymbolType > > denormalizeRHS ( ext::variant < DefaultSymbolType, ext::pair < DefaultSymbolType, DefaultSymbolType > > && symbol );

	template < class FirstSymbolType, class SecondSymbolType, class ThirdSymbolType >
	static ext::variant < ext::vector < FirstSymbolType >, ext::pair < SecondSymbolType, ext::vector < ThirdSymbolType > > > denormalizeRHS ( ext::variant < ext::vector < DefaultSymbolType >, ext::pair < DefaultSymbolType, ext::vector < DefaultSymbolType > > > && symbol );

	template < class FirstSymbolType, class SecondSymbolType, class ThirdSymbolType >
	static ext::variant < ext::vector < FirstSymbolType >, ext::pair < ext::vector < SecondSymbolType >, ThirdSymbolType > > denormalizeRHS ( ext::variant < ext::vector < DefaultSymbolType >, ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType > > && symbol );

	template < class FirstSymbolType, class SecondSymbolType, class ThirdSymbolType, class FourthSymbolType >
	static ext::variant < ext::vector < FirstSymbolType >, ext::tuple < ext::vector < SecondSymbolType >, ThirdSymbolType, ext::vector < FourthSymbolType > > > denormalizeRHS ( ext::variant < ext::vector < DefaultSymbolType >, ext::tuple < ext::vector < DefaultSymbolType >, DefaultSymbolType, ext::vector < DefaultSymbolType > > > && symbols );

};

template < class FirstSymbolType, class SecondSymbolType >
ext::pair < FirstSymbolType, ext::vector < SecondSymbolType > > GrammarDenormalize::denormalizeRHS ( ext::pair < DefaultSymbolType, ext::vector < DefaultSymbolType > > && symbol ) {
	return ext::make_pair ( alphabet::SymbolDenormalize::denormalizeSymbol < FirstSymbolType > ( std::move ( symbol.first ) ), alphabet::SymbolDenormalize::denormalizeSymbols < SecondSymbolType > ( std::move ( symbol.second ) ) );
}

template < class FirstSymbolType, class SecondSymbolType, class ThirdSymbolType >
ext::variant < FirstSymbolType, ext::pair < SecondSymbolType, ThirdSymbolType > > GrammarDenormalize::denormalizeRHS ( ext::variant < DefaultSymbolType, ext::pair < DefaultSymbolType, DefaultSymbolType > > && symbol ) {
	if ( symbol.template is < DefaultSymbolType > ( ) ) {
		return ext::variant < FirstSymbolType, ext::pair < SecondSymbolType, ThirdSymbolType > > ( alphabet::SymbolDenormalize::denormalizeSymbol < FirstSymbolType > ( std::move ( symbol.template get < DefaultSymbolType > ( ) ) ) );
	} else {
		ext::pair < DefaultSymbolType, DefaultSymbolType > & inner = symbol.template get < ext::pair < DefaultSymbolType, DefaultSymbolType > > ( );
		return ext::variant < FirstSymbolType, ext::pair < SecondSymbolType, ThirdSymbolType > > ( ext::make_pair ( alphabet::SymbolDenormalize::denormalizeSymbol < SecondSymbolType > ( std::move ( inner.first ) ), alphabet::SymbolDenormalize::denormalizeSymbol < ThirdSymbolType > ( std::move ( inner.second ) ) ) );
	}
}

template < class FirstSymbolType, class SecondSymbolType, class ThirdSymbolType >
ext::variant < ext::vector < FirstSymbolType >, ext::pair < SecondSymbolType, ext::vector < ThirdSymbolType > > > GrammarDenormalize::denormalizeRHS ( ext::variant < ext::vector < DefaultSymbolType >, ext::pair < DefaultSymbolType, ext::vector < DefaultSymbolType > > > && symbol ) {
	if ( symbol.template is < ext::vector < DefaultSymbolType > > ( ) ) {
		return ext::variant < ext::vector < FirstSymbolType >, ext::pair < SecondSymbolType, ext::vector < ThirdSymbolType > > > ( alphabet::SymbolDenormalize::denormalizeSymbols < FirstSymbolType > ( std::move ( symbol.template get < ext::vector < DefaultSymbolType > > ( ) ) ) );
	} else {
		ext::pair < DefaultSymbolType, ext::vector < DefaultSymbolType > > & inner = symbol.template get < ext::pair < DefaultSymbolType, ext::vector < DefaultSymbolType > > > ( );
		return ext::variant < ext::vector < FirstSymbolType >, ext::pair < SecondSymbolType, ext::vector < ThirdSymbolType > > > ( ext::make_pair ( alphabet::SymbolDenormalize::denormalizeSymbol < SecondSymbolType > ( std::move ( inner.first ) ), alphabet::SymbolDenormalize::denormalizeSymbols < ThirdSymbolType > ( std::move ( inner.second ) ) ) );
	}
}

template < class FirstSymbolType, class SecondSymbolType, class ThirdSymbolType >
ext::variant < ext::vector < FirstSymbolType >, ext::pair < ext::vector < SecondSymbolType >, ThirdSymbolType > > GrammarDenormalize::denormalizeRHS ( ext::variant < ext::vector < DefaultSymbolType >, ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType > > && symbol ) {
	if ( symbol.template is < ext::vector < DefaultSymbolType > > ( ) ) {
		return ext::variant < ext::vector < FirstSymbolType >, ext::pair < ext::vector < SecondSymbolType >, ThirdSymbolType > > ( alphabet::SymbolDenormalize::denormalizeSymbols < FirstSymbolType > ( std::move ( symbol.template get < ext::vector < DefaultSymbolType > > ( ) ) ) );
	} else {
		ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType > & inner = symbol.template get < ext::pair < ext::vector < DefaultSymbolType >, DefaultSymbolType > > ( );
		return ext::variant < ext::vector < FirstSymbolType >, ext::pair < ext::vector < SecondSymbolType >, ThirdSymbolType > > ( ext::make_pair ( alphabet::SymbolDenormalize::denormalizeSymbols < SecondSymbolType > ( std::move ( inner.first ) ), alphabet::SymbolDenormalize::denormalizeSymbol < ThirdSymbolType > ( std::move ( inner.second ) ) ) );
	}
}

template < class FirstSymbolType, class SecondSymbolType, class ThirdSymbolType, class FourthSymbolType >
ext::variant < ext::vector < FirstSymbolType >, ext::tuple < ext::vector < SecondSymbolType >, ThirdSymbolType, ext::vector < FourthSymbolType > > > GrammarDenormalize::denormalizeRHS ( ext::variant < ext::vector < DefaultSymbolType >, ext::tuple < ext::vector < DefaultSymbolType >, DefaultSymbolType, ext::vector < DefaultSymbolType > > > && symbols ) {
	if ( symbols.template is < ext::vector < DefaultSymbolType > > ( ) ) {
		return ext::variant < ext::vector < FirstSymbolType >, ext::tuple < ext::vector < SecondSymbolType >, ThirdSymbolType, ext::vector < FourthSymbolType > > > ( alphabet::SymbolDenormalize::denormalizeSymbols < FirstSymbolType > ( std::move ( symbols.template get < ext::vector < DefaultSymbolType > > ( ) ) ) );
	} else {
		ext::tuple < ext::vector < DefaultSymbolType >, DefaultSymbolType, ext::vector < DefaultSymbolType > > & inner = symbols.template get < ext::tuple < ext::vector < DefaultSymbolType >, DefaultSymbolType, ext::vector < DefaultSymbolType > > > ( );

		ext::vector < SecondSymbolType > first = alphabet::SymbolDenormalize::denormalizeSymbols < SecondSymbolType > ( std::move ( std::get < 0 > ( inner ) ) );
		ThirdSymbolType second = alphabet::SymbolDenormalize::denormalizeSymbol < ThirdSymbolType > ( std::move ( std::get < 1 > ( inner ) ) );
		ext::vector < FourthSymbolType > third = alphabet::SymbolDenormalize::denormalizeSymbols < FourthSymbolType > ( std::move ( std::get < 2 > ( inner ) ) );

		return ext::variant < ext::vector < FirstSymbolType >, ext::tuple < ext::vector < SecondSymbolType >, ThirdSymbolType, ext::vector < FourthSymbolType > > > ( ext::make_tuple ( std::move ( first ), std::move ( second ), std::move ( third ) ) );
	}
}

} /* namespace grammar */

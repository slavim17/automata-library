#include "NonContractingGrammar.h"
#include <grammar/Grammar.h>
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < grammar::NonContractingGrammar < > > ( );
auto xmlRead = registration::XmlReaderRegister < grammar::NonContractingGrammar < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, grammar::NonContractingGrammar < > > ( );

} /* namespace */

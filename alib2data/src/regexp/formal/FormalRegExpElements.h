#pragma once

#include "FormalRegExpElement.h"
#include "FormalRegExpAlternation.h"
#include "FormalRegExpConcatenation.h"
#include "FormalRegExpIteration.h"
#include "FormalRegExpEpsilon.h"
#include "FormalRegExpEmpty.h"
#include "FormalRegExpSymbol.h"


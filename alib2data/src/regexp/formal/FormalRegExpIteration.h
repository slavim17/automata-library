/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/utility>

#include <exception/CommonException.h>

#include "FormalRegExpElement.h"

namespace regexp {

/**
 * \brief Represents the iteration operator in the regular expression. The node has exactly one child.
 *
 * The structure is derived from UnaryNode that provides the child node and its accessors.
 *
 * The node can be visited by the FormalRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class FormalRegExpIteration : public ext::UnaryNode < FormalRegExpElement < SymbolType > > {
	/**
	 * @copydoc regexp::FormalRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename FormalRegExpElement < SymbolType >::Visitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc regexp::FormalRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename FormalRegExpElement < SymbolType >::RvalueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the iteration node with explicit iterated element
	 *
	 * \param element the iterated element
	 */
	explicit FormalRegExpIteration ( FormalRegExpElement < SymbolType > && );

	/**
	 * \brief Creates a new instance of the iteration node with explicit iterated element
	 *
	 * \param element the iterated element
	 */
	explicit FormalRegExpIteration ( const FormalRegExpElement < SymbolType > & );

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	FormalRegExpIteration < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	FormalRegExpIteration < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRegExpElement::clone ( ) const &
	 */
	ext::smart_ptr < UnboundedRegExpElement < SymbolType > > asUnbounded ( ) const override;

	/**
	 * @copydoc FormalRegExpElement::testSymbol() const
	 */
	bool testSymbol ( const SymbolType & symbol ) const override;

	/**
	 * @copydoc FormalRegExpElement::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc FormalRegExpElement::checkAlphabet()
	 */
	bool checkAlphabet ( const ext::set < SymbolType > & alphabet ) const override;

	/**
	 * Getter of the iterated element
	 *
	 * \return iterated element
	 */
	const FormalRegExpElement < SymbolType > & getElement ( ) const;

	/**
	 * Getter of the iterated element
	 *
	 * \return iterated element
	 */
	FormalRegExpElement < SymbolType > & getElement ( );

	/**
	 * Setter of the iterated element
	 *
	 * \param iterated element
	 */
	void setElement ( FormalRegExpElement < SymbolType > && element );

	/**
	 * Setter of the iterated element
	 *
	 * \param iterated element
	 */
	void setElement ( const FormalRegExpElement < SymbolType > & element );

	/**
	 * @copydoc FormalRegExpElement < SymbolType >::operator <=> ( const FormalRegExpElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const FormalRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> static_cast < decltype ( ( * this ) ) > ( other );

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FormalRegExpIteration < SymbolType > & ) const;

	/**
	 * @copydoc FormalRegExpElement < SymbolType >::operator == ( const FormalRegExpElement < SymbolType > & other ) const;
	 */
	bool operator == ( const FormalRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == static_cast < decltype ( ( * this ) ) > ( other );

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FormalRegExpIteration < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRegExpElement < SymbolType > >::operator >> ( ext::ostream & )
	 */
	void operator >>( ext::ostream & out ) const override;
};

} /* namespace regexp */

#include "FormalRegExpEmpty.h"
#include "../unbounded/UnboundedRegExpIteration.h"

namespace regexp {

template < class SymbolType >
FormalRegExpIteration < SymbolType >::FormalRegExpIteration ( FormalRegExpElement < SymbolType > && element ) : ext::UnaryNode < FormalRegExpElement < SymbolType > > ( std::move ( element ) ) {
}

template < class SymbolType >
FormalRegExpIteration < SymbolType >::FormalRegExpIteration ( const FormalRegExpElement < SymbolType > & element ) : FormalRegExpIteration ( ext::move_copy ( element ) ) {
}

template < class SymbolType >
const FormalRegExpElement < SymbolType > & FormalRegExpIteration < SymbolType >::getElement ( ) const {
	return this->getChild ( );
}

template < class SymbolType >
FormalRegExpElement < SymbolType > & FormalRegExpIteration < SymbolType >::getElement ( ) {
	return this->getChild ( );
}

template < class SymbolType >
void FormalRegExpIteration < SymbolType >::setElement ( FormalRegExpElement < SymbolType > && element ) {
	this->setChild ( std::move ( element ) );
}

template < class SymbolType >
void FormalRegExpIteration < SymbolType >::setElement ( const FormalRegExpElement < SymbolType > & element ) {
	setElement ( ext::move_copy ( element ) );
}

template < class SymbolType >
FormalRegExpIteration < SymbolType > * FormalRegExpIteration < SymbolType >::clone ( ) const & {
	return new FormalRegExpIteration ( * this );
}

template < class SymbolType >
FormalRegExpIteration < SymbolType > * FormalRegExpIteration < SymbolType >::clone ( ) && {
	return new FormalRegExpIteration ( std::move ( * this ) );
}

template < class SymbolType >
ext::smart_ptr < UnboundedRegExpElement < SymbolType > > FormalRegExpIteration < SymbolType >::asUnbounded ( ) const {
	return ext::smart_ptr < UnboundedRegExpElement < SymbolType > > ( new UnboundedRegExpIteration < SymbolType > ( * getElement ( ).asUnbounded ( ) ) );
}

template < class SymbolType >
std::strong_ordering FormalRegExpIteration < SymbolType >::operator <=> ( const FormalRegExpIteration < SymbolType > & other ) const {
	return getElement ( ) <=> other.getElement ( );
}

template < class SymbolType >
bool FormalRegExpIteration < SymbolType >::operator == ( const FormalRegExpIteration < SymbolType > & other ) const {
	return getElement ( ) == other.getElement ( );
}

template < class SymbolType >
void FormalRegExpIteration < SymbolType >::operator >>( ext::ostream & out ) const {
	out << "(RegExpFormalRegExpIteration " << getElement ( ) << ")";
}

template < class SymbolType >
bool FormalRegExpIteration < SymbolType >::testSymbol ( const SymbolType & symbol ) const {
	return getElement ( ).testSymbol ( symbol );
}

template < class SymbolType >
void FormalRegExpIteration < SymbolType >::computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const {
	getElement ( ).computeMinimalAlphabet ( alphabet );
}

template < class SymbolType >
bool FormalRegExpIteration < SymbolType >::checkAlphabet ( const ext::set < SymbolType > & alphabet ) const {
	return getElement ( ).checkAlphabet ( alphabet );
}

} /* namespace regexp */

extern template class regexp::FormalRegExpIteration < DefaultSymbolType >;


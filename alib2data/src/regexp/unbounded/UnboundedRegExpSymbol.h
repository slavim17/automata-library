/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include "UnboundedRegExpElement.h"

namespace regexp {

/**
 * \brief Represents the symbol in the regular expression. The can't have any children.
 *
 * The structure is derived from NullaryNode disallowing adding any child.
 *
 * The node can be visited by the UnboundedRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class UnboundedRegExpSymbol : public ext::NullaryNode < UnboundedRegExpElement < SymbolType > > {
	/**
	 * The symbol of the node.
	 */
	SymbolType m_symbol;

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::Visitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::RvalueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the symbol node using the actual symbol to represent.
	 *
	 * \param symbol the value of the represented symbol
	 */
	explicit UnboundedRegExpSymbol ( SymbolType symbol );

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpSymbol < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpSymbol < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc UnboundedRegExpElement::cloneAsFormal() const
	 */
	ext::smart_ptr < FormalRegExpElement < SymbolType > > asFormal ( ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::testSymbol() const
	 */
	bool testSymbol ( const SymbolType & symbol ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::checkAlphabet()
	 */
	bool checkAlphabet ( const ext::set < SymbolType > & alphabet ) const override;

	/**
	 * Getter of the symbol.
	 *
	 * \return the symbol
	 */
	const SymbolType & getSymbol ( ) const &;

	/**
	 * Getter of the symbol.
	 *
	 * \return the symbol
	 */
	SymbolType && getSymbol ( ) &&;

	/**
	 * @copydoc UnboundedRegExpElement < SymbolType >::operator <=> ( const UnboundedRegExpElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const UnboundedRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> static_cast < decltype ( ( * this ) ) > ( other );

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const UnboundedRegExpSymbol < SymbolType > & ) const;

	/**
	 * @copydoc UnboundedRegExpElement < SymbolType >::operator == ( const UnboundedRegExpElement < SymbolType > & other ) const;
	 */
	bool operator == ( const UnboundedRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == static_cast < decltype ( ( * this ) ) > ( other );

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const UnboundedRegExpSymbol < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::operator >> ( ext::ostream & )
	 */
	void operator >>( ext::ostream & out ) const override;
};

} /* namespace regexp */

#include "UnboundedRegExpSymbol.h"
#include "../formal/FormalRegExpSymbol.h"

namespace regexp {

template < class SymbolType >
UnboundedRegExpSymbol < SymbolType >::UnboundedRegExpSymbol ( SymbolType symbol ) : m_symbol ( std::move ( symbol ) ) {
}

template < class SymbolType >
UnboundedRegExpSymbol < SymbolType > * UnboundedRegExpSymbol < SymbolType >::clone ( ) const & {
	return new UnboundedRegExpSymbol ( * this );
}

template < class SymbolType >
UnboundedRegExpSymbol < SymbolType > * UnboundedRegExpSymbol < SymbolType >::clone ( ) && {
	return new UnboundedRegExpSymbol ( std::move ( * this ) );
}

template < class SymbolType >
ext::smart_ptr < FormalRegExpElement < SymbolType > > UnboundedRegExpSymbol < SymbolType >::asFormal ( ) const {
	return ext::smart_ptr < FormalRegExpElement < SymbolType > > ( new FormalRegExpSymbol < SymbolType > ( this->m_symbol ) );
}

template < class SymbolType >
std::strong_ordering UnboundedRegExpSymbol < SymbolType >::operator <=> ( const UnboundedRegExpSymbol < SymbolType > & other ) const {
	return m_symbol <=> other.m_symbol;
}

template < class SymbolType >
bool UnboundedRegExpSymbol < SymbolType >::operator == ( const UnboundedRegExpSymbol < SymbolType > & other ) const {
	return m_symbol == other.m_symbol;
}

template < class SymbolType >
void UnboundedRegExpSymbol < SymbolType >::operator >>( ext::ostream & out ) const {
	out << "(UnboundedRegExpSymbol " << m_symbol << ")";
}

template < class SymbolType >
bool UnboundedRegExpSymbol < SymbolType >::testSymbol ( const SymbolType & symbol ) const {
	return symbol == this->m_symbol;
}

template < class SymbolType >
bool UnboundedRegExpSymbol < SymbolType >::checkAlphabet ( const ext::set < SymbolType > & alphabet ) const {
	return alphabet.count ( m_symbol );
}

template < class SymbolType >
void UnboundedRegExpSymbol < SymbolType >::computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const {
	alphabet.insert ( this->m_symbol );
}

template < class SymbolType >
const SymbolType & UnboundedRegExpSymbol < SymbolType >::getSymbol ( ) const & {
	return this->m_symbol;
}

template < class SymbolType >
SymbolType && UnboundedRegExpSymbol < SymbolType >::getSymbol ( ) && {
	return std::move ( this->m_symbol );
}

} /* namespace regexp */

extern template class regexp::UnboundedRegExpSymbol < DefaultSymbolType >;


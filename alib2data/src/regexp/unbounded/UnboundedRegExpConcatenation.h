/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/ptr_vector>

#include <exception/CommonException.h>

#include "UnboundedRegExpElement.h"

namespace regexp {

/**
 * \brief Represents the concatenation operator in the regular expression. The node can have 0 to n children in list of UnboundedRegExpElement as operands of the concatenation.
 *
 * The structure is derived from VararyNode that provides the children list and its accessors.
 *
 * The node can be visited by the UnboundedRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class UnboundedRegExpConcatenation : public ext::VararyNode < UnboundedRegExpElement < SymbolType > > {
	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::Visitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::RvalueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the concatenation node. By default it is semantically equivalent to epsilon.
	 */
	explicit UnboundedRegExpConcatenation ( ) = default;

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpConcatenation < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpConcatenation < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc UnboundedRegExpElement::cloneAsFormal() const
	 */
	ext::smart_ptr < FormalRegExpElement < SymbolType > > asFormal ( ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::testSymbol() const
	 */
	bool testSymbol ( const SymbolType & symbol ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::checkAlphabet()
	 */
	bool checkAlphabet ( const ext::set < SymbolType > & alphabet ) const override;

	/**
	 * Getter of child nodes of the regexp node
	 *
	 * \return child nodes
	 */
	const ext::ptr_vector < UnboundedRegExpElement < SymbolType > > & getElements ( ) const;

	/**
	 * Getter of child nodes of the regexp node
	 *
	 * \return child nodes
	 */
	const ext::ptr_vector < UnboundedRegExpElement < SymbolType > > & getElements ( );

	/**
	 * Node appending method. The node is added to the current list of nodes.
	 *
	 * \param appended node
	 */
	void appendElement ( UnboundedRegExpElement < SymbolType > && element );

	/**
	 * Node appending method. The node is added to the current list of nodes.
	 *
	 * \param appended node
	 */
	void appendElement ( const UnboundedRegExpElement < SymbolType > & element );

	/**
	 * @copydoc UnboundedRegExpElement < SymbolType >::operator <=> ( const UnboundedRegExpElement < SymbolType > & other ) const;
	 */
	std::strong_ordering operator <=> ( const UnboundedRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this <=> static_cast < decltype ( ( * this ) ) > ( other );

		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );
	}

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const UnboundedRegExpConcatenation < SymbolType > & ) const;

	/**
	 * @copydoc UnboundedRegExpElement < SymbolType >::operator == ( const UnboundedRegExpElement < SymbolType > & other ) const;
	 */
	bool operator == ( const UnboundedRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return * this == static_cast < decltype ( ( * this ) ) > ( other );

		return false;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const UnboundedRegExpConcatenation < SymbolType > & ) const;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::operator >> ( ext::ostream & )
	 */
	void operator >>( ext::ostream & out ) const override;
};

} /* namespace regexp */

#include "../formal/FormalRegExpConcatenation.h"
#include "../formal/FormalRegExpEpsilon.h"

namespace regexp {

template < class SymbolType >
const ext::ptr_vector < UnboundedRegExpElement < SymbolType > > & UnboundedRegExpConcatenation < SymbolType >::getElements ( ) const {
	return this->getChildren();
}

template < class SymbolType >
const ext::ptr_vector < UnboundedRegExpElement < SymbolType > > & UnboundedRegExpConcatenation < SymbolType >::getElements ( ) {
	return this->getChildren();
}

template < class SymbolType >
void UnboundedRegExpConcatenation < SymbolType >::appendElement ( UnboundedRegExpElement < SymbolType > && element ) {
	this->pushBackChild ( std::move ( element ) );
}

template < class SymbolType >
void UnboundedRegExpConcatenation < SymbolType >::appendElement ( const UnboundedRegExpElement < SymbolType > & element ) {
	this->appendElement ( ext::move_copy ( element ) );
}

template < class SymbolType >
UnboundedRegExpConcatenation < SymbolType > * UnboundedRegExpConcatenation < SymbolType >::clone ( ) const & {
	return new UnboundedRegExpConcatenation ( * this );
}

template < class SymbolType >
UnboundedRegExpConcatenation < SymbolType > * UnboundedRegExpConcatenation < SymbolType >::clone ( ) && {
	return new UnboundedRegExpConcatenation ( std::move ( * this ) );
}

template < class SymbolType >
ext::smart_ptr < FormalRegExpElement < SymbolType > > UnboundedRegExpConcatenation < SymbolType >::asFormal ( ) const {
	if ( getElements ( ).empty ( ) ) return ext::smart_ptr < FormalRegExpElement < SymbolType > > ( new FormalRegExpEpsilon < SymbolType > ( ) );

	ext::smart_ptr < FormalRegExpElement < SymbolType > > res = getElements ( )[ getElements ( ).size ( ) - 1 ].asFormal ( );

	for ( size_t i = getElements ( ).size ( ) - 1; i >= 1; i-- )
		res = ext::smart_ptr < FormalRegExpElement < SymbolType > > ( new FormalRegExpConcatenation < SymbolType > ( std::move ( * getElements ( )[ i - 1 ].asFormal ( ) ), std::move ( * res ) ) );

	return res;
}

template < class SymbolType >
std::strong_ordering UnboundedRegExpConcatenation < SymbolType >::operator <=> ( const UnboundedRegExpConcatenation < SymbolType > & other ) const {
	return getElements ( ) <=> other.getElements ( );
}

template < class SymbolType >
bool UnboundedRegExpConcatenation < SymbolType >::operator == ( const UnboundedRegExpConcatenation < SymbolType > & other ) const {
	return getElements ( ) == other.getElements ( );
}

template < class SymbolType >
void UnboundedRegExpConcatenation < SymbolType >::operator >>( ext::ostream & out ) const {
	out << "(UnboundedRegExpConcatenation";

	for ( const UnboundedRegExpElement < SymbolType > & element : this->getElements ( ) )
		out << " " << element;

	out << ")";
}

template < class SymbolType >
bool UnboundedRegExpConcatenation < SymbolType >::testSymbol ( const SymbolType & symbol ) const {
	for ( const UnboundedRegExpElement < SymbolType > & element : this->getElements ( ) )
		if ( element.testSymbol ( symbol ) ) return true;

	return false;
}

template < class SymbolType >
bool UnboundedRegExpConcatenation < SymbolType >::checkAlphabet ( const ext::set < SymbolType > & alphabet ) const {
	for ( const UnboundedRegExpElement < SymbolType > & element : this->getElements ( ) )
		if ( ! element.checkAlphabet ( alphabet ) ) return false;

	return true;
}

template < class SymbolType >
void UnboundedRegExpConcatenation < SymbolType >::computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const {
	for ( const UnboundedRegExpElement < SymbolType > & element : this->getElements ( ) )
		element.computeMinimalAlphabet ( alphabet );
}

} /* namespace regexp */

extern template class regexp::UnboundedRegExpConcatenation < DefaultSymbolType >;


#include "UnboundedRegExp.h"
#include "UnboundedRegExpElements.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class regexp::UnboundedRegExp < >;
template class abstraction::ValueHolder < regexp::UnboundedRegExp < > >;
template const regexp::UnboundedRegExp < > & abstraction::retrieveValue < const regexp::UnboundedRegExp < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const regexp::UnboundedRegExp < > & >;
template class registration::NormalizationRegisterImpl < regexp::UnboundedRegExp < > >;

template class regexp::UnboundedRegExpStructure < DefaultSymbolType >;
template class regexp::UnboundedRegExpElement < DefaultSymbolType >;
template class regexp::UnboundedRegExpAlternation < DefaultSymbolType >;
template class regexp::UnboundedRegExpConcatenation < DefaultSymbolType >;
template class regexp::UnboundedRegExpIteration < DefaultSymbolType >;
template class regexp::UnboundedRegExpEpsilon < DefaultSymbolType >;
template class regexp::UnboundedRegExpEmpty < DefaultSymbolType >;
template class regexp::UnboundedRegExpSymbol < DefaultSymbolType >;

namespace {

auto unboundedRegExpFromFormalRegExp = registration::CastRegister < regexp::UnboundedRegExp < >, regexp::FormalRegExp < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < regexp::UnboundedRegExp < > > ( );

} /* namespace */

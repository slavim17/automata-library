#include "UnrankedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnrankedPattern < >;
template class abstraction::ValueHolder < tree::UnrankedPattern < > >;
template const tree::UnrankedPattern < > & abstraction::retrieveValue < const tree::UnrankedPattern < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const tree::UnrankedPattern < > & >;
template class registration::NormalizationRegisterImpl < tree::UnrankedPattern < > >;

namespace {

auto components = registration::ComponentRegister < tree::UnrankedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnrankedPattern < > > ( );

auto UnrankedPatternFromRankedPattern = registration::CastRegister < tree::UnrankedPattern < >, tree::RankedPattern < > > ( );

} /* namespace */

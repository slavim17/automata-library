#include "UnrankedTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnrankedTree < >;
template class abstraction::ValueHolder < tree::UnrankedTree < > >;
template const tree::UnrankedTree < > & abstraction::retrieveValue < const tree::UnrankedTree < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const tree::UnrankedTree < > & >;
template class registration::NormalizationRegisterImpl < tree::UnrankedTree < > >;

namespace {

auto components = registration::ComponentRegister < tree::UnrankedTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnrankedTree < > > ( );

auto UnrankedTreeFromRankedTree = registration::CastRegister < tree::UnrankedTree < >, tree::RankedTree < > > ( );

} /* namespace */

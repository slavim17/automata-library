#include "TreeException.h"

namespace tree {

TreeException::TreeException(const std::string& cause) :
		CommonException(cause) {
}

} /* namespace tree */

#include "RankedPattern.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::RankedPattern < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::RankedPattern < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::RankedPattern < > > ( );

} /* namespace */

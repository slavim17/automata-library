#include "PrefixRankedBarTree.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::PrefixRankedBarTree < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::PrefixRankedBarTree < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::PrefixRankedBarTree < > > ( );

} /* namespace */

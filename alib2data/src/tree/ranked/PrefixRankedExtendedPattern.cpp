#include "PrefixRankedExtendedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedExtendedPattern < >;
template class abstraction::ValueHolder < tree::PrefixRankedExtendedPattern < > >;
template const tree::PrefixRankedExtendedPattern < > & abstraction::retrieveValue < const tree::PrefixRankedExtendedPattern < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const tree::PrefixRankedExtendedPattern < > & >;
template class registration::NormalizationRegisterImpl < tree::PrefixRankedExtendedPattern < > >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedExtendedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedExtendedPattern < > > ( );

auto PrefixRankedExtendedPatternFromRankedExtendedPattern = registration::CastRegister < tree::PrefixRankedExtendedPattern < >, tree::RankedExtendedPattern < > > ( );
auto PrefixRankedExtendedPatternFromPrefixRankedPattern = registration::CastRegister < tree::PrefixRankedExtendedPattern < >, tree::PrefixRankedPattern < > > ( );

auto LinearStringFromPrefixRankedPattern = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedExtendedPattern < > > ( );

} /* namespace */

#include "RankedNonlinearPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::RankedNonlinearPattern < >;
template class abstraction::ValueHolder < tree::RankedNonlinearPattern < > >;
template const tree::RankedNonlinearPattern < > & abstraction::retrieveValue < const tree::RankedNonlinearPattern < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const tree::RankedNonlinearPattern < > & >;
template class registration::NormalizationRegisterImpl < tree::RankedNonlinearPattern < > >;

namespace {

auto components = registration::ComponentRegister < tree::RankedNonlinearPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::RankedNonlinearPattern < > > ( );

} /* namespace */

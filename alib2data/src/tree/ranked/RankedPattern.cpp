#include "RankedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::RankedPattern < >;
template class abstraction::ValueHolder < tree::RankedPattern < > >;
template const tree::RankedPattern < > & abstraction::retrieveValue < const tree::RankedPattern < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const tree::RankedPattern < > & >;
template class registration::NormalizationRegisterImpl < tree::RankedPattern < > >;

namespace {

auto components = registration::ComponentRegister < tree::RankedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::RankedPattern < > > ( );

auto RankedPatternFromUnrankedPattern = registration::CastRegister < tree::RankedPattern < >, tree::UnrankedPattern < > > ( );

} /* namespace */

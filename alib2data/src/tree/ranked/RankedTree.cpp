#include "RankedTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::RankedTree < >;
template class abstraction::ValueHolder < tree::RankedTree < > >;
template const tree::RankedTree < > & abstraction::retrieveValue < const tree::RankedTree < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const tree::RankedTree < > & >;
template class registration::NormalizationRegisterImpl < tree::RankedTree < > >;

namespace {

auto components = registration::ComponentRegister < tree::RankedTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::RankedTree < > > ( );

auto RankedTreeFromUnrankedTree = registration::CastRegister < tree::RankedTree < >, tree::UnrankedTree < > > ( );
auto RankedTreeFromPostfixRankedTree = registration::CastRegister < tree::RankedTree < >, tree::PostfixRankedTree < > > ( );
auto RankedTreeFromPrefixRankedTree = registration::CastRegister < tree::RankedTree < >, tree::PrefixRankedTree < > > ( );

} /* namespace */

#include "PrefixRankedBarTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedBarTree < >;
template class abstraction::ValueHolder < tree::PrefixRankedBarTree < > >;
template const tree::PrefixRankedBarTree < > & abstraction::retrieveValue < const tree::PrefixRankedBarTree < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const tree::PrefixRankedBarTree < > & >;
template class registration::NormalizationRegisterImpl < tree::PrefixRankedBarTree < > >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedBarTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedBarTree < > > ( );

auto PrefixRankedBarTreeFromRankedTree = registration::CastRegister < tree::PrefixRankedBarTree < >, tree::RankedTree < > > ( );

auto LinearStringFromPrefixRankedBarTree = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedBarTree < > > ( );

} /* namespace */

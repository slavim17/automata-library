#include "PrefixRankedBarPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedBarPattern < >;
template class abstraction::ValueHolder < tree::PrefixRankedBarPattern < > >;
template const tree::PrefixRankedBarPattern < > & abstraction::retrieveValue < const tree::PrefixRankedBarPattern < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const tree::PrefixRankedBarPattern < > & >;
template class registration::NormalizationRegisterImpl < tree::PrefixRankedBarPattern < > >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedBarPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedBarPattern < > > ( );

auto PrefixRankedBarPatternFromRankedPattern = registration::CastRegister < tree::PrefixRankedBarPattern < >, tree::RankedPattern < > > ( );
auto PrefixRankedBarPatternFromPrefixRankedBarTree = registration::CastRegister < tree::PrefixRankedBarPattern < >, tree::PrefixRankedBarTree < > > ( );

auto LinearStringFromPrefixRankedBarPattern = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedBarPattern < > > ( );

} /* namespace */

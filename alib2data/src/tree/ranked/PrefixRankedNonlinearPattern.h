/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace tree {

template < class SymbolType = DefaultSymbolType >
class PrefixRankedNonlinearPattern;

} /* namespace tree */

#include <ext/algorithm>

#include <alib/set>
#include <alib/vector>
#include <alib/tree>
#include <alib/deque>

#include <core/modules.hpp>
#include <common/ranked_symbol.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <alphabet/Wildcard.h>

#include <core/type_util.hpp>
#include <core/type_details_base.hpp>
#include <alphabet/common/SymbolNormalize.h>
#include <tree/common/TreeNormalize.h>
#include <alphabet/common/SymbolDenormalize.h>
#include <tree/common/TreeDenormalize.h>

#include <string/LinearString.h>

#include "PrefixRankedTree.h"
#include "PrefixRankedPattern.h"
#include "RankedTree.h"
#include "RankedPattern.h"
#include "RankedNonlinearPattern.h"

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace tree {

/**
 * \brief
 * Nonlinear tree pattern represented as linear sequece as result of preorder traversal. The representation is so called ranked, therefore it consists of ranked symbols. The rank of the ranked symbol needs to be compatible with unsigned integer. Additionally the pattern contains a special wildcard symbol representing any subtree and nonlinear variables each to represent same subtree (in the particular occurrence in a tree).
 *
 * \details
 * T = (A, C, W \in A, V \in A ),
 * A (Alphabet) = finite set of ranked symbols,
 * C (Content) = linear representation of the pattern content
 * W (Wildcard) = special symbol representing any subtree
 * V (Variables) = finite set of special symbols each representing same subtree
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class PrefixRankedNonlinearPattern final : public core::Components < PrefixRankedNonlinearPattern < SymbolType >, ext::set < common::ranked_symbol < SymbolType > >, module::Set, std::tuple < component::GeneralAlphabet, component::NonlinearAlphabet >, common::ranked_symbol < SymbolType >, module::Value, component::SubtreeWildcardSymbol > {
	/**
	 * Linear representation of the pattern content.
	 */
	ext::vector < common::ranked_symbol < SymbolType > > m_Data;

	/**
	 * Checker of validity of the representation of the ettern
	 *
	 * \throws TreeException when new pattern representation is not valid
	 */
	void arityChecksum ( const ext::vector < common::ranked_symbol < SymbolType > > & data );

public:
	/**
	 * \brief Creates a new instance of the pattern with concrete alphabet, content, wildcard, and nonlinear variables.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param nonlinearVariables the set of nonlinear variables
	 * \param alphabet the initial alphabet of the pattern
	 * \param data the initial pattern in linear representation
	 */
	explicit PrefixRankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nonlinearVariables, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * \brief Creates a new instance of the pattern with concrete content, wildcard, and nonlinear variables. The alphabet is deduced from the content.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param nonlinearVariables the set of nonlinear variables
	 * \param data the initial pattern in linear representation
	 */
	explicit PrefixRankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nonlinearVariables, ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * \brief Creates a new instance of the pattern with concrete content and wildcard. The alphabet is deduced from the content. Nonlinear variables are defaultly constructed to empty set.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param data the initial pattern in linear representation
	 */
	explicit PrefixRankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * \brief Creates a new instance of the pattern based on the PrefixRankedTree. The linear representation is copied from tree. Alphabet as well. subtree wildcard is defaultly constructed and nonlinear variables are defaultly constructed to empty set.
	 *
	 * \param tree representation of a tree.
	 */
	explicit PrefixRankedNonlinearPattern ( const PrefixRankedTree < SymbolType > & tree );

	/**
	 * \brief Creates a new instance of the pattern based on the PrefixRankedPattern. The linear representation is copied from tree. Subtree wildcard and alphabet as well. Nonlinear variables are defaultly constructed to empty set.
	 *
	 * \param tree representation of a tree.
	 */
	explicit PrefixRankedNonlinearPattern ( const PrefixRankedPattern < SymbolType > & tree );

	/**
	 * \brief Creates a new instance of the pattern based on the RankedTree. The linear representation is constructed by preorder traversal on the tree parameter. The subtree wildcard is defaultly constructed and nonlinear variables are defaultly constructed to empty set.
	 *
	 * \param tree representation of a tree.
	 */
	explicit PrefixRankedNonlinearPattern ( const RankedTree < SymbolType > & tree );

	/**
	 * \brief Creates a new instance of the pattern based on the RankedPattern. The linear representation is constructed by preorder traversal on the tree parameter. The subtree wildcard is provided by the pattern parameter and nonlinear variables are defaultly constructed to empty set.
	 *
	 * \param tree representation of a pattern.
	 */
	explicit PrefixRankedNonlinearPattern ( const RankedPattern < SymbolType > & tree );

	/**
	 * \brief Creates a new instance of the pattern based on the RankedNonlinearPattern. The linear representation is constructed by preorder traversal on the tree parameter. The subtree wildcard and nonlinear variables are provided by the pattern parameter.
	 *
	 * \param tree representation of a pattern.
	 */
	explicit PrefixRankedNonlinearPattern ( const RankedNonlinearPattern < SymbolType > & tree );

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getAlphabet ( ) const & {
		return this->template accessComponent < component::GeneralAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getAlphabet ( ) && {
		return std::move ( this->template accessComponent < component::GeneralAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of an alphabet symbols.
	 *
	 * \param symbols the new symbols to be added to the alphabet
	 */
	void extendAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & symbols ) {
		this->template accessComponent < component::GeneralAlphabet > ( ).add ( symbols );
	}

	/**
	 * Getter of the wildcard symbol.
	 *
	 * \returns the wildcard symbol of the pattern
	 */
	const common::ranked_symbol < SymbolType > & getSubtreeWildcard ( ) const & {
		return this->template accessComponent < component::SubtreeWildcardSymbol > ( ).get ( );
	}

	/**
	 * Getter of the wildcard symbol.
	 *
	 * \returns the wildcard symbol of the pattern
	 */
	common::ranked_symbol < SymbolType > && getSubtreeWildcard ( ) && {
		return std::move ( this->template accessComponent < component::SubtreeWildcardSymbol > ( ).get ( ) );
	}

	/**
	 * Getter of the nonlinear variables.
	 *
	 * \returns the nonlinear variables of the pattern
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getNonlinearVariables ( ) const & {
		return this->template accessComponent < component::NonlinearAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the nonlinear variables.
	 *
	 * \returns the nonlinear variables of the pattern
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getNonlinearVariables ( ) && {
		return std::move ( this->template accessComponent < component::NonlinearAlphabet > ( ).get ( ) );
	}

	/**
	 * Getter of the pattern representation.
	 *
	 * \return List of symbols forming the linear representation of the pattern.
	 */
	const ext::vector < common::ranked_symbol < SymbolType > > & getContent ( ) const &;

	/**
	 * Getter of the pattern representation.
	 *
	 * \return List of symbols forming the linear representation of the pattern.
	 */
	ext::vector < common::ranked_symbol < SymbolType > > && getContent ( ) &&;

	/**
	 * Setter of the representation of the pattern.
	 *
	 * \throws TreeException when new pattern representation is not valid or when symbol of the representation are not present in the alphabet
	 *
	 * \param data new List of symbols forming the representation of the pattern.
	 */
	void setContent ( ext::vector < common::ranked_symbol < SymbolType > > data );

	/**
	 * @return true if pattern is an empty word (vector length is 0). The method is present to allow compatibility with strings. Tree is never empty in this datatype.
	 */
	bool isEmpty ( ) const;

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	auto operator <=> ( const PrefixRankedNonlinearPattern & other ) const {
		return std::tie ( m_Data, getAlphabet ( ), getSubtreeWildcard ( ), getNonlinearVariables ( ) ) <=> std::tie ( other.m_Data, other.getAlphabet ( ), other.getSubtreeWildcard ( ), other.getNonlinearVariables ( ) );
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const PrefixRankedNonlinearPattern & other ) const {
		return std::tie ( m_Data, getAlphabet ( ), getSubtreeWildcard ( ), getNonlinearVariables ( ) ) == std::tie ( other.m_Data, other.getAlphabet ( ), other.getSubtreeWildcard ( ), other.getNonlinearVariables ( ) );
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const PrefixRankedNonlinearPattern & instance ) {
		out << "(PrefixRankedNonlinearPattern";
		out << " alphabet = " << instance.getAlphabet ( );
		out << " content = " << instance.getContent ( );
		out << " nonlinearVariables = " << instance.getNonlinearVariables ( );
		out << " subtreeWildcard = " << instance.getSubtreeWildcard ( );
		out << ")";
		return out;
	}

	/**
	 * \brief Creates a new instance of the string from a linear representation of a tree
	 *
	 * \returns tree casted to string
	 */
	explicit operator string::LinearString < common::ranked_symbol < SymbolType > > ( ) const {
		return string::LinearString < common::ranked_symbol < SymbolType > > ( getAlphabet ( ), getContent ( ) );
	}
};

template < class SymbolType >
PrefixRankedNonlinearPattern < SymbolType >::PrefixRankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nonlinearVariables, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::vector < common::ranked_symbol < SymbolType > > data ) : core::Components < PrefixRankedNonlinearPattern, ext::set < common::ranked_symbol < SymbolType > >, module::Set, std::tuple < component::GeneralAlphabet, component::NonlinearAlphabet >, common::ranked_symbol < SymbolType >, module::Value, component::SubtreeWildcardSymbol > ( std::move ( alphabet ), std::move ( nonlinearVariables ), std::move ( subtreeWildcard ) ) {
	setContent ( std::move ( data ) );
}

template < class SymbolType >
PrefixRankedNonlinearPattern < SymbolType >::PrefixRankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nonlinearVariables, ext::vector < common::ranked_symbol < SymbolType > > data ) : PrefixRankedNonlinearPattern ( subtreeWildcard, nonlinearVariables, ext::set < common::ranked_symbol < SymbolType > > ( data.begin ( ), data.end ( ) ) + ext::set < common::ranked_symbol < SymbolType > > { subtreeWildcard } + nonlinearVariables, data ) {
}

template < class SymbolType >
PrefixRankedNonlinearPattern < SymbolType >::PrefixRankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::vector < common::ranked_symbol < SymbolType > > data ) : PrefixRankedNonlinearPattern ( subtreeWildcard, {}, ext::set < common::ranked_symbol < SymbolType > > ( data.begin ( ), data.end ( ) ) + ext::set < common::ranked_symbol < SymbolType > > { subtreeWildcard }, data ) {
}

template < class SymbolType >
PrefixRankedNonlinearPattern < SymbolType >::PrefixRankedNonlinearPattern ( const PrefixRankedTree < SymbolType > & tree ) : PrefixRankedNonlinearPattern ( alphabet::Wildcard::instance < common::ranked_symbol < SymbolType > > ( ), { }, tree.getAlphabet ( ), tree.getContent ( ) ) {
}

template < class SymbolType >
PrefixRankedNonlinearPattern < SymbolType >::PrefixRankedNonlinearPattern ( const PrefixRankedPattern < SymbolType > & tree ) : PrefixRankedNonlinearPattern ( tree.getSubtreeWildcard ( ), { }, tree.getAlphabet ( ), tree.getContent ( ) ) {
}

template < class SymbolType >
PrefixRankedNonlinearPattern < SymbolType >::PrefixRankedNonlinearPattern ( const RankedTree < SymbolType > & tree ) : PrefixRankedNonlinearPattern ( alphabet::Wildcard::instance < common::ranked_symbol < SymbolType > > ( ), { }, tree.getAlphabet ( ), TreeAuxiliary::treeToPrefix ( tree.getContent ( ) ) ) {
}

template < class SymbolType >
PrefixRankedNonlinearPattern < SymbolType >::PrefixRankedNonlinearPattern ( const RankedPattern < SymbolType > & tree ) : PrefixRankedNonlinearPattern ( tree.getSubtreeWildcard ( ), { }, tree.getAlphabet ( ), TreeAuxiliary::treeToPrefix ( tree.getContent ( ) ) ) {
}

template < class SymbolType >
PrefixRankedNonlinearPattern < SymbolType >::PrefixRankedNonlinearPattern ( const RankedNonlinearPattern < SymbolType > & tree ) : PrefixRankedNonlinearPattern ( tree.getSubtreeWildcard ( ), tree.getNonlinearVariables ( ), tree.getAlphabet ( ), TreeAuxiliary::treeToPrefix ( tree.getContent ( ) ) ) {
}

template < class SymbolType >
const ext::vector < common::ranked_symbol < SymbolType > > & PrefixRankedNonlinearPattern < SymbolType >::getContent ( ) const & {
	return this->m_Data;
}

template < class SymbolType >
ext::vector < common::ranked_symbol < SymbolType > > && PrefixRankedNonlinearPattern < SymbolType >::getContent ( ) && {
	return std::move ( this->m_Data );
}

template < class SymbolType >
void PrefixRankedNonlinearPattern < SymbolType >::setContent ( ext::vector < common::ranked_symbol < SymbolType > > data ) {
	arityChecksum ( data );

	ext::set < common::ranked_symbol < SymbolType > > minimalAlphabet ( data.begin ( ), data.end ( ) );
	std::set_difference ( minimalAlphabet.begin ( ), minimalAlphabet.end ( ), getAlphabet ( ).begin ( ), getAlphabet ( ).end ( ), ext::callback_iterator ( [ ] ( const common::ranked_symbol < SymbolType > & ) {
			throw TreeException ( "Input symbols not in the alphabet." );
		} ) );

	this->m_Data = std::move ( data );
}

template < class SymbolType >
void PrefixRankedNonlinearPattern < SymbolType >::arityChecksum ( const ext::vector < common::ranked_symbol < SymbolType > > & data ) {
	if ( std::accumulate ( data.begin ( ), data.end ( ), 1, [ ] ( int current, const common::ranked_symbol < SymbolType > & symbol ) {
				return current + symbol.getRank ( ) - 1;
			} ) != 0 )
		throw TreeException ( "The string does not form a tree" );
}

template < class SymbolType >
bool PrefixRankedNonlinearPattern < SymbolType >::isEmpty ( ) const {
	return this->m_Data.empty ( );
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the pattern's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::PrefixRankedNonlinearPattern < SymbolType >, common::ranked_symbol < SymbolType >, component::GeneralAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in the pattern.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::PrefixRankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		const ext::vector < common::ranked_symbol < SymbolType > > & m_content = pattern.getContent ( );

		return std::find ( m_content.begin ( ), m_content.end ( ), symbol ) != m_content.end ( ) || pattern.template accessComponent < component::SubtreeWildcardSymbol > ( ).get ( ) == symbol;
	}

	/**
	 * Returns true as all symbols are possibly available to be in an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const tree::PrefixRankedNonlinearPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
		return true;
	}

	/**
	 * All symbols are valid as symbols of an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 */
	static void valid ( const tree::PrefixRankedNonlinearPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
	}
};

/**
 * Helper class specifying constraints for the pattern's internal nonlinear variables component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::PrefixRankedNonlinearPattern < SymbolType >, common::ranked_symbol < SymbolType >, component::NonlinearAlphabet > {
public:
	/**
	 * Returns false. Nonlinear symbol is only a mark that the pattern itself does require further.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::PrefixRankedNonlinearPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
		return false;
	}

	/**
	 * Determines whether the symbol is available in the pattern's alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const tree::PrefixRankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		return pattern.template accessComponent < component::GeneralAlphabet > ( ).get ( ).count ( symbol );
	}

	/**
	 * Nonlinear variable needs to have zero arity and needs to be different from subtree wildcard.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \throws TreeException if the symbol does not have zero arity
	 */
	static void valid ( const tree::PrefixRankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol) {
		if ( symbol.getRank ( ) != 0 )
			throw tree::TreeException ( "Nonlinear variable has nonzero arity" );

		if ( pattern.template accessComponent < component::SubtreeWildcardSymbol > ( ).get ( ) == symbol )
			throw tree::TreeException ( "Symbol " + ext::to_string ( symbol ) + "cannot be set as nonlinear variable since it is already subtree wildcard" );
	}
};

/**
 * Helper class specifying constraints for the pattern's internal subtree wildcard element.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class ElementConstraint< tree::PrefixRankedNonlinearPattern < SymbolType >, common::ranked_symbol < SymbolType >, component::SubtreeWildcardSymbol > {
public:
	/**
	 * Determines whether the symbol is available in the pattern's alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is already in the alphabet of the pattern
	 */
	static bool available ( const tree::PrefixRankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		return pattern.template accessComponent < component::GeneralAlphabet > ( ).get ( ).count ( symbol );
	}

	/**
	 * Subtree wildcard needs to have zero arity and it needs to be different from any nonlinear variable of the tree.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \throws TreeException if the symbol does not have zero arity
	 */
	static void valid ( const tree::PrefixRankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol) {
		if ( symbol.getRank ( ) != 0 )
			throw tree::TreeException ( "SubtreeWildcard symbol has nonzero arity" );

		if ( pattern.template accessComponent < component::NonlinearAlphabet > ( ).get ( ).count ( symbol ) )
			throw tree::TreeException ( "Symbol " + ext::to_string ( symbol ) + "cannot be set as subtree wildcard since it is already nonlinear variable" );
	}
};

template < class SymbolType >
struct type_util < tree::PrefixRankedNonlinearPattern < SymbolType > > {
	static tree::PrefixRankedNonlinearPattern < SymbolType > denormalize ( tree::PrefixRankedNonlinearPattern < > && value ) {
		common::ranked_symbol < SymbolType > wildcard = alphabet::SymbolDenormalize::denormalizeRankedSymbol < SymbolType > ( std::move ( value ).getSubtreeWildcard ( ) );
		ext::set < common::ranked_symbol < SymbolType > > nonlinearAlphabet = alphabet::SymbolDenormalize::denormalizeRankedAlphabet < SymbolType > ( std::move ( value ).getNonlinearVariables ( ) );
		ext::set < common::ranked_symbol < SymbolType > > alphabet = alphabet::SymbolDenormalize::denormalizeRankedAlphabet < SymbolType > ( std::move ( value ).getAlphabet ( ) );
		ext::vector < common::ranked_symbol < SymbolType > > content = alphabet::SymbolDenormalize::denormalizeRankedSymbols < SymbolType > ( std::move ( value ).getContent ( ) );
		return tree::PrefixRankedNonlinearPattern < SymbolType > ( std::move ( wildcard ), std::move ( nonlinearAlphabet ), std::move ( alphabet ), std::move ( content ) );
	}

	static tree::PrefixRankedNonlinearPattern < > normalize ( tree::PrefixRankedNonlinearPattern < SymbolType > && value ) {
		common::ranked_symbol < DefaultSymbolType > wildcard = alphabet::SymbolNormalize::normalizeRankedSymbol ( std::move ( value ).getSubtreeWildcard ( ) );
		ext::set < common::ranked_symbol < DefaultSymbolType > > nonlinearAlphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getNonlinearVariables ( ) );
		ext::set < common::ranked_symbol < DefaultSymbolType > > alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getAlphabet ( ) );
		ext::vector < common::ranked_symbol < DefaultSymbolType > > content = alphabet::SymbolNormalize::normalizeRankedSymbols ( std::move ( value ).getContent ( ) );
		return tree::PrefixRankedNonlinearPattern < > ( std::move ( wildcard ), std::move ( nonlinearAlphabet ), std::move ( alphabet ), std::move ( content ) );
	}

	static std::unique_ptr < type_details_base > type ( const tree::PrefixRankedNonlinearPattern < SymbolType > & arg ) {
		core::unique_ptr_set < type_details_base > subTypesSymbol;
		for ( const common::ranked_symbol < SymbolType > & item : arg.getAlphabet ( ) )
			subTypesSymbol.insert ( type_util < SymbolType >::type ( item.getSymbol ( ) ) );

		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_variant_type::make_variant ( std::move ( subTypesSymbol ) ) );
		return std::make_unique < type_details_template > ( "tree::PrefixRankedNonlinearPattern", std::move ( sub_types_vec ) );
	}
};

template < class SymbolType >
struct type_details_retriever < tree::PrefixRankedNonlinearPattern < SymbolType > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < SymbolType >::get ( ) );
		return std::make_unique < type_details_template > ( "tree::PrefixRankedNonlinearPattern", std::move ( sub_types_vec ) );
	}
};

} /* namespace core */

extern template class tree::PrefixRankedNonlinearPattern < >;
extern template class abstraction::ValueHolder < tree::PrefixRankedNonlinearPattern < > >;
extern template const tree::PrefixRankedNonlinearPattern < > & abstraction::retrieveValue < const tree::PrefixRankedNonlinearPattern < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
extern template class registration::DenormalizationRegisterImpl < const tree::PrefixRankedNonlinearPattern < > & >;
extern template class registration::NormalizationRegisterImpl < tree::PrefixRankedNonlinearPattern < > >;

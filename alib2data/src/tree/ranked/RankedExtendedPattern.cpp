#include "RankedExtendedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::RankedExtendedPattern < >;
template class abstraction::ValueHolder < tree::RankedExtendedPattern < > >;
template const tree::RankedExtendedPattern < > & abstraction::retrieveValue < const tree::RankedExtendedPattern < > & > ( const std::shared_ptr < abstraction::Value > & param, bool move );
template class registration::DenormalizationRegisterImpl < const tree::RankedExtendedPattern < > & >;
template class registration::NormalizationRegisterImpl < tree::RankedExtendedPattern < > >;

namespace {

auto components = registration::ComponentRegister < tree::RankedExtendedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::RankedExtendedPattern < > > ( );

auto RankedExtendedPatternFromRankedPattern = registration::CastRegister < tree::RankedExtendedPattern < >, tree::RankedPattern < > > ( );

} /* namespace */

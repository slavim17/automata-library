#include "InitialStateLabel.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace label {

InitialStateLabel::InitialStateLabel() = default;

ext::ostream & operator << ( ext::ostream & out, const InitialStateLabel & ) {
	return out << "(InitialStateLabel)";
}

} /* namespace label */

namespace core {

label::InitialStateLabel type_util < label::InitialStateLabel >::denormalize ( label::InitialStateLabel && arg ) {
	return arg;
}

label::InitialStateLabel type_util < label::InitialStateLabel >::normalize ( label::InitialStateLabel && arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < label::InitialStateLabel >::type ( const label::InitialStateLabel & ) {
	return std::make_unique < type_details_type > ( "label::InitialStateLabel" );
}

std::unique_ptr < type_details_base > type_details_retriever < label::InitialStateLabel >::get ( ) {
	return std::make_unique < type_details_type > ( "label::InitialStateLabel" );
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < label::InitialStateLabel > ( );

} /* namespace */

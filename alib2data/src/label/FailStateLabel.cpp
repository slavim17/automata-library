#include "FailStateLabel.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace label {

FailStateLabel::FailStateLabel() = default;

ext::ostream & operator << ( ext::ostream & out, const FailStateLabel & ) {
	return out << "(FailStateLabel)";
}

} /* namespace label */

namespace core {

label::FailStateLabel type_util < label::FailStateLabel >::denormalize ( label::FailStateLabel && arg ) {
	return arg;
}

label::FailStateLabel type_util < label::FailStateLabel >::normalize ( label::FailStateLabel && arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < label::FailStateLabel >::type ( const label::FailStateLabel & ) {
	return std::make_unique < type_details_type > ( "label::FailStateLabel" );
}

std::unique_ptr < type_details_base > type_details_retriever < label::FailStateLabel >::get ( ) {
	return std::make_unique < type_details_type > ( "label::FailStateLabel" );
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < label::FailStateLabel > ( );

} /* namespace */

/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <compare>

#include <object/Object.h>
#include <object/ObjectFactory.h>

#include <core/type_util.hpp>
#include <core/type_details_base.hpp>

namespace label {

/**
 * \brief
 * Represents label of the fail state of an automaton.
 */
class FailStateLabel {
public:
	/**
	 * \brief
	 * Creates a new instance of the label.
	 */
	explicit FailStateLabel ( );

	/**
	 * The three way comparison implementation
	 *
	 * \param other the other instance
	 *
	 * \returns the ordering between this object and the @p other.
	 */
	std::strong_ordering operator <=> ( const FailStateLabel & ) const {
		return std::strong_ordering::equal;
	}

	/**
	 * The equality comparison implementation.
	 *
	 * \param other the other object to compare with.
	 *
	 * \returns true if this and other objects are equal, false othervise
	 */
	bool operator == ( const FailStateLabel & ) const {
		return true;
	}

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param os ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend ext::ostream & operator << ( ext::ostream & out, const FailStateLabel & instance );

	/**
	 * \brief Factory for the label construction of the label based on given type.
	 */
	template < typename Base >
	static inline Base instance ( );
};

template < typename Base >
inline Base FailStateLabel::instance ( ) {
	if constexpr ( std::is_integral_v < Base > ) {
		return -1;
	} else if constexpr ( std::is_same_v < Base, std::string > ) {
		return std::string ( "fail" );
	} else if constexpr ( std::is_same_v < Base, FailStateLabel > ) {
		return FailStateLabel ( );
	} else if constexpr ( std::is_same_v < Base, object::Object > ) {
		return object::ObjectFactory < >::construct ( FailStateLabel ( ) );
	} else {
		static_assert ( ! std::is_same_v < Base, Base >, "Unsupported type of instance" );
	}
}

} /* namespace label */

namespace core {

template < >
struct type_util < label::FailStateLabel > {
	static label::FailStateLabel denormalize ( label::FailStateLabel && arg );

	static label::FailStateLabel normalize ( label::FailStateLabel && arg );

	static std::unique_ptr < type_details_base > type ( const label::FailStateLabel & arg );
};

template < >
struct type_details_retriever < label::FailStateLabel > {
	static std::unique_ptr < type_details_base > get ( );
};

} /* namespace core */

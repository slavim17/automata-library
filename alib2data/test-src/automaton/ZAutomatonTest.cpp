#include <catch2/catch.hpp>

#include "automaton/TA/ArcFactoredNondeterministicZAutomaton.h"
#include "automaton/TA/NondeterministicZAutomaton.h"
#include "automaton/xml/TA/NondeterministicZAutomaton.h"

TEST_CASE ( "Z-Automaton", "[unit][data][automaton]" )
{
	SECTION ("Nondeterministic Z-Automata, accept trees where all node's second-to-right child has same label as the node")
	{
		char a = 'a';
		char b = 'b';
		char c = 'c';
		ext::set < char > sigma = { a, b, c };

		std::string pa = "pa";
		std::string pb = "pb";
		std::string pc = "pc";
		std::string qa = "qa";
		std::string qb = "qb";
		std::string qc = "qc";
		std::string ra = "ra";
		std::string rb = "rb";
		std::string rc = "rc";
		std::string qf = "p";

		std::map < char, std::string > p { { a, pa }, { b, pb }, { c, pc } };
		std::map < char, std::string > q { { a, qa }, { b, qb }, { c, qc } };
		std::map < char, std::string > r { { a, ra }, { b, rb }, { c, rc } };

		SECTION ("Nondeterministic Z-Automaton")
		{
			automaton::NondeterministicZAutomaton < char, std::string > automaton;

			automaton.setStates ( { pa, pb, pc, qa, qb, qc } );
			automaton.setInputAlphabet ( sigma );

			for ( auto x : sigma ) {
				for ( auto y : sigma ) {
					automaton.addTransition ( x      , { y }      , q [ x ] );
					automaton.addTransition ( x      , { p [ y ] }, q [ x ] );

					automaton.addTransition ( q [ x ], { y }      , q [ x ] );
					automaton.addTransition ( q [ x ], { p [ y ] }, q [ x ] );

					automaton.addTransition ( x      , { x, y }            , p [ x ] );
					automaton.addTransition ( x      , { p [ x ], y }      , p [ x ] );
					automaton.addTransition ( x      , { x, p [ y ] }      , p [ x ] );
					automaton.addTransition ( x      , { p [ x ], p [ y ] }, p [ x ] );

					automaton.addTransition ( q [ x ], { x, y }            , p [ x ] );
					automaton.addTransition ( q [ x ], { p [ x ], y }      , p [ x ] );
					automaton.addTransition ( q [ x ], { x, p [ y ] }      , p [ x ] );
					automaton.addTransition ( q [ x ], { p [ x ], p [ y ] }, p [ x ] );
				}
			}

			for ( char x : sigma ) {
				automaton.addFinalState ( p [ x ] );
			}

			CHECK_THROWS_AS ( automaton.removeInputSymbol ( a ), exception::CommonException );
			CHECK_NOTHROW ( automaton.addInputSymbol ( 'd' ) );
			CHECK_NOTHROW ( automaton.removeInputSymbol ( 'd' ) );
			// factory::XmlDataFactory::toFile ( automaton, "/tmp/ZAutomaton1.xml" );
		}

		SECTION ( "Nondeterministic Z-Automata with transitions in arc-factored normal form" )
		{
			automaton::NondeterministicZAutomaton < char, std::string > automaton;

			automaton.setStates ( { pa, pb, pc, qa, qb, qc, ra, rb, rc, qf } );
			automaton.setInputAlphabet ( sigma );

			for ( char x : sigma ) {
				automaton.addTransition ( x, { }, p [ x ] );
				automaton.addTransition ( x, { }, q [ x ] );
			}

			for ( char x : sigma ) {
				for ( char y : sigma ) {
					automaton.addTransition ( q [ x ], { p [ y ] }, q [ x ] );
					automaton.addTransition ( q [ x ], { p [ x ] }, r [ x ] );
					automaton.addTransition ( r [ x ], { p [ y ] }, p [ x ] );
					automaton.addTransition ( r [ x ], { p [ y ] }, qf );
				}
			}

			automaton.addFinalState ( qf );

			CHECK_THROWS_AS ( automaton.removeInputSymbol ( a ), exception::CommonException );
			CHECK_NOTHROW ( automaton.addInputSymbol ( 'd' ) );
			CHECK_NOTHROW ( automaton.removeInputSymbol ( 'd' ) );

			//	factory::XmlDataFactory::toFile ( automaton, "/tmp/ZAutomaton2.xml" );
		}

		SECTION ( "Nondeterministic Arc-Factored Normal Form Z-Automata" )
		{
			automaton::ArcFactoredNondeterministicZAutomaton < char, std::string > automaton;

			automaton.setStates ( { pa, pb, pc, qa, qb, qc, ra, rb, rc, qf } );
			automaton.setInputAlphabet ( sigma );

			for ( char x : sigma ) {
				automaton.addTransition ( x, p [ x ] );
				automaton.addTransition ( x, q [ x ] );
			}

			for ( char x : sigma ) {
				for ( char y : sigma ) {
					automaton.addTransition ( { q [ x ], p [ y ] }, q [ x ] );
					automaton.addTransition ( { q [ x ], p [ x ] }, r [ x ] );
					automaton.addTransition ( { r [ x ], p [ y ] }, p [ x ] );
					automaton.addTransition ( { r [ x ], p [ y ] }, qf );
				}
			}

			automaton.addFinalState ( qf );

			CHECK_THROWS_AS ( automaton.removeInputSymbol ( a ), exception::CommonException );
			CHECK_NOTHROW ( automaton.addInputSymbol ( 'd' ) );
			CHECK_NOTHROW ( automaton.removeInputSymbol ( 'd' ) );

			//	factory::XmlDataFactory::toFile ( automaton, "/tmp/ZAutomaton2.xml" );
		}
	}
}

#include <Models/AlgorithmModelBox.hpp>

#include <string>

#include <registry/AlgorithmRegistry.hpp>

AlgorithmModelBox::AlgorithmModelBox(Algorithm* p_algorithm)
    : ModelBox(ModelType::Algorithm, p_algorithm->getInputCount())
    , algorithm(p_algorithm) {
        if ( algorithm == nullptr )
		throw std::runtime_error ( "Algorithm was null" );
    }

std::shared_ptr<abstraction::Value> AlgorithmModelBox::evaluate() {
    std::vector<std::shared_ptr<abstraction::Value>> params;
    for (auto* input: this->inputs)
    {
        if (!input)
            return nullptr;
        params.push_back(input->getCachedResultOrEvaluate());
        if (!params.back())
            return nullptr;
    }
    this->result = this->algorithm->execute(params);
    return this->result;
}

std::string AlgorithmModelBox::getName() const {
    return this->algorithm->getPrettyName();
}

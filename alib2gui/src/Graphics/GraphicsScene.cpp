#include <Graphics/GraphicsScene.hpp>

#include <QGraphicsView>

GraphicsScene::GraphicsScene(QObject* parent)
    : QGraphicsScene(parent)
{}

void GraphicsScene::wheelEvent(QGraphicsSceneWheelEvent* event) {

    QGraphicsView* view = views().first();
    qreal factor = 1.0 + event->delta() / 1200.0;
    view->scale(factor, factor);
    if (factor > 1.0)
        view->centerOn(event->scenePos());
    event->accept();
}
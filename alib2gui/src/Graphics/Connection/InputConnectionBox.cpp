#include <Graphics/Connection/InputConnectionBox.hpp>

InputConnectionBox::InputConnectionBox(GraphicsBox* parent, size_t p_slot)
    : ConnectionBox(parent, ConnectionBox::Type::Input)
    , slot(p_slot)
{}

void InputConnectionBox::setConnection(Connection* p_connection) {
    Q_ASSERT((this->connection == nullptr) != (p_connection == nullptr));
    this->connection = p_connection;
}

void InputConnectionBox::on_Disconnect() {
    this->connection->destroy();
    Q_ASSERT(!this->connection);
}

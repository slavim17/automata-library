# install paths
# -------------

include(GNUInstallDirs)

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set (CMAKE_INSTALL_PREFIX /usr)
endif()

message ( STATUS "[Install] Install prefix: ${CMAKE_INSTALL_PREFIX}")

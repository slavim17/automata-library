#include "UnboundedRegExp.h"
#include <regexp/RegExp.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < regexp::UnboundedRegExp < > > ( );
auto stringReader = registration::StringReaderRegister < regexp::RegExp, regexp::UnboundedRegExp < > > ( );

} /* namespace */

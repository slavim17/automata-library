#include "StringFromStringLexer.h"

namespace string {

StringFromStringLexer::Token StringFromStringLexer::next(ext::istream& input) {
	StringFromStringLexer::Token token;
	token.type = TokenType::ERROR;
	token.value = "";
	token.raw = "";
	char character;

L0:
	character = static_cast < char > ( input.get ( ) );
	if ( input.eof ( ) || character == EOF ) {
		token.type = TokenType::TEOF;
		return token;
	} else if ( ext::isspace ( character ) ) {
		token.raw += character;
		goto L0;
	} else if(character == '<') {
		token.type = TokenType::LESS;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '>') {
		token.type = TokenType::GREATER;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '"') {
		token.type = TokenType::QUOTE;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '#') {
		token.value += character;
		token.raw += character;
		goto L1;
	} else {
		input.clear ( );
		input.unget ( );
		putback(input, token);
		token.raw = "";
		token.type = TokenType::ERROR;
		return token;
	}

L1:
	character = static_cast < char > ( input.get ( ) );
	if(input.eof()) {
		token.type = TokenType::TEOF;
		return token;
	} else if(character == '$') {
		token.type = TokenType::TERM;
		token.value += character;
		token.raw += character;
		return token;
	} else {
		input.clear ( );
		input.unget ( );
		putback(input, token);
		token.raw = "";
		token.type = TokenType::ERROR;
		return token;
	}
}

} /* namespace string */

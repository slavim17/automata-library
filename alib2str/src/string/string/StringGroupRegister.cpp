#include <string/String.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister < string::String > ( );

} /* namespace */

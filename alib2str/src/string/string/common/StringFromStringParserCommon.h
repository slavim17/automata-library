#pragma once

#include <alib/vector>

#include <core/stringApi.hpp>

#include <string/StringFromStringLexer.h>

#include <alphabet/End.h>

namespace string {

class StringFromStringParserCommon {
public:
	template < class SymbolType >
	static ext::vector < SymbolType > parseContent ( ext::istream & input );
};

template < class SymbolType >
ext::vector < SymbolType > StringFromStringParserCommon::parseContent ( ext::istream & input ) {
	ext::vector < SymbolType > data;
	string::StringFromStringLexer::Token token = string::StringFromStringLexer::next ( input );
	while ( token.type != string::StringFromStringLexer::TokenType::GREATER && token.type != string::StringFromStringLexer::TokenType::QUOTE ) {
		if ( token.type == string::StringFromStringLexer::TokenType::TERM ) {
			data.push_back ( alphabet::End::instance < SymbolType > ( ) );
		} else {
			string::StringFromStringLexer::putback ( input, token );
			data.push_back ( core::stringApi < SymbolType >::parse ( input ) );
		}

		token = string::StringFromStringLexer::next ( input );
	}
	string::StringFromStringLexer::putback ( input, token );
	return data;
}

} /* namespace string */


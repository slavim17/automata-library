#include "CyclicString.h"
#include <string/String.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < string::CyclicString < > > ( );
auto stringReader = registration::StringReaderRegister < string::String, string::CyclicString < > > ( );

} /* namespace */

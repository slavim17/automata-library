#pragma once

#include <tree/unranked/UnrankedNonlinearPattern.h>
#include <core/stringApi.hpp>

#include <tree/TreeFromStringLexer.h>

#include <tree/string/common/TreeFromStringParserCommon.h>
#include <tree/string/common/TreeToStringComposerCommon.h>

namespace core {

template<class SymbolType >
struct stringApi < tree::UnrankedNonlinearPattern < SymbolType > > {
	static tree::UnrankedNonlinearPattern < SymbolType > parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const tree::UnrankedNonlinearPattern < SymbolType > & tree );
};

template<class SymbolType >
tree::UnrankedNonlinearPattern < SymbolType > stringApi < tree::UnrankedNonlinearPattern < SymbolType > >::parse ( ext::istream & input ) {
	tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next ( input );
	if ( token.type != tree::TreeFromStringLexer::TokenType::UNRANKED_NONLINEAR_PATTERN )
		throw exception::CommonException ( "Unrecognised UNRANKED_NONLINEAR_PATTERN token." );

	ext::set < SymbolType > nonlinearVariables;
	bool isPattern = false;
	bool isExtendedPattern = false;

	ext::tree < SymbolType > content = tree::TreeFromStringParserCommon::parseUnrankedContent < SymbolType > ( input, isPattern, isExtendedPattern, nonlinearVariables );

	if ( isExtendedPattern )
		throw exception::CommonException ( "Unexpected node wildcards recognised" );

	return tree::UnrankedNonlinearPattern < SymbolType > ( alphabet::Wildcard::instance < SymbolType > ( ), alphabet::Gap::instance < SymbolType > ( ), nonlinearVariables, content );
}

template<class SymbolType >
bool stringApi < tree::UnrankedNonlinearPattern < SymbolType > >::first ( ext::istream & input ) {
	tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next ( input );
	bool res = token.type == tree::TreeFromStringLexer::TokenType::UNRANKED_NONLINEAR_PATTERN;
	tree::TreeFromStringLexer::putback ( input, token );
	return res;
}

template<class SymbolType >
void stringApi < tree::UnrankedNonlinearPattern < SymbolType > >::compose ( ext::ostream &, const tree::UnrankedNonlinearPattern < SymbolType > & ) {
	throw exception::CommonException ( "Unimplemented." );
}

} /* namespace core */


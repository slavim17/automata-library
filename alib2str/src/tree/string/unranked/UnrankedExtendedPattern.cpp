#include "UnrankedExtendedPattern.h"
#include <tree/Tree.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < tree::UnrankedExtendedPattern < > > ( );
auto stringReader = registration::StringReaderRegister < tree::Tree, tree::UnrankedExtendedPattern < > > ( );

} /* namespace */

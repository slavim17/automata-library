#include "RankedExtendedPattern.h"
#include <tree/Tree.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < tree::RankedExtendedPattern < > > ( );
auto stringReader = registration::StringReaderRegister < tree::Tree, tree::RankedExtendedPattern < > > ( );

} /* namespace */

#pragma once

#include <grammar/ContextFree/CFG.h>
#include <core/stringApi.hpp>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

namespace core {

template < class TerminalSymbolType, class NonterminalSymbolType >
struct stringApi < grammar::CFG < TerminalSymbolType, NonterminalSymbolType > > {
	static grammar::CFG < TerminalSymbolType, NonterminalSymbolType > parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const grammar::CFG < TerminalSymbolType, NonterminalSymbolType > & grammar );
};

template < class TerminalSymbolType, class NonterminalSymbolType >
grammar::CFG < TerminalSymbolType, NonterminalSymbolType > stringApi < grammar::CFG < TerminalSymbolType, NonterminalSymbolType > >::parse ( ext::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
	if(token.type != grammar::GrammarFromStringLexer::TokenType::CFG)
		throw exception::CommonException("Unrecognised CFG token.");

	return grammar::GrammarFromStringParserCommon::parseCFLikeGrammar < grammar::CFG < TerminalSymbolType, NonterminalSymbolType > > ( input );
}

template < class TerminalSymbolType, class NonterminalSymbolType >
bool stringApi < grammar::CFG < TerminalSymbolType, NonterminalSymbolType > >::first ( ext::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next ( input );
	bool res = token.type == grammar::GrammarFromStringLexer::TokenType::CFG;
	grammar::GrammarFromStringLexer::putback ( input, token );
	return res;
}

template < class TerminalSymbolType, class NonterminalSymbolType >
void stringApi < grammar::CFG < TerminalSymbolType, NonterminalSymbolType > >::compose ( ext::ostream & output, const grammar::CFG < TerminalSymbolType, NonterminalSymbolType > & grammar ) {
	output << "CFG";
	grammar::GrammarToStringComposerCommon::composeCFLikeGrammar ( output, grammar );
}

} /* namespace core */


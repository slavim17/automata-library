#include "CNF.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::CNF < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::CNF < > > ( );

} /* namespace */

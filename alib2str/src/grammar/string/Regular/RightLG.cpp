#include "RightLG.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::RightLG < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::RightLG < > > ( );

} /* namespace */

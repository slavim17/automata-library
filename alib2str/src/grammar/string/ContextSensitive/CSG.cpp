#include "CSG.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::CSG < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::CSG < > > ( );

} /* namespace */

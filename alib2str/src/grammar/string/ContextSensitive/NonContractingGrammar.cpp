#include "NonContractingGrammar.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::NonContractingGrammar < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::NonContractingGrammar < > > ( );

} /* namespace */

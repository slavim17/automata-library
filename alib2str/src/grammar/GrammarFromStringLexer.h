#pragma once

#include <ext/istream>

#include <alib/string>

#include <common/lexer.hpp>

namespace grammar {

class GrammarFromStringLexer : public ext::Lexer < GrammarFromStringLexer > {
public:
	enum class TokenType {
		SET_BEGIN,
		SET_END,
		COMMA,
		TUPLE_BEGIN,
		TUPLE_END,
		SEPARATOR,
		EPSILON,
		MAPS_TO,
		RIGHT_RG,
		LEFT_RG,
		RIGHT_LG,
		LEFT_LG,
		LG,
		CFG,
		EPSILON_FREE_CFG,
		GNF,
		CNF,
		CSG,
		NON_CONTRACTING_GRAMMAR,
		CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR,
		UNRESTRICTED_GRAMMAR,
		TEOF,
		ERROR,
	};

	static Token next(ext::istream& input);
};

} /* namepsace grammar */


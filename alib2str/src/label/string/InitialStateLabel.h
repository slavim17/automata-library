#pragma once

#include <label/InitialStateLabel.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < label::InitialStateLabel > {
	static label::InitialStateLabel parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const label::InitialStateLabel & label );
};

} /* namespace core */


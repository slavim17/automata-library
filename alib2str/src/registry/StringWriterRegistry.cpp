#include <registry/StringWriterRegistry.hpp>
#include <exception/CommonException.h>

namespace abstraction {

ext::map < core::type_details, std::unique_ptr < StringWriterRegistry::Entry > > & StringWriterRegistry::getEntries ( ) {
	static ext::map < core::type_details, std::unique_ptr < Entry > > writers;
	return writers;
}

void StringWriterRegistry::unregisterStringWriter ( const core::type_details & param ) {
	if ( getEntries ( ).erase ( param ) == 0u )
		throw std::invalid_argument ( "Entry " + ext::to_string ( param ) + " not registered." );
}

void StringWriterRegistry::registerStringWriter ( core::type_details param, std::unique_ptr < Entry > entry ) {
	auto iter = getEntries ( ).insert ( std::make_pair ( std::move ( param ), std::move ( entry ) ) );
	if ( ! iter.second )
		throw std::invalid_argument ( "Entry " + ext::to_string ( iter.first->first ) + " already registered." );
}

std::unique_ptr < abstraction::OperationAbstraction > StringWriterRegistry::getAbstraction ( const core::type_details & param ) {
	auto type = getEntries ( ).find ( param );
	if ( type == getEntries ( ).end ( ) )
		throw exception::CommonException ( "Entry " + ext::to_string ( param ) + " not available." );

	return type->second->getAbstraction ( );
}

} /* namespace abstraction */

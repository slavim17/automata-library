#include "DFTA.h"
#include <automaton/Automaton.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < automaton::DFTA < > > ( );
auto stringReader = registration::StringReaderRegister < automaton::Automaton, automaton::DFTA < > > ( );

} /* namespace */

#include "AutomatonFromStringParserCommon.h"

namespace automaton {

void AutomatonFromStringParserCommon::initialFinalState(ext::istream& input, bool& rightArrow, bool& leftArrow) {
	rightArrow = false;
	leftArrow = false;

	AutomatonFromStringLexer::Token token = AutomatonFromStringLexer::next(input);
	if(token.type == AutomatonFromStringLexer::TokenType::IN) {
		rightArrow = true;
		token = AutomatonFromStringLexer::next(input);

		if(token.type == AutomatonFromStringLexer::TokenType::OUT) {
			leftArrow = true;
		} else {
			AutomatonFromStringLexer::putback(input, token);
		}
	} else if(token.type == AutomatonFromStringLexer::TokenType::OUT) {
		leftArrow = true;
		token = AutomatonFromStringLexer::next(input);

		if(token.type == AutomatonFromStringLexer::TokenType::IN) {
			rightArrow = true;
		} else {
			AutomatonFromStringLexer::putback(input, token);
		}
	} else {
		AutomatonFromStringLexer::putback(input, token);
	}
}

} /* namespace automaton */

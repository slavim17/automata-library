#pragma once

#include <ostream>
#include <alib/vector>

#include <core/stringApi.hpp>

namespace automaton {

struct AutomatonToStringComposerCommon {
	template < class Type >
	static void composeList ( ext::ostream & output, const ext::vector < Type > & list );
};

template < class Type >
void AutomatonToStringComposerCommon::composeList ( ext::ostream & output, const ext::vector < Type > & list ) {
	output << '[';
	bool first = true;
	for ( const Type & value : list ) {
		if ( ! first )
			output << ", ";
		first = false;

		core::stringApi < Type >::compose ( output, value );
	}
	output << ']';
}

} /* namespace automaton */


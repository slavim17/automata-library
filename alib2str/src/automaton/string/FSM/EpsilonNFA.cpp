#include "EpsilonNFA.h"
#include <automaton/Automaton.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < automaton::EpsilonNFA < > > ( );
auto stringReader = registration::StringReaderRegister < automaton::Automaton, automaton::EpsilonNFA < > > ( );

} /* namespace */

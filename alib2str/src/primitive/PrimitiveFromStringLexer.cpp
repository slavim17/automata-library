#include "PrimitiveFromStringLexer.h"

namespace primitive {

PrimitiveFromStringLexer::Token PrimitiveFromStringLexer::next(ext::istream& input) {
	PrimitiveFromStringLexer::Token token;
	token.type = TokenType::ERROR;
	token.value = "";
	token.raw = "";
	char character;

L0:
	character = static_cast < char > ( input.get ( ) );
	if ( input.eof ( ) ) {
		token.type = TokenType::TEOF;
		return token;
	} else if ( ext::isspace ( character ) ) {
		token.raw += character;
		goto L0;
	} else if((character >= 'a' && character <= 'z') || (character >= 'A' && character <= 'Z') || character == '_' ) {
		token.type = TokenType::STRING;
		token.value += character;
		token.raw += character;
		goto L1;
	} else if(character >= '0' && character <= '9') {
		token.type = TokenType::INTEGER;
		token.value += character;
		token.raw += character;
		goto L2;
	} else {
		input.clear ( );
		input.unget ( );
		putback(input, token);
		token.raw = "";
		token.type = TokenType::ERROR;
		return token;
	}
L1:
	character = static_cast < char > ( input.get ( ) );
	if(input.eof()) {
		return token;
	} else if ( ( character >= 'a' && character <= 'z' ) || ( character >= 'A' && character <= 'Z' ) || character == '_' || ( character >= '0' && character <= '9' ) ) {
		token.value += character;
		token.raw += character;
		goto L1;
	} else {
		input.clear ( );
		input.unget ( );
		return token;
	}
L2:
	character = static_cast < char > ( input.get ( ) );
	if(input.eof()) {
		return token;
	} else if(character >= '0' && character <= '9') {
		token.value += character;
		token.raw += character;
		goto L2;
	} else {
		input.clear ( );
		input.unget ( );
		return token;
	}
}

} /* namespace primitive */

#pragma once

#include <core/stringApi.hpp>

#include <primitive/PrimitiveFromStringLexer.h>

namespace core {

template < >
struct stringApi < unsigned long > {
	static unsigned long parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, unsigned long primitive );
};

} /* namespace core */


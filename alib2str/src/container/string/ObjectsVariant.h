#pragma once

#include <alib/variant>
#include <core/stringApi.hpp>

namespace core {

template < class ... Types >
struct stringApi < ext::variant < Types ... > > {
	static ext::variant < Types ... > parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const ext::variant < Types ... > & container );

private:
	class VariantStringApiCallback {
		ext::ostream & m_out;

	public:
		VariantStringApiCallback ( ext::ostream & out ) : m_out ( out ) {
		}

		template < class ValueType >
		void operator ( ) ( const ValueType & value ) {
			stringApi < ValueType >::compose ( m_out, value );
		}
	};

	template < class T, class R, class ... Ts >
	static ext::variant < Types ... > parse ( ext::istream & input ) {
		if ( stringApi < T >::first ( input ) )
			return ext::variant < Types ... > ( stringApi < T >::parse ( input ) );
		else
			return parse < R, Ts ... > ( input );
	}

	template < class T >
	static ext::variant < Types ... > parse ( ext::istream & input ) {
		return ext::variant < Types ... > ( stringApi < T >::parse ( input ) );
	}
};

template < class ... Types >
ext::variant < Types ... > stringApi < ext::variant < Types ... > >::parse ( ext::istream & input ) {
	return parse < Types ... > ( input );
}

template < class ... Types >
bool stringApi < ext::variant < Types ... > >::first ( ext::istream & input ) {
	return ( ... && stringApi < Types >::first ( input ) );
}

template < class ... Types >
void stringApi < ext::variant < Types ... > >::compose ( ext::ostream & output, const ext::variant < Types ... > & container ) {
	ext::visit ( VariantStringApiCallback ( output ), container );
}

} /* namespace core */


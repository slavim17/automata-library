#include "ContainerFromStringLexer.h"

namespace container {

ContainerFromStringLexer::Token ContainerFromStringLexer::next(ext::istream& input) {
	ContainerFromStringLexer::Token token;
	token.type = TokenType::ERROR;
	token.value = "";
	token.raw = "";
	char character;

L0:
	character = static_cast < char > ( input.get ( ) );
	if ( input.eof ( ) || character == EOF ) {
		token.type = TokenType::TEOF;
		return token;
	} else if ( ext::isspace ( character ) ) {
		token.raw += character;
		goto L0;
	} else if(character == '{') {
		token.type = TokenType::SET_BEGIN;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '}') {
		token.type = TokenType::SET_END;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '[') {
		token.type = TokenType::VECTOR_BEGIN;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == ']') {
		token.type = TokenType::VECTOR_END;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == '(') {
		token.type = TokenType::PAIR_BEGIN;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == ')') {
		token.type = TokenType::PAIR_END;
		token.value += character;
		token.raw += character;
		return token;
	} else if(character == ',') {
		token.type = TokenType::COMMA;
		token.value += character;
		token.raw += character;
		return token;
	} else {
		input.clear ( );
		input.unget ( );
		putback(input, token);
		token.raw = "";
		token.type = TokenType::ERROR;
		return token;
	}
}

} /* namespace container */

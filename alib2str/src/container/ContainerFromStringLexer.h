#pragma once

#include <ext/istream>

#include <alib/string>

#include <common/lexer.hpp>

namespace container {

class ContainerFromStringLexer : public ext::Lexer < ContainerFromStringLexer > {
public:
	enum class TokenType {
		VECTOR_BEGIN,
		VECTOR_END,
		SET_BEGIN,
		SET_END,
		PAIR_BEGIN,
		PAIR_END,
		COMMA,
		TEOF,
		ERROR
	};

	static Token next(ext::istream& input);
};

} /* namespace container */


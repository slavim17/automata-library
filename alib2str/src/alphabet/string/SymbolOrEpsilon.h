#pragma once

#include <common/symbol_or_epsilon.hpp>
#include <core/stringApi.hpp>

namespace core {

template < class SymbolType >
struct stringApi < common::symbol_or_epsilon < SymbolType > > {
	static common::symbol_or_epsilon < SymbolType > parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const common::symbol_or_epsilon < SymbolType > & symbol );
};

template < class SymbolType >
common::symbol_or_epsilon < SymbolType > stringApi < common::symbol_or_epsilon < SymbolType > >::parse ( ext::istream & ) {
	throw exception::CommonException("Parsing of symbol or epsilon from string not implemented.");
}

template < class SymbolType >
bool stringApi < common::symbol_or_epsilon < SymbolType > >::first ( ext::istream & ) {
	return false;
}

template < class SymbolType >
void stringApi < common::symbol_or_epsilon < SymbolType > >::compose ( ext::ostream & output, const common::symbol_or_epsilon < SymbolType > & symbol ) {
	if ( symbol.is_epsilon ( ) )
		output << "#E";
	else
		core::stringApi < SymbolType >::compose ( output, symbol.getSymbol ( ) );
}

} /* namespace core */


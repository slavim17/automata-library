#pragma once

#include <alphabet/Gap.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::Gap > {
	static alphabet::Gap parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::Gap & symbol );
};

} /* namespace core */

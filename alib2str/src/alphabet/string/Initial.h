#pragma once

#include <alphabet/Initial.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::Initial > {
	static alphabet::Initial parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::Initial & symbol );
};

} /* namespace core */


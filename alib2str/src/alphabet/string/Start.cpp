#include "Start.h"
#include <alphabet/Start.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::Start stringApi < alphabet::Start >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing Start from string not implemented");
}

bool stringApi < alphabet::Start >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::Start >::compose ( ext::ostream & output, const alphabet::Start & ) {
	output << "#^";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::Start > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::Start > ( );

} /* namespace */

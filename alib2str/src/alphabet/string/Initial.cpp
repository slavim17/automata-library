#include "Initial.h"
#include <alphabet/Initial.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::Initial stringApi < alphabet::Initial >::parse ( ext::istream & ) {
	throw exception::CommonException ( "parsing Initial from string not implemented" );
}

bool stringApi < alphabet::Initial >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::Initial >::compose ( ext::ostream & output, const alphabet::Initial & ) {
	output << "#I";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::Initial > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::Initial > ( );

} /* namespace */

#pragma once

#include <alphabet/End.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::End > {
	static alphabet::End parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::End & symbol );
};

} /* namespace core */


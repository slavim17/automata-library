#include "VariablesBar.h"
#include <alphabet/VariablesBar.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::VariablesBar stringApi < alphabet::VariablesBar >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing VariablesBar from string not implemented");
}

bool stringApi < alphabet::VariablesBar >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::VariablesBar >::compose ( ext::ostream & output, const alphabet::VariablesBar & ) {
	output << "#/";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::VariablesBar > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::VariablesBar > ( );

} /* namespace */

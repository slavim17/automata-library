#pragma once

#include <alphabet/VariablesBar.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::VariablesBar > {
	static alphabet::VariablesBar parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::VariablesBar & symbol );
};

} /* namespace core */


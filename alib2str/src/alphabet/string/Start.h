#pragma once

#include <alphabet/Start.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::Start > {
	static alphabet::Start parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::Start & symbol );
};

} /* namespace core */


#pragma once

#include <alphabet/Wildcard.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::Wildcard > {
	static alphabet::Wildcard parse ( ext::istream & input );
	static bool first ( ext::istream & input );
	static void compose ( ext::ostream & output, const alphabet::Wildcard & symbol );
};

} /* namespace core */


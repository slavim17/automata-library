#include "End.h"
#include <alphabet/End.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::End stringApi < alphabet::End >::parse ( ext::istream & ) {
	throw exception::CommonException("parsing End from string not implemented");
}

bool stringApi < alphabet::End >::first ( ext::istream & ) {
	return false;
}

void stringApi < alphabet::End >::compose ( ext::ostream & output, const alphabet::End & ) {
	output << "#$";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::End > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::End > ( );

} /* namespace */

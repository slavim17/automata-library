#pragma once

#include <ext/functional>
#include <ext/memory>
#include <ext/algorithm>
#include <ext/istream>
#include <ext/typeinfo>

#include <alib/list>
#include <alib/map>
#include <alib/string>

#include <object/Object.h>
#include <object/ObjectFactory.h>

#include "exception/CommonException.h"

namespace core {

template < typename T >
struct stringApi;

template < >
struct stringApi < object::Object > {
public:
	class GroupReader {
	public:
		virtual object::Object parse ( ext::istream & input ) = 0;

		virtual ~GroupReader ( ) = default;
	};

private:
	static ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < GroupReader > > > & parseFunctions ( );

	template < class Type >
	class ReaderRegister : public GroupReader {
	public:
		~ReaderRegister ( ) override = default;

		object::Object parse ( ext::istream & input ) override {
			return object::ObjectFactory < >::construct ( stringApi < Type >::parse ( input ) );
		}
	};

public:
	class GroupWriter {
	public:
		virtual void compose ( ext::ostream & output, const object::Object & group ) = 0;

		virtual ~GroupWriter ( ) = default;
	};

private:
	static ext::map < core::type_details, std::unique_ptr < GroupWriter > > & composeFunctions ( );

	template < class Type >
	class WriterRegister : public GroupWriter {
	public:
		~WriterRegister ( ) override = default;

		void compose ( ext::ostream & output, const object::Object & group ) override {
			stringApi < Type >::compose ( output, static_cast < const object::AnyObject < Type > & > ( group.getData ( ) ).getData ( ) );
		}
	};

public:
	static void unregisterStringReader ( ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < GroupReader > > >::const_iterator iter );

	static ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < GroupReader > > >::const_iterator registerStringReader ( std::function < bool ( ext::istream & ) > first, std::unique_ptr < GroupReader > entry );

	template < class Type >
	static ext::list < std::pair < std::function < bool ( ext::istream & ) >, std::unique_ptr < GroupReader > > >::const_iterator registerStringReader ( ) {
		return registerStringReader ( stringApi < Type >::first, std::unique_ptr < GroupReader > ( new ReaderRegister < Type > ( ) ) );
	}

	static void unregisterStringWriter ( const core::type_details & type );

	template < class Type >
	static void unregisterStringWriter ( ) {
		unregisterStringWriter ( core::type_details::get < Type > ( ) );
	}

	static void registerStringWriter ( core::type_details type, std::unique_ptr < GroupWriter > entry );

	template < class Type >
	static void registerStringWriter ( ) {
		registerStringWriter ( core::type_details::get < Type > ( ), std::unique_ptr < GroupWriter > ( new WriterRegister < Type > ( ) ) );
	}

	static object::Object parse ( ext::istream & input );

	static bool first ( ext::istream & input );

	static void compose ( ext::ostream & output, const object::Object & data );
};

} /* namespace core */


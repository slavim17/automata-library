#include "FormalRTE.h"
#include <rte/RTE.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < rte::FormalRTE < > > ( );
auto stringReader = registration::StringReaderRegister < rte::RTE, rte::FormalRTE < > > ( );

} /* namespace */

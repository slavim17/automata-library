#pragma once

#include <ext/istream>

#include <alib/string>

#include <common/lexer.hpp>

namespace rte {

class RTEFromStringLexer :  public ext::Lexer < RTEFromStringLexer > {
public:
	enum class TokenType {
		LPAR,
		RPAR,
		PLUS,
		STAR,
		DOT,
		COMMA,
		EMPTY,
		RANK,
		TEOF,
		ERROR
	};

	static Token next(ext::istream& input);
};

} /* namespace rte */


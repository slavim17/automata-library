#include <alib/vector>
#include <catch2/catch.hpp>
#include <ext/sstream>

#include "testing/TestFiles.hpp"
#include "testing/TimeoutAqlTest.hpp"

const unsigned RAND_STATES = 15;
const unsigned RAND_ALPHABET = 5;
const unsigned RAND_RANK = 4;
const double RAND_DENSITY = 5;
const size_t ITERATIONS = 50;

std::string qGenNFTA ( ) {
    return ext::concat ( "execute automaton::generate::RandomTreeAutomatonFactory (size_t)", rand ( ) % RAND_STATES + 1, " <(alphabet::generate::AsFullRankedAlphabet <(alphabet::generate::GenerateAlphabet (size_t)", rand ( ) % RAND_ALPHABET + 1, " true true)" " (size_t)", rand ( ) % RAND_RANK, ") (double)\"", RAND_DENSITY, "\"" );
}

TEST_CASE ( "FTA-RTE conversions test", "[integration]" ) {
    static const std::string qMinimize ( "automaton::determinize::Determinize - | automaton::simplify::Trim - | automaton::simplify::Minimize - | automaton::simplify::Normalize -" );

    SECTION ( "RTE Files test" ) {
        for ( const std::string& inputFile : TestFiles::Get ( "/rte/rte*.xml$" ) ) {
            ext::vector< std::string > qs = {
                ext::concat ( "execute < ", inputFile, " | rte::convert::ToFTAGlushkov - > $gen" ),
                ext::concat ( "quit compare::AutomatonCompare <( $gen | ", qMinimize, " ) <( $gen | automaton::convert::ToRTEStateElimination - | rte::convert::ToFTAGlushkov - | ", qMinimize, ")" )
            };

            TimeoutAqlTest ( 10s, qs );
        }
    }

    SECTION ( "RTE Random tests" ) {
        for ( size_t i = 0; i < ITERATIONS; i++ ) {
            ext::vector< std::string > qs = {
                ext::concat ( qGenNFTA ( ), " > $gen" ),
                ext::concat ( "quit compare::AutomatonCompare <( $gen | ", qMinimize, " )  <( $gen | automaton::convert::ToRTEStateElimination - | rte::convert::ToFTAGlushkov - | ", qMinimize, ")" )
            };

            TimeoutAqlTest ( 10s, qs );
        }
    }

    // ------------------------------------------------------------------------------------------------------

    SECTION ( "DFTA Files test" ) {
        auto files = GENERATE (
            TestFiles::Get ( "/automaton/DFTA.*.xml$" ),
            TestFiles::Get ( "/automaton/NFTA.*.xml$" ) );

        for ( const std::string& file : files ) {
            ext::vector< std::string > qs = {
                ext::concat ( "execute < ", file, " > $fta1" ),
                "execute $fta1 | automaton::convert::ToRTEStateElimination - | rte::convert::ToFTAGlushkov - > $fta2",
                ext::concat ( "execute $fta1 | ", qMinimize, " > $m1" ),
                ext::concat ( "execute $fta2 | ", qMinimize, " > $m2" ),
                "quit compare::AutomatonCompare $m1 $m2",
            };

            TimeoutAqlTest ( 10s, qs );
        }
    }
}

/*
 * UndefinedBehaviour.h
 */

#pragma once

namespace debug {

class UndefinedBehaviour {

public:
	static int undefined_behaviour ( );
};

} /* namespace debug */

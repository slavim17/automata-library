#include <catch2/catch.hpp>

#include <core/type_util.hpp>
#include <core/type_details.hpp>

#include <alib/variant>
#include <alib/set>
#include <alib/vector>

#include <object/ObjectFactory.h>

class Foo { };

TEST_CASE ( "Compontents", "[unit][core]" ) {
	SECTION ( "is_specialized" ) {
		REQUIRE ( ! core::is_specialized < core::type_util < Foo > > );
	}

	SECTION ( "vector of type_details" ) {
		ext::vector < core::type_details > data;
		data.push_back ( core::type_details::get < ext::variant < int, char > > ( ) );
	}

	SECTION ( "type compatibility requirements" ) {
		auto int_t = core::type_details::get < int > ( );
		auto univ_t = core::type_details::universal_type ( );

		REQUIRE ( int_t.compatible_with ( univ_t ) );
		REQUIRE ( ! univ_t.compatible_with ( int_t ) );

		auto set_int = core::type_details::get < ext::set < int > > ( );
		std::vector < std::unique_ptr < core::type_details_base > > subTypes;
		subTypes.push_back ( std::make_unique < core::type_details_universal_type > ( ) );
		auto set_univ = std::make_unique < core::type_details_template > ( "ext::set", std::move ( subTypes ) );

		REQUIRE ( static_cast < const core::type_details_base & > ( set_int ).compatible_with ( * set_univ ) );
		REQUIRE ( ! set_univ->compatible_with ( set_int ) );

		std::vector < std::unique_ptr < core::type_details_base > > subTypes2;
		subTypes2.push_back ( std::make_unique < core::type_details_universal_type > ( ) );
		auto vec_univ = std::make_unique < core::type_details_template > ( "ext::vector", std::move ( subTypes2 ) );
		std::vector < std::unique_ptr < core::type_details_base > > subTypes3;
		subTypes3.push_back ( std::move ( vec_univ ) );
		auto set_vec_univ = std::make_unique < core::type_details_template > ( "ext::set", std::move ( subTypes3 ) );

		auto set_unk = core::type_details::get ( ext::set < int > { } );

		REQUIRE ( set_vec_univ->compatible_with ( * set_univ ) );
		REQUIRE ( static_cast < const core::type_details_base & > ( set_unk ).compatible_with ( * set_vec_univ ) );
	}

	SECTION ( "size_t" ) {
		auto size_t_variant = core::type_details::get < size_t > ( );
		auto size_t_explicit = core::type_details::as_type ( "size_t" );

		REQUIRE ( size_t_variant.compatible_with ( size_t_explicit ) );
		REQUIRE ( size_t_explicit.compatible_with ( size_t_variant ) );
	}
}

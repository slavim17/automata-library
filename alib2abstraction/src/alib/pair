#pragma once

#include <ext/pair>

#include <core/type_details.hpp>
#include <object/Object.h>
#include <object/AnyObject.h>
#include <object/ObjectFactory.h>
#include <ext/typeinfo>
#include <factory/NormalizeFactory.hpp>

namespace core {

template < typename T, typename U >
struct type_util < ext::pair < T, U > > {
	static ext::pair < T, U > denormalize ( ext::pair < object::Object, object::Object > && arg ) {
		return ext::make_pair ( factory::NormalizeFactory::denormalize < T > ( std::move ( arg.first ) ), factory::NormalizeFactory::denormalize < U > ( std::move ( arg.second ) ) );
	}

	static ext::pair < object::Object, object::Object > normalize ( ext::pair < T, U > && arg ) {
		return ext::make_pair ( factory::NormalizeFactory::normalize < T > ( std::move ( arg.first ) ), factory::NormalizeFactory::normalize < U > ( std::move ( arg.second ) ) );
	}

	static std::unique_ptr < type_details_base > type ( const ext::pair < T, U > & arg ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_util < T >::type ( arg.first ) );
		sub_types_vec.push_back ( type_util < U >::type ( arg.second ) );

		return std::make_unique < type_details_template > ( "ext::pair", std::move ( sub_types_vec ) );
	}
};

template < class T, class U >
struct type_details_retriever < ext::pair < T, U > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < T >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < U >::get ( ) );
		return std::make_unique < type_details_template > ( "ext::pair", std::move ( sub_types_vec ) );
	}
};

template < class T, class U >
struct type_details_retriever < std::pair < T, U > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < T >::get ( ) );
		sub_types_vec.push_back ( type_details_retriever < U >::get ( ) );
		return std::make_unique < type_details_template > ( "std::pair", std::move ( sub_types_vec ) );
	}
};

}

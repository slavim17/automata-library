#pragma once

#include <ext/tuple>

#include <core/type_details.hpp>
#include <object/Object.h>
#include <object/AnyObject.h>
#include <object/ObjectFactory.h>
#include <ext/typeinfo>
#include <factory/NormalizeFactory.hpp>

namespace core {

template < typename T, typename R >
using select_second = R;

template < typename ... Ts >
struct type_util < ext::tuple < Ts ... > > {
	static ext::tuple < Ts ... > denormalize ( ext::tuple < select_second < Ts, object::Object > ... > && arg ) {
		return std::apply ( [ ] ( auto && ... elem ) {
			return ext::make_tuple ( factory::NormalizeFactory::denormalize < Ts > ( std::move ( elem ) ) ... );
		}, std::move ( arg ) );
	}

	static ext::tuple < select_second < Ts, object::Object > ... > normalize ( ext::tuple < Ts ... > && arg ) {
		return std::apply ( [ ] ( auto && ... elem ) {
			return ext::make_tuple ( factory::NormalizeFactory::normalize < Ts > ( std::move ( elem ) ) ... );
		}, std::move ( arg ) );
	}

	static std::unique_ptr < type_details_base > type ( const ext::tuple < Ts ... > & arg ) {
		return std::apply ( [ ] ( auto & ... elem ) {
			std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
			( sub_types_vec.push_back ( type_util < Ts >::type ( elem ) ), ... );
			return std::make_unique < type_details_template > ( "ext::tuple", std::move ( sub_types_vec ) );
		}, arg );
	}
};

template < class ... Ts >
struct type_details_retriever < ext::tuple < Ts ... > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		( sub_types_vec.push_back ( type_details_retriever < Ts >::get ( ) ), ... );
		return std::make_unique < type_details_template > ( "ext::tuple", std::move ( sub_types_vec ) );
	}
};

}

#pragma once

#include <ext/set>

#include <core/type_details.hpp>
#include <object/Object.h>
#include <object/AnyObject.h>
#include <object/ObjectFactory.h>
#include <ext/typeinfo>
#include <factory/NormalizeFactory.hpp>

namespace core {

template < typename T >
struct type_util < ext::set < T > > {
	static ext::set < T > denormalize ( ext::set < object::Object > && arg ) {
		ext::set < T > res;
		for ( object::Object && item : ext::make_mover ( arg ) )
			res.insert ( factory::NormalizeFactory::denormalize < T > ( std::move ( item ) ) );

		return res;
	}

	static ext::set < object::Object > normalize ( ext::set < T > && arg ) {
		ext::set < object::Object > res;
		for ( T && item : ext::make_mover ( arg ) )
			res.insert ( factory::NormalizeFactory::normalize < T > ( std::move ( item ) ) );

		return res;
	}

	static std::unique_ptr < type_details_base > type ( const ext::set < T > & arg ) {
		core::unique_ptr_set < type_details_base > subTypes;
		for ( const T & item : arg )
			subTypes.insert ( type_util < T >::type ( item ) );

		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_variant_type::make_variant ( std::move ( subTypes ) ) );
		return std::make_unique < type_details_template > ( "ext::set", std::move ( sub_types_vec ) );
	}
};

template < class T >
struct type_details_retriever < ext::set < T > > {
	static std::unique_ptr < type_details_base > get ( ) {
		std::vector < std::unique_ptr < type_details_base > > sub_types_vec;
		sub_types_vec.push_back ( type_details_retriever < T >::get ( ) );
		return std::make_unique < type_details_template > ( "ext::set", std::move ( sub_types_vec ) );
	}
};

}

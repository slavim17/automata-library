#pragma once

#include <string>

#include <ext/type_traits>
#include <ext/typeindex>

#include <core/type_details_base.hpp>

#include <object/Object.h>

namespace core {

template < >
struct type_util < unsigned > {
	static unsigned denormalize ( unsigned arg );

	static unsigned normalize ( unsigned arg );

	static std::unique_ptr < type_details_base > type ( unsigned arg );
};

template < >
struct type_util < long unsigned > {
	static long unsigned denormalize ( long unsigned arg );

	static long unsigned normalize ( long unsigned arg );

	static std::unique_ptr < type_details_base > type ( long unsigned arg );
};

template < >
struct type_util < int > {
	static int denormalize ( int arg );

	static int normalize ( int arg );

	static std::unique_ptr < type_details_base > type ( int arg );
};

template < >
struct type_util < long > {
	static long denormalize ( long arg );

	static long normalize ( long arg );

	static std::unique_ptr < type_details_base > type ( long arg );
};

template < >
struct type_util < char > {
	static char denormalize ( char arg );

	static char normalize ( char arg );

	static std::unique_ptr < type_details_base > type ( char arg );
};

template < >
struct type_util < bool > {
	static bool denormalize ( bool arg );

	static bool normalize ( bool arg );

	static std::unique_ptr < type_details_base > type ( bool arg );
};

template < >
struct type_util < double > {
	static double denormalize ( double arg );

	static double normalize ( double arg );

	static std::unique_ptr < type_details_base > type ( double arg );
};

template < >
struct type_util < std::string > {
	static std::string denormalize ( std::string arg );

	static std::string normalize ( std::string arg );

	static std::unique_ptr < type_details_base > type ( const std::string & arg );
};

template < >
struct type_util < ext::ostream > {
	static std::unique_ptr < type_details_base > type ( const ext::ostream & arg );
};

} /* namespace core */

#include <core/type_util.hpp>
#include <object/Object.h>
#include <ext/typeinfo>

#include <object/ObjectFactory.h>

namespace core {

long unsigned type_util < long unsigned >::denormalize ( long unsigned arg ) {
	return arg;
}

long unsigned type_util < long unsigned >::normalize ( long unsigned arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < long unsigned >::type ( long unsigned ) {
	return type_details_retriever < long unsigned >::get ( );
}

// ----------------------------------------------------------------------------------------------------------------

unsigned type_util < unsigned >::denormalize ( unsigned arg ) {
	return arg;
}

unsigned type_util < unsigned >::normalize ( unsigned arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < unsigned >::type ( unsigned ) {
	return type_details_retriever < unsigned >::get ( );
}

// ----------------------------------------------------------------------------------------------------------------

long type_util < long >::denormalize ( long arg ) {
	return arg;
}

long type_util < long >::normalize ( long arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < long >::type ( long ) {
	return type_details_retriever < long >::get ( );
}

// ----------------------------------------------------------------------------------------------------------------

int type_util < int >::denormalize ( int arg ) {
	return arg;
}

int type_util < int >::normalize ( int arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < int >::type ( int ) {
	return type_details_retriever < int >::get ( );
}

// ----------------------------------------------------------------------------------------------------------------

char type_util < char >::denormalize ( char arg ) {
	return arg;
}

char type_util < char >::normalize ( char arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < char >::type ( char ) {
	return type_details_retriever < char >::get ( );
}

// ----------------------------------------------------------------------------------------------------------------

bool type_util < bool >::denormalize ( bool arg ) {
	return arg;
}

bool type_util < bool >::normalize ( bool arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < bool >::type ( bool ) {
	return type_details_retriever < bool >::get ( );
}

// ----------------------------------------------------------------------------------------------------------------

double type_util < double >::denormalize ( double arg ) {
	return arg;
}

double type_util < double >::normalize ( double arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < double >::type ( double ) {
	return type_details_retriever < double >::get ( );
}

// ----------------------------------------------------------------------------------------------------------------

std::string type_util < std::string >::denormalize ( std::string arg ) {
	return arg;
}

std::string type_util < std::string >::normalize ( std::string arg ) {
	return arg;
}

std::unique_ptr < type_details_base > type_util < std::string >::type ( const std::string & ) {
	return type_details_retriever < std::string >::get ( );
}

// ----------------------------------------------------------------------------------------------------------------

std::unique_ptr < type_details_base > type_util < ext::ostream >::type ( const ext::ostream & ) {
	return std::make_unique < type_details_type > ( "ext::ostream" );
}

} /* namespace core */

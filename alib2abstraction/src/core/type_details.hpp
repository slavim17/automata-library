#pragma once

#include <memory>

#include <ext/string>
#include <ext/variant>
#include <ext/ostream>

#include <core/type_details_base.hpp>

namespace core {

template < >
struct type_details_retriever < void > {
	static std::unique_ptr < type_details_base > get ( );
};

class type_details {
	std::shared_ptr < type_details_base > m_type_details;

	type_details ( std::unique_ptr < type_details_base > data );

public:
	template < class T >
	static type_details get ( ) {
		static type_details res ( type_details_retriever < T >::get ( ) );
		return res; // the copy should be cheap type_details internally uses shared_ptr
	}

	template < class T >
	static type_details get ( const T & arg ) {
		if constexpr ( core::is_specialized < core::type_util < T > > ) {
			return core::type_util < T >::type ( arg );
		} else {
			return core::type_details::get < T > ( );
		}
	}

	static type_details universal_type ( );

	static type_details void_type ( );

	static type_details as_type ( const std::string & string );

	static type_details as_type ( const std::string & string, const std::vector < std::string > & templateParams );

	bool compatible_with ( const type_details & other ) const;

	friend std::ostream & operator << ( std::ostream & os, const type_details & arg );

	std::strong_ordering operator <=> ( const type_details & other ) const;

	bool operator == ( const type_details & other ) const;

	operator const type_details_base & ( ) const;
};

template < class T >
struct type_details_retriever < T * > {
	static std::unique_ptr < type_details_base > get ( ) {
		return std::make_unique < type_details_pointer > ( std::is_const_v < T >, type_details_retriever < std::decay_t < T > >::get ( ) );
	}
};

template < class T >
struct type_details_retriever < const T && > {
	static std::unique_ptr < type_details_base > get ( ) {
		return std::make_unique < type_details_reference > ( true, true, type_details_retriever < std::decay_t < T > >::get ( ) );
	}
};

template < class T >
struct type_details_retriever < T && > {
	static std::unique_ptr < type_details_base > get ( ) {
		return std::make_unique < type_details_reference > ( false, true, type_details_retriever < std::decay_t < T > >::get ( ) );
	}
};

template < class T >
struct type_details_retriever < const T & > {
	static std::unique_ptr < type_details_base > get ( ) {
		return std::make_unique < type_details_reference > ( true, false, type_details_retriever < std::decay_t < T > >::get ( ) );
	}
};

template < class T >
struct type_details_retriever < T & > {
	static std::unique_ptr < type_details_base > get ( ) {
		return std::make_unique < type_details_reference > ( false, false, type_details_retriever < std::decay_t < T > >::get ( ) );
	}
};

template < >
struct type_details_retriever < unsigned > {
	static std::unique_ptr < type_details_base > get ( );
};

template < >
struct type_details_retriever < long unsigned > {
	static std::unique_ptr < type_details_base > get ( );
};

template < >
struct type_details_retriever < int > {
	static std::unique_ptr < type_details_base > get ( );
};

template < >
struct type_details_retriever < long > {
	static std::unique_ptr < type_details_base > get ( );
};

template < >
struct type_details_retriever < char > {
	static std::unique_ptr < type_details_base > get ( );
};

template < >
struct type_details_retriever < bool > {
	static std::unique_ptr < type_details_base > get ( );
};

template < >
struct type_details_retriever < double > {
	static std::unique_ptr < type_details_base > get ( );
};

template < >
struct type_details_retriever < std::string > {
	static std::unique_ptr < type_details_base > get ( );
};

template < >
struct type_details_retriever < ext::ostream > {
	static std::unique_ptr < type_details_base > get ( );
};

} /* namespace core */

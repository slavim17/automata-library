#include <core/type_details.hpp>

namespace core {

std::unique_ptr < type_details_base > type_details_retriever < void >::get ( ) {
	return std::make_unique < type_details_type > ( "void" );
}

type_details::type_details ( std::unique_ptr < type_details_base > data ) : m_type_details ( std::move ( data ) ) {
}

type_details type_details::universal_type ( ) {
	static type_details res ( std::make_unique < core::type_details_universal_type > ( ) );
	return res;
}

type_details type_details::void_type ( ) {
	static type_details res ( type_details_retriever < void >::get ( ) );
	return res;
}

type_details type_details::as_type ( const std::string & string ) {
	if ( string == "size_t" ) // FIXME hack
		return get < size_t > ( );
	if ( string == "unsigned int" )
		return get < unsigned > ( );
	return type_details ( std::make_unique < type_details_type > ( string ) );
}

type_details type_details::as_type ( const std::string & string, const std::vector < std::string > & templateParams ) {
	std::vector < std::unique_ptr < type_details_base > > templates ( templateParams.size ( ) );
	for ( const std::string & templateParam : templateParams ) {
		templates.push_back ( std::make_unique < type_details_type > ( templateParam ) );
	}
	return type_details ( std::make_unique < type_details_template > ( string, std::move ( templates ) ) );
}

bool type_details::compatible_with ( const type_details & other ) const {
	return m_type_details->compatible_with ( * other.m_type_details );
}

std::ostream & operator << ( std::ostream & os, const type_details & arg ) {
	return os << ( * arg.m_type_details );
}

std::strong_ordering type_details::operator <=> ( const type_details & other ) const {
	return * m_type_details <=> * other.m_type_details;
}

bool type_details::operator == ( const type_details & other ) const {
	return * m_type_details == * other.m_type_details;
}

type_details::operator const type_details_base & ( ) const {
	return * m_type_details;
}

std::unique_ptr < type_details_base > type_details_retriever < unsigned >::get ( ) {
	return std::make_unique < type_details_type > ( "unsigned" );
}

std::unique_ptr < type_details_base > type_details_retriever < long unsigned >::get ( ) {
	return std::make_unique < type_details_type > ( "long unsigned" );
}

std::unique_ptr < type_details_base > type_details_retriever < int >::get ( ) {
	return std::make_unique < type_details_type > ( "int" );
}

std::unique_ptr < type_details_base > type_details_retriever < long >::get ( ) {
	return std::make_unique < type_details_type > ( "long" );
}

std::unique_ptr < type_details_base > type_details_retriever < char >::get ( ) {
	return std::make_unique < type_details_type > ( "char" );
}

std::unique_ptr < type_details_base > type_details_retriever < bool >::get ( ) {
	return std::make_unique < type_details_type > ( "bool" );
}

std::unique_ptr < type_details_base > type_details_retriever < double >::get ( ) {
	return std::make_unique < type_details_type > ( "double" );
}

std::unique_ptr < type_details_base > type_details_retriever < std::string >::get ( ) {
	return std::make_unique < type_details_type > ( "std::string" );
}

std::unique_ptr < type_details_base > type_details_retriever < ext::ostream >::get ( ) {
	return std::make_unique < type_details_type > ( "ext::ostream" );
}

} /* namespace core */

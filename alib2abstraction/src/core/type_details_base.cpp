#include <ostream>

#include <ext/string>

#include <core/type_details_base.hpp>

namespace core {

std::pair < std::string, std::vector < std::string > > type_details_base::parseName ( const std::string & name ) {
	ext::vector < std::string > res = ext::explode ( name, "::" );
	std::string unqualified_name = res.back ( );
	res.pop_back ( );
	return std::make_pair ( std::move ( unqualified_name ), std::move ( res ) );
}

std::ostream & operator << ( std::ostream & os, const type_details_base & arg ) {
	arg.print ( os );
	return os;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

void type_details_type::print ( std::ostream & os ) const {
	for ( const std::string & namespace_name : m_namespaces ) {
		os << namespace_name << "::";
	}
	os << m_name;
}

type_details_type::type_details_type ( std::pair < std::string, std::vector < std::string > > nameData ) : m_name ( std::move ( nameData.first ) ), m_namespaces ( std::move ( nameData.second ) ) {
}

type_details_type::type_details_type ( const std::string & name ) : type_details_type ( parseName ( name ) ) {
}

bool type_details_type::compatible_with ( const type_details_base & other ) const {
	if ( dynamic_cast < const type_details_universal_type * > ( & other ) != nullptr )
		return true;

	auto vari = dynamic_cast < const type_details_variant_type * > ( & other );
	if ( vari != nullptr )
		return std::any_of ( vari->m_variants.begin ( ), vari->m_variants.end ( ), [ & ] ( const std::unique_ptr < type_details_base > & variant ) { return this->compatible_with ( * variant ); } );

	auto templ = dynamic_cast < const type_details_template * > ( & other );
	if ( templ != nullptr )
		return m_name == templ->m_name;

	auto casted = dynamic_cast < const type_details_type * > ( & other );
	if ( casted == nullptr )
		return false;

	return m_name == casted->m_name;
}

std::strong_ordering type_details_type::operator <=> ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );

	return * this <=> static_cast < decltype ( ( * this ) ) > ( other );
}

std::strong_ordering type_details_type::operator <=> ( const type_details_type & other ) const {
	return std::tie ( m_name, m_namespaces ) <=> std::tie ( other.m_name, other.m_namespaces );
}

bool type_details_type::operator == ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return false;

	return * this == static_cast < decltype ( ( * this ) ) > ( other );
}

bool type_details_type::operator == ( const type_details_type & other ) const {
	return std::tie ( m_name, m_namespaces ) == std::tie ( other.m_name, other.m_namespaces );
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

void type_details_template::print ( std::ostream & os ) const {
	for ( const std::string & namespace_name : m_namespaces ) {
		os << namespace_name << "::";
	}
	os << m_name << '<';
	for ( auto iter = m_templates.begin ( ); iter != m_templates.end ( ); ++ iter ) {
		os << (iter != m_templates.begin ( ) ? ", " : "" ) << * * iter;
	}
	os << '>';
}

type_details_template::type_details_template ( std::pair < std::string, std::vector < std::string > > nameData, std::vector < std::unique_ptr < type_details_base > > templates ) : m_name ( std::move ( nameData.first ) ), m_namespaces ( std::move ( nameData.second ) ), m_templates ( std::move ( templates ) ) {
}

type_details_template::type_details_template ( const std::string & name, std::vector < std::unique_ptr < type_details_base > > templates ) : type_details_template ( parseName ( name ), std::move ( templates ) ) {
}

bool type_details_template::compatible_with ( const type_details_base & other ) const {
	if ( dynamic_cast < const type_details_universal_type * > ( & other ) != nullptr )
		return true;

	auto vari = dynamic_cast < const type_details_variant_type * > ( & other );
	if ( vari != nullptr )
		return std::any_of ( vari->m_variants.begin ( ), vari->m_variants.end ( ), [ & ] ( const std::unique_ptr < type_details_base > & variant ) { return this->compatible_with ( * variant ); } );

	auto type = dynamic_cast < const type_details_type * > ( & other );
	if ( type != nullptr )
		return m_name == type->m_name;

	auto casted = dynamic_cast < const type_details_template * > ( & other );
	if ( casted == nullptr )
		return false;

	bool res = m_name == casted->m_name && m_templates.size ( ) == casted->m_templates.size ( ); // && std::all_of ( templates.begin ( ), templates.end ( ), [ ] ( const unique_ptr < type_details_base > & templ ) {  ;
	for ( auto it1 = m_templates.begin ( ), it2 = casted->m_templates.begin ( ); res && it1 != m_templates.end ( ); ++ it1, ++ it2 ) {
		res = res && ( * it1 )->compatible_with ( * * it2 );
	}
	return res;
}

std::strong_ordering type_details_template::operator <=> ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );

	return * this <=> static_cast < decltype ( ( * this ) ) > ( other );
}

std::strong_ordering type_details_template::operator <=> ( const type_details_template & other ) const {
	auto res = std::tie ( m_name, m_namespaces ) <=> std::tie ( other.m_name, other.m_namespaces );
	if ( res != 0 )
		return res;

	res = m_templates.size ( ) <=> other.m_templates.size ( );
	for ( size_t i = 0; i < m_templates.size ( ) && res == 0; ++ i )
		res = ( * m_templates [ i ] ) <=> ( * other.m_templates [ i ] );

	return res;
}

bool type_details_template::operator == ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return false;

	return * this == static_cast < decltype ( ( * this ) ) > ( other );
}

bool type_details_template::operator == ( const type_details_template & other ) const {
	bool res = std::tie ( m_name, m_namespaces ) == std::tie ( other.m_name, other.m_namespaces );
	if ( ! res )
		return res;

	res = m_templates.size ( ) == other.m_templates.size ( );
	for ( size_t i = 0; i < m_templates.size ( ) && res ; ++ i )
		res = ( * m_templates [ i ] ) == ( * other.m_templates [ i ] );

	return res;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

void type_details_pointer::print ( std::ostream & os ) const {
	os << (m_is_const_qualified ? "const " : "" ) << * m_sub_type << " *";
}

type_details_pointer::type_details_pointer ( bool is_const_qualified, std::unique_ptr < type_details_base > sub_type ) : m_is_const_qualified ( is_const_qualified ), m_sub_type ( std::move ( sub_type ) ) {
}

bool type_details_pointer::compatible_with ( const type_details_base & other ) const {
	auto casted = dynamic_cast < const type_details_pointer * > ( & other );
	if ( casted == nullptr )
		return false;

	return m_is_const_qualified == casted->m_is_const_qualified && m_sub_type->compatible_with ( * casted->m_sub_type );
}

std::strong_ordering type_details_pointer::operator <=> ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );

	return * this <=> static_cast < decltype ( ( * this ) ) > ( other );
}

std::strong_ordering type_details_pointer::operator <=> ( const type_details_pointer & other ) const {
	return std::tie ( m_is_const_qualified, * m_sub_type ) <=> std::tie ( other.m_is_const_qualified, * other.m_sub_type );
}

bool type_details_pointer::operator == ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return false;

	return * this == static_cast < decltype ( ( * this ) ) > ( other );
}

bool type_details_pointer::operator == ( const type_details_pointer & other ) const {
	return std::tie ( m_is_const_qualified, * m_sub_type ) == std::tie ( other.m_is_const_qualified, * other.m_sub_type );
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

void type_details_reference::print ( std::ostream & os ) const {
	os << (m_is_const_qualified ? "const " : "" ) << * m_sub_type << ( m_is_rvalue ? " &&" : " &" );
}

type_details_reference::type_details_reference ( bool is_const_qualified, bool is_rvalue, std::unique_ptr < type_details_base > sub_type ) : m_is_const_qualified ( is_const_qualified ), m_is_rvalue ( is_rvalue ), m_sub_type ( std::move ( sub_type ) ) {
}

bool type_details_reference::compatible_with ( const type_details_base & other ) const {
	auto casted = dynamic_cast < const type_details_reference * > ( & other );
	if ( casted == nullptr )
		return false;

	return m_is_const_qualified == casted->m_is_const_qualified && m_is_rvalue == casted->m_is_rvalue && m_sub_type->compatible_with ( * casted->m_sub_type );
}

std::strong_ordering type_details_reference::operator <=> ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );

	return * this <=> static_cast < decltype ( ( * this ) ) > ( other );
}

std::strong_ordering type_details_reference::operator <=> ( const type_details_reference & other ) const {
	return std::tie ( m_is_const_qualified, m_is_rvalue, * m_sub_type ) <=> std::tie ( other.m_is_const_qualified, m_is_rvalue, * other.m_sub_type );
}

bool type_details_reference::operator == ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return false;

	return * this == static_cast < decltype ( ( * this ) ) > ( other );
}

bool type_details_reference::operator == ( const type_details_reference & other ) const {
	return std::tie ( m_is_const_qualified, m_is_rvalue, * m_sub_type ) == std::tie ( other.m_is_const_qualified, m_is_rvalue, * other.m_sub_type );
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

void type_details_universal_type::print ( std::ostream & os ) const {
	os << "Universal type";
}

bool type_details_universal_type::compatible_with ( const type_details_base & other ) const {
	if ( dynamic_cast < const type_details_universal_type * > ( & other ) != nullptr )
		return true;

	return false;
}

std::strong_ordering type_details_universal_type::operator <=> ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );

	return * this <=> static_cast < decltype ( ( * this ) ) > ( other );
}

std::strong_ordering type_details_universal_type::operator <=> ( const type_details_universal_type & ) const {
	return std::strong_ordering::equal;
}

bool type_details_universal_type::operator == ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return false;

	return * this == static_cast < decltype ( ( * this ) ) > ( other );
}

bool type_details_universal_type::operator == ( const type_details_universal_type & ) const {
	return true;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

void type_details_unknown_type::print ( std::ostream & os ) const {
	os << "Unknown type";
}

bool type_details_unknown_type::compatible_with ( const type_details_base & ) const {
	return true;
}

std::strong_ordering type_details_unknown_type::operator <=> ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );

	return * this <=> static_cast < decltype ( ( * this ) ) > ( other );
}

std::strong_ordering type_details_unknown_type::operator <=> ( const type_details_unknown_type & ) const {
	return std::strong_ordering::equal;
}

bool type_details_unknown_type::operator == ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return false;

	return * this == static_cast < decltype ( ( * this ) ) > ( other );
}

bool type_details_unknown_type::operator == ( const type_details_unknown_type & ) const {
	return true;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

void type_details_variant_type::print ( std::ostream & os ) const {
	os << "variant<";
	for ( auto iter = m_variants.begin ( ); iter != m_variants.end ( ); ++ iter ) {
		os << (iter != m_variants.begin ( ) ? ", " : "" ) << * * iter;
	}
	os << '>';
}

type_details_variant_type::type_details_variant_type ( unique_ptr_set < type_details_base > variants ) : m_variants ( std::move ( variants ) ) {
}

bool type_details_variant_type::compatible_with ( const type_details_base & other ) const {
	return std::all_of ( m_variants.begin ( ), m_variants.end ( ), [ & ] ( const std::unique_ptr < type_details_base > & variant ) { return variant->compatible_with ( other ); } );
}

std::strong_ordering type_details_variant_type::operator <=> ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );

	return * this <=> static_cast < decltype ( ( * this ) ) > ( other );
}

std::strong_ordering type_details_variant_type::operator <=> ( const type_details_variant_type & other ) const {
	auto res = m_variants.size ( ) <=> other.m_variants.size ( );
	for ( auto iter1 = m_variants.begin ( ), iter2 = other.m_variants.begin ( ); iter1 != m_variants.end ( ) && res == 0; ++ iter1, ++ iter2 )
		res = * * iter1 <=> * * iter2;

	return res;
}

bool type_details_variant_type::operator == ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return false;

	return * this == static_cast < decltype ( ( * this ) ) > ( other );
}

bool type_details_variant_type::operator == ( const type_details_variant_type & other ) const {
	auto res = m_variants.size ( ) == other.m_variants.size ( );
	for ( auto iter1 = m_variants.begin ( ), iter2 = other.m_variants.begin ( ); iter1 != m_variants.end ( ) && res; ++ iter1, ++ iter2 )
		res = * * iter1 == * * iter2;

	return res;
}

std::unique_ptr < type_details_base > type_details_variant_type::make_variant ( core::unique_ptr_set < type_details_base > && types ) {
	for ( auto iter = types.begin ( ); iter != types.end ( ); ) {
		auto compatible_with = [ & ] ( const std::unique_ptr < type_details_base > & second ) { return ( * iter )->compatible_with ( * second ); };
		if ( std::any_of ( types.begin ( ), iter, compatible_with ) || std::any_of ( std::next ( iter ), types.end ( ), compatible_with ) ) {
			iter = types.erase ( iter );
		} else {
			++ iter;
		}
	}

	if ( types.empty ( ) )
		return std::make_unique < type_details_unknown_type > ( );
	else if ( types.size ( ) == 1 )
		return std::move ( types.extract ( types.begin ( ) ).value ( ) );
	else
		return std::make_unique < type_details_variant_type > ( std::move ( types ) );
}

// --------------------------------------------------------------------------------------------------------------------------------------------------

void type_details_function::print ( std::ostream & os ) const {
	os << * m_return_type << "(";
	for ( auto iter = m_param_types.begin ( ); iter != m_param_types.end ( ); ++ iter ) {
		os << (iter != m_param_types.begin ( ) ? ", " : "" ) << * * iter;
	}
	os << ")";
}

type_details_function::type_details_function ( std::unique_ptr < type_details_base > return_type, std::vector < std::unique_ptr < type_details_base > > param_types ) : m_return_type ( std::move ( return_type ) ), m_param_types ( std::move ( param_types ) ) {
}

bool type_details_function::compatible_with ( const type_details_base & other ) const {
	auto casted = dynamic_cast < const type_details_function * > ( & other );
	if ( casted == nullptr )
		return false;

	bool res = m_return_type->compatible_with ( * casted->m_return_type ) && m_param_types.size ( ) == casted->m_param_types.size ( );
	for ( auto it1 = m_param_types.begin ( ), it2 = casted->m_param_types.begin ( ); res && it1 == m_param_types.end ( ); ++ it1, ++ it2 ) {
		res = res && ( * it1 )->compatible_with ( * * it2 );
	}
	return res;
}

std::strong_ordering type_details_function::operator <=> ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return ext::type_index ( typeid ( * this ) ) <=> ext::type_index ( typeid ( other ) );

	return * this <=> static_cast < decltype ( ( * this ) ) > ( other );
}

std::strong_ordering type_details_function::operator <=> ( const type_details_function & other ) const {
	auto res = * m_return_type <=> * other.m_return_type;
	if ( res != 0 )
		return res;

	res = m_param_types.size ( ) <=> other.m_param_types.size ( );
	for ( size_t i = 0; i < m_param_types.size ( ) && res == 0; ++ i )
		res = ( * m_param_types [ i ] ) <=> ( * other.m_param_types [ i ] );

	return res;
}

bool type_details_function::operator == ( const type_details_base & other ) const {
	if ( ext::type_index ( typeid ( * this ) ) != ext::type_index ( typeid ( other ) ) )
		return false;

	return * this == static_cast < decltype ( ( * this ) ) > ( other );
}

bool type_details_function::operator == ( const type_details_function & other ) const {
	auto res = * m_return_type == * other.m_return_type;
	if ( ! res )
		return res;

	res = m_param_types.size ( ) == other.m_param_types.size ( );
	for ( size_t i = 0; i < m_param_types.size ( ) && res ; ++ i )
		res = ( * m_param_types [ i ] ) == ( * other.m_param_types [ i ] );

	return res;
}

} /* namespace core */

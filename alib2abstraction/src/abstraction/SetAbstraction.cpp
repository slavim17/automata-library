#include <abstraction/SetAbstraction.hpp>

#include <factory/NormalizeFactory.hpp>

#include <alib/set>

namespace abstraction {

std::shared_ptr < abstraction::Value > SetAbstraction::run ( ) const {
	ext::set < object::Object > theSet;
	for ( const std::shared_ptr < abstraction::Value > & param : this->getParams ( ) ) {
		theSet.insert ( abstraction::retrieveValue < object::Object > ( param ) );
	}

	object::Object normalized = factory::NormalizeFactory::normalize < ext::set < object::Object > > ( std::move ( theSet ) );
	return std::make_shared < abstraction::ValueHolder < object::Object > > ( std::move ( normalized ), true );
}

} /* namespace abstraction */

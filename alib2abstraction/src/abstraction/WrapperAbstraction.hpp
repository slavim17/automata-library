#pragma once

#include <memory>

#include <ext/array>

#include <abstraction/OperationAbstraction.hpp>
#include <common/AbstractionHelpers.hpp>

namespace abstraction {

class UnspecifiedType {
};

template < size_t NumberOfParams >
class WrapperAbstractionImpl : public OperationAbstraction {
	std::unique_ptr < OperationAbstraction > m_abstraction;
	ext::array < std::shared_ptr < abstraction::Value >, NumberOfParams > m_params;

protected:
	ext::array < std::shared_ptr < abstraction::Value >, NumberOfParams > & getParams ( ) {
		return m_params;
	}

	virtual std::unique_ptr < abstraction::OperationAbstraction > evalAbstractionFunction ( ) = 0;

	std::unique_ptr < OperationAbstraction > & getAbstraction ( ) {
		return m_abstraction;
	}

	const std::unique_ptr < OperationAbstraction > & getAbstraction ( ) const {
		return m_abstraction;
	}

	void attachInputsToAbstraction ( ) {
		for ( size_t index = 0; index < NumberOfParams; ++ index )
			this->m_abstraction->attachInput ( m_params [ index ], index );
	}

public:
	explicit WrapperAbstractionImpl ( ) {
		if constexpr ( NumberOfParams != 0 )
			for ( size_t i = 0; i < NumberOfParams; ++ i ) {
				m_params [ i ] = nullptr;
			}
	}

private:
	void attachInput ( const std::shared_ptr < abstraction::Value > & input, size_t index ) override {
		if ( index >= m_params.size ( ) )
			throw std::invalid_argument ( "Parameter index " + ext::to_string ( index ) + " out of bounds.");

		m_params [ index ] = input;
	}

	void detachInput ( size_t index ) override {
		if ( index >= m_params.size ( ) )
			throw std::invalid_argument ( "Parameter index " + ext::to_string ( index ) + " out of bounds.");

		m_params [ index ] = nullptr;
	}

public:
	bool inputsAttached ( ) const override {
		return std::all_of ( m_params.begin ( ), m_params.end ( ), [ ] ( const std::shared_ptr < abstraction::Value > & param ) { return static_cast < bool > ( param ); } );
	}

	std::shared_ptr < abstraction::Value > eval ( ) override {
		if ( ! inputsAttached ( ) )
			return nullptr;

		m_abstraction = this->evalAbstractionFunction ( );

		this->attachInputsToAbstraction ( );

		return this->getAbstraction ( )->eval ( );
	}

	size_t numberOfParams ( ) const override {
		return NumberOfParams;
	}

	core::type_details getReturnType ( ) const override {
		if ( this->m_abstraction )
			return this->m_abstraction->getReturnType ( );
		else
			throw std::domain_error ( "ReturnType not available before evaluation." );
	}

	abstraction::TypeQualifiers::TypeQualifierSet getReturnTypeQualifiers ( ) const override {
		if ( this->m_abstraction )
			return this->m_abstraction->getReturnTypeQualifiers ( );
		else
			throw std::domain_error ( "ReturnTypeQualifiers not available before evaluation." );
	}

};

} /* namespace abstraction */

extern template class abstraction::WrapperAbstractionImpl < 1 >;
extern template class abstraction::WrapperAbstractionImpl < 2 >;
extern template class abstraction::WrapperAbstractionImpl < 3 >;

namespace abstraction {

template < class ... ParamTypes >
class WrapperAbstraction : public WrapperAbstractionImpl < sizeof ... ( ParamTypes ) > {
	std::function < std::unique_ptr < abstraction::OperationAbstraction > ( ParamTypes ... ) > m_WrapperFinder;

public:
	explicit WrapperAbstraction ( std::function < std::unique_ptr < abstraction::OperationAbstraction > ( ParamTypes ... ) > wrapperFinder ) : m_WrapperFinder ( std::move ( wrapperFinder ) ) {
	}

	std::unique_ptr < abstraction::OperationAbstraction > evalAbstractionFunction ( ) override {
		return abstraction::apply < ParamTypes ... > ( m_WrapperFinder, this->getParams ( ) );
	}

	core::type_details getParamType ( size_t index ) const override {
		return abstraction::paramType < ParamTypes ... > ( index );
	}

	abstraction::TypeQualifiers::TypeQualifierSet getParamTypeQualifiers ( size_t index ) const override {
		return abstraction::paramTypeQualifiers < ParamTypes ... > ( index );
	}

};

} /* namespace abstraction */

namespace core {

template < >
struct type_details_retriever < abstraction::UnspecifiedType > {
	static std::unique_ptr < type_details_base > get ( );
};

} /* namespace core */

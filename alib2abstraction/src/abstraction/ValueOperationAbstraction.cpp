#include <abstraction/ValueOperationAbstraction.hpp>

namespace abstraction {

core::type_details ValueOperationAbstraction < void >::getReturnType ( ) const {
	return core::type_details::void_type ( );
}

abstraction::TypeQualifiers::TypeQualifierSet ValueOperationAbstraction < void >::getReturnTypeQualifiers ( ) const {
	return abstraction::TypeQualifiers::typeQualifiers < void > ( );
}

} /* namespace abstraction */

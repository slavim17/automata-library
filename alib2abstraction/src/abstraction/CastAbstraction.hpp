#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <object/ObjectFactory.h>

namespace abstraction {

template < class ReturnType, class ParamType >
class CastAbstraction : virtual public NaryOperationAbstraction < const ParamType & >, virtual public ValueOperationAbstraction < ReturnType > {
public:
	std::shared_ptr < abstraction::Value > run ( ) const override {
		const std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );

		return std::make_shared < abstraction::ValueHolder < ReturnType > > ( ReturnType ( retrieveValue < const ParamType & > ( param ) ), true );
	}

};

} /* namespace abstraction */


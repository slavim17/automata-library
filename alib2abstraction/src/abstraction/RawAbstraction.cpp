#include "RawAbstraction.hpp"

namespace abstraction {

void RawAbstraction::attachInput ( const std::shared_ptr < abstraction::Value > & input, size_t index ) {
	if ( index >= m_params.size ( ) )
		throw std::invalid_argument ( "Parameter index " + ext::to_string ( index ) + " out of bounds.");

	m_params [ index ] = input;
}

void RawAbstraction::detachInput ( size_t index ) {
	if ( index >= m_params.size ( ) )
		throw std::invalid_argument ( "Parameter index " + ext::to_string ( index ) + " out of bounds.");

	m_params [ index ] = nullptr;
}

bool RawAbstraction::inputsAttached ( ) const {
	auto attached_lambda = [ ] ( const std::shared_ptr < abstraction::Value > & param ) {
		return param != nullptr;
	};

	return std::all_of ( m_params.begin ( ), m_params.end ( ), attached_lambda );
}

std::shared_ptr < abstraction::Value > RawAbstraction::eval ( ) {
	if ( ! inputsAttached ( ) )
		return nullptr;

	return this->run ( );
}

} /* namespace abstraction */

#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/NormalizeFactory.hpp>

namespace abstraction {

template < class ReturnType >
class NormalizeAbstraction : virtual public NaryOperationAbstraction < ReturnType && >, virtual public ValueOperationAbstraction < object::Object > {
public:
	std::shared_ptr < abstraction::Value > run ( ) const override {
		const std::shared_ptr < abstraction::Value > & rawParam = std::get < 0 > ( this->getParams ( ) );
		ReturnType && param = retrieveValue < ReturnType && > ( rawParam );

		object::Object normalized = factory::NormalizeFactory::normalize < std::decay_t < ReturnType > > ( std::move ( param ) );
		return std::make_shared < abstraction::ValueHolder < object::Object > > ( std::move ( normalized ), true );
	}

};

} /* namespace abstraction */


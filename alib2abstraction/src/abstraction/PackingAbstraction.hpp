#pragma once

#include <ext/memory>
#include <ext/array>

#include <abstraction/OperationAbstraction.hpp>
#include <abstraction/Value.hpp>

namespace abstraction {

class PackingAbstractionImpl : public OperationAbstraction {
protected:
	struct ConnectionTarget {
		size_t targetId;
		size_t paramPosition;
	};

	struct ConnectionSource {
		size_t sourceId;
		size_t paramPosition;
	};

private:
	ext::vector < std::pair < std::unique_ptr < abstraction::OperationAbstraction >, std::vector < ConnectionSource > > > m_abstractions;

public:
	explicit PackingAbstractionImpl ( ext::vector < std::unique_ptr < abstraction::OperationAbstraction > > abstractions );

	std::shared_ptr < abstraction::Value > recursiveEval ( size_t id, std::vector < std::shared_ptr < abstraction::Value > > & cache );

	void setInnerConnection ( size_t sourceId, size_t targetId, size_t paramPosition );

protected:
	const ext::vector < std::pair < std::unique_ptr < abstraction::OperationAbstraction >, std::vector < ConnectionSource > > > & getAbstractions ( ) const {
		return m_abstractions;
	}

};

template < size_t NumberOfParams >
class PackingAbstraction : public PackingAbstractionImpl {
	ext::array < std::pair < ext::vector < ConnectionTarget >, std::shared_ptr < abstraction::Value > >, NumberOfParams > m_connections;
	size_t m_resultId;

public:
	void setOuterConnection ( size_t sourceId, size_t targetId, size_t paramPosition ) {
		m_connections [ sourceId ].first.push_back ( ConnectionTarget { targetId, paramPosition } );
	}

	PackingAbstraction ( ext::vector < std::unique_ptr < abstraction::OperationAbstraction > > abstractions, size_t resultId ) : PackingAbstractionImpl ( std::move ( abstractions ) ), m_resultId ( resultId ) {
	}

private:
	void attachInput ( const std::shared_ptr < abstraction::Value > & input, size_t index ) override {
		try {
			for ( const ConnectionTarget & target : m_connections [ index ].first )
				getAbstractions ( ) [ target.targetId ].first->attachInput ( input, target.paramPosition );
		} catch ( ... ) {
			this->detachInput ( index );
			throw;
		}
		m_connections [ index ].second = input;
	}

	void detachInput ( size_t index ) override {
		for ( const ConnectionTarget & target : m_connections [ index ].first )
			getAbstractions ( ) [ target.targetId ].first->detachInput ( target.paramPosition );

		m_connections [ index ].second = nullptr;
	}

	bool inputsAttached ( ) const override {
		// Note: it is up to the designer of the inner connections among packed abstraction to make sure once the outter connections are set the execution does not fail due to unattached inputs.

		return std::all_of ( m_connections.begin ( ), m_connections.end ( ), [ ] ( const std::pair < ext::vector < ConnectionTarget >, std::shared_ptr < abstraction::Value > > & connection ) {
			return static_cast < bool > ( connection.second );
		} );
	}

public:
	std::shared_ptr < abstraction::Value > eval ( ) override {
		if ( ! inputsAttached ( ) )
			return nullptr;

		std::vector < std::shared_ptr < abstraction::Value > > cache ( getAbstractions ( ).size ( ) );
		return recursiveEval ( m_resultId, cache );
	}

	size_t numberOfParams ( ) const override {
		return NumberOfParams;
	}

	core::type_details getParamType ( size_t index ) const override {
		return getAbstractions ( ) [ m_connections.at ( index ).first [ 0 ].targetId ].first->getParamType ( m_connections.at ( index ).first [ 0 ].paramPosition );
	}

	abstraction::TypeQualifiers::TypeQualifierSet getParamTypeQualifiers ( size_t index ) const override {
		return getAbstractions ( ) [ m_connections.at ( index ).first [ 0 ].targetId ].first->getParamTypeQualifiers ( m_connections.at ( index ).first [ 0 ].paramPosition );
	}

	core::type_details getReturnType ( ) const override {
		return getAbstractions ( ) [ m_resultId ].first->getReturnType ( );
	}

	abstraction::TypeQualifiers::TypeQualifierSet getReturnTypeQualifiers ( ) const override {
		return getAbstractions ( ) [ m_resultId ].first->getReturnTypeQualifiers ( );
	}

};

} /* namespace abstraction */

extern template class abstraction::PackingAbstraction < 2 >;
extern template class abstraction::PackingAbstraction < 3 >;


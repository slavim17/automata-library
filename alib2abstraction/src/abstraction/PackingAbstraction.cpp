#include "PackingAbstraction.hpp"

template class abstraction::PackingAbstraction < 2 >;
template class abstraction::PackingAbstraction < 3 >;

namespace abstraction {

PackingAbstractionImpl::PackingAbstractionImpl ( ext::vector < std::unique_ptr < abstraction::OperationAbstraction > > abstractions ) {
	std::transform ( abstractions.begin ( ), abstractions.end ( ), std::back_inserter ( m_abstractions ),
		[ ] ( std::unique_ptr < abstraction::OperationAbstraction > & abstraction ) { return std::make_pair ( std::move ( abstraction ), std::vector < ConnectionSource > { } ); }
	);
}

std::shared_ptr < abstraction::Value > PackingAbstractionImpl::recursiveEval ( size_t id, std::vector < std::shared_ptr < abstraction::Value > > & cache ) {
	for ( ConnectionSource connection : m_abstractions [ id ].second ) {
		if ( ! cache [ connection.sourceId ] )
			cache [ connection.sourceId ] = recursiveEval ( connection.sourceId, cache );

		m_abstractions [ id ].first->attachInput ( cache [ connection.sourceId ], connection.paramPosition );
	}

	return m_abstractions [ id ].first->eval ( );
}

void PackingAbstractionImpl::setInnerConnection ( size_t sourceId, size_t targetId, size_t paramPosition ) {
	m_abstractions [ targetId ].second.push_back ( ConnectionSource { sourceId, paramPosition } );
}

} /* namespace abstraction */

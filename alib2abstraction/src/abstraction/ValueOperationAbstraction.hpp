#pragma once

#include <ext/array>
#include <ext/memory>

#include <common/TypeQualifiers.hpp>

#include <abstraction/ValueHolder.hpp>
#include <common/AbstractionHelpers.hpp>

namespace abstraction {

template < class ReturnType >
class ValueOperationAbstraction : virtual public OperationAbstraction {
public:
	template < typename ... ParamTypes, typename Callable >
	static inline std::shared_ptr < abstraction::Value > run_helper ( Callable callback, const ext::array < std::shared_ptr < abstraction::Value >, sizeof ... ( ParamTypes ) > & inputs ) {
		return std::make_shared < abstraction::ValueHolder < ReturnType > > ( abstraction::apply < ParamTypes ... > ( callback, inputs ), ! std::is_lvalue_reference_v < ReturnType > );
	}

	abstraction::TypeQualifiers::TypeQualifierSet getReturnTypeQualifiers ( ) const override {
		return abstraction::TypeQualifiers::typeQualifiers < ReturnType > ( );
	}

	core::type_details getReturnType ( ) const override {
		return core::type_details::get < std::decay_t < ReturnType > > ( );
	}

};

template < >
class ValueOperationAbstraction < void > : virtual public OperationAbstraction {
public:
	template < typename ... ParamTypes, typename Callable >
	static inline std::shared_ptr < abstraction::Value > run_helper ( Callable callback, const ext::array < std::shared_ptr < abstraction::Value >, sizeof ... ( ParamTypes ) > & inputs ) {
		abstraction::apply < ParamTypes ... > ( callback, inputs );
		return std::make_shared < abstraction::Void > ( );
	}

	core::type_details getReturnType ( ) const override;

	abstraction::TypeQualifiers::TypeQualifierSet getReturnTypeQualifiers ( ) const override;
};

} /* namespace abstraction */


#pragma once

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class BaseRegistryEntry {
public:
	virtual ~BaseRegistryEntry ( ) = default;

	virtual std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const = 0;
};

} /* namespace abstraction */


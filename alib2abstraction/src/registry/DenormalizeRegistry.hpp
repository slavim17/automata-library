#pragma once

#include <ext/memory>
#include <ext/string>
#include <ext/map>
#include <ext/list>
#include <ext/typeinfo>

#include <abstraction/OperationAbstraction.hpp>
#include "BaseRegistryEntry.hpp"

namespace abstraction {

class DenormalizeRegistry {
public:
	class Entry : public BaseRegistryEntry {
	};

private:
	template < class ParamType >
	class EntryImpl : public Entry {
	public:
		EntryImpl ( ) = default;

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < core::type_details, std::list < std::unique_ptr < Entry > > > & getEntries ( );

public:
	static void unregisterDenormalize ( const core::type_details & param, std::list < std::unique_ptr < Entry > >::const_iterator iter );

	template < class ParamType >
	static void unregisterDenormalize ( std::list < std::unique_ptr < Entry > >::const_iterator iter ) {
		core::type_details ret = core::type_details::get < std::decay_t < ParamType > > ( );
		unregisterDenormalize ( ret, iter );
	}

	static std::list < std::unique_ptr < Entry > >::const_iterator registerDenormalize ( core::type_details param, std::unique_ptr < Entry > entry );

	template < class ParamType >
	static std::list < std::unique_ptr < Entry > >::const_iterator registerDenormalize ( core::type_details param ) {
		return registerDenormalize ( std::move ( param ), std::unique_ptr < Entry > ( new EntryImpl < ParamType > ( ) ) );
	}

	template < class ParamType >
	static std::list < std::unique_ptr < Entry > >::const_iterator registerDenormalize ( ) {
		core::type_details param = core::type_details::get < std::decay_t < ParamType > > ( );
		return registerDenormalize < ParamType > ( std::move ( param ) );
	}

	static bool hasDenormalize ( const core::type_details & param );

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const core::type_details & param );

	static ext::list < std::string > list ( );
};

} /* namespace abstraction */

#include <abstraction/DenormalizeAbstraction.hpp>

namespace abstraction {

template < class ParamType >
std::unique_ptr < abstraction::OperationAbstraction > DenormalizeRegistry::EntryImpl < ParamType >::getAbstraction ( ) const {
	return std::make_unique < DenormalizeAbstraction < ParamType > > ( );
}

} /* namespace abstraction */


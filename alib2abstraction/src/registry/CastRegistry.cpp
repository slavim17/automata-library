#include <registry/CastRegistry.hpp>

#include <ext/algorithm>
#include <exception>

namespace abstraction {

ext::map < ext::pair < core::type_details, core::type_details >, std::unique_ptr < CastRegistry::Entry > > & CastRegistry::getEntries ( ) {
	static ext::map < ext::pair < core::type_details, core::type_details >, std::unique_ptr < Entry > > casts;
	return casts;
}

void CastRegistry::unregisterCast ( const core::type_details & target, const core::type_details & param ) {
	if ( getEntries ( ).erase ( ext::tie ( target, param ) ) == 0u )
		throw std::invalid_argument ( ext::concat ( "Entry from ", param, " to ", target, " not registered." ) );
}

void CastRegistry::registerCast ( core::type_details target, core::type_details param, std::unique_ptr < Entry > entry ) {
	auto iter = getEntries ( ).insert ( std::make_pair ( ext::make_pair ( std::move ( target ), std::move ( param ) ), std::move ( entry ) ) );
	if ( ! iter.second )
		throw std::invalid_argument ( ext::concat ( "Entry from ", iter.first->first.second, " to ", iter.first->first.first, " already registered." ) );
}

std::unique_ptr < abstraction::OperationAbstraction > CastRegistry::getAbstraction ( const core::type_details & target, const core::type_details & param ) {
	auto entry = getEntries ( ).end ( );
	for ( auto iter = getEntries ( ).begin ( ); iter != getEntries ( ).end ( ); ++ iter )
		if ( param.compatible_with ( iter->first.second ) && iter->first.first.compatible_with ( target ) ) {
			if ( entry == getEntries ( ).end ( ) )
				entry = iter;
			else
				throw std::invalid_argument ( ext::concat ( "Entry from ", param, " to ", target, " is ambigous." ) );
		}

	if ( entry == getEntries ( ).end ( ) )
		throw std::invalid_argument ( ext::concat ( "Entry from ", param, " to ", target, " not available." ) );

	return entry->second->getAbstraction ( );
}

bool CastRegistry::isNoOp ( const core::type_details & target, const core::type_details & param ) {
	return target.compatible_with ( param );
}

bool CastRegistry::castAvailable ( const core::type_details & target, const core::type_details & param, bool implicitOnly ) {
	return std::any_of ( getEntries ( ).begin ( ), getEntries ( ).end ( ), [ & ] ( const std::pair < const ext::pair < core::type_details, core::type_details >, std::unique_ptr < Entry > > & entry ) {
		return ( param.compatible_with ( entry.first.second ) && entry.first.first.compatible_with ( target ) )
			&& ( ! implicitOnly || ! entry.second->isExplicit ( ) );
		} );
}

ext::list < ext::pair < core::type_details, bool > > CastRegistry::listFrom ( const core::type_details & type ) {
	ext::list < ext::pair < core::type_details, bool > > res;

	for ( const std::pair < const ext::pair < core::type_details, core::type_details >, std::unique_ptr < Entry > > & entry : getEntries ( ) )
		if ( type.compatible_with ( entry.first.second ) )
			res.push_back ( ext::make_pair ( entry.first.first, entry.second->isExplicit ( ) ) );

	return res;
}

ext::list < ext::pair < core::type_details, bool > > CastRegistry::listTo ( const core::type_details & type ) {
	ext::list < ext::pair < core::type_details, bool > > res;

	for ( const std::pair < const ext::pair < core::type_details, core::type_details >, std::unique_ptr < Entry > > & entry : getEntries ( ) )
		if ( type.compatible_with ( entry.first.first ) )
			res.push_back ( ext::make_pair ( entry.first.second, entry.second->isExplicit ( ) ) );

	return res;
}

ext::list < ext::tuple < core::type_details, core::type_details, bool > > CastRegistry::list ( ) {
	ext::list < ext::tuple < core::type_details, core::type_details, bool > > res;

	std::transform ( getEntries ( ).begin ( ), getEntries ( ).end ( ), std::back_inserter ( res ), [ ] ( const std::pair < const ext::pair < core::type_details, core::type_details >, std::unique_ptr < Entry > > & entry ) {
		return ext::make_tuple ( entry.first.first, entry.first.second, entry.second->isExplicit ( ) );
	} );

	return res;
}

} /* namespace abstraction */

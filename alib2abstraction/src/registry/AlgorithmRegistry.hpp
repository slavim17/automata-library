#pragma once

#include <optional>

#include <ext/functional>
#include <ext/memory>
#include <ext/typeinfo>
#include <ext/vector>
#include <ext/list>
#include <ext/string>
#include <ext/set>
#include <ext/map>
#include <ext/tuple>

#include <abstraction/OperationAbstraction.hpp>
#include "AlgorithmRegistryInfo.hpp"
#include "BaseRegistryEntry.hpp"

#include <common/TypeQualifiers.hpp>
#include <common/AlgorithmCategories.hpp>

namespace abstraction {

class AlgorithmRegistry {
public:
	class Entry : public BaseRegistryEntry {
		AlgorithmFullInfo m_entryInfo;

		std::optional<std::string> m_documentation;

	public:
		explicit Entry ( AlgorithmFullInfo entryInfo ) : m_entryInfo ( std::move ( entryInfo ) ) {
		}

		const AlgorithmFullInfo & getEntryInfo ( ) const {
			return m_entryInfo;
		}

		const std::optional < std::string > & getDocumentation ( ) {
			return m_documentation;
		}

		void setDocumentation ( std::string_view doc ) {
			m_documentation = doc;
		}
	};

private:
	template < class ObjectType, class Return, class ... Params >
	class MemberImpl : public Entry {
		std::function < Return ( typename std::remove_reference < ObjectType >::type *, Params ... ) > m_callback;

	public:
		explicit MemberImpl ( std::array < std::string, sizeof ... ( Params ) > paramNames, std::function < Return ( typename std::remove_reference < ObjectType >::type *, Params ... ) > callback ) : Entry ( AlgorithmFullInfo::methodEntryInfo < ObjectType, Return, Params ... > ( std::move ( paramNames ) ) ), m_callback ( std::move ( callback ) ) {
		}

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	template < class Return, class ... Params >
	class EntryImpl : public Entry {
		std::function < Return ( Params ... ) > m_callback;

	public:
		explicit EntryImpl ( AlgorithmCategories::AlgorithmCategory category, std::array < std::string, sizeof ... ( Params ) > paramNames, std::function < Return ( Params ... ) > callback ) : Entry ( AlgorithmFullInfo::algorithmEntryInfo < Return, Params ... > ( category, std::move ( paramNames ) ) ), m_callback ( std::move ( callback ) ) {
		}

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	template < class Return, class ... Params >
	class WrapperImpl : public Entry {
		std::function < std::unique_ptr < abstraction::OperationAbstraction > ( Params ... ) > m_wrapperFinder;

	public:
		explicit WrapperImpl ( std::array < std::string, sizeof ... ( Params ) > paramNames, std::function < std::unique_ptr < abstraction::OperationAbstraction > ( Params ... ) > wrapperFinder ) : Entry ( AlgorithmFullInfo::wrapperEntryInfo < Return, Params ... > ( std::move ( paramNames ) ) ), m_wrapperFinder ( std::move ( wrapperFinder ) ) {
		}

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	class RawImpl : public Entry {
		std::function < std::shared_ptr < abstraction::Value > ( const std::vector < std::shared_ptr < abstraction::Value > > & ) > m_rawCallback;

	public:
		explicit RawImpl ( ext::pair < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet > result, ext::vector < ext::tuple < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet, std::string > > paramSpecs, std::function < std::shared_ptr < abstraction::Value > ( const std::vector < std::shared_ptr < abstraction::Value > > & ) > rawCallback ) : Entry ( AlgorithmFullInfo::rawEntryInfo ( std::move ( result ), std::move ( paramSpecs ) ) ), m_rawCallback ( std::move ( rawCallback ) ) {
		}

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( ) const override;
	};

	static ext::map < ext::pair < std::string, ext::vector < std::string > >, ext::pair < std::string, ext::list < std::unique_ptr < Entry > > > > & getEntries ( );

	static bool isRegistered ( const std::string & algorithm, const ext::vector < std::string > & templateParams, const AlgorithmBaseInfo & entryInfo );

	static void registerInternal ( std::string algorithm, ext::vector < std::string > templateParams, std::unique_ptr < Entry > value );

	static void unregisterInternal ( const std::string & algorithm, const ext::vector < std::string > & templateParams, const AlgorithmBaseInfo & entryInfo );

	static void setDocumentation ( const std::string & algorithm, const ext::vector < std::string > & templateParams, const AlgorithmBaseInfo & entryInfo, std::string_view documentation );

	static void setCommonDocumentation ( const std::string & algorithm, const ext::vector < std::string > & templateParams, std::string_view documentation );

	static ext::list < std::unique_ptr < Entry > > & findAbstractionGroup ( const std::string & name, const ext::vector < std::string > & templateParams );

public:
	template < class Algo, class ObjectType, class ... ParamTypes >
	static void setDocumentationOfMethod ( const std::string & methodName, std::string_view documentation ) {
		std::string algorithm = ext::to_string < Algo > ( ) + "::" + methodName;
		ext::vector < std::string > templateParams;

		setDocumentation ( algorithm, templateParams, AlgorithmBaseInfo::methodEntryInfo < ObjectType &, ParamTypes ... > ( ), documentation );
	}

	template < class Algo, class ... ParamTypes >
	static void setDocumentationOfAlgorithm ( AlgorithmCategories::AlgorithmCategory category, std::string_view documentation ) {
		std::string algorithm = ext::to_string < Algo > ( );
		ext::vector < std::string > templateParams = ext::get_template_info ( algorithm );
		algorithm = ext::erase_template_info ( algorithm );

		setDocumentation ( algorithm, templateParams, AlgorithmBaseInfo::algorithmEntryInfo < ParamTypes ... > ( category ), documentation );
	}

	template < class Algo, class ... ParamTypes >
	static void setDocumentationOfWrapper ( std::string_view documentation ) {
		std::string algorithm = ext::to_string < Algo > ( );
		ext::vector < std::string > templateParams = ext::get_template_info ( algorithm );
		algorithm = ext::erase_template_info ( algorithm );

		setDocumentation ( algorithm, templateParams, AlgorithmBaseInfo::wrapperEntryInfo < ParamTypes ... > ( ), documentation );
	}

	template < class Algo >
	static void setCommonAlgoDocumentation ( std::string_view documentation ) {
		std::string algorithm = ext::to_string < Algo > ( );
		ext::vector < std::string > templateParams = ext::get_template_info ( algorithm );
		algorithm = ext::erase_template_info ( algorithm );

		setCommonDocumentation ( algorithm, templateParams, documentation );
	}

	template < class Algo, class ObjectType, class ... ParamTypes >
	static void unregisterMethod ( const std::string & methodName ) {
		std::string algorithm = ext::to_string < Algo > ( ) + "::" + methodName;
		ext::vector < std::string > templateParams;

		unregisterInternal ( algorithm, templateParams, AlgorithmBaseInfo::methodEntryInfo < ObjectType &, ParamTypes ... > ( ) );
	}

	template < class Algo, class ... ParamTypes >
	static void unregisterAlgorithm ( AlgorithmCategories::AlgorithmCategory category ) {
		std::string algorithm = ext::to_string < Algo > ( );
		ext::vector < std::string > templateParams = ext::get_template_info ( algorithm );
		algorithm = ext::erase_template_info ( algorithm );

		unregisterInternal ( algorithm, templateParams, AlgorithmBaseInfo::algorithmEntryInfo < ParamTypes ... > ( category ) );
	}

	template < class Algo, class ... ParamTypes >
	static void unregisterWrapper ( ) {
		std::string algorithm = ext::to_string < Algo > ( );
		ext::vector < std::string > templateParams = ext::get_template_info ( algorithm );
		algorithm = ext::erase_template_info ( algorithm );

		unregisterInternal ( algorithm, templateParams, AlgorithmBaseInfo::wrapperEntryInfo < ParamTypes ... > ( ) );
	}

	template < class Algo >
	static void unregisterRaw ( ext::vector < ext::pair < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet > > parameterSpecs ) {
		std::string algorithm = ext::to_string < Algo > ( );
		ext::vector < std::string > templateParams = ext::get_template_info ( algorithm );
		algorithm = ext::erase_template_info ( algorithm );

		unregisterInternal ( algorithm, templateParams, AlgorithmBaseInfo::rawEntryInfo ( std::move ( parameterSpecs ) ) );
	}

	template < class Algo, class ObjectType, class ReturnType, class ... ParamTypes >
	static void registerMethod ( ReturnType ( ObjectType:: * callback ) ( ParamTypes ... ), const std::string & methodName, std::array < std::string, sizeof ... ( ParamTypes ) > paramNames ) {
		std::string algorithm = ext::to_string < Algo > ( ) + "::" + methodName;
		ext::vector < std::string > templateParams;

		registerInternal ( std::move ( algorithm ), std::move ( templateParams ), std::make_unique < MemberImpl < ObjectType &, ReturnType, ParamTypes ... > > ( std::move ( paramNames ), callback ) );
	}

	template < class Algo, class ObjectType, class ReturnType, class ... ParamTypes >
	static void registerMethod ( ReturnType ( ObjectType:: * callback ) ( ParamTypes ... ) const, const std::string & methodName, std::array < std::string, sizeof ... ( ParamTypes ) > paramNames ) {
		std::string algorithm = ext::to_string < Algo > ( ) + "::" + methodName;
		ext::vector < std::string > templateParams;

		registerInternal ( std::move ( algorithm ), std::move ( templateParams ), std::make_unique < MemberImpl < const ObjectType &, ReturnType, ParamTypes ... > > ( std::move ( paramNames ), callback ) );
	}

	template < class Algo, class ReturnType, class ... ParamTypes >
	static void registerAlgorithm ( ReturnType ( * callback ) ( ParamTypes ... ), AlgorithmCategories::AlgorithmCategory category, std::array < std::string, sizeof ... ( ParamTypes ) > paramNames ) {
		std::string algorithm = ext::to_string < Algo > ( );
		ext::vector < std::string > templateParams = ext::get_template_info ( algorithm );
		algorithm = ext::erase_template_info ( algorithm );

		registerInternal ( std::move ( algorithm ), std::move ( templateParams ), std::make_unique < EntryImpl < ReturnType, ParamTypes ... > > ( category, std::move ( paramNames ), callback ) );
	}

	template < class Algo, class ReturnType, class ... ParamTypes >
	static void registerWrapper ( std::unique_ptr < abstraction::OperationAbstraction > ( * callback ) ( ParamTypes ... ), std::array < std::string, sizeof ... ( ParamTypes ) > paramNames ) {
		std::string algorithm = ext::to_string < Algo > ( );
		ext::vector < std::string > templateParams = ext::get_template_info ( algorithm );
		algorithm = ext::erase_template_info ( algorithm );

		registerInternal ( std::move ( algorithm ), std::move ( templateParams ), std::make_unique < WrapperImpl < ReturnType, ParamTypes ... > > ( std::move ( paramNames ), callback ) );
	}

	template < class Algo >
	static void registerRaw ( std::shared_ptr < abstraction::Value > ( * callback ) ( const std::vector < std::shared_ptr < abstraction::Value > > & ), ext::pair < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet > result, ext::vector < ext::tuple < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet, std::string > > paramSpecs ) {
		std::string algorithm = ext::to_string < Algo > ( );
		registerRaw ( algorithm, callback, std::move ( result ), std::move ( paramSpecs ) );
	}

	static void registerRaw ( std::string algorithm, std::function < std::shared_ptr < abstraction::Value > ( const std::vector < std::shared_ptr < abstraction::Value > > & ) > callback, ext::pair < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet > result, ext::vector < ext::tuple < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet, std::string > > paramSpecs ) {
		ext::vector < std::string > templateParams = ext::get_template_info ( algorithm );
		algorithm = ext::erase_template_info ( algorithm );

		registerInternal ( std::move ( algorithm ), std::move ( templateParams ), std::make_unique < RawImpl > ( std::move ( result ), std::move ( paramSpecs ), std::move ( callback ) ) );
	}

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & name, const ext::vector < std::string > & templateParams, const ext::vector < core::type_details > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory category );

	static std::optional < std::string > getCommonAlgoDocumentation ( const std::string & algorithm, const ext::vector < std::string > & templateParams );

	static ext::set < ext::pair < std::string, ext::vector < std::string > > > listGroup ( const std::string & group );

	static ext::list < ext::tuple < AlgorithmFullInfo, std::optional < std::string > > > listOverloads ( const std::string & algorithm, const ext::vector < std::string > & templateParams );

	static ext::set < ext::pair < std::string, ext::vector < std::string > > > list ( );
};

} /* namespace abstraction */

#include <abstraction/MemberAbstraction.hpp>
#include <abstraction/AlgorithmAbstraction.hpp>
#include <abstraction/WrapperAbstraction.hpp>

namespace abstraction {

template < class Object, class Return, class ... Params >
std::unique_ptr < abstraction::OperationAbstraction > AlgorithmRegistry::MemberImpl < Object, Return, Params ... >::getAbstraction ( ) const {
	return std::make_unique < abstraction::MemberAbstraction < Object, Return, Params ... > > ( m_callback );
}

template < class Return, class ... Params >
std::unique_ptr < abstraction::OperationAbstraction > AlgorithmRegistry::EntryImpl < Return, Params ... >::getAbstraction ( ) const {
	return std::make_unique < abstraction::AlgorithmAbstraction < Return, Params ... > > ( m_callback );
}

template < class Return, class ... Params >
std::unique_ptr < abstraction::OperationAbstraction > AlgorithmRegistry::WrapperImpl < Return, Params ... >::getAbstraction ( ) const {
	return std::make_unique < abstraction::WrapperAbstraction < Params ... > > ( m_wrapperFinder );
}

} /* namespace abstraction */

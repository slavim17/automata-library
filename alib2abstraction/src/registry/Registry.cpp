#include <registry/Registry.h>

#include <registry/AlgorithmRegistry.hpp>
#include <registry/ValuePrinterRegistry.hpp>
#include <registry/CastRegistry.hpp>
#include <registry/DenormalizeRegistry.hpp>
#include <registry/NormalizeRegistry.hpp>
#include <registry/ContainerRegistry.hpp>
#include <registry/OperatorRegistry.hpp>

namespace abstraction {

ext::set < ext::pair < std::string, ext::vector < std::string > > > Registry::listAlgorithmGroup ( const std::string & group ) {
	return AlgorithmRegistry::listGroup ( group );
}

ext::set < ext::pair < std::string, ext::vector < std::string > > > Registry::listAlgorithms ( ) {
	return AlgorithmRegistry::list ( );
}

ext::list < ext::pair < core::type_details, bool > > Registry::listCastsFrom ( const core::type_details & type ) {
	return CastRegistry::listFrom ( type );
}

ext::list < ext::pair < core::type_details, bool > > Registry::listCastsTo ( const core::type_details & type ) {
	return CastRegistry::listTo ( type );
}

ext::list < ext::tuple < core::type_details, core::type_details, bool > > Registry::listCasts ( ) {
	return CastRegistry::list ( );
}

ext::list < ext::tuple < AlgorithmFullInfo, std::optional < std::string > > > Registry::listOverloads ( const std::string & algorithm, const ext::vector < std::string > & templateParams ) {
	return AlgorithmRegistry::listOverloads ( algorithm, templateParams );
}

std::optional < std::string > Registry::getDocumentation ( const std::string & algorithm, const ext::vector < std::string > & templateParams ) {
	return AlgorithmRegistry::getCommonAlgoDocumentation ( algorithm, templateParams );
}

ext::list < ext::pair < Operators::BinaryOperators, AlgorithmFullInfo > > Registry::listBinaryOperators ( ) {
	return OperatorRegistry::listBinaryOverloads ( );
}

ext::list < ext::pair < Operators::PrefixOperators, AlgorithmFullInfo > > Registry::listPrefixOperators ( ) {
	return OperatorRegistry::listPrefixOverloads ( );
}

ext::list < ext::pair < Operators::PostfixOperators, AlgorithmFullInfo > > Registry::listPostfixOperators ( ) {
	return OperatorRegistry::listPostfixOverloads ( );
}

ext::list < std::string > Registry::listNormalizations ( ) {
	return NormalizeRegistry::list ( );
}

ext::list < std::string > Registry::listDenormalizations ( ) {
	return DenormalizeRegistry::list ( );
}

std::unique_ptr < abstraction::OperationAbstraction > Registry::getContainerAbstraction ( const std::string & container ) {
	return ContainerRegistry::getAbstraction ( container );
}

std::unique_ptr < abstraction::OperationAbstraction > Registry::getAlgorithmAbstraction ( const std::string & name, const ext::vector < std::string > & templateParams, const ext::vector < core::type_details > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory category ) {
	return AlgorithmRegistry::getAbstraction ( name, templateParams, paramTypes, typeQualifiers, category );
}

std::unique_ptr < abstraction::OperationAbstraction > Registry::getBinaryOperatorAbstraction ( Operators::BinaryOperators type, const ext::vector < core::type_details > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory category ) {
	return OperatorRegistry::getBinaryAbstraction ( type, paramTypes, typeQualifiers, category );
}

std::unique_ptr < abstraction::OperationAbstraction > Registry::getPrefixOperatorAbstraction ( Operators::PrefixOperators type, const ext::vector < core::type_details > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory category ) {
	return OperatorRegistry::getPrefixAbstraction ( type, paramTypes, typeQualifiers, category );
}

std::unique_ptr < abstraction::OperationAbstraction > Registry::getPostfixOperatorAbstraction ( Operators::PostfixOperators type, const ext::vector < core::type_details > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory category ) {
	return OperatorRegistry::getPostfixAbstraction ( type, paramTypes, typeQualifiers, category );
}

std::unique_ptr < abstraction::OperationAbstraction > Registry::getCastAbstraction ( const core::type_details & target, const core::type_details & param ) {
	return CastRegistry::getAbstraction ( target, param );
}

bool Registry::isCastNoOp ( const core::type_details & target, const core::type_details & param ) {
	return CastRegistry::isNoOp ( target, param );
}

std::unique_ptr < abstraction::OperationAbstraction > Registry::getNormalizeAbstraction ( const core::type_details & param ) {
	return NormalizeRegistry::getAbstraction ( param );
}

bool Registry::hasNormalize ( const core::type_details & param ) {
	return NormalizeRegistry::hasNormalize ( param );
}

std::unique_ptr < abstraction::OperationAbstraction > Registry::getValuePrinterAbstraction ( const core::type_details & param ) {
	return ValuePrinterRegistry::getAbstraction ( param );
}

std::unique_ptr < abstraction::OperationAbstraction > Registry::getDenormalizeAbstraction ( const core::type_details & param ) {
	return DenormalizeRegistry::getAbstraction ( param );
}

bool Registry::hasDenormalize ( const core::type_details & param ) {
	return DenormalizeRegistry::hasDenormalize ( param );
}

} /* namespace abstraction */

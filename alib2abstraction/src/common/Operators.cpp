#include <common/Operators.hpp>

namespace abstraction {

std::ostream & operator << ( std::ostream & os, Operators::BinaryOperators oper ) {
	return os << Operators::toString ( oper );
}

std::ostream & operator << ( std::ostream & os, Operators::PrefixOperators oper ) {
	return os << Operators::toString ( oper );
}

std::ostream & operator << ( std::ostream & os, Operators::PostfixOperators oper ) {
	return os << Operators::toString ( oper );
}

} /* namespace abstraction */

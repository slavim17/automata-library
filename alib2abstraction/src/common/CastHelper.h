#pragma once

#include <string>
#include <memory>
#include <abstraction/Value.hpp>
#include <abstraction/TemporariesHolder.h>

namespace abstraction {

class CastHelper {
public:
	static std::shared_ptr < abstraction::Value > eval ( abstraction::TemporariesHolder & environment, std::shared_ptr < abstraction::Value > param, const core::type_details & type );

};

} /* namespace abstraction */


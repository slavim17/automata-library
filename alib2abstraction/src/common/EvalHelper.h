#pragma once

#include <ext/string>
#include <ext/memory>
#include <ext/vector>
#include <common/AlgorithmCategories.hpp>
#include <common/Operators.hpp>
#include <abstraction/Value.hpp>
#include <abstraction/TemporariesHolder.h>

namespace abstraction {

class EvalHelper {
public:
	static std::shared_ptr < abstraction::Value > evalAbstraction ( abstraction::TemporariesHolder & environment, std::unique_ptr < abstraction::OperationAbstraction > abstraction, const ext::vector < std::shared_ptr < abstraction::Value > > & params );

	static std::shared_ptr < abstraction::Value > evalAlgorithm ( abstraction::TemporariesHolder & environment, const std::string & name, const ext::vector < std::string > & templateParams, const ext::vector < std::shared_ptr < abstraction::Value > > & params, abstraction::AlgorithmCategories::AlgorithmCategory category );

	static std::shared_ptr < abstraction::Value > evalOperator ( abstraction::TemporariesHolder & environment, abstraction::Operators::BinaryOperators type, const ext::vector < std::shared_ptr < abstraction::Value > > & params, abstraction::AlgorithmCategories::AlgorithmCategory category );

	static std::shared_ptr < abstraction::Value > evalOperator ( abstraction::TemporariesHolder & environment, abstraction::Operators::PrefixOperators type, const ext::vector < std::shared_ptr < abstraction::Value > > & params, abstraction::AlgorithmCategories::AlgorithmCategory category );

	static std::shared_ptr < abstraction::Value > evalOperator ( abstraction::TemporariesHolder & environment, abstraction::Operators::PostfixOperators type, const ext::vector < std::shared_ptr < abstraction::Value > > & params, abstraction::AlgorithmCategories::AlgorithmCategory category );
};

} /* namespace abstraction */


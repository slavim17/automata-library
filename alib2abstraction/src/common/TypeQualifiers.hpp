#pragma once

#include <ext/set>
#include <ext/type_traits>

namespace abstraction {

class TypeQualifiers {
public:
	enum class TypeQualifierSet {
		NONE = 0x0,
		CONST = 0x1,
		LREF = 0x2,
		RREF = 0x4,
	};

private:
	static constexpr bool is ( TypeQualifierSet first, TypeQualifierSet second ) {
		return ( static_cast < unsigned > ( first ) & static_cast < unsigned > ( second ) ) == static_cast < unsigned > ( second );
	}

public:
	friend constexpr TypeQualifierSet operator | ( TypeQualifierSet first, TypeQualifierSet second ) {
		unsigned res = static_cast < unsigned > ( first ) | static_cast < unsigned > ( second );
		if ( is ( static_cast < TypeQualifierSet > ( res ), TypeQualifiers::TypeQualifierSet::RREF ) && is ( static_cast < TypeQualifierSet > ( res ), TypeQualifiers::TypeQualifierSet::LREF ) )
			res &= ~ static_cast < unsigned > ( TypeQualifiers::TypeQualifierSet::RREF ); // decay LREF and RREF to LREF
		return static_cast < TypeQualifierSet > ( res );
	}

	static constexpr bool isConst ( TypeQualifierSet arg ) {
		return is ( arg, TypeQualifiers::TypeQualifierSet::CONST );
	}

	static constexpr bool isRef ( TypeQualifierSet arg ) {
		return isRvalueRef ( arg ) || isLvalueRef ( arg );
	}

	static constexpr bool isRvalueRef ( TypeQualifierSet arg ) {
		return is ( arg, TypeQualifiers::TypeQualifierSet::RREF );
	}

	static constexpr bool isLvalueRef ( TypeQualifierSet arg ) {
		return is ( arg, TypeQualifiers::TypeQualifierSet::LREF );
	}

	template < class Type >
	static constexpr TypeQualifierSet typeQualifiers ( ) {
		TypeQualifierSet res = TypeQualifierSet::NONE;

		if ( std::is_lvalue_reference < Type >::value )
			res = res | TypeQualifierSet::LREF;

		if ( std::is_rvalue_reference < Type >::value )
			res = res | TypeQualifierSet::RREF;

		if ( std::is_const < typename std::remove_reference < Type >::type >::value )
			res = res | TypeQualifierSet::CONST;

		return res;
	}

	friend std::ostream & operator << ( std::ostream & os, TypeQualifierSet typeQualifiers );
};

} /* namespace abstraction */


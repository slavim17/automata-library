#pragma once

#include <registry/CastRegistry.hpp>

#include <registration/NormalizationRegistration.hpp>
#include <registration/DenormalizationRegistration.hpp>

namespace registration {

template < class To, class From >
class CastRegister {
	registration::NormalizationRegister < To > normalize;
	registration::DenormalizationRegister < From > denormalize;

public:
	explicit CastRegister ( ) {
		abstraction::CastRegistry::registerCast < To, From > ( );
	}

	explicit CastRegister ( To ( * castFunction ) ( const From & ) ) {
		abstraction::CastRegistry::registerCastAlgorithm < To, From > ( castFunction );
	}

	~CastRegister ( ) {
		abstraction::CastRegistry::unregisterCast < To, From > ( );
	}
};

} /* namespace registration */


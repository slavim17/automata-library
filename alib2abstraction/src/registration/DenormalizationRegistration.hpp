#pragma once

#include <registry/DenormalizeRegistry.hpp>

#include <ext/registration>

#include <object/Object.h>

namespace registration {

class DenormalizationRegisterEmpty {
};

template < class ParamType >
class DenormalizationRegisterImpl : public ext::Register < std::list < std::unique_ptr < abstraction::DenormalizeRegistry::Entry > >::const_iterator > {
public:
	explicit DenormalizationRegisterImpl ( );
};

template < class ParamType >
DenormalizationRegisterImpl < ParamType >::DenormalizationRegisterImpl ( ) : ext::Register < std::list < std::unique_ptr < abstraction::DenormalizeRegistry::Entry > >::const_iterator > ( [ ] ( ) {
		return abstraction::DenormalizeRegistry::registerDenormalize < ParamType > ( );
	}, [ ] ( std::list < std::unique_ptr < abstraction::DenormalizeRegistry::Entry > >::const_iterator iter ) {
		abstraction::DenormalizeRegistry::unregisterDenormalize < ParamType > ( iter );
	} ) {
}

template < class ParamType >
using DenormalizationRegister = std::conditional_t < core::is_specialized < core::type_util < std::decay_t < ParamType > > > && ! std::is_same_v < std::decay_t < ParamType >, object::Object >, DenormalizationRegisterImpl < ParamType >, DenormalizationRegisterEmpty >;

} /* namespace registration */


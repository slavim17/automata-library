#include "ObjectFactory.h"

namespace object {

Object ObjectFactoryImpl < Object >::construct ( Object && object ) {
	return std::move ( object );
}

Object ObjectFactoryImpl < const Object & >::construct ( const Object & object ) {
	return object;
}

Object ObjectFactoryImpl < Object & >::construct ( Object & object ) {
	return object;
}

Object ObjectFactoryImpl < const char * const & >::construct ( const char * const string ) {
	return ObjectFactoryImpl < std::string >::construct ( std::string ( string ) );
}

Object ObjectFactoryImpl < char * const & >::construct ( char * const string ) {
	return ObjectFactoryImpl < std::string >::construct ( std::string ( string ) );
}

Object ObjectFactoryImpl < const char * & >::construct ( const char * const string ) {
	return ObjectFactoryImpl < std::string >::construct ( std::string ( string ) );
}

Object ObjectFactoryImpl < char * & >::construct ( char * const string ) {
	return ObjectFactoryImpl < std::string >::construct ( std::string ( string ) );
}

Object ObjectFactoryImpl < const char * const >::construct ( const char * const string ) {
	return ObjectFactoryImpl < std::string >::construct ( std::string ( string ) );
}

Object ObjectFactoryImpl < char * const >::construct ( char * const string ) {
	return ObjectFactoryImpl < std::string >::construct ( std::string ( string ) );
}

Object ObjectFactoryImpl < const char * >::construct ( const char * string ) {
	return ObjectFactoryImpl < std::string >::construct ( std::string ( string ) );
}

Object ObjectFactoryImpl < char * >::construct ( char * string ) {
	return ObjectFactoryImpl < std::string >::construct ( std::string ( string ) );
}

template Object ObjectFactoryImpl < std::string >::construct ( std::string && );

} /* namespace core */

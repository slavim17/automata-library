#include "SimpleRulesRemover.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto SimpleRulesRemoverCFG = registration::AbstractRegister < grammar::simplify::SimpleRulesRemover, grammar::CFG < >, const grammar::CFG < > & > ( grammar::simplify::SimpleRulesRemover::remove, "grammar" ).setDocumentation (
"Removes simple rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without simple rules" );

auto SimpleRulesRemoverEpsilonFreeCFG = registration::AbstractRegister < grammar::simplify::SimpleRulesRemover, grammar::EpsilonFreeCFG < >, const grammar::EpsilonFreeCFG < > & > ( grammar::simplify::SimpleRulesRemover::remove, "grammar" ).setDocumentation (
"Removes simple rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without simple rules" );

auto SimpleRulesRemoverCNF = registration::AbstractRegister < grammar::simplify::SimpleRulesRemover, grammar::CNF < >, const grammar::CNF < > & > ( grammar::simplify::SimpleRulesRemover::remove, "grammar" ).setDocumentation (
"Removes simple rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without simple rules" );

auto SimpleRulesRemoverGNF = registration::AbstractRegister < grammar::simplify::SimpleRulesRemover, grammar::GNF < >, const grammar::GNF < > & > ( grammar::simplify::SimpleRulesRemover::remove, "grammar" ).setDocumentation (
"Removes simple rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without simple rules" );

auto SimpleRulesRemoverLG = registration::AbstractRegister < grammar::simplify::SimpleRulesRemover, grammar::LG < >, const grammar::LG < > & > ( grammar::simplify::SimpleRulesRemover::remove, "grammar" ).setDocumentation (
"Removes simple rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without simple rules" );

auto SimpleRulesRemoverLeftLG = registration::AbstractRegister < grammar::simplify::SimpleRulesRemover, grammar::LeftLG < >, const grammar::LeftLG < > & > ( grammar::simplify::SimpleRulesRemover::remove, "grammar" ).setDocumentation (
"Removes simple rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without simple rules" );

auto SimpleRulesRemoverLeftRG = registration::AbstractRegister < grammar::simplify::SimpleRulesRemover, grammar::LeftRG < >, const grammar::LeftRG < > & > ( grammar::simplify::SimpleRulesRemover::remove, "grammar" ).setDocumentation (
"Removes simple rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without simple rules" );

auto SimpleRulesRemoverRightLG = registration::AbstractRegister < grammar::simplify::SimpleRulesRemover, grammar::RightLG < >, const grammar::RightLG < > & > ( grammar::simplify::SimpleRulesRemover::remove, "grammar" ).setDocumentation (
"Removes simple rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without simple rules" );

auto SimpleRulesRemoverRightRG = registration::AbstractRegister < grammar::simplify::SimpleRulesRemover, grammar::RightRG < >, const grammar::RightRG < > & > ( grammar::simplify::SimpleRulesRemover::remove, "grammar" ).setDocumentation (
"Removes simple rules from the given grammar.\n\
\n\
@param grammar the modified grammar\n\
@return grammar without simple rules" );

} /* namespace */

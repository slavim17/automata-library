#include "UnreachableSymbolsRemover.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto UnreachableSymbolsRemoverCFG = registration::AbstractRegister < grammar::simplify::UnreachableSymbolsRemover, grammar::CFG < >, const grammar::CFG < > & > ( grammar::simplify::UnreachableSymbolsRemover::remove, "grammar" ).setDocumentation (
"Removes unreachable symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unreachable symbols" );

auto UnreachableSymbolsRemoverEpsilonFreeCFG = registration::AbstractRegister < grammar::simplify::UnreachableSymbolsRemover, grammar::EpsilonFreeCFG < >, const grammar::EpsilonFreeCFG < > & > ( grammar::simplify::UnreachableSymbolsRemover::remove, "grammar" ).setDocumentation (
"Removes unreachable symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unreachable symbols" );

auto UnreachableSymbolsRemoverGNF = registration::AbstractRegister < grammar::simplify::UnreachableSymbolsRemover, grammar::GNF < >, const grammar::GNF < > & > ( grammar::simplify::UnreachableSymbolsRemover::remove, "grammar" ).setDocumentation (
"Removes unreachable symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unreachable symbols" );

auto UnreachableSymbolsRemoverCNF = registration::AbstractRegister < grammar::simplify::UnreachableSymbolsRemover, grammar::CNF < >, const grammar::CNF < > & > ( grammar::simplify::UnreachableSymbolsRemover::remove, "grammar" ).setDocumentation (
"Removes unreachable symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unreachable symbols" );

auto UnreachableSymbolsRemoverLG = registration::AbstractRegister < grammar::simplify::UnreachableSymbolsRemover, grammar::LG < >, const grammar::LG < > & > ( grammar::simplify::UnreachableSymbolsRemover::remove, "grammar" ).setDocumentation (
"Removes unreachable symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unreachable symbols" );

auto UnreachableSymbolsRemoverLeftLG = registration::AbstractRegister < grammar::simplify::UnreachableSymbolsRemover, grammar::LeftLG < >, const grammar::LeftLG < > & > ( grammar::simplify::UnreachableSymbolsRemover::remove, "grammar" ).setDocumentation (
"Removes unreachable symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unreachable symbols" );

auto UnreachableSymbolsRemoverLeftRG = registration::AbstractRegister < grammar::simplify::UnreachableSymbolsRemover, grammar::LeftRG < >, const grammar::LeftRG < > & > ( grammar::simplify::UnreachableSymbolsRemover::remove, "grammar" ).setDocumentation (
"Removes unreachable symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unreachable symbols" );

auto UnreachableSymbolsRemoverRightLG = registration::AbstractRegister < grammar::simplify::UnreachableSymbolsRemover, grammar::RightLG < >, const grammar::RightLG < > & > ( grammar::simplify::UnreachableSymbolsRemover::remove, "grammar" ).setDocumentation (
"Removes unreachable symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unreachable symbols" );

auto UnreachableSymbolsRemoverRightRG = registration::AbstractRegister < grammar::simplify::UnreachableSymbolsRemover, grammar::RightRG < >, const grammar::RightRG < > & > ( grammar::simplify::UnreachableSymbolsRemover::remove, "grammar" ).setDocumentation (
"Removes unreachable symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unreachable symbols" );

} /* namespace */

#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>

#include "IsLanguageGeneratingEpsilon.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto IsLanguageGeneratingEpsilonCFG = registration::AbstractRegister < grammar::properties::IsLanguageGeneratingEpsilon, bool, const grammar::CFG < > & > ( grammar::properties::IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon, "grammar" ).setDocumentation (
"Decides whether \\e in L( grammar )\n\
Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();\n\
\n\
@param grammar the tested grammar\n\
@returns true if \\e in L(@p grammar)" );

auto IsLanguageGeneratingEpsilonEpsilonFreeCFG = registration::AbstractRegister < grammar::properties::IsLanguageGeneratingEpsilon, bool, const grammar::EpsilonFreeCFG < > & > ( grammar::properties::IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon, "grammar" ).setDocumentation (
"Decides whether \\e in L( grammar )\n\
Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();\n\
\n\
@param grammar the tested grammar\n\
@returns true if \\e in L(@p grammar)" );

auto IsLanguageGeneratingEpsilonGNF = registration::AbstractRegister < grammar::properties::IsLanguageGeneratingEpsilon, bool, const grammar::GNF < > & > ( grammar::properties::IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon, "grammar" ).setDocumentation (
"Decides whether \\e in L( grammar )\n\
Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();\n\
\n\
@param grammar the tested grammar\n\
@returns true if \\e in L(@p grammar)" );

auto IsLanguageGeneratingEpsilonCNF = registration::AbstractRegister < grammar::properties::IsLanguageGeneratingEpsilon, bool, const grammar::CNF < > & > ( grammar::properties::IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon, "grammar" ).setDocumentation (
"Decides whether \\e in L( grammar )\n\
Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();\n\
\n\
@param grammar the tested grammar\n\
@returns true if \\e in L(@p grammar)" );

auto IsLanguageGeneratingEpsilonLG = registration::AbstractRegister < grammar::properties::IsLanguageGeneratingEpsilon, bool, const grammar::LG < > & > ( grammar::properties::IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon, "grammar" ).setDocumentation (
"Decides whether \\e in L( grammar )\n\
Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();\n\
\n\
@param grammar the tested grammar\n\
@returns true if \\e in L(@p grammar)" );

auto IsLanguageGeneratingEpsilonLeftLG = registration::AbstractRegister < grammar::properties::IsLanguageGeneratingEpsilon, bool, const grammar::LeftLG < > & > ( grammar::properties::IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon, "grammar" ).setDocumentation (
"Decides whether \\e in L( grammar )\n\
Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();\n\
\n\
@param grammar the tested grammar\n\
@returns true if \\e in L(@p grammar)" );

auto IsLanguageGeneratingEpsilonLeftRG = registration::AbstractRegister < grammar::properties::IsLanguageGeneratingEpsilon, bool, const grammar::LeftRG < > & > ( grammar::properties::IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon, "grammar" ).setDocumentation (
"Decides whether \\e in L( grammar )\n\
Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();\n\
\n\
@param grammar the tested grammar\n\
@returns true if \\e in L(@p grammar)" );

auto IsLanguageGeneratingEpsilonRightLG = registration::AbstractRegister < grammar::properties::IsLanguageGeneratingEpsilon, bool, const grammar::RightLG < > & > ( grammar::properties::IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon, "grammar" ).setDocumentation (
"Decides whether \\e in L( grammar )\n\
Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();\n\
\n\
@param grammar the tested grammar\n\
@returns true if \\e in L(@p grammar)" );

auto IsLanguageGeneratingEpsilonRightRG = registration::AbstractRegister < grammar::properties::IsLanguageGeneratingEpsilon, bool, const grammar::RightRG < > & > ( grammar::properties::IsLanguageGeneratingEpsilon::isLanguageGeneratingEpsilon, "grammar" ).setDocumentation (
"Decides whether \\e in L( grammar )\n\
Severals steps implemented in method @see grammar::properties::NullableNonterminals::getNullableNonterminals();\n\
\n\
@param grammar the tested grammar\n\
@returns true if \\e in L(@p grammar)" );

} /* namespace */


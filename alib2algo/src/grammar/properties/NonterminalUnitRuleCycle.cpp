#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>

#include "NonterminalUnitRuleCycle.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto NonterminalUnitRuleCycleCFG = registration::AbstractRegister < grammar::properties::NonterminalUnitRuleCycle, ext::set < DefaultSymbolType >, const grammar::CFG < > &, const DefaultSymbolType & > ( grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle, "grammar", "nonterminal" ).setDocumentation (
"Retrieves set N = {B : A->^* B} for given @p grammar and @p nonterminal.\n\
Source: Melichar, algorithm 2.6, step 1\n\
\n\
@param grammar grammar\n\
@param nonterminal nonterminal\n\
@return set of nonterminals that can be derived from the given nonterminal in finite number of steps" );

auto NonterminalUnitRuleCycleEpsilonFreeCFG = registration::AbstractRegister < grammar::properties::NonterminalUnitRuleCycle, ext::set < DefaultSymbolType >, const grammar::EpsilonFreeCFG < > &, const DefaultSymbolType & > ( grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle, "grammar", "nonterminal" ).setDocumentation (
"Retrieves set N = {B : A->^* B} for given @p grammar and @p nonterminal.\n\
Source: Melichar, algorithm 2.6, step 1\n\
\n\
@param grammar grammar\n\
@param nonterminal nonterminal\n\
@return set of nonterminals that can be derived from the given nonterminal in finite number of steps" );

auto NonterminalUnitRuleCycleGNF = registration::AbstractRegister < grammar::properties::NonterminalUnitRuleCycle, ext::set < DefaultSymbolType >, const grammar::GNF < > &, const DefaultSymbolType & > ( grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle, "grammar", "nonterminal" ).setDocumentation (
"Retrieves set N = {B : A->^* B} for given @p grammar and @p nonterminal.\n\
Source: Melichar, algorithm 2.6, step 1\n\
\n\
@param grammar grammar\n\
@param nonterminal nonterminal\n\
@return set of nonterminals that can be derived from the given nonterminal in finite number of steps" );

auto NonterminalUnitRuleCycleCNF = registration::AbstractRegister < grammar::properties::NonterminalUnitRuleCycle, ext::set < DefaultSymbolType >, const grammar::CNF < > &, const DefaultSymbolType & > ( grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle, "grammar", "nonterminal" ).setDocumentation (
"Retrieves set N = {B : A->^* B} for given @p grammar and @p nonterminal.\n\
Source: Melichar, algorithm 2.6, step 1\n\
\n\
@param grammar grammar\n\
@param nonterminal nonterminal\n\
@return set of nonterminals that can be derived from the given nonterminal in finite number of steps" );

auto NonterminalUnitRuleCycleLG = registration::AbstractRegister < grammar::properties::NonterminalUnitRuleCycle, ext::set < DefaultSymbolType >, const grammar::LG < > &, const DefaultSymbolType & > ( grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle, "grammar", "nonterminal" ).setDocumentation (
"Retrieves set N = {B : A->^* B} for given @p grammar and @p nonterminal.\n\
Source: Melichar, algorithm 2.6, step 1\n\
\n\
@param grammar grammar\n\
@param nonterminal nonterminal\n\
@return set of nonterminals that can be derived from the given nonterminal in finite number of steps" );

auto NonterminalUnitRuleCycleLeftLG = registration::AbstractRegister < grammar::properties::NonterminalUnitRuleCycle, ext::set < DefaultSymbolType >, const grammar::LeftLG < > &, const DefaultSymbolType & > ( grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle, "grammar", "nonterminal" ).setDocumentation (
"Retrieves set N = {B : A->^* B} for given @p grammar and @p nonterminal.\n\
Source: Melichar, algorithm 2.6, step 1\n\
\n\
@param grammar grammar\n\
@param nonterminal nonterminal\n\
@return set of nonterminals that can be derived from the given nonterminal in finite number of steps" );

auto NonterminalUnitRuleCycleLeftRG = registration::AbstractRegister < grammar::properties::NonterminalUnitRuleCycle, ext::set < DefaultSymbolType >, const grammar::LeftRG < > &, const DefaultSymbolType & > ( grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle, "grammar", "nonterminal" ).setDocumentation (
"Retrieves set N = {B : A->^* B} for given @p grammar and @p nonterminal.\n\
Source: Melichar, algorithm 2.6, step 1\n\
\n\
@param grammar grammar\n\
@param nonterminal nonterminal\n\
@return set of nonterminals that can be derived from the given nonterminal in finite number of steps" );

auto NonterminalUnitRuleCycleRightLG = registration::AbstractRegister < grammar::properties::NonterminalUnitRuleCycle, ext::set < DefaultSymbolType >, const grammar::RightLG < > &, const DefaultSymbolType & > ( grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle, "grammar", "nonterminal" ).setDocumentation (
"Retrieves set N = {B : A->^* B} for given @p grammar and @p nonterminal.\n\
Source: Melichar, algorithm 2.6, step 1\n\
\n\
@param grammar grammar\n\
@param nonterminal nonterminal\n\
@return set of nonterminals that can be derived from the given nonterminal in finite number of steps" );

auto NonterminalUnitRuleCycleRightRG = registration::AbstractRegister < grammar::properties::NonterminalUnitRuleCycle, ext::set < DefaultSymbolType >, const grammar::RightRG < > &, const DefaultSymbolType & > ( grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle, "grammar", "nonterminal" ).setDocumentation (
"Retrieves set N = {B : A->^* B} for given @p grammar and @p nonterminal.\n\
Source: Melichar, algorithm 2.6, step 1\n\
\n\
@param grammar grammar\n\
@param nonterminal nonterminal\n\
@return set of nonterminals that can be derived from the given nonterminal in finite number of steps" );

} /* namespace */

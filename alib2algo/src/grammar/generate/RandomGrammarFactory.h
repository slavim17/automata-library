#pragma once

#include <string>

#include <ext/algorithm>
#include <ext/random>

#include <alib/deque>
#include <alib/set>

#include <exception/CommonException.h>

#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace generate {

class RandomGrammarFactory {
public:
	/**
	 * Generates a random context free grammar.
	 *
	 * \tparam TerminalSymbolType the type of terminal symbols of the random grammar
	 * \tparam NonterminalSymbolType the type of nonterminal symbols of the random grammar
	 *
	 * \param nonterminals the nonterminals in the generated grammar
	 * \param terminals the terminals in the generated grammar
	 * \param density density of the rule set of the generated grammar
	 *
	 * \return random context free grammar
	 */
	template < class TerminalSymbolType, class NonterminalSymbolType >
	static grammar::CFG < TerminalSymbolType, NonterminalSymbolType > generateCFG ( ext::set < NonterminalSymbolType > nonterminals, ext::set < TerminalSymbolType > terminals, double density );

private:
	template < class TerminalSymbolType, class NonterminalSymbolType >
	static grammar::CFG < TerminalSymbolType, NonterminalSymbolType > randomCFG ( const ext::deque < NonterminalSymbolType > & nonterminals, const ext::deque < TerminalSymbolType > & terminals, double density );
};

template < class TerminalSymbolType, class NonterminalSymbolType >
grammar::CFG < TerminalSymbolType, NonterminalSymbolType > RandomGrammarFactory::generateCFG ( ext::set < NonterminalSymbolType > nonterminals, ext::set < TerminalSymbolType > terminals, double density ) {
	ext::deque < TerminalSymbolType > terminals2 ( terminals.begin ( ), terminals.end ( ) );
	ext::deque < NonterminalSymbolType > nonterminals2 ( nonterminals.begin ( ), nonterminals.end ( ) );
	return RandomGrammarFactory::randomCFG ( nonterminals2, terminals2, density );
}

template < class TerminalSymbolType, class NonterminalSymbolType >
grammar::CFG < TerminalSymbolType, NonterminalSymbolType > RandomGrammarFactory::randomCFG ( const ext::deque < NonterminalSymbolType > & nonterminals, const ext::deque < TerminalSymbolType > & terminals, double density ) {
	if( terminals.empty ( ) )
		throw exception::CommonException( "Terminals count must be greater than 0." );

	if( nonterminals.empty ( ) )
		throw exception::CommonException( "Nonterminals count must be greater than 0." );

	grammar::CFG < TerminalSymbolType, NonterminalSymbolType > grammar(nonterminals.front());
	grammar.setTerminalAlphabet({terminals.begin(), terminals.end()});
	grammar.setNonterminalAlphabet({nonterminals.begin(), nonterminals.end()});

	if ( ext::uniform_unsigned_event ( 50 ) ( ext::random_devices::semirandom ) )
		grammar.addRule(grammar.getInitialSymbol(), {});

	int rules = 0;
	while(rules < terminals.size() * nonterminals.size() * density / 100) {
		size_t nonterminal = std::uniform_int_distribution < size_t > ( 0, nonterminals.size ( ) - 1 ) ( ext::random_devices::semirandom );
		const NonterminalSymbolType & lhs = nonterminals [ nonterminal ];

		size_t rhsSize = std::uniform_int_distribution < size_t > ( 0, 4 ) ( ext::random_devices::semirandom );
		ext::vector < ext::variant < TerminalSymbolType, NonterminalSymbolType > > rhs;
		int nonterminalsOnRHS = 0;
		for ( size_t i = 0; i < rhsSize; i++) {
			size_t symbol = std::uniform_int_distribution < size_t > ( 0, nonterminals.size ( ) + terminals.size ( ) - 1 ) ( ext::random_devices::semirandom );
			if ( symbol < nonterminals.size ( ) ) {
				rhs.push_back ( ext::variant < TerminalSymbolType, NonterminalSymbolType > ( nonterminals [ symbol ] ) );
				nonterminalsOnRHS++;
			} else {
				rhs.push_back ( ext::variant < TerminalSymbolType, NonterminalSymbolType > ( terminals [ symbol - nonterminals.size ( ) ] ) );
			}
		}

		rules += nonterminalsOnRHS;
		grammar.addRule(lhs, rhs);
	}

	return grammar;
}

} /* namespace generate */

} /* namespace grammar */


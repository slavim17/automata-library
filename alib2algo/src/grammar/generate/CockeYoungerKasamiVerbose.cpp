#include "CockeYoungerKasamiVerbose.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto CockeYoungerKasamiVerboseCNF = registration::AbstractRegister < grammar::generate::CockeYoungerKasamiVerbose, ext::vector < ext::vector < ext::set < DefaultSymbolType > > >, const grammar::CNF < > &, const string::LinearString < > & > ( grammar::generate::CockeYoungerKasamiVerbose::generate, "grammar", "string" ).setDocumentation (
"Implements the Cocke Younger Kasami algorithm to test whether string is in language generated by a grammar.\n\
\n\
@param grammar context free grammar in chomsky's normal form\n\
@param string the tested string\n\
@return the internal table constructed by the Cock Younger Kasami algorithm" );

} /* namespace */

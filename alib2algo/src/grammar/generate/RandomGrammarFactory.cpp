#include "RandomGrammarFactory.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GenerateCFG1 = registration::AbstractRegister < grammar::generate::RandomGrammarFactory, grammar::CFG < >, ext::set < DefaultSymbolType >, ext::set < DefaultSymbolType >, double > ( grammar::generate::RandomGrammarFactory::generateCFG, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "nonterminals", "terminals", "density" ).setDocumentation (
"Generates a random context free grammar.\n\
\n\
@param nonterminals the nonterminals in the generated grammar\n\
@param terminals the terminals in the generated grammar\n\
@param density density of the rule set of the generated grammar\n\
@return random context free grammar" );

} /* namespace */

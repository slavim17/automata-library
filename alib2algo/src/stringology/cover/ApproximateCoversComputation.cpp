#include "ApproximateCoversComputation.h"
#include <registration/AlgoRegistration.hpp>

namespace stringology::cover {

auto ApproximateCoversLinearString = registration::AbstractRegister < ApproximateCoversComputation, ext::set < ext::pair < string::LinearString < DefaultSymbolType >, unsigned int > >, const string::LinearString < > &, unsigned > ( ApproximateCoversComputation::compute );

}  /* namespace stringology::cover */

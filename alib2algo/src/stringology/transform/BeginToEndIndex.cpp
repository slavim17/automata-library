#include "BeginToEndIndex.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BeginToEndIndexLinearString = registration::AbstractRegister < stringology::transform::BeginToEndIndex, ext::set < unsigned >, const string::LinearString < > &, const ext::set < unsigned > & > ( stringology::transform::BeginToEndIndex::transform, "pattern", "indexes" ).setDocumentation (
"Transforms a set of occurrences represented by begin indexes to set of occurrences representing by end indexes.\n\
\n\
@param pattern the pattern that was being searched for\n\
@param indexes the original occurrences\n\
@return the set of transformed occurences" );

} /* namespace */

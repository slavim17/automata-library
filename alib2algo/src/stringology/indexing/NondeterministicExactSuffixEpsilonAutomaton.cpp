/*
 * Author: Radovan Cerveny
 */

#include "NondeterministicExactSuffixEpsilonAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto SuffixAutomatonNondeterministicLinearString = registration::AbstractRegister < stringology::indexing::NondeterministicExactSuffixEpsilonAutomaton, automaton::EpsilonNFA < DefaultSymbolType, unsigned >, const string::LinearString < > & > ( stringology::indexing::NondeterministicExactSuffixEpsilonAutomaton::construct );

} /* namespace */

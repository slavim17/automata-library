#include "SuffixTrieNaive.h"

#include <string/LinearString.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto suffixTrieNaiveLinearString = registration::AbstractRegister < stringology::indexing::SuffixTrieNaive, indexes::stringology::SuffixTrie < DefaultSymbolType >, const string::LinearString < > & > ( stringology::indexing::SuffixTrieNaive::construct );

} /* namespace */

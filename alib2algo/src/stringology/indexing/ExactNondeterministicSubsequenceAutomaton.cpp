#include "ExactNondeterministicSubsequenceAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactNondeterministicSubsequenceAutomatonLinearString = registration::AbstractRegister < stringology::indexing::ExactNondeterministicSubsequenceAutomaton, automaton::EpsilonNFA < DefaultSymbolType, unsigned >, const string::LinearString < > & > ( stringology::indexing::ExactNondeterministicSubsequenceAutomaton::construct );

} /* namespace */

#include "ExactSubsequenceAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactSubsequenceAutomatonLinearString = registration::AbstractRegister < stringology::indexing::ExactSubsequenceAutomaton, automaton::DFA < DefaultSymbolType, unsigned >, const string::LinearString < > & > ( stringology::indexing::ExactSubsequenceAutomaton::construct );

} /* namespace */

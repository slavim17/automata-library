#include "CompressedBitParallelIndexConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto compressedCompressedBitParallelIndexConstructionLinearString = registration::AbstractRegister < stringology::indexing::CompressedBitParallelIndexConstruction, indexes::stringology::CompressedBitParallelIndex < DefaultSymbolType >, const string::LinearString < > & > ( stringology::indexing::CompressedBitParallelIndexConstruction::construct );

} /* namespace */

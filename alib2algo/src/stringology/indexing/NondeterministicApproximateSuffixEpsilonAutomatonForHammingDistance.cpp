#include "NondeterministicApproximateSuffixEpsilonAutomatonForHammingDistance.h"
#include <registration/AlgoRegistration.hpp>

namespace stringology::indexing {

auto NDApproximateSuffixAutomatonEpsilon = registration::AbstractRegister < NondeterministicApproximateSuffixEpsilonAutomatonForHammingDistance, automaton::EpsilonNFA < DefaultSymbolType, ext::pair < unsigned, unsigned > >, const string::LinearString < > &, unsigned > ( NondeterministicApproximateSuffixEpsilonAutomatonForHammingDistance::construct );

}  /* namespace stringology::indexing */

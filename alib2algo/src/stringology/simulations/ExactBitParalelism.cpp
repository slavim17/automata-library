#include "ExactBitParalelism.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactBitParalelismLinearString = registration::AbstractRegister < stringology::simulations::ExactBitParalelism, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::simulations::ExactBitParalelism::search );

} /* namespace */

#include "GeneralizedLevenshteinDynamicProgramming.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GeneralizedLevenshteinDynamicProgrammingLinearString = registration::AbstractRegister < stringology::simulations::GeneralizedLevenshteinDynamicProgramming, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > &, unsigned > ( stringology::simulations::GeneralizedLevenshteinDynamicProgramming::search );

} /* namespace */

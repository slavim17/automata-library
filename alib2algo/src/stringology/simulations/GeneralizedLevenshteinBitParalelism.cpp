#include "GeneralizedLevenshteinBitParalelism.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GeneralizedLevenshteinBitParalelismLinearString = registration::AbstractRegister < stringology::simulations::GeneralizedLevenshteinBitParalelism, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & , unsigned > ( stringology::simulations::GeneralizedLevenshteinBitParalelism::search );

} /* namespace */

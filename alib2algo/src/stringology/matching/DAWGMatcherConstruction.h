/*
 * Author: Radovan Cerveny
 */

#pragma once

#include <indexes/stringology/SuffixAutomaton.h>
#include <string/LinearString.h>

#include <stringology/indexing/ExactSuffixAutomaton.h>

namespace stringology {

namespace matching {

class DAWGMatcherConstruction {
public:
	 /**
	 * Linear time on-line construction of minimal suffix automaton for given pattern.
	 * @return minimal suffix automaton for given pattern.
	 */
	template < class SymbolType >
	static indexes::stringology::SuffixAutomaton < SymbolType > construct ( const string::LinearString < SymbolType > & pattern );

};

template < class SymbolType >
indexes::stringology::SuffixAutomaton < SymbolType > DAWGMatcherConstruction::construct ( const string::LinearString < SymbolType > & pattern ) {
	auto patternData = pattern.getContent ( );
	reverse ( patternData.begin ( ), patternData.end ( ) );
	string::LinearString < SymbolType > reversedPattern ( pattern.getAlphabet ( ), std::move ( patternData ) );

	return stringology::indexing::ExactSuffixAutomaton::construct ( reversedPattern );
}

} /* namespace matching */

} /* namespace stringology */


/*
 * Author: Radovan Cerveny
 */

#include "NaiveDAWGMatcherConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto NaiveDAWGMatcherConstructionLinearString = registration::AbstractRegister < stringology::matching::NaiveDAWGMatcherConstruction, indexes::stringology::SuffixAutomaton < >, const string::LinearString < > & > ( stringology::matching::NaiveDAWGMatcherConstruction::naiveConstruct );

} /* namespace */

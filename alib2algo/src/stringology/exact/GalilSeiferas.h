#pragma once

#include <alib/measure>

#include <alib/set>
#include <alib/vector>

#include <string/LinearString.h>
#include <algorithm>
#include <common/createUnique.hpp>
#include <alphabet/End.h>

#include <stringology/common/CommonAlgorithm.h>

namespace stringology::exact {

/**
 * Implementation of the Galil-Seiferas algorithm from article “ Time-Space-Optimal String Matching
 * Zvi Galil and Joel Seiferas
 */
class GalilSeiferas{
public:
	/**
	 * Search for pattern in linear string.
	 * @return set set of occurences
	 */
	template < class SymbolType >
	static ext::set < unsigned > match ( const string::LinearString < SymbolType > & subject, const string::LinearString < SymbolType > & pattern );
};

template < class SymbolType >
ext::set < unsigned > GalilSeiferas::match ( const string::LinearString < SymbolType > & subject, const string::LinearString < SymbolType > & pattern ) {
	ext::set < unsigned > occ;

	// add terminating symbol to subject
	SymbolType endSymbol = ::common::createUnique ( alphabet::End::instance < SymbolType > ( ), subject.getAlphabet ( ), pattern.getAlphabet ( ) );
	ext::vector < SymbolType > extendedSubject = subject.getContent ( );
	extendedSubject.push_back ( endSymbol );

	// add terminating symbol to pattern
	ext::vector < SymbolType > extendedPattern = pattern.getContent ( );
	extendedPattern.push_back ( endSymbol );

	size_t n = extendedSubject.size() - 1;
	size_t m = extendedPattern.size() - 1;

	unsigned k = 4;
	size_t p = 0;
	size_t q = 0;
	size_t s = 0;
	size_t p1 = 1;
	size_t q1 = 0;
	size_t p2 = 0;
	size_t q2 = 0;

	measurements::start ( "Algorithm", measurements::Type::ALGORITHM );
newp1:
	while( extendedPattern[s + p1 + q1] == extendedPattern[s + q1] )
		++ q1;
	if ( p1 + q1 >= k * p1 ) {
		p2 = q1;
		q2 = 0;
		goto newp2;
	}

	if ( s + p1 + q1 == m )
		goto search;
	p1 += std::max( static_cast < size_t > ( 1 ) , common::div_up ( q1 , k ) );
	q1 = 0;
	goto newp1;

newp2:
	while( extendedPattern[s + p2 + q2] == extendedPattern[s + q2] && p2 + q2 < k* p2 )
		++ q2;
	if ( p2 + q2 == k * p2 )
		goto parse;
	if ( s + p2 + q2 == m )
		goto search;
	if ( q2 == p1 + q1 ) {
		p2 += p1;
		q2 -= p1;
	} else {
		p2 += std::max ( static_cast < size_t > ( 1 ) , common::div_up(q2 , k));
		q2 = 0;
	}
	goto newp2;

parse:
	while ( extendedPattern[s + p1 + q1] == extendedPattern[s + q1])
		++ q1;
	while ( p1 + q1 >= k * p1 ) {
		s += p1;
		q1 -= p1;
	}
	p1 += std::max ( static_cast < size_t > ( 1 ) , common::div_up(q1 , k ));
	q1 = 0;
	if ( p1 < p2 )
		goto parse;
	else
		goto newp1;

search:
	while ( p <= n - m ) {
		while ( p + s + q < n && extendedSubject[p + s + q] == extendedPattern[s + q])
			++q;
		if (q == m - s && std::equal( extendedSubject.begin() + p, extendedSubject.begin() + p + s , extendedPattern.begin()))
			occ.insert(p);
		if (q == p1 + q1) {
			p += p1;
			q -= p1;
		} else {
			p += std::max ( static_cast < size_t > ( 1 ), common::div_up(q, k));
			q = 0;
		}
	}

	measurements::end();
	return occ;
}

} /* namespace stringology::exact */

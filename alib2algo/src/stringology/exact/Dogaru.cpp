#include "Dogaru.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto Dogaru = registration::AbstractRegister < stringology::exact::Dogaru, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::Dogaru::match );

} /* namespace */

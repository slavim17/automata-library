#include "SequentialSampling.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto SequentialSampling = registration::AbstractRegister < stringology::exact::SequentialSampling, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::SequentialSampling::match );

} /* namespace */

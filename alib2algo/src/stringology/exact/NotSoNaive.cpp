#include "NotSoNaive.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto NotSoNaive = registration::AbstractRegister < stringology::exact::NotSoNaive, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::NotSoNaive::match );

} /* namespace */

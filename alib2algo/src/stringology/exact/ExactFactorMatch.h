#pragma once

#include <alib/set>

#include <string/LinearString.h>

namespace stringology {

namespace exact {

class ExactFactorMatch {
public:
	/**
	 * Performs conversion.
	 * @return left regular grammar equivalent to source automaton.
	 */
	template < class SymbolType >
	static ext::set<unsigned> match(const string::LinearString < SymbolType >& subject, const string::LinearString < SymbolType >& pattern);
};

template < class SymbolType >
ext::set < unsigned > ExactFactorMatch::match ( const string::LinearString < SymbolType > & subject, const string::LinearString < SymbolType > & pattern ) {
	ext::set < unsigned > occ;

	for ( unsigned i = 0; i + pattern.getContent ( ).size ( ) <= subject.getContent ( ).size ( ); i++ ) {
		unsigned j = 0;

		for ( ; j < pattern.getContent ( ).size ( ); j++ )
			if ( pattern.getContent ( )[j] != subject.getContent ( )[i + j] ) break;

		if ( j == pattern.getContent ( ).size ( ) )
			occ.insert ( i );
	}

	return occ;
}

} /* namespace exact */

} /* namespace stringology */


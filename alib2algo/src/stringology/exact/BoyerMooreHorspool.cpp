#include "BoyerMooreHorspool.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BoyerMooreHorspoolLinearStringLinearString = registration::AbstractRegister < stringology::exact::BoyerMooreHorspool, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::BoyerMooreHorspool::match );

} /* namespace */

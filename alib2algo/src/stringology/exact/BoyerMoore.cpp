#include "BoyerMoore.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BoyerMooreLinearStringLinearString = registration::AbstractRegister < stringology::exact::BoyerMoore, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::BoyerMoore::match );

} /* namespace */

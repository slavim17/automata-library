#include <registration/AlgoRegistration.hpp>
#include "RestrictedApproximateSeedComputation.h"

namespace stringology::seed {
    auto RestrictedApproximateSeedsLinearString = registration::AbstractRegister < RestrictedApproximateSeedComputation, ext::set < ext::pair < string::LinearString < DefaultSymbolType >, unsigned > >, const string::LinearString < > &, unsigned > ( RestrictedApproximateSeedComputation::compute );

}



#include <registration/AlgoRegistration.hpp>
#include "ExactSeedComputation.h"

namespace stringology::seed {
    auto ExactSeedsLinearString = registration::AbstractRegister < ExactSeedComputation, ext::set < ext::pair < string::LinearString < DefaultSymbolType >, unsigned > >, const string::LinearString < > & > ( ExactSeedComputation::compute );

}

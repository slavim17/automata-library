#include "PeriodicPrefix.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto PeriodicPrefix = registration::AbstractRegister < string::properties::PeriodicPrefix, ext::pair < size_t, size_t >, const string::LinearString < > & > ( string::properties::PeriodicPrefix::construct );

} /* namespace */

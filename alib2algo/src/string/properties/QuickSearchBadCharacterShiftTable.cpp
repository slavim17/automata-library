#include "QuickSearchBadCharacterShiftTable.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto QuickSearchBadCharacterShiftTableLinearString = registration::AbstractRegister < string::properties::QuickSearchBadCharacterShiftTable, ext::map < DefaultSymbolType, size_t >, const string::LinearString < > & > ( string::properties::QuickSearchBadCharacterShiftTable::qsbcs );

} /* namespace */

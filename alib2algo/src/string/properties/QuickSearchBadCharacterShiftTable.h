#pragma once

#include <alib/map>

#include <string/LinearString.h>

namespace string {

namespace properties {

/**
* Computation of BCS table for the QuickSearch algorithm, as presented in the Daniel M. Sunday article.
*/
class QuickSearchBadCharacterShiftTable {
public:
	/**
	 * Creates a bad character shift table which can be later used for the QuickSearch algorithm.
	 * @return the BCS table in form of a map where key is the character from an alphabet and value is the shift.
	 */
	template < class SymbolType >
	static ext::map < SymbolType, size_t > qsbcs ( const string::LinearString < SymbolType > & pattern );

};

template < class SymbolType >
ext::map<SymbolType, size_t> QuickSearchBadCharacterShiftTable::qsbcs(const string::LinearString < SymbolType >& pattern) {
	ext::map<SymbolType, size_t> bcs;

	/* Initialization of BCS. */
	for(const SymbolType & symbol : pattern.getAlphabet ( ) )
		bcs.insert(std::make_pair(symbol, pattern.getContent().size() + 1));

	/* Filling out BCS. */
	for(size_t i = 0; i < pattern.getContent().size(); i++)
		bcs [ pattern.getContent ( ) [ i ] ] = pattern.getContent().size() - i;

	return bcs;
}

} /* namespace properties */

} /* namespace string */


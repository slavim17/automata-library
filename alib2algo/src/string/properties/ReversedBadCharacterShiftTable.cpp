#include "ReversedBadCharacterShiftTable.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ReversedBadCharacterShiftTableLinearString = registration::AbstractRegister < string::properties::ReversedBadCharacterShiftTable, ext::map < DefaultSymbolType, size_t >, const string::LinearString < > & > ( string::properties::ReversedBadCharacterShiftTable::bcs );

} /* namespace */

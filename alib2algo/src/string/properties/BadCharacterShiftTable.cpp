#include "BadCharacterShiftTable.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BadCharacterShiftTableLinearString = registration::AbstractRegister < string::properties::BadCharacterShiftTable, ext::map < DefaultSymbolType, size_t >, const string::LinearString < > & > ( string::properties::BadCharacterShiftTable::bcs );

} /* namespace */

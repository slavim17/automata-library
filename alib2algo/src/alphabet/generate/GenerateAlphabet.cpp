#include "GenerateAlphabet.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto GenerateCharacterAlphabet = registration::AbstractRegister < alphabet::generate::GenerateAlphabet, ext::set < char >, size_t, bool, bool > ( alphabet::generate::GenerateAlphabet::generateCharacterAlphabet, "alphabetSize", "randomizeAlphabet", "lowerCase" ).setDocumentation (
"Generates a random alphabet.\n\
\n\
@param alphabetSize size of the alphabet (the number of symbols selected from a-z or A-Z)\n\
@param randomizeAlphabet selects random symbols from a-z range if true\n\
@param lowerCase if true the generator produces lower case character, upper case is used if false\n\
@return random alphabet" );

auto GenerateIntegerAlphabet = registration::AbstractRegister < alphabet::generate::GenerateAlphabet, ext::set < int >, size_t > ( alphabet::generate::GenerateAlphabet::generateIntegerAlphabet, "alphabetSize" ).setDocumentation (
"Generates a random alphabet.\n\
\n\
@param alphabetSize size of the alphabet (non-negative integer)\n\
@return random alphabet" );

} /* namespace */

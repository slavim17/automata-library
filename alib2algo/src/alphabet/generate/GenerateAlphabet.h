#pragma once

#include <string>
#include <algorithm>

#include <ext/deque>
#include <ext/random>

#include <alib/set>

#include <exception/CommonException.h>

namespace alphabet::generate {

class GenerateAlphabet {
public:
static ext::set < char > generateCharacterAlphabet ( size_t alphabetSize, bool randomizeAlphabet, bool lowerCase ) {
	constexpr size_t ENGLISH_ALPHABET_SIZE = 26;
	if ( alphabetSize > ENGLISH_ALPHABET_SIZE )
		throw exception::CommonException ( "Alphabet too big." );

	ext::deque < char > alphabet;
	for ( char i = 'a'; i <= 'z'; i++ )
		alphabet.push_back ( lowerCase ? i : i + 'A' - 'a');

	if ( randomizeAlphabet )
		shuffle ( alphabet.begin ( ), alphabet.end ( ), ext::random_devices::semirandom );

	alphabet.resize ( alphabetSize );

	return ext::set < char > ( alphabet.begin ( ), alphabet.end ( ) );
}

static ext::set < int > generateIntegerAlphabet ( size_t alphabetSize ) {
	if ( alphabetSize <= 0 )
		throw exception::CommonException ( "Alphabet size must be greater than 0." );

	ext::set < int > alphabet;

	for ( size_t i = 0; i < alphabetSize; ++ i )
		alphabet.insert ( static_cast < int > ( ext::random_devices::semirandom ( ) % alphabetSize ) );

	return alphabet;
}

};

} /* namespace alphabet::generate */

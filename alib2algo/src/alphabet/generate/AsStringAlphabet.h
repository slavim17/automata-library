#pragma once

#include <alib/set>
#include <ext/string>

namespace alphabet::generate {

class AsStringAlphabet {
public:
template < class T >
static ext::set < std::string > asStringAlphabet ( const ext::set < T > & alphabet ) {
	ext::set < std::string > res;

	for ( const T & symbol : alphabet ) {
		res.insert ( ext::to_string ( symbol ) );
	}

	return res;
}

};

} /* namespace alphabet::generate */

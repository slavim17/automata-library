#pragma once

#include <limits>

#include <tree/ranked/RankedTree.h>
#include <tree/ranked/RankedPattern.h>
#include <tree/ranked/RankedExtendedPattern.h>
#include <tree/ranked/RankedNonlinearPattern.h>
#include <tree/unranked/UnrankedTree.h>
#include <tree/unranked/UnrankedPattern.h>
#include <tree/unranked/UnrankedExtendedPattern.h>

namespace tree {

namespace generate {

class RandomRankedTreeFactory {
public:
	static tree::RankedTree < > generateRankedTree ( int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank = std::numeric_limits < size_t >::max ( ) );
};

class RandomRankedPatternFactory {
public:
	static tree::RankedPattern < > generateRankedPattern ( int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank = std::numeric_limits < size_t >::max ( ) );
};

class RandomRankedExtendedPatternFactory {
public:
	static tree::RankedExtendedPattern < > generateRankedExtendedPattern ( int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank = std::numeric_limits < size_t >::max ( ), double nodeWildcardProbability = 10 );
};

class RandomRankedNonlinearPatternFactory {
public:
	static tree::RankedNonlinearPattern < > generateRankedNonlinearPattern ( int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, bool singleNonlinearVariable, size_t maxRank = std::numeric_limits < size_t >::max ( ) );
};

class RandomUnrankedTreeFactory {
public:
	static tree::UnrankedTree < > generateUnrankedTree ( int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank = std::numeric_limits < size_t >::max ( ) );
};

class RandomUnrankedPatternFactory {
public:
	static tree::UnrankedPattern < > generateUnrankedPattern ( int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank = std::numeric_limits < size_t >::max ( ) );
};

class RandomUnrankedExtendedPatternFactory {
public:
	static tree::UnrankedExtendedPattern < > generateUnrankedExtendedPattern ( int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank = std::numeric_limits < size_t >::max ( ), double nodeWildcardProbability = 10 );
};

} /* namespace generate */

} /* namespace tree */


#include "ExactSubtreeRepeatsNaive.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactRepeatsNaiveRankedTree = registration::AbstractRegister < tree::properties::ExactSubtreeRepeatsNaive, tree::RankedTree < unsigned >, const tree::RankedTree < > & > ( tree::properties::ExactSubtreeRepeatsNaive::repeats );
auto ExactRepeatsNaivePrefixRankedTree = registration::AbstractRegister < tree::properties::ExactSubtreeRepeatsNaive, tree::PrefixRankedTree < unsigned >, const tree::PrefixRankedTree < > & > ( tree::properties::ExactSubtreeRepeatsNaive::repeats );
auto ExactRepeatsNaivePostfixRankedTree = registration::AbstractRegister < tree::properties::ExactSubtreeRepeatsNaive, tree::PostfixRankedTree < unsigned >, const tree::PostfixRankedTree < > & > ( tree::properties::ExactSubtreeRepeatsNaive::repeats );
auto ExactRepeatsNaivePrefixRankedBarTree = registration::AbstractRegister < tree::properties::ExactSubtreeRepeatsNaive, tree::PrefixRankedBarTree < unsigned >, const tree::PrefixRankedBarTree < > & > ( tree::properties::ExactSubtreeRepeatsNaive::repeats );

} /* namespace */

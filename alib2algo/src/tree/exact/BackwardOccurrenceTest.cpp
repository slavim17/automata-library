#include "BackwardOccurrenceTest.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BackwardOccurrenceTestPrefixRankedBarTree = registration::AbstractRegister < tree::exact::BackwardOccurrenceTest, ext::pair < size_t, size_t >, const tree::PrefixRankedBarTree < > &, const ext::vector < int > &, const tree::PrefixRankedBarTree < > &, size_t > ( tree::exact::BackwardOccurrenceTest::occurrence, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "subject", "subjectSubtreeJumpTable", "pattern", "position" );

} /* namespace */

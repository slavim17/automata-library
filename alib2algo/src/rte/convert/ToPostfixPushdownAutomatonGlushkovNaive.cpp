#include "ToPostfixPushdownAutomatonGlushkovNaive.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto ToPostfixPushdownAutomatonGlushkovNaiveFormalRTE = registration::AbstractRegister < rte::convert::ToPostfixPushdownAutomatonGlushkovNaive, automaton::NPDA < ext::variant < common::ranked_symbol < DefaultSymbolType >, alphabet::End >, ext::variant < common::ranked_symbol < ext::pair < DefaultSymbolType, unsigned > >, alphabet::BottomOfTheStack >, char >, const rte::FormalRTE < > & > ( rte::convert::ToPostfixPushdownAutomatonGlushkovNaive::convert );

} /* namespace */

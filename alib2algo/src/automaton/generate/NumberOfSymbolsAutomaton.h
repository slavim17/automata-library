/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <alib/set>
#include <alib/string>

#include <exception/CommonException.h>

#include <automaton/FSM/NFA.h>

namespace automaton::generate {

/**
 * Algorithm NFA accepting strings with given number of symbols A modulo N.
 */
class NumberOfSymbolsAutomaton {
public:
	/**
	 * Generates automaton accepting strings over alphabet where the number of symbol given by @p symbol is modulo @p modulo equal to @p final_modulo.
	 * @tparam SymbolType the type of terminal symbols of the random automaton
	 *
	 * @param modulo number of symbols to modulo by
	 * @param alphabet Input alphabet of the automaton
	 * @param symbol the counted symbol
	 * @param final_modulo number of symbols to recognise mod modulo
	 *
	 * @return nondeterministic finite automaton
	 */
	template < class SymbolType >
	static automaton::NFA < SymbolType, unsigned > generateNFA ( size_t modulo, const ext::set < SymbolType > & alphabet, SymbolType symbol, size_t final_modulo );

	/**
	 * \overload
	 *
	 * Generates automaton accepting strings over alphabet where the number of symbol given by @p symbol is modulo @p modulo equal to @p final_modulo.
	 *
	 * @param modulo number of symbols to modulo by
	 * @param alphabetSize size of the alphabet (1-26)
	 * @param randomizedAlphabet selects random symbols from a-z range if true
	 * @param symbol the counted symbol
	 * @param final_modulo number of symbols to recognise mod modulo
	 *
	 * @return nondeterministic finite automaton
	 */
	static automaton::NFA < std::string, unsigned > generateNFA ( size_t modulo, size_t alphabetSize, bool randomizedAlphabet, char symbol, size_t final_modulo );
};

template < class SymbolType >
automaton::NFA < SymbolType, unsigned > NumberOfSymbolsAutomaton::generateNFA ( size_t modulo, const ext::set < SymbolType > & alphabet, SymbolType symbol, size_t final_modulo ) {
	if ( ! alphabet.contains ( symbol ) )
		throw exception::CommonException ( "Symbol is not in the alphabet." );

	automaton::NFA < SymbolType, unsigned > automaton ( 0 );

	automaton.setInputAlphabet ( alphabet );

	for ( size_t i = 1; i < modulo; ++ i )
		automaton.addState ( i );

	for ( size_t i = 0; i < modulo; ++ i )
		automaton.addTransition ( i, symbol, ( i + 1 ) % modulo );

	for ( size_t i = 0; i < modulo; ++ i )
		for ( const SymbolType & alphabet_symbol : alphabet )
			if ( alphabet_symbol != symbol )
				automaton.addTransition ( i, alphabet_symbol, i );

	automaton.addFinalState ( final_modulo );

	return automaton;
}

} /* namespace automaton::generate */


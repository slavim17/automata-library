#include "RandomizeAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RandomizeAutomatonDFA = registration::AbstractRegister < automaton::generate::RandomizeAutomaton, automaton::DFA < >, const automaton::DFA < > & > ( automaton::generate::RandomizeAutomaton::randomize, "fsm" ).setDocumentation (
"Shuffle the set of states of the automaton.\n\
\n\
@param fsm automaton to shuffle" );

auto RandomizeAutomatonMultiInitialStateNFA = registration::AbstractRegister < automaton::generate::RandomizeAutomaton, automaton::MultiInitialStateNFA < >, const automaton::MultiInitialStateNFA < > & > ( automaton::generate::RandomizeAutomaton::randomize, "fsm" ).setDocumentation (
"Shuffle the set of states of the automaton.\n\
\n\
@param fsm automaton to shuffle" );

auto RandomizeAutomatonNFA = registration::AbstractRegister < automaton::generate::RandomizeAutomaton, automaton::NFA < >, const automaton::NFA < > & > ( automaton::generate::RandomizeAutomaton::randomize, "fsm" ).setDocumentation (
"Shuffle the set of states of the automaton.\n\
\n\
@param fsm automaton to shuffle" );

auto RandomizeAutomatonEpsilonNFA = registration::AbstractRegister < automaton::generate::RandomizeAutomaton, automaton::EpsilonNFA < >, const automaton::EpsilonNFA < > & > ( automaton::generate::RandomizeAutomaton::randomize, "fsm" ).setDocumentation (
"Shuffle the set of states of the automaton.\n\
\n\
@param fsm automaton to shuffle" );

} /* namespace */

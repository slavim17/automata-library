#include "RandomTreeAutomatonFactory.h"
#include <registration/AlgoRegistration.hpp>

namespace automaton::generate {

unsigned RandomTreeAutomatonFactory::ithAccessibleState ( const ext::deque < bool > & VStates, size_t i ) {
	i ++;
	for( size_t j = 0; j < VStates.size ( ); j++ ) {
		if( VStates[ j ] )
			i --;

		if( i == 0 )
			return j;
	}
	throw std::logic_error ( "Not enough states in deque of visited states" );
}

unsigned RandomTreeAutomatonFactory::ithInaccessibleState ( const ext::deque < bool > & VStates, size_t i ) {
	i ++;
	for( size_t j = 0; j < VStates.size ( ); j++ ) {
		if( ! VStates[ j ] )
			i --;

		if( i == 0 )
			return j;
	}
	throw std::logic_error ( "Not enough states in deque of visited states" );
}

} /* namespace automaton::generate */

namespace {

auto GenerateNFTA1 = registration::AbstractRegister < automaton::generate::RandomTreeAutomatonFactory, automaton::NFTA < DefaultSymbolType, unsigned >, size_t, const ext::set < common::ranked_symbol < DefaultSymbolType > > &, double > ( automaton::generate::RandomTreeAutomatonFactory::generateNFTA, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "statesCount", "alphabet", "density" ).setDocumentation (
"Generates a random finite automaton.\n\
@param statesCount number of states in the generated automaton\n\
@param alphabet Input alphabet of the automaton\n\
@param density density of the transition function\n\
@return random nondeterministic finite automaton" );

} /* namespace */

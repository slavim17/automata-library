#include "ToRTEStateElimination.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToRTEStateEliminationDFTA = registration::AbstractRegister < automaton::convert::ToRTEStateElimination, rte::FormalRTE < ext::variant < DefaultSymbolType, DefaultStateType > >, const automaton::DFTA < > & > ( automaton::convert::ToRTEStateElimination::convert, "automaton" );
auto ToRTEStateEliminationNFTA = registration::AbstractRegister < automaton::convert::ToRTEStateElimination, rte::FormalRTE < ext::variant < DefaultSymbolType, DefaultStateType > >, const automaton::NFTA < > & > ( automaton::convert::ToRTEStateElimination::convert, "automaton" );

} /* namespace */

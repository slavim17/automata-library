#include "ToPostfixPushdownAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToAutomatonDFTA = registration::AbstractRegister < automaton::convert::ToPostfixPushdownAutomaton, automaton::DPDA < ext::variant < common::ranked_symbol < DefaultSymbolType >, alphabet::End >, ext::variant < DefaultStateType, alphabet::BottomOfTheStack >, char >, const automaton::DFTA < > & > ( automaton::convert::ToPostfixPushdownAutomaton::convert, "dfta" ).setDocumentation (
"Performs the conversion of the deterministic FTA to the deterministic PDA\n\
\n\
@param dfta Deterministic finite tree automaton to convert\n\
@return (D)PDA equivalent to original finite tree automaton reading linearized postfix tree" );

auto ToAutomatonNFTA = registration::AbstractRegister < automaton::convert::ToPostfixPushdownAutomaton, automaton::NPDA < ext::variant < common::ranked_symbol < DefaultSymbolType >, alphabet::End >, ext::variant < DefaultStateType, alphabet::BottomOfTheStack >, char >, const automaton::NFTA < > & > ( automaton::convert::ToPostfixPushdownAutomaton::convert, "nfta" ).setDocumentation (
"Performs the conversion of the deterministic FTA to the deterministic PDA\n\
\n\
@param nfta Nondeterministic finite tree automaton to convert\n\
@return (N)PDA equivalent to original finite tree automaton reading linearized postfix tree" );

} /* namespace */

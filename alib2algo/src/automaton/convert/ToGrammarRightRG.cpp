#include "ToGrammarRightRG.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToGrammarRightRGNFA = registration::AbstractRegister < automaton::convert::ToGrammarRightRG, grammar::RightRG < >, const automaton::NFA < > & > ( automaton::convert::ToGrammarRightRG::convert, "automaton" ).setDocumentation (
"Performs the conversion of the finite automaton to right regular grammar.\n\
\n\
@param automaton a finite automaton to convert\n\
@return right regular grammar equivalent to the source @p automaton." );

auto ToGrammarRightRGDFA = registration::AbstractRegister < automaton::convert::ToGrammarRightRG, grammar::RightRG < >, const automaton::DFA < > & > ( automaton::convert::ToGrammarRightRG::convert, "automaton" ).setDocumentation (
"Performs the conversion of the finite automaton to right regular grammar.\n\
\n\
@param automaton a finite automaton to convert\n\
@return right regular grammar equivalent to the source @p automaton." );

} /* namespace */

#include "Determinize.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto DeterminizeRealTimeHeightDeterministicNPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::RealTimeHeightDeterministicDPDA < DefaultSymbolType, ext::pair < ext::set < ext::pair < DefaultStateType, DefaultStateType > >, common::symbol_or_epsilon < DefaultSymbolType > >, ext::set < ext::pair < DefaultStateType, DefaultStateType > > >, const automaton::RealTimeHeightDeterministicNPDA < > & > ( automaton::determinize::Determinize::determinize, "npda" ).setDocumentation (
"Determinization of nondeterministic real-time height-deterministic pushdown automata.\n\
\n\
@param npda nondeterministic real-time height-deterministic pushdown automaton\n\
@return deterministic pushdown automaton equivalent to @p npda" );

auto DeterminizeNPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::RealTimeHeightDeterministicDPDA < DefaultSymbolType, ext::pair < ext::set < ext::pair < ext::variant < DefaultStateType, std::string >, ext::variant < DefaultStateType, std::string > > >, common::symbol_or_epsilon < DefaultSymbolType > >, ext::set < ext::pair < ext::variant < DefaultStateType, std::string >, ext::variant < DefaultStateType, std::string > > > >, const automaton::NPDA < > & > ( automaton::determinize::Determinize::determinize, "npda" ).setDocumentation (
"Determinization of nondeterministic pushdown automata is implemented as a cast of such automaton to RhPDA.\n\
\n\
@param npda nondeterministic pushdown automaton\n\
@return nondeterministic pushdown automaton equivalent to @p npda" );

} /* namespace */

#include "Determinize.h"
#include <registration/AlgoRegistration.hpp>

namespace {

using deterministicAutomata = ext::variant <
	automaton::DFA < >,
	automaton::DFTA < >,
	automaton::UnorderedDFTA < >,
	automaton::InputDrivenDPDA < >,
	automaton::VisiblyPushdownDPDA < >,
	automaton::RealTimeHeightDeterministicDPDA < >,
	automaton::SinglePopDPDA < >,
	automaton::DPDA < >,
	automaton::OneTapeDTM < >
>;

auto DeterminizeDeterministicAutomata = registration::AbstractRegister < automaton::determinize::Determinize, deterministicAutomata, const deterministicAutomata & > ( [ ] ( const auto & param ) { return param; }, "automaton" ).setDocumentation (
"No-op determinisation of automata that are already deterministic.\n\
\n\
@param automaton a deterministic automaton\n\
@return the parameter unchanged" );

auto DeterminizeDocumentation = registration::DocumentationRegister < automaton::determinize::Determinize > ( "Implementation of various algorithms for determinization of various kinds of automata." );

} /* namespace */

#include "Determinize.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto DeterminizeArcFactoredNondeterministicZAutomaton = registration::AbstractRegister < automaton::determinize::Determinize, automaton::ArcFactoredDeterministicZAutomaton < DefaultSymbolType, ext::set < DefaultStateType > >, const automaton::ArcFactoredNondeterministicZAutomaton < > & > ( automaton::determinize::Determinize::determinize, "automaton" );

} /* namespace */

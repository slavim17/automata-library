#include "Determinize.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto DeterminizeInputDrivenNPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::InputDrivenDPDA < DefaultSymbolType, DefaultSymbolType, ext::set < DefaultStateType > >, const automaton::InputDrivenNPDA < > & > ( automaton::determinize::Determinize::determinize, "npda" ).setDocumentation (
"Implementation of determinization for input-driven pushdown automata.\n\
\n\
@param npda nondeterministic input-driven pushdown automaton\n\
@return deterministic input-driven pushdown automaton equivalent to @p npda" );

} /* namespace */

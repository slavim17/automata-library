/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <automaton/FSM/MultiInitialStateNFA.h>
#include <automaton/FSM/NFA.h>
#include <automaton/FSM/DFA.h>

namespace automaton {

namespace transform {

/**
 * Transformation of a finite automaton to a reverse finite automaton.
 * For a finite automaton A1 we create a finite automaton A such that L(A) = L(A1)^R (i.e. all strings are reversed).
 */
class Reverse {
public:
	/**
	 * Computation of reverse of a finite automaton.
	 * @tparam SymbolType Type for input symbols.
	 * @tparam StateType Type for states.
	 * @param automaton the automaton to reverse
	 * @return multi-initial state nondeterministic FA accepting reversed language of @p automaton
	 */
	template < class SymbolType, class StateType >
	static automaton::MultiInitialStateNFA < SymbolType, StateType > convert(const automaton::DFA < SymbolType, StateType > & automaton);

	/**
	 * @overload
	 */
	template < class SymbolType, class StateType >
	static automaton::MultiInitialStateNFA < SymbolType, StateType > convert(const automaton::NFA < SymbolType, StateType > & automaton);

	/**
	 * @overload
	 */
	template < class SymbolType, class StateType >
	static automaton::MultiInitialStateNFA < SymbolType, StateType > convert(const automaton::MultiInitialStateNFA < SymbolType, StateType > & automaton);
};

template < class SymbolType, class StateType >
automaton::MultiInitialStateNFA < SymbolType, StateType > Reverse::convert(const automaton::DFA < SymbolType, StateType > & automaton) {
	automaton::MultiInitialStateNFA < SymbolType, StateType > res;

	res.setStates(automaton.getStates());
	res.addFinalState(automaton.getInitialState());
	res.setInitialStates( automaton.getFinalStates() );
	res.setInputAlphabet(automaton.getInputAlphabet());

	for(const auto& t : automaton.getTransitions())
		res.addTransition(t.second, t.first.second, t.first.first);

	return res;
}

template < class SymbolType, class StateType >
automaton::MultiInitialStateNFA < SymbolType, StateType > Reverse::convert(const automaton::NFA < SymbolType, StateType > & automaton) {
	automaton::MultiInitialStateNFA < SymbolType, StateType > res;

	res.setStates(automaton.getStates());
	res.addFinalState(automaton.getInitialState());
	res.setInitialStates( automaton.getFinalStates() );
	res.setInputAlphabet(automaton.getInputAlphabet());

	for(const auto& t : automaton.getTransitions())
		res.addTransition(t.second, t.first.second, t.first.first);

	return res;
}

template < class SymbolType, class StateType >
automaton::MultiInitialStateNFA < SymbolType, StateType > Reverse::convert(const automaton::MultiInitialStateNFA < SymbolType, StateType > & automaton) {
	automaton::MultiInitialStateNFA < SymbolType, StateType > res;

	res.setStates(automaton.getStates());
	res.setFinalStates(automaton.getInitialStates());
	res.setInitialStates( automaton.getFinalStates() );
	res.setInputAlphabet(automaton.getInputAlphabet());

	for(const auto& t : automaton.getTransitions())
		res.addTransition ( t.second, t.first.second, t.first.first);

	return res;
}

} /* namespace transform */

} /* namespace automaton */


#include "AutomatonIterationEpsilonTransition.h"
#include <common/createUnique.hpp>
#include <registration/AlgoRegistration.hpp>

namespace {

auto AutomatonIterationEpsilonTransitionDFA2 = registration::AbstractRegister < automaton::transform::AutomatonIterationEpsilonTransition, automaton::EpsilonNFA < >, const automaton::DFA < > & > ( automaton::transform::AutomatonIterationEpsilonTransition::iteration, "automaton" ).setDocumentation (
"Iteration of a finite automaton using epsilon transitions.\n\
\n\
@param automaton automaton to iterate\n\
@return nondeterministic FA representing the intersection of @p automaton" );

auto AutomatonIterationEpsilonTransitionNFA2 = registration::AbstractRegister < automaton::transform::AutomatonIterationEpsilonTransition, automaton::EpsilonNFA < >, const automaton::NFA < > & > ( automaton::transform::AutomatonIterationEpsilonTransition::iteration, "automaton" ).setDocumentation (
"Iteration of a finite automaton using epsilon transitions.\n\
\n\
@param automaton automaton to iterate\n\
@return nondeterministic FA representing the intersection of @p automaton" );

auto AutomatonIterationEpsilonTransitionEpsilonNFA2 = registration::AbstractRegister < automaton::transform::AutomatonIterationEpsilonTransition, automaton::EpsilonNFA < >, const automaton::EpsilonNFA < > & > ( automaton::transform::AutomatonIterationEpsilonTransition::iteration, "automaton" ).setDocumentation (
"Iteration of a finite automaton using epsilon transitions.\n\
\n\
@param automaton automaton to iterate\n\
@return nondeterministic FA representing the intersection of @p automaton" );

} /* namespace */

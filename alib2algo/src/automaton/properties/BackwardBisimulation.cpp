#include "BackwardBisimulation.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BackwardBisimulationDFA = registration::AbstractRegister < automaton::properties::BackwardBisimulation, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::DFA < > & > ( automaton::properties::BackwardBisimulation::backwardBisimulation, "fta" ).setDocumentation (
"Computes a relation on states of the DFA satisfying the backward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the backward bisimulation." );

auto BackwardBisimulationNFA = registration::AbstractRegister < automaton::properties::BackwardBisimulation, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::NFA < > & > ( automaton::properties::BackwardBisimulation::backwardBisimulation, "fta" ).setDocumentation (
"Computes a relation on states of the NFA satisfying the backward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the backward bisimulation." );

auto BackwardBisimulationAFDZA = registration::AbstractRegister < automaton::properties::BackwardBisimulation, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::ArcFactoredDeterministicZAutomaton < > & > ( automaton::properties::BackwardBisimulation::backwardBisimulation, "afdza" ).setDocumentation (
"Computes a relation on states of the Arc Factored Deterministic Z Automaton satisfying the backward bisimulation definition.\n\
\n\
@param afdza the examined automaton.\n\
@return set of pairs of states of the @p afdza that are the backward bisimulation." );

auto BackwardBisimulationAFNZA = registration::AbstractRegister < automaton::properties::BackwardBisimulation, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::ArcFactoredNondeterministicZAutomaton < > & > ( automaton::properties::BackwardBisimulation::backwardBisimulation, "afnza" ).setDocumentation (
"Computes a relation on states of the Arc Factored Nondeterministic Z Automaton satisfying the backward bisimulation definition.\n\
\n\
@param afnza the examined automaton.\n\
@return set of pairs of states of the @p afnza that are the backward bisimulation." );

auto BackwardBisimulationDFTA = registration::AbstractRegister < automaton::properties::BackwardBisimulation, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::DFTA < > & > ( automaton::properties::BackwardBisimulation::backwardBisimulation, "fta" ).setDocumentation (
"Computes a relation on states of the DFTA satisfying the backward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the backward bisimulation." );

auto BackwardBisimulationNFTA = registration::AbstractRegister < automaton::properties::BackwardBisimulation, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::NFTA < > & > ( automaton::properties::BackwardBisimulation::backwardBisimulation, "fta" ).setDocumentation (
"Computes a relation on states of the NFTA satisfying the backward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the backward bisimulation." );

auto BackwardBisimulationUnorderedDFTA = registration::AbstractRegister < automaton::properties::BackwardBisimulation, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::UnorderedDFTA < > & > ( automaton::properties::BackwardBisimulation::backwardBisimulation, "fta" ).setDocumentation (
"Computes a relation on states of the UnorderedDFTA satisfying the backward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the backward bisimulation." );

auto BackwardBisimulationUnorderedNFTA = registration::AbstractRegister < automaton::properties::BackwardBisimulation, ext::set < ext::pair < DefaultStateType, DefaultStateType > >, const automaton::UnorderedNFTA < > & > ( automaton::properties::BackwardBisimulation::backwardBisimulation, "fta" ).setDocumentation (
"Computes a relation on states of the UnorderedNFTA satisfying the backward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the backward bisimulation." );

} /* namespace */

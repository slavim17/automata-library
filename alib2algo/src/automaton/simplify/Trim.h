/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "UselessStatesRemover.h"
#include "UnreachableStatesRemover.h"

namespace automaton {

namespace simplify {

/**
 * Algorithm for the removal of dead states from a finite automaton.
 * Firstly, it calls the useless states removal algorithm, then unreachable states removal algorithm.
 *
 * @sa automaton::simplify::UselessStatesRemover
 * @sa automaton::simplify::UnreachableStatesRemover
 */
class Trim {
public:
	/**
	 * Removes inaccessible and useless states from the given automaton. Uses first the @sa UselessStatesRemover and next @sa UnreachableStatesRemover in the process.
	 *
	 * @tparam T type of a finite automaton
	 *
	 * @param fsm finite automaton or finite tree automaton to trim
	 *
	 * @return the trimmed automaton equivalent to @p automaton
	 */
	template < class T >
	static T trim ( const T & fsm );
};

template < class T >
T Trim::trim ( const T & fsm ) {
	return UselessStatesRemover::remove ( UnreachableStatesRemover::remove ( fsm ) );
}

} /* namespace simplify */

} /* namespace automaton */


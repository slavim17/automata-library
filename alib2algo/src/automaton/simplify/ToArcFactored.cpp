#include "ToArcFactored.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToArcFactoredNondeterministicZAutomaton = registration::AbstractRegister < automaton::simplify::ToArcFactored, automaton::ArcFactoredNondeterministicZAutomaton < DefaultSymbolType, ext::vector < ext::variant < DefaultSymbolType, DefaultStateType > > >, const automaton::NondeterministicZAutomaton < > & > ( automaton::simplify::ToArcFactored::convert, "automaton" ).setDocumentation (
"Converts a general ZAutomaton to an arc factored ZAutomaton.\n\
\n\
@param automaton the automaton to convert\n\
@return an automaton equivalent to @p which satisfies the definition of arc factored ZAutomata" );

} /* namespace */

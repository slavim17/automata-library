#include "Rename.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RenameDFA = registration::AbstractRegister < automaton::simplify::Rename, automaton::DFA < DefaultSymbolType, unsigned >, const automaton::DFA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states.\n\
\n\
@param automaton finite automaton to normalize\n\
@return @p automaton with renamed properties" );

auto RenameNFA = registration::AbstractRegister < automaton::simplify::Rename, automaton::NFA < DefaultSymbolType, unsigned >, const automaton::NFA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states.\n\
\n\
@param automaton finite automaton to normalize\n\
@return @p automaton with renamed properties" );

auto RenameDFTA = registration::AbstractRegister < automaton::simplify::Rename, automaton::DFTA < DefaultSymbolType, unsigned >, const automaton::DFTA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states.\n\
\n\
@param automaton finite tree automaton to normalize\n\
@return @p automaton with renamed properties" );

auto RenameNFTA = registration::AbstractRegister < automaton::simplify::Rename, automaton::NFTA < DefaultSymbolType, unsigned >, const automaton::NFTA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states.\n\
\n\
@param automaton finite tree automaton to normalize\n\
@return @p automaton with renamed properties" );

auto RenameDPDA = registration::AbstractRegister < automaton::simplify::Rename, automaton::DPDA < DefaultSymbolType, unsigned, unsigned >, const automaton::DPDA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states and pushdown store symbols.\n\
\n\
@param pda pushdown automaton to rename\n\
@return @p pda with renamed states and pushdown store symbols" );

auto RenameNPDA = registration::AbstractRegister < automaton::simplify::Rename, automaton::NPDA < DefaultSymbolType, unsigned, unsigned >, const automaton::NPDA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states and pushdown store symbols.\n\
\n\
@param pda pushdown automaton to rename\n\
@return @p pda with renamed states and pushdown store symbols" );

auto RenameSinglePopDPDA = registration::AbstractRegister < automaton::simplify::Rename, automaton::SinglePopDPDA < DefaultSymbolType, unsigned, unsigned >, const automaton::SinglePopDPDA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states and pushdown store symbols.\n\
\n\
@param pda pushdown automaton to rename\n\
@return @p pda with renamed states and pushdown store symbols" );

auto RenameInputDrivenDPDA = registration::AbstractRegister < automaton::simplify::Rename, automaton::InputDrivenDPDA < DefaultSymbolType, unsigned, unsigned >, const automaton::InputDrivenDPDA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states and pushdown store symbols.\n\
\n\
@param pda pushdown automaton to rename\n\
@return @p pda with renamed states and pushdown store symbols" );

auto RenameVisiblyPushdownDPDA = registration::AbstractRegister < automaton::simplify::Rename, automaton::VisiblyPushdownDPDA < DefaultSymbolType, unsigned, unsigned >, const automaton::VisiblyPushdownDPDA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states and pushdown store symbols.\n\
\n\
@param pda pushdown automaton to rename\n\
@return @p pda with renamed states and pushdown store symbols" );

auto RenameRealTimeHeightDeterministicDPDA = registration::AbstractRegister < automaton::simplify::Rename, automaton::RealTimeHeightDeterministicDPDA < DefaultSymbolType, unsigned, unsigned >, const automaton::RealTimeHeightDeterministicDPDA < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states and pushdown store symbols.\n\
\n\
@param pda pushdown automaton to rename\n\
@return @p pda with renamed states and pushdown store symbols" );

auto RenameNondeterministicZAutomaton = registration::AbstractRegister < automaton::simplify::Rename, automaton::ArcFactoredNondeterministicZAutomaton < DefaultSymbolType, unsigned >, const automaton::ArcFactoredNondeterministicZAutomaton < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states.\n\
\n\
@param automaton z automaton to rename\n\
@return @p automaton with renamed states" );

auto RenameDeterministicZAutomaton = registration::AbstractRegister < automaton::simplify::Rename, automaton::ArcFactoredDeterministicZAutomaton < DefaultSymbolType, unsigned >, const automaton::ArcFactoredDeterministicZAutomaton < > & > ( automaton::simplify::Rename::rename, "automaton" ).setDocumentation (
"Rename automaton's states.\n\
\n\
@param automaton z automaton to rename\n\
@return @p automaton with renamed states" );

} /* namespace */

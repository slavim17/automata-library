#include "UselessStatesRemover.h"
#include "automaton/TA/ArcFactoredDeterministicZAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto UselessStatesRemoverEpsilonNFA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::EpsilonNFA < >, const automaton::EpsilonNFA < > & > ( automaton::simplify::UselessStatesRemover::remove, "automaton" ).setDocumentation (
"Removes useless states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without useless states" );

auto UselessStatesRemoverNFA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::NFA < >, const automaton::NFA < > & > ( automaton::simplify::UselessStatesRemover::remove, "automaton" ).setDocumentation (
"Removes useless states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without useless states" );

auto UselessStatesRemoverCompactNFA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::CompactNFA < >, const automaton::CompactNFA < > & > ( automaton::simplify::UselessStatesRemover::remove, "automaton" ).setDocumentation (
"Removes useless states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without useless states" );

auto UselessStatesRemoverExtendedNFA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::ExtendedNFA < >, const automaton::ExtendedNFA < > & > ( automaton::simplify::UselessStatesRemover::remove, "automaton" ).setDocumentation (
"Removes useless states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without useless states" );

auto UselessStatesRemoverDFA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::DFA < >, const automaton::DFA < > & > ( automaton::simplify::UselessStatesRemover::remove, "automaton" ).setDocumentation (
"Removes useless states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without useless states" );

auto UselessStatesRemoverMultiInitialStateNFA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::MultiInitialStateNFA < >, const automaton::MultiInitialStateNFA < > & > ( automaton::simplify::UselessStatesRemover::remove, "automaton" ).setDocumentation (
"Removes useless states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without useless states" );

auto UselessStatesRemoverDFTA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::DFTA < >, const automaton::DFTA < > & > ( automaton::simplify::UselessStatesRemover::remove, "fta" ).setDocumentation (
"Removes unreachable states from a deterministic finite tree automaton.\n\
\n\
@param fta automaton to trim\n\
@return @p autoamton without unreachable states" );

auto UselessStatesRemoverNFTA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::NFTA < >, const automaton::NFTA < > & > ( automaton::simplify::UselessStatesRemover::remove, "fta" ).setDocumentation (
"Removes unreachable states from a deterministic finite tree automaton.\n\
\n\
@param fta automaton to trim\n\
@return @p autoamton without unreachable states" );

auto UselessStatesRemoverAFDZA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::ArcFactoredDeterministicZAutomaton < >, const automaton::ArcFactoredDeterministicZAutomaton < > & > ( automaton::simplify::UselessStatesRemover::remove, "afdza" ).setDocumentation (
"Removes unreachable states from a deterministic arc-factored z-automaton.\n\
\n\
@param afdza automaton to trim\n\
@return @p afdza without unreachable states" );

auto UselessStatesRemoverAFNZA = registration::AbstractRegister < automaton::simplify::UselessStatesRemover, automaton::ArcFactoredNondeterministicZAutomaton < >, const automaton::ArcFactoredNondeterministicZAutomaton < > & > ( automaton::simplify::UselessStatesRemover::remove, "afnza" ).setDocumentation (
"Removes unreachable states from a nondeterministic arc-factored z-automaton.\n\
\n\
@param afnza automaton to trim\n\
@return @p afnza without unreachable states" );

} /* namespace */

#include "ToAutomatonDerivation.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ToAutomatonDerivationFormalRegExp = registration::AbstractRegister < regexp::convert::ToAutomatonDerivation, automaton::DFA < DefaultSymbolType, unsigned >, const regexp::FormalRegExp < > & > ( regexp::convert::ToAutomatonDerivation::convert ).setDocumentation (
"Implements conversion of regular expressions to finite automata using Brzozowski's derivation algorithm.\n\
\n\
@param regexp the regexp to convert\n\
@return finite automaton accepting the language described by the original regular expression" );

auto ToAutomatonDerivationUnboundedRegExp = registration::AbstractRegister < regexp::convert::ToAutomatonDerivation, automaton::DFA < DefaultSymbolType, unsigned >, const regexp::UnboundedRegExp < > & > ( regexp::convert::ToAutomatonDerivation::convert, "regexp" ).setDocumentation (
"Implements conversion of regular expressions to finite automata using Brzozowski's derivation algorithm.\n\
\n\
@param regexp the regexp to convert\n\
@return finite automaton accepting the language described by the original regular expression" );

} /* namespace */

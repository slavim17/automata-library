#pragma once

#include <regexp/formal/FormalRegExp.h>
#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/properties/LanguageIsEmpty.h>
#include <regexp/properties/LanguageIsEpsilon.h>
#include <regexp/unbounded/UnboundedRegExp.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>

namespace regexp::properties {

class InfiniteLanguage {
public:
	/**
	 * @brief Is language of the regular expression infinite?
	 * @tparam SymbolType the type of symbol in the tested regular expression
	 * @param regexp the regexp to test
	 * @return true of the language described by the regular expression is infinite
	 */
	template < class SymbolType >
	static bool isInfiniteLanguage(const regexp::FormalRegExpElement < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool isInfiniteLanguage(const regexp::FormalRegExpStructure < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool isInfiniteLanguage(const regexp::FormalRegExp < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool isInfiniteLanguage(const regexp::UnboundedRegExpElement < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool isInfiniteLanguage(const regexp::UnboundedRegExpStructure < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool isInfiniteLanguage(const regexp::UnboundedRegExp < SymbolType > & regexp);

	template < class SymbolType >
	class Unbounded {
	public:
		static bool visit(const regexp::UnboundedRegExpAlternation < SymbolType > & alternation);
		static bool visit(const regexp::UnboundedRegExpConcatenation < SymbolType > & concatenation);
		static bool visit(const regexp::UnboundedRegExpIteration < SymbolType > & iteration);
		static bool visit(const regexp::UnboundedRegExpSymbol < SymbolType > & symbol);
		static bool visit(const regexp::UnboundedRegExpEmpty < SymbolType > & empty);
		static bool visit(const regexp::UnboundedRegExpEpsilon < SymbolType > & epsilon);
	};

	template < class SymbolType >
	class Formal {
	public:
		static bool visit(const regexp::FormalRegExpAlternation < SymbolType > & alternation);
		static bool visit(const regexp::FormalRegExpConcatenation < SymbolType > & concatenation);
		static bool visit(const regexp::FormalRegExpIteration < SymbolType > & iteration);
		static bool visit(const regexp::FormalRegExpSymbol < SymbolType > & symbol);
		static bool visit(const regexp::FormalRegExpEmpty < SymbolType > & empty);
		static bool visit(const regexp::FormalRegExpEpsilon < SymbolType > & epsilon);
	};
};

// ----------------------------------------------------------------------------

template < class SymbolType >
bool InfiniteLanguage::isInfiniteLanguage(const regexp::FormalRegExpElement < SymbolType > & regexp) {
	return regexp.template accept<bool, InfiniteLanguage::Formal < SymbolType >>();
}

template < class SymbolType >
bool InfiniteLanguage::isInfiniteLanguage(const regexp::FormalRegExpStructure < SymbolType > & regexp) {
	return isInfiniteLanguage(regexp.getStructure());
}

template < class SymbolType >
bool InfiniteLanguage::isInfiniteLanguage(const regexp::FormalRegExp < SymbolType > & regexp) {
	return isInfiniteLanguage(regexp.getRegExp());
}

// ----------------------------------------------------------------------------

template < class SymbolType >
bool InfiniteLanguage::isInfiniteLanguage(const regexp::UnboundedRegExpElement < SymbolType > & regexp) {
	return regexp.template accept<bool, InfiniteLanguage::Unbounded < SymbolType >>();
}

template < class SymbolType >
bool InfiniteLanguage::isInfiniteLanguage(const regexp::UnboundedRegExpStructure < SymbolType > & regexp) {
	return isInfiniteLanguage(regexp.getStructure());
}

template < class SymbolType >
bool InfiniteLanguage::isInfiniteLanguage(const regexp::UnboundedRegExp < SymbolType > & regexp) {
	return isInfiniteLanguage(regexp.getRegExp());
}

// ---------------------------------------------------------------------------

template < class SymbolType >
bool InfiniteLanguage::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpAlternation < SymbolType > & alternation) {
	return std::any_of ( alternation.getElements ( ).begin ( ), alternation.getElements ( ).end ( ), [ ] ( const UnboundedRegExpElement < SymbolType > & element ) {
		return element.template accept < bool, InfiniteLanguage::Unbounded < SymbolType > > ( );
	} );
}

template < class SymbolType >
bool InfiniteLanguage::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpConcatenation < SymbolType > & concatenation) {
	// if any concatenation element is empty, it can't be infinite language
	if ( std::any_of ( concatenation.getElements ( ).begin ( ), concatenation.getElements ( ).end ( ), [ ] ( const UnboundedRegExpElement < SymbolType > & element ) {
		return LanguageIsEmpty::isLanguageEmpty ( element );
	} ) ) {
		return false;
	}

	// if all are non-empty, it is sufficient that one of the elements is infinite
	return std::any_of ( concatenation.getElements ( ).begin ( ), concatenation.getElements ( ).end ( ), [ ] ( const UnboundedRegExpElement < SymbolType > & element ) {
		return element.template accept < bool, InfiniteLanguage::Unbounded < SymbolType > > ( );
	} );
}

template < class SymbolType >
bool InfiniteLanguage::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpIteration < SymbolType > & iteration) {
	return ! LanguageIsEmpty::isLanguageEmpty ( iteration ) && ! LanguageIsEpsilon::languageIsEpsilon ( iteration );
}

template < class SymbolType >
bool InfiniteLanguage::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpSymbol < SymbolType > &) {
	return false;
}

template < class SymbolType >
bool InfiniteLanguage::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpEpsilon < SymbolType > &) {
	return false;
}

template < class SymbolType >
bool InfiniteLanguage::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpEmpty < SymbolType > &) {
	return false;
}

// ----------------------------------------------------------------------------

template < class SymbolType >
bool InfiniteLanguage::Formal < SymbolType >::visit(const regexp::FormalRegExpAlternation < SymbolType > & alternation) {
	return alternation.getLeftElement().template accept<bool, InfiniteLanguage::Formal < SymbolType >>() || alternation.getRightElement().template accept<bool, InfiniteLanguage::Formal < SymbolType >>();
}

template < class SymbolType >
bool InfiniteLanguage::Formal < SymbolType >::visit(const regexp::FormalRegExpConcatenation < SymbolType > & concatenation) {
	return
		( ! LanguageIsEmpty::isLanguageEmpty ( concatenation.getRightElement ( ) ) && concatenation.getLeftElement().template accept<bool, InfiniteLanguage::Formal < SymbolType >>() ) ||
		( ! LanguageIsEmpty::isLanguageEmpty ( concatenation.getRightElement ( ) ) && concatenation.getRightElement().template accept<bool, InfiniteLanguage::Formal < SymbolType >>() );
}

template < class SymbolType >
bool InfiniteLanguage::Formal < SymbolType >::visit(const regexp::FormalRegExpIteration < SymbolType > & iteration) {
	return ! LanguageIsEmpty::isLanguageEmpty ( iteration ) && ! LanguageIsEpsilon::languageIsEpsilon ( iteration );
}

template < class SymbolType >
bool InfiniteLanguage::Formal < SymbolType >::visit(const regexp::FormalRegExpSymbol < SymbolType > &) {
	return false;
}

template < class SymbolType >
bool InfiniteLanguage::Formal < SymbolType >::visit(const regexp::FormalRegExpEmpty < SymbolType > &) {
	return false;
}

template < class SymbolType >
bool InfiniteLanguage::Formal < SymbolType >::visit(const regexp::FormalRegExpEpsilon < SymbolType > &) {
	return false;
}

}

#pragma once

#include <algorithm>
#include <regexp/formal/FormalRegExp.h>
#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/properties/LanguageIsEmpty.h>
#include <regexp/unbounded/UnboundedRegExp.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>

namespace regexp::properties {

/**
 * Determines whether regular expression (or its subtree) describes a language that is equal to epsilon (\eps = h(regexp)).
 *
 */
class LanguageIsEpsilon {
public:
	/**
	 * @brief Determines whether regular expression (or its subtree) describes a language that is equal to epsilon (\eps = h(regexp)).
	 * @tparam SymbolType the type of symbol in the tested regular expression
	 * @param regexp the regexp to test
	 * @return true of the language described by the regular expression is epsilon
	 */
	template < class SymbolType >
	static bool languageIsEpsilon(const regexp::FormalRegExpElement < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool languageIsEpsilon(const regexp::FormalRegExpStructure < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool languageIsEpsilon(const regexp::FormalRegExp < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool languageIsEpsilon(const regexp::UnboundedRegExpElement < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool languageIsEpsilon(const regexp::UnboundedRegExpStructure < SymbolType > & regexp);

	/**
	 * @overload
	 */
	template < class SymbolType >
	static bool languageIsEpsilon(const regexp::UnboundedRegExp < SymbolType > & regexp);

	template < class SymbolType >
	class Unbounded {
	public:
		static bool visit(const regexp::UnboundedRegExpAlternation < SymbolType > & alternation);
		static bool visit(const regexp::UnboundedRegExpConcatenation < SymbolType > & concatenation);
		static bool visit(const regexp::UnboundedRegExpIteration < SymbolType > & iteration);
		static bool visit(const regexp::UnboundedRegExpSymbol < SymbolType > & symbol);
		static bool visit(const regexp::UnboundedRegExpEmpty < SymbolType > & empty);
		static bool visit(const regexp::UnboundedRegExpEpsilon < SymbolType > & epsilon);
	};

	template < class SymbolType >
	class Formal {
	public:
		static bool visit(const regexp::FormalRegExpAlternation < SymbolType > & alternation);
		static bool visit(const regexp::FormalRegExpConcatenation < SymbolType > & concatenation);
		static bool visit(const regexp::FormalRegExpIteration < SymbolType > & iteration);
		static bool visit(const regexp::FormalRegExpSymbol < SymbolType > & symbol);
		static bool visit(const regexp::FormalRegExpEmpty < SymbolType > & empty);
		static bool visit(const regexp::FormalRegExpEpsilon < SymbolType > & epsilon);
	};
};

// ----------------------------------------------------------------------------

template < class SymbolType >
bool LanguageIsEpsilon::languageIsEpsilon(const regexp::FormalRegExpElement < SymbolType > & regexp) {
	return regexp.template accept<bool, LanguageIsEpsilon::Formal < SymbolType >>();
}

template < class SymbolType >
bool LanguageIsEpsilon::languageIsEpsilon(const regexp::FormalRegExpStructure < SymbolType > & regexp) {
	return languageIsEpsilon(regexp.getStructure());
}

template < class SymbolType >
bool LanguageIsEpsilon::languageIsEpsilon(const regexp::FormalRegExp < SymbolType > & regexp) {
	return languageIsEpsilon(regexp.getRegExp());
}

// ----------------------------------------------------------------------------

template < class SymbolType >
bool LanguageIsEpsilon::languageIsEpsilon(const regexp::UnboundedRegExpElement < SymbolType > & regexp) {
	return regexp.template accept<bool, LanguageIsEpsilon::Unbounded < SymbolType >>();
}

template < class SymbolType >
bool LanguageIsEpsilon::languageIsEpsilon(const regexp::UnboundedRegExpStructure < SymbolType > & regexp) {
	return languageIsEpsilon(regexp.getStructure());
}

template < class SymbolType >
bool LanguageIsEpsilon::languageIsEpsilon(const regexp::UnboundedRegExp < SymbolType > & regexp) {
	return languageIsEpsilon(regexp.getRegExp());
}

// ---------------------------------------------------------------------------

template < class SymbolType >
bool LanguageIsEpsilon::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpAlternation < SymbolType > & alternation) {
	bool existsEpsilon = false;
	return std::all_of ( alternation.getElements ( ).begin ( ), alternation.getElements ( ).end ( ), [ & ] ( const auto & e ) {
			bool isEpsilon = e.template accept < bool, LanguageIsEpsilon::Unbounded < SymbolType > > ( );
			existsEpsilon = existsEpsilon || isEpsilon;
			return isEpsilon || LanguageIsEmpty::isLanguageEmpty ( e );
		} ) && existsEpsilon;
}

template < class SymbolType >
bool LanguageIsEpsilon::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpConcatenation < SymbolType > & concatenation) {
	return std::all_of ( concatenation.getElements ( ).begin ( ), concatenation.getElements ( ).end ( ), [ ] ( const UnboundedRegExpElement < SymbolType > & element ) {
		return element.template accept < bool, LanguageIsEpsilon::Unbounded < SymbolType > > ( );
	} );
}

template < class SymbolType >
bool LanguageIsEpsilon::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpIteration < SymbolType > & iteration) {
	return LanguageIsEmpty::isLanguageEmpty ( iteration.getElement ( ) ) || iteration.getElement().template accept<bool, LanguageIsEpsilon::Unbounded < SymbolType > >();
}

template < class SymbolType >
bool LanguageIsEpsilon::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpSymbol < SymbolType > &) {
	return false;
}

template < class SymbolType >
bool LanguageIsEpsilon::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpEpsilon < SymbolType > &) {
	return true;
}

template < class SymbolType >
bool LanguageIsEpsilon::Unbounded < SymbolType >::visit(const regexp::UnboundedRegExpEmpty < SymbolType > &) {
	return false;
}

// ----------------------------------------------------------------------------

template < class SymbolType >
bool LanguageIsEpsilon::Formal < SymbolType >::visit(const regexp::FormalRegExpAlternation < SymbolType > & alternation) {
	// ALL {eps,empty} + {eps,empty} but not ALL empty
	auto leftEmpty = LanguageIsEmpty::isLanguageEmpty ( alternation.getLeftElement ( ) );
	auto rightEmpty = LanguageIsEmpty::isLanguageEmpty ( alternation.getLeftElement ( ) );
	auto leftEps = alternation.getLeftElement().template accept<bool, LanguageIsEpsilon::Formal < SymbolType >>();
	auto rightEps = alternation.getRightElement().template accept<bool, LanguageIsEpsilon::Formal < SymbolType >>();

	return ! ( leftEmpty && rightEmpty ) && ( leftEmpty || leftEps ) && ( rightEmpty || rightEps );
}

template < class SymbolType >
bool LanguageIsEpsilon::Formal < SymbolType >::visit(const regexp::FormalRegExpConcatenation < SymbolType > & concatenation) {
	return concatenation.getLeftElement().template accept<bool, LanguageIsEpsilon::Formal < SymbolType >>() && concatenation.getRightElement().template accept<bool, LanguageIsEpsilon::Formal < SymbolType >>();
}

template < class SymbolType >
bool LanguageIsEpsilon::Formal < SymbolType >::visit(const regexp::FormalRegExpIteration < SymbolType > & iteration) {
	return LanguageIsEmpty::isLanguageEmpty ( iteration.getElement ( ) ) || iteration.getElement().template accept<bool, LanguageIsEpsilon::Formal < SymbolType > >();
}

template < class SymbolType >
bool LanguageIsEpsilon::Formal < SymbolType >::visit(const regexp::FormalRegExpSymbol < SymbolType > &) {
	return false;
}

template < class SymbolType >
bool LanguageIsEpsilon::Formal < SymbolType >::visit(const regexp::FormalRegExpEmpty < SymbolType > &) {
	return false;
}

template < class SymbolType >
bool LanguageIsEpsilon::Formal < SymbolType >::visit(const regexp::FormalRegExpEpsilon < SymbolType > &) {
	return true;
}

}

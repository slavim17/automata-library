#include "LanguageIsEmpty.h"
#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto LanguageEmptyFormalRegExp = registration::AbstractRegister < regexp::properties::LanguageIsEmpty, bool, const regexp::FormalRegExp < > & > ( regexp::properties::LanguageIsEmpty::isLanguageEmpty );
auto LanguageEmptyUnboundedRegExp = registration::AbstractRegister < regexp::properties::LanguageIsEmpty, bool, const regexp::UnboundedRegExp < > & > ( regexp::properties::LanguageIsEmpty::isLanguageEmpty );

} /* namespace */

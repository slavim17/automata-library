#include "LanguageContainsEpsilon.h"
#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto FormalRegExp = registration::AbstractRegister < regexp::properties::LanguageContainsEpsilon, bool, const regexp::FormalRegExp < > & > ( regexp::properties::LanguageContainsEpsilon::languageContainsEpsilon );
auto UnboundedRegExp = registration::AbstractRegister < regexp::properties::LanguageContainsEpsilon, bool, const regexp::UnboundedRegExp < > & > ( regexp::properties::LanguageContainsEpsilon::languageContainsEpsilon );

} /* namespace */

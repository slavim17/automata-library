#include "LanguageIsEpsilon.h"
#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto FormalRegExp = registration::AbstractRegister < regexp::properties::LanguageIsEpsilon, bool, const regexp::FormalRegExp < > & > ( regexp::properties::LanguageIsEpsilon::languageIsEpsilon );
auto UnboundedRegExp = registration::AbstractRegister < regexp::properties::LanguageIsEpsilon, bool, const regexp::UnboundedRegExp < > & > ( regexp::properties::LanguageIsEpsilon::languageIsEpsilon );

} /* namespace */

#include "CompressedBitParallelismPatterns.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto CompressedBitParallelismPatternsPrefixRankedPattern = registration::AbstractRegister < arbology::query::CompressedBitParallelismPatterns, ext::set < unsigned >, const indexes::arbology::CompressedBitParallelTreeIndex < > &, const tree::PrefixRankedPattern < > & > ( arbology::query::CompressedBitParallelismPatterns::query );
auto CompressedBitParallelismPatternsPrefixRankedBarPattern = registration::AbstractRegister < arbology::query::CompressedBitParallelismPatterns, ext::set < unsigned >, const indexes::arbology::CompressedBitParallelTreeIndex < > &, const tree::PrefixRankedBarPattern < > & > ( arbology::query::CompressedBitParallelismPatterns::query );

} /* namespace */

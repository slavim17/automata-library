#pragma once

#include <alib/set>
#include <alib/map>

#include <common/ranked_symbol.hpp>

#include <tree/properties/QuickSearchBadCharacterShiftTable.h>
#include <tree/properties/SubtreeJumpTable.h>
#include <tree/properties/ExactSubtreeRepeatsNaive.h>
#include <tree/exact/BackwardOccurrenceTest.h>

#include <tree/ranked/PrefixRankedBarTree.h>
#include <tree/ranked/PrefixRankedBarPattern.h>
#include <tree/ranked/PrefixRankedBarNonlinearPattern.h>

namespace arbology {

namespace exact {

/**
* Implementation of the Quick Search algorithm for tree pattern matching.
* This variant searches the subject tree from left to right, while comparing matches from right to left.
* This algorithm makes use of a Bad character shift table as well as a Subtree jump table.
*/
class QuickSearch {
public:
	/**
	* Search for a tree pattern in a tree.
	* @return set set of occurences
	*/
	template < class SymbolType >
	static ext::set < unsigned > match ( const tree::PrefixRankedBarTree < SymbolType > & subject, const tree::PrefixRankedBarTree < SymbolType > & pattern );
	template < class SymbolType >
	static ext::set < unsigned > match ( const tree::PrefixRankedBarTree < SymbolType > & subject, const tree::PrefixRankedBarPattern < SymbolType > & pattern );
	template < class SymbolType >
	static ext::set < unsigned > match ( const tree::PrefixRankedBarTree < SymbolType > & subject, const tree::PrefixRankedBarNonlinearPattern < SymbolType > & pattern );

};

template < class SymbolType >
ext::set < unsigned > QuickSearch::match ( const tree::PrefixRankedBarTree < SymbolType > & subject, const tree::PrefixRankedBarTree < SymbolType > & pattern ) {
	return match ( subject, tree::PrefixRankedBarPattern < SymbolType > ( pattern ) );
}

template < class SymbolType >
ext::set < unsigned > QuickSearch::match ( const tree::PrefixRankedBarTree < SymbolType > & subject, const tree::PrefixRankedBarPattern < SymbolType > & pattern ) {
	ext::set < unsigned > occ;
	ext::map < common::ranked_symbol < SymbolType >, size_t > bcs = tree::properties::QuickSearchBadCharacterShiftTable::bcs ( pattern ); // NOTE: the subjects alphabet must be a subset or equal to the pattern
	ext::vector < int > subjectSubtreeJumpTable = tree::properties::SubtreeJumpTable::compute ( subject );

	// index to the subject
	unsigned i = pattern.getContent ( ).size ( ) - 1;

	// main loop of the algorithm over all possible indexes where the pattern can start
	while ( i < subject.getContent ( ).size ( ) ) {
		// pair j and offset
		ext::pair < size_t, size_t > jOffset = tree::exact::BackwardOccurrenceTest::occurrence ( subject, subjectSubtreeJumpTable, pattern, i );

		 // match was found
		if ( jOffset.first == 0 )
			occ.insert ( jOffset.second );

		if ( i + 1 >= subject.getContent ( ).size ( ) ) {
			break;
		}

		// shift heuristics
		i += bcs[subject.getContent ( )[i + 1]];
	}

	return occ;
}

template < class SymbolType >
ext::set < unsigned > QuickSearch::match ( const tree::PrefixRankedBarTree < SymbolType > & subject, const tree::PrefixRankedBarNonlinearPattern < SymbolType > & pattern ) {
	ext::set < unsigned > occ;
	ext::map < common::ranked_symbol < SymbolType >, size_t > bcs = tree::properties::QuickSearchBadCharacterShiftTable::bcs ( pattern ); //NOTE: the subjects alphabet must be a subset or equal to the pattern

	ext::vector < int > subjectSubtreeJumpTable = tree::properties::SubtreeJumpTable::compute ( subject );
	tree::PrefixRankedBarTree < unsigned > repeats = tree::properties::ExactSubtreeRepeatsNaive::repeats ( subject );

	// index to the subject
	unsigned i = pattern.getContent ( ).size ( ) - 1;

	// main loop of the algorithm over all possible indexes where the pattern can start
	while ( i < subject.getContent ( ).size ( ) ) {
		// pair j and offset
		ext::pair < size_t, size_t > jOffset = tree::exact::BackwardOccurrenceTest::occurrence ( subject, subjectSubtreeJumpTable, repeats, pattern, i );

		 // match was found
		if ( jOffset.first == 0 )
			occ.insert ( jOffset.second );

		if ( i + 1 >= subject.getContent ( ).size ( ) ) {
			break;
		}

		// shift heuristics
		i += bcs[subject.getContent ( )[i + 1]];
	}

	return occ;
}

} /* namespace exact */

} /* namespace arbology */


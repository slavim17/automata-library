#include "NonlinearFullAndLinearIndexConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto nonlinearFullAndLinearIndexConstructionPrefixRankedTree = registration::AbstractRegister < arbology::indexing::NonlinearFullAndLinearIndexConstruction, indexes::arbology::NonlinearFullAndLinearIndex < >, const tree::PrefixRankedTree < > & > ( arbology::indexing::NonlinearFullAndLinearIndexConstruction::construct );

auto nonlinearFullAndLinearIndexConstructionPrefixRankedBarTree = registration::AbstractRegister < arbology::indexing::NonlinearFullAndLinearIndexConstruction, indexes::arbology::NonlinearFullAndLinearIndex < >, const tree::PrefixRankedBarTree < > & > ( arbology::indexing::NonlinearFullAndLinearIndexConstruction::construct );

} /* namespace */

#include "ExactSubtreeRepeatsFromSubtreeAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactRepeatsFromSubtreeAutomatonPrefixRankedTree = registration::AbstractRegister < arbology::properties::ExactSubtreeRepeatsFromSubtreeAutomaton, tree::PrefixRankedTree < unsigned >, const tree::PrefixRankedTree < > & > ( arbology::properties::ExactSubtreeRepeatsFromSubtreeAutomaton::repeats );

} /* namespace */

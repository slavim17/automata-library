#pragma once

#include "RegularEquationSolver.h"

namespace equations {

template < class TerminalSymbolType, class VariableSymbolType >
class RightRegularEquationSolver : public RegularEquationSolver < TerminalSymbolType, VariableSymbolType > {
	/**
	 * @copydoc RegularEquationSolver::concatenate(regexp::UnboundedRegExpElement < TerminalSymbolType > &&, regexp::UnboundedRegExpElement < TerminalSymbolType > &&)
	 */
	regexp::UnboundedRegExpConcatenation < TerminalSymbolType > concatenate ( regexp::UnboundedRegExpElement < TerminalSymbolType > && left, regexp::UnboundedRegExpElement < TerminalSymbolType > && right ) override {
		regexp::UnboundedRegExpConcatenation < TerminalSymbolType > concat;
		concat.appendElement ( std::move ( left ) );
		concat.appendElement ( std::move ( right ) );
		return concat;
	}
};

} /* namespace equations */

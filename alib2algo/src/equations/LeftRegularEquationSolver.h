#pragma once

#include "RegularEquationSolver.h"

namespace equations {

template < class TerminalSymbolType, class VariableSymbolType >
class LeftRegularEquationSolver : public RegularEquationSolver < TerminalSymbolType, VariableSymbolType > {
	/**
	 * @copydoc RegularEquationSolver::concatenate(regexp::UnboundedRegExpElement < TerminalSymbolType > &&, regexp::UnboundedRegExpElement < TerminalSymbolType > &&)
	 */
	regexp::UnboundedRegExpConcatenation < TerminalSymbolType > concatenate ( regexp::UnboundedRegExpElement < TerminalSymbolType > && right, regexp::UnboundedRegExpElement < TerminalSymbolType > && left ) override {
		regexp::UnboundedRegExpConcatenation < TerminalSymbolType > concat;
		concat.appendElement ( std::move ( left ) );
		concat.appendElement ( std::move ( right ) );
		return concat;
	}
};

} /* namespace equations */

#include <catch2/catch.hpp>
#include <string/LinearString.h>
#include <stringology/seed/ExactSeedComputation.h>


TEST_CASE ( "Exact seed computation", "[unit][stringology][seed]" ) {
	string::LinearString < char > pattern;
	ext::set < ext::pair < string::LinearString < char >, unsigned > > expectedSeeds;

    SECTION ( "Compute exact seeds for empty string" ) {
		pattern = string::LinearString < char > ( "" );
		expectedSeeds = { };
    }

    SECTION ( "Compute exact seeds for 'abab'" ) {
        pattern = string::LinearString < char > ( "abab" );
        expectedSeeds = {
			{ string::LinearString < char > ( { 'a', 'b' }, { 'a', 'b' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b' }, { 'a', 'b', 'a' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b' }, { 'b', 'a' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b' }, { 'b', 'a', 'b' } ), 0 },
        };
    }

    SECTION ( "Compute exact seeds for 'abcabc'" ) {
        pattern = string::LinearString < char > ( "abcabc" );
        expectedSeeds = {
			{ string::LinearString < char > ( { 'a', 'b', 'c' }, { 'a', 'b', 'c' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b', 'c' }, { 'b', 'c', 'a' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b', 'c' }, { 'c', 'a', 'b' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b', 'c' }, { 'a', 'b', 'c', 'a' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b', 'c' }, { 'b', 'c', 'a', 'b' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b', 'c' }, { 'c', 'a', 'b', 'c' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b', 'c' }, { 'a', 'b', 'c', 'a', 'b' } ), 0 },
			{ string::LinearString < char > ( { 'a', 'b', 'c' }, { 'b', 'c', 'a', 'b', 'c' } ), 0 },
		};
    }

    SECTION ( "Compute exact seeds for 'abcd'" ) {
        pattern = string::LinearString < char > ( "abcd" );
        expectedSeeds = { };
    }

    SECTION ( "Compute exact seeds for 'abba'" ) {
        pattern = string::LinearString < char > ( "abba" );
		expectedSeeds = {
			{ string::LinearString < char >( { 'a', 'b' }, { 'a', 'b', 'b' } ), 0 },
			{ string::LinearString < char >( { 'a', 'b' }, { 'b', 'b', 'a' } ), 0 },
		};
    }

	CHECK ( stringology::seed::ExactSeedComputation::compute( pattern ) == expectedSeeds );
}

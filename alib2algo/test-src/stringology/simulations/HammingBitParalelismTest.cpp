#include <catch2/catch.hpp>

#include <string/LinearString.h>
#include <stringology/simulations/HammingBitParalelism.h>

TEST_CASE ( "Hamming Bit Parallelism", "[unit][algo][stringology][simulations]" ) {
	SECTION ( "Simple" ) {
		auto text = string::LinearString<>("adcabcaabadbbca");
		auto pattern = string::LinearString<>("adbbca");

		ext::set<unsigned int> expected_result = {7,15};
		auto result = stringology::simulations::HammingBitParalelism::search(text, pattern, 3);
		CHECK(expected_result == result);
	}
}

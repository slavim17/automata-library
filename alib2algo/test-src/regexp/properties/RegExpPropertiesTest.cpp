#include <catch2/catch.hpp>

#include <factory/StringDataFactory.hpp>
#include <regexp/string/UnboundedRegExp.h>
#include <regexp/properties/InfiniteLanguage.h>
#include <regexp/properties/LanguageContainsEpsilon.h>
#include <regexp/properties/LanguageIsEmpty.h>
#include <regexp/properties/LanguageIsEpsilon.h>

TEST_CASE ( "RegExp properties", "[unit][algo][regexp][properties]" ) {
	SECTION ( "Contains epsilon" ) {
		{
			std::string input = "#E + ( (a #E) + a*)";
			regexp::UnboundedRegExp < > re = factory::StringDataFactory::fromString (input);

			CHECK(regexp::properties::LanguageContainsEpsilon::languageContainsEpsilon(re));
		}
		{
			std::string input = "( a* )( a* )";
			regexp::UnboundedRegExp < > re = factory::StringDataFactory::fromString (input);

			CHECK(regexp::properties::LanguageContainsEpsilon::languageContainsEpsilon(re));
		}
		{
			std::string input = "a + #0";
			regexp::UnboundedRegExp < > re = factory::StringDataFactory::fromString (input);

			CHECK(! regexp::properties::LanguageContainsEpsilon::languageContainsEpsilon(re));
		}
		{
			std::string input = "#E + a #E + a*";
			regexp::UnboundedRegExp < > re = factory::StringDataFactory::fromString (input);

			CHECK(regexp::properties::LanguageContainsEpsilon::languageContainsEpsilon(re));
		}
		{
			std::string input = "a* a*";
			regexp::UnboundedRegExp < > re = factory::StringDataFactory::fromString (input);

			CHECK(regexp::properties::LanguageContainsEpsilon::languageContainsEpsilon(re));
		}
		{
			std::string input = "a s d #E + #E #0";
			regexp::UnboundedRegExp < > re = factory::StringDataFactory::fromString (input);

			CHECK(! regexp::properties::LanguageContainsEpsilon::languageContainsEpsilon(re));
		}
		{
			std::string input = "a + #0";
			regexp::UnboundedRegExp < > re = factory::StringDataFactory::fromString (input);

			CHECK(! regexp::properties::LanguageContainsEpsilon::languageContainsEpsilon(re));
		}
	}

	SECTION ( "Describes empty language" ) {
		{
			std::string input = "(#E #0 ) + ( #0 a + (b ( #0 (a*) ) ) )";
			regexp::UnboundedRegExp < > re = factory::StringDataFactory::fromString (input);

			CHECK(regexp::properties::LanguageIsEmpty::isLanguageEmpty(re));
		}
		{
			std::string input = "(#E + a ) + ( #0 a + (b ( #0 (a*) ) ) )";
			regexp::UnboundedRegExp < > re = factory::StringDataFactory::fromString (input);

			CHECK(! regexp::properties::LanguageIsEmpty::isLanguageEmpty(re));
		}
	}

	SECTION ( "Is exactly epsilon language" ) {
		std::string inp;
		bool expected;

		SECTION ( "1" ) {
			inp = "(#E #0 ) + ( #0 a + (b ( #0 (a*) ) ) )";
			expected = false;
		}

		SECTION ( "2" ) {
			inp = "#E";
			expected = true;
		}

		SECTION ( "3" ) {
			inp = "#0";
			expected = false;
		}

		SECTION ( "4" ) {
			inp = "#E + #0";
			expected = true;
		}

		SECTION ( "5" ) {
			inp = "(#E** + #0)* + #E + a b c";
			expected = false;
		}

		SECTION ( "6" ) {
			inp = "(#E** + #0)* + #E + #0*";
			expected = true;
		}

		SECTION ( "7" ) {
			inp = "a* + #E";
			expected = false;
		}

		INFO ( inp );
		INFO ( expected );
		regexp::UnboundedRegExp < > reU = factory::StringDataFactory::fromString ( inp );
		CHECK ( regexp::properties::LanguageIsEpsilon::languageIsEpsilon ( reU ) == expected );
	}

	SECTION ( "Is infinite language" ) {
		std::string inp;
		bool expected;

		SECTION ( "1" ) {
			inp = "(#E #0 ) + ( #0 a + (b ( #0 (a*) ) ) )";
			expected = false;
		}

		SECTION ( "2" ) {
			inp = "(a + #E)*";
			expected = true;
		}

		SECTION ( "3" ) {
			inp = "#E*";
			expected = false;
		}

		SECTION ( "4" ) {
			inp = "#0*";
			expected = false;
		}

		SECTION ( "5" ) {
			inp = "a + b + c";
			expected = false;
		}

		SECTION ( "6" ) {
			inp = "a + b* + c";
			expected = true;
		}

		SECTION ( "7" ) {
			inp = "( a* ) ( #0 a )";
			expected = false;
		}

		SECTION ( "8" ) {
			inp = "( a* ) a";
			expected = true;
		}

		INFO ( inp );
		INFO ( expected );
		regexp::UnboundedRegExp < > reU = factory::StringDataFactory::fromString ( inp );
		CHECK ( regexp::properties::InfiniteLanguage::isInfiniteLanguage ( reU ) == expected );
	}

}

#include <catch2/catch.hpp>

#include <alib/list>

#include "automaton/transform/Compaction.h"
#include <automaton/FSM/DFA.h>
#include <automaton/FSM/CompactDFA.h>

TEST_CASE ( "Automata Compaction", "[unit][algo][automaton][transform]" ) {
	SECTION ( "DFA" ) {
		{
			std::string q0 = std::string("0");
			std::string q1 = std::string("1");
			std::string q2 = std::string("2");
			std::string q3 = std::string("3");
			std::string q4 = std::string("4");
			std::string q5 = std::string("5");
			char a = 'a';
			char b = 'b';

			automaton::DFA < char, std::string > m1(q0);
			automaton::CompactDFA < char, std::string > m2(q0);

			m1.setInputAlphabet({a, b});
			m1.setStates({q0, q1, q2, q3, q4, q5});
			m1.addTransition(q0, a, q1);
			m1.addTransition(q1, b, q2);
			m1.addTransition(q2, a, q3);
			m1.addTransition(q3, b, q4);
			m1.addTransition(q4, a, q5);
			m1.addFinalState(q5);

			m2.setInputAlphabet({a, b});
			m2.setStates({q0, q5});
			m2.addTransition(q0, ext::vector <char > {a, b, a, b, a}, q5);
			m2.addFinalState(q5);

			automaton::CompactDFA < char, std::string > c1 = automaton::transform::Compaction::convert(m1);

			CHECK (c1 == m2);
		}{
			std::string q0 = std::string("0");
			std::string q1 = std::string("1");
			std::string q2 = std::string("2");
			std::string q3 = std::string("3");
			std::string q4 = std::string("4");
			std::string q5 = std::string("5");
			char a = 'a';
			char b = 'b';

			automaton::DFA < char, std::string > m1(q0);
			automaton::CompactDFA < char, std::string > m2(q0);

			m1.setInputAlphabet({a, b});
			m1.setStates({q0, q1, q2, q3, q4, q5});
			m1.addTransition(q0, a, q1);
			m1.addTransition(q1, b, q2);
			m1.addTransition(q2, a, q3);
			m1.addTransition(q3, b, q4);
			m1.addTransition(q4, a, q5);
			m1.addTransition(q5, a, q1);
			m1.addFinalState(q5);

			m2.setInputAlphabet({a, b});
			m2.setStates({q0, q5});
			m2.addTransition(q0, ext::vector < char > { a, b, a, b, a }, q5);
			m2.addTransition(q5, ext::vector < char > { a, b, a, b, a }, q5);
			m2.addFinalState(q5);

			automaton::CompactDFA < char, std::string > c1 = automaton::transform::Compaction::convert(m1);

			CHECK (c1 == m2);

		}
		{
			std::string q0 = std::string("0");
			std::string q1 = std::string("1");
			std::string q2 = std::string("2");
			std::string q3 = std::string("3");
			std::string q4 = std::string("4");
			std::string q5 = std::string("5");
			std::string q6 = std::string("6");
			char a = 'a';
			char b = 'b';

			automaton::DFA < char, std::string > m1(q0);
			automaton::CompactDFA < char, std::string > m2(q0);

			m1.setInputAlphabet({a, b});
			m1.setStates({q0, q1, q2, q3, q4, q5, q6});
			m1.addTransition(q0, a, q1);
			m1.addTransition(q1, a, q2);
			m1.addTransition(q2, a, q3);
			m1.addTransition(q3, a, q4);
			m1.addTransition(q4, a, q2);
			m1.addTransition(q1, b, q5);
			m1.addTransition(q5, b, q6);
			m1.addTransition(q6, a, q0);
			m1.addFinalState(q3);
			m1.addFinalState(q5);

			m2.setInputAlphabet({a, b});
			m2.setStates({q0, q1, q3, q5});
			m2.addTransition(q0, ext::vector < char > {a}, q1);
			m2.addTransition(q1, ext::vector < char > {a, a}, q3);
			m2.addTransition(q3, ext::vector < char > {a, a, a}, q3);
			m2.addTransition(q1, ext::vector < char > {b}, q5);
			m2.addTransition(q5, ext::vector < char > {b, a, a}, q1);
			m2.addFinalState(q3);
			m2.addFinalState(q5);

			automaton::CompactDFA < char, std::string > c1 = automaton::transform::Compaction::convert(m1);

			CHECK (c1 == m2);
		}
	}
}

#include <catch2/catch.hpp>

#include <alib/list>

#include "automaton/simplify/Normalize.h"

TEST_CASE ( "Normalize", "[unit][algo][automaton][simplify]" ) {
	SECTION ( "DFA" ) {
		automaton::DFA < std::string, int > automaton(0);

		automaton.addState(0);
		automaton.addState(1);
		automaton.addState(2);
		automaton.addInputSymbol(std::string("a"));
		automaton.addInputSymbol(std::string("b"));

		automaton.addTransition(0, std::string("a"), 1);
		automaton.addTransition(1, std::string("b"), 2);

		automaton.addFinalState(2);

		automaton::DFA < std::string, unsigned > normalized = automaton::simplify::Normalize::normalize(automaton);

		CHECK(normalized.getStates().size() == automaton.getStates().size());
		CHECK(normalized.getTransitions().size() == automaton.getTransitions().size());

		automaton::DFA < std::string, unsigned > reference(0);

		reference.addState(0);
		reference.addState(1);
		reference.addState(2);
		reference.addInputSymbol(std::string("a"));
		reference.addInputSymbol(std::string("b"));

		reference.addTransition(0, std::string("a"), 1);
		reference.addTransition(1, std::string("b"), 2);

		reference.addFinalState(2);

		CHECK ( normalized == reference );
	}
}

#include <catch2/catch.hpp>

#include <alib/list>

#include "automaton/simplify/Trim.h"

#include "automaton/FSM/DFA.h"
#include "automaton/TA/DFTA.h"

TEST_CASE ( "Trim Automaton", "[unit][algo][automaton][simplify]" ) {
	SECTION ( "DFA" ) {
		automaton::DFA < std::string, int > automaton(1);

		automaton.addState(1);
		automaton.addState(2);
		automaton.addState(3);
		automaton.addInputSymbol(std::string("a"));
		automaton.addInputSymbol(std::string("b"));

		automaton.addTransition(1, std::string("a"), 2);
		automaton.addTransition(2, std::string("b"), 1);
		automaton.addTransition(3, std::string("b"), 1);

		automaton.addFinalState(1);

		automaton::DFA < std::string, int > trimed = automaton::simplify::Trim::trim(automaton);

		CHECK (trimed.getStates().size() == 2);
	}


	SECTION ( "DFTA" ) {
		automaton::DFTA < std::string, int > automaton;

		ext::vector<int> q;
		for (int state = 0; state <= 11; ++ state) {
			q.push_back(state);
			automaton.addState(state);
		}
		automaton.addFinalState(q[2]);
		automaton.addFinalState(q[11]);

		const common::ranked_symbol < std::string > a (std::string("a"), 2);
		const common::ranked_symbol < std::string > b (std::string("b"), 1);
		const common::ranked_symbol < std::string > c (std::string("c"), 0);
		automaton.addInputSymbol(a);
		automaton.addInputSymbol(b);
		automaton.addInputSymbol(c);

		automaton.addTransition(c, {}, q[0]);
		automaton.addTransition(a, {q[0], q[0]}, q[1]);
		automaton.addTransition(b, {q[1]}, q[2]);

		//unreachable and useless
		automaton.addTransition(a, {q[3], q[4]}, q[5]);
		automaton.addTransition(b, {q[5]}, q[6]);

		//useless
		automaton.addTransition(a, {q[2], q[2]}, q[7]);
		automaton.addTransition(a, {q[7], q[7]}, q[8]);

		//unreachable
		automaton.addTransition(a, {q[9], q[9]}, q[10]);
		automaton.addTransition(a, {q[10], q[10]}, q[11]);

		automaton::DFTA < std::string, int > trimed = automaton::simplify::Trim::trim(automaton);

		automaton::DFTA < std::string, int > correct;
		correct.addState(q[0]);
		correct.addState(q[1]);
		correct.addState(q[2]);
		correct.addFinalState(q[2]);
		correct.addInputSymbol(a);
		correct.addInputSymbol(b);
		correct.addInputSymbol(c);
		correct.addTransition(c, {}, q[0]);
		correct.addTransition(a, {q[0], q[0]}, q[1]);
		correct.addTransition(b, {q[1]}, q[2]);

		CHECK (trimed == correct);
	}
}

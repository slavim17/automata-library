#include "CommonException.h"

#include <global/GlobalData.h>

#include <registration/ValuePrinterRegistration.hpp>

#include <sstream>

#ifdef DEBUG
	#include <debug/simpleStacktrace.h>
#endif

namespace exception {

CommonException::CommonException ( std::string cause ) : m_cause(std::move(cause)) {
	#ifdef DEBUG
		ext::ostringstream ss;
		ext::simpleStacktrace ( ss );
		this->m_backtrace = std::move ( ss ).str ( );
	#else
		this->m_backtrace = "";
	#endif

	for(int i = 0; i < common::GlobalData::argc; i++) {
		if(i != 0) this->m_command += " ";
		this->m_command += common::GlobalData::argv[i];
	}
}

const char * CommonException::what ( ) const noexcept {
	return m_cause.c_str ( );
}

const std::string & CommonException::getCause ( ) const {
	return m_cause;
}

const std::string & CommonException::getBacktrace ( ) const {
	return m_backtrace;
}

const std::string & CommonException::getCommand ( ) const {
	return m_command;
}

std::ostream & operator << ( std::ostream & os, const CommonException & data ) {
	return os << data.what ( ) << std::endl << data.getBacktrace ( ) << std::endl << data.getCommand ( );
}

} /* namespace exception */

namespace {

// auto valuePrinter = registration::ValuePrinterRegister < exception::CommonException > ( ); // FIXME is this really needed?

} /* namespace */

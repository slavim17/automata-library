#include <cstdio>
#include <csignal>
#include <cstdlib>
#include <cstring>

#include <unistd.h>
#include <execinfo.h>

#include "sigHandler.h"
#include "simpleStacktrace.h"

#include <ext/iostream>
#include <global/GlobalData.h>

namespace ext {

#ifdef DEBUG
	void SigHandler::handler(int sigNo) {
		const char * sigStr = strsignal ( sigNo );
		if ( sigStr )
			common::Streams::out << strsignal ( sigNo ) << std::endl;
		else
			common::Streams::out << "Unknown signal (" << sigNo << ")" << std::endl;

		ext::simpleStacktrace ( common::Streams::out );

		/* reraise */
		signal ( sigNo, SIG_DFL );
		raise ( sigNo );
		// exit ( EXIT_FAILURE );
	}
#else
	void SigHandler::handler ( int ) {
	}
#endif

SigHandler::SigHandler() {
	#ifdef DEBUG
		signal(SIGSEGV, SigHandler::handler);
		signal(SIGINT, SigHandler::handler);
	#endif
}

SigHandler SigHandler::HANDLER;

} /* namespace ext */

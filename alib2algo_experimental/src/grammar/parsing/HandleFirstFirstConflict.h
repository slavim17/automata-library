#pragma once

#include <grammar/ContextFree/CFG.h>
#include <alib/vector>

namespace grammar {

namespace parsing {

class HandleFirstFirstConflict {
public:
	static void handleFirstFirstConflict ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const DefaultSymbolType & nonterminal, const ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > & rhsds );
};

} /* namespace parsing */

} /* namespace grammar */


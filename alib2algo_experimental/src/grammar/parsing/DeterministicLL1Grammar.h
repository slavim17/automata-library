#pragma once

#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class DeterministicLL1Grammar {
public:
	static grammar::CFG < > convert ( const grammar::CFG < > & param );

};

} /* namespace parsing */

} /* namespace grammar */


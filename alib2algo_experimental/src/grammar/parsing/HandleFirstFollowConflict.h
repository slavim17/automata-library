#pragma once

#include <grammar/ContextFree/CFG.h>
#include <alib/vector>
#include <alib/set>

namespace grammar {

namespace parsing {

class HandleFirstFollowConflict {
public:
	static void handleFirstFollowConflict ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const DefaultSymbolType & nonterminal, const ext::set < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > & /* rhsds */ );
};

} /* namespace parsing */

} /* namespace grammar */


#include "MinimizeGenerator.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto GenerateDFA = registration::AbstractRegister < automaton::generate::MinimizeGenerator, automaton::DFA < DefaultSymbolType, unsigned >, size_t, size_t, size_t, size_t, const ext::set < DefaultSymbolType > &, double, size_t > ( automaton::generate::MinimizeGenerator::generateMinimizeDFA, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "statesMinimal", "statesDuplicates", "statesUnreachable", "statesUseless", "alphabet", "density", "steps" );

} /* namespace */

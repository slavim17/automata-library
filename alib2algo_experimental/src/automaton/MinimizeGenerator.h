#pragma once

#include <automaton/FSM/DFA.h>

#include "RandomAutomatonFactory2.h"
#include <automaton/simplify/UnreachableStatesRemover.h>
#include <automaton/simplify/UselessStatesRemover.h>
#include <automaton/simplify/Minimize.h>
#include <automaton/generate/RandomizeAutomaton.h>

#include <exception/CommonException.h>

#include <global/GlobalData.h>

namespace automaton::generate {

class MinimizeGenerator {
public:
	template < class SymbolType >
	static automaton::DFA < SymbolType, unsigned > generateMinimizeDFA ( size_t statesMinimal, size_t statesDuplicates, size_t statesUnreachable, size_t statesUseless, const ext::set < SymbolType > & alphabet, double density, size_t expectedSteps );
};

template < class SymbolType >
automaton::DFA < SymbolType, unsigned > MinimizeGenerator::generateMinimizeDFA ( size_t statesMinimal, size_t statesDuplicates, size_t statesUnreachable, size_t statesUseless, const ext::set < SymbolType > & alphabet, double density, size_t expectedSteps ) {
	size_t limit = 10000;

	ext::ostringstream ss;
	auto backup = common::Streams::log;
	common::Streams::log = ext::reference_wrapper < ext::ostream > ( ss );

	auto verbose = common::GlobalData::verbose;
	common::GlobalData::verbose = true;

	while ( limit -- ) {
		ss.str ( "" );

		automaton::DFA < SymbolType, unsigned > automaton = RandomAutomatonFactory2::generateDFA ( statesMinimal, statesDuplicates, statesUnreachable, statesUseless, alphabet, density );
		automaton = automaton::generate::RandomizeAutomaton::randomize ( automaton );

		size_t states = automaton.getStates ( ).size ( );
		automaton::DFA < SymbolType, unsigned > unreachable = automaton::simplify::UnreachableStatesRemover::remove ( automaton );
		if ( unreachable.getStates ( ).size ( ) + statesUnreachable != states )
			continue;

		states = unreachable.getStates ( ).size ( );
		automaton::DFA < SymbolType, unsigned > useless = automaton::simplify::UselessStatesRemover::remove ( unreachable );
		if ( useless.getStates ( ).size ( ) + statesUseless != states )
			continue;

		size_t steps;
		states = useless.getStates ( ).size ( );
		automaton::DFA < SymbolType, unsigned > minimal = automaton::simplify::Minimize::minimize ( useless, steps );
		if ( ( expectedSteps != 0 && steps != expectedSteps ) || minimal.getStates ( ).size ( ) + statesDuplicates != states )
			continue;

		states = minimal.getStates ( ).size ( );
		if ( statesMinimal != states )
			continue;

		common::GlobalData::verbose = verbose;
		common::Streams::log = backup;

		if ( common::GlobalData::verbose )
			common::Streams::log << ss.str ( );

		return automaton;
	}

	throw exception::CommonException ( "Generating automaton for minimization failed." );
}

} /* namespace automaton::generate */

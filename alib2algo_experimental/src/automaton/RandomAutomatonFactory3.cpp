#include "RandomAutomatonFactory3.h"
#include <registration/AlgoRegistration.hpp>

namespace automaton::generate {

ext::vector < unsigned > RandomAutomatonFactory3::makeVector ( size_t begin, size_t end ) {
	ext::vector < unsigned > res;
	for ( ; begin < end; ++ begin )
		res.push_back ( begin );
	return res;
}

} /* namespace automaton::generate */

namespace {

auto Generate = registration::AbstractRegister < automaton::generate::RandomAutomatonFactory3, automaton::MultiInitialStateNFA < DefaultSymbolType, unsigned >, size_t, size_t, size_t, size_t, size_t, size_t, const ext::set < DefaultSymbolType > &, double, bool > ( automaton::generate::RandomAutomatonFactory3::generate, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "statesMinimal", "statesDuplicates", "statesUnreachable", "statesUseless", "initialStates", "finalStates", "alphabet", "density", "deterministic" );

} /* namespace */


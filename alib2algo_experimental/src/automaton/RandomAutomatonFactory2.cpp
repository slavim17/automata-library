#include "RandomAutomatonFactory2.h"
#include <registration/AlgoRegistration.hpp>

namespace automaton::generate {

size_t RandomAutomatonFactory2::randomSourceState ( size_t statesMinimal, size_t visited, size_t depleted, const ext::deque < bool > & VStates, const ext::deque < bool > & DStates ) {
	size_t y = ext::random_devices::semirandom() % ( visited - depleted ) + 1; // select y-th accessible state

	for( size_t i = 0, cnt = 0; i < statesMinimal; i++ ) {
		if( VStates [ i ] && ! DStates [ i ] )
			cnt ++;

		if( cnt == y )
			return i;
	}

	return -1;
}

size_t RandomAutomatonFactory2::randomTargetState ( size_t statesMinimal, size_t visited, const ext::deque < bool > & VStates ) {
	size_t z = ext::random_devices::semirandom() % ( statesMinimal - visited ) + 1; // select z-th inaccessible state

	for( size_t i = 0, cnt = 0; i < statesMinimal; i++ ) {
		if( ! VStates[ i ] )
			cnt ++;

		if( cnt == z )
			return i;
	}

	return -1;
}

} /* namespace automaton::generate */

namespace {

auto GenerateDFA = registration::AbstractRegister < automaton::generate::RandomAutomatonFactory2, automaton::DFA < DefaultSymbolType, unsigned >, size_t, size_t, size_t, size_t, const ext::set < DefaultSymbolType > &, double > ( automaton::generate::RandomAutomatonFactory2::generateDFA, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "statesMinimal", "statesDuplicates", "statesUnreachable", "statesUseless", "alphabet", "density" );

} /* namespace */

#include "TestLine.hpp"

#include <lexer/Lexer.h>
#include <parser/Parser.h>

#include <readline/StringLineInterface.h>

#include <ext/exception>
#include <global/GlobalData.h>

void testLine ( std::string line, cli::Environment & environment ) {
	try {
		cli::Parser ( cli::Lexer ( cli::CharSequence ( cli::StringLineInterface ( line ) ) ) ).parse ( )->run ( environment );
	} catch ( ... ) {
		alib::ExceptionHandler::handle ( common::Streams::err );
		throw;
	}
}

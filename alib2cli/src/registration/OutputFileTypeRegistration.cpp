#include <ext/typeinfo>

#include "OutputFileTypeRegistration.hpp"

#include <exception/CommonException.h>

#include <registry/XmlRegistry.h>

#include <abstraction/XmlTokensComposerAbstraction.hpp>

#include <registry/Registry.h>

namespace {

	std::unique_ptr < abstraction::OperationAbstraction > dummy ( const core::type_details & ) {
		ext::vector < std::string > templateParams;
		ext::vector < core::type_details > paramTypes { core::type_details::get < std::string > ( ), core::type_details::universal_type ( ) };
		abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;
		ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > paramTypeQualifiers { abstraction::TypeQualifiers::typeQualifiers < const std::string & > ( ), abstraction::TypeQualifiers::typeQualifiers < const std::string & > ( ) };

		return abstraction::Registry::getAlgorithmAbstraction ( "xml::builtin::WriteFile", templateParams, paramTypes, paramTypeQualifiers, category );
	}

	std::unique_ptr < abstraction::OperationAbstraction > dummy2 ( const core::type_details & ) {
		ext::vector < std::string > templateParams;
		ext::vector < core::type_details > paramTypes { core::type_details::get < std::string > ( ), core::type_details::get < std::string > ( ) };
		abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;
		ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > paramTypeQualifiers { abstraction::TypeQualifiers::typeQualifiers < const std::string & > ( ), abstraction::TypeQualifiers::typeQualifiers < const std::string & > ( ) };

		return abstraction::Registry::getAlgorithmAbstraction ( "cli::builtin::WriteFile", templateParams, paramTypes, paramTypeQualifiers, category );
	}

auto xmlOutputFileHandler = registration::OutputFileRegister ( "xml", dummy );
auto fileOutputFileHandler = registration::OutputFileRegister ( "file", dummy2 );

}

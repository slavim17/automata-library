#include <registry/InputFileRegistry.hpp>
#include <abstraction/XmlComposerAbstraction.hpp>

namespace abstraction {

ext::map < std::string, std::unique_ptr < InputFileRegistry::Entry > > & InputFileRegistry::getEntries ( ) {
	static ext::map < std::string, std::unique_ptr < Entry > > inputFileHandlers;
	return inputFileHandlers;
}

void InputFileRegistry::registerInputFileHandler ( const std::string & fileType, std::unique_ptr < abstraction::OperationAbstraction > ( * callback ) ( const core::type_details & type ) ) {
	auto iter = getEntries ( ).insert ( std::make_pair ( fileType, std::unique_ptr < Entry > ( new EntryImpl ( callback ) ) ) );
	if ( ! iter.second )
		throw std::invalid_argument ( "Entry " + iter.first->first + " already registered." );
}

void InputFileRegistry::unregisterInputFileHandler ( const std::string & fileType ) {
	if ( getEntries ( ).erase ( fileType ) == 0u )
		throw std::invalid_argument ( "Entry " + fileType + " not registered." );
}

std::unique_ptr < abstraction::OperationAbstraction > InputFileRegistry::getAbstraction ( const std::string & fileType, const core::type_details & type ) {
	auto res = getEntries ( ).find ( fileType );
	if ( res == getEntries ( ).end ( ) )
		throw exception::CommonException ( "Entry " + fileType + " not available." );

	return res->second->getAbstraction ( type );
}

std::unique_ptr < abstraction::OperationAbstraction > InputFileRegistry::EntryImpl::getAbstraction ( const core::type_details & type ) const {
	return m_callback ( type );
}

} /* namespace abstraction */

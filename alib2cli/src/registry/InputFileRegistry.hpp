#pragma once

#include <ext/memory>
#include <ext/functional>

#include <alib/string>
#include <alib/map>

#include <exception/CommonException.h>
#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class InputFileRegistry {
	class Entry {
	public:
		virtual std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const core::type_details & type ) const = 0;

		virtual ~Entry ( ) = default;
	};

	class EntryImpl : public Entry {
		std::function < std::unique_ptr < abstraction::OperationAbstraction > ( const core::type_details & type ) > m_callback;
	public:
		explicit EntryImpl ( std::unique_ptr < abstraction::OperationAbstraction > ( * callback ) ( const core::type_details & type ) ) : m_callback ( callback ) {
		}

		std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const core::type_details & type ) const override;
	};

	static ext::map < std::string, std::unique_ptr < Entry > > & getEntries ( );

public:
	static void registerInputFileHandler ( const std::string & fileType, std::unique_ptr < abstraction::OperationAbstraction > ( * callback ) ( const core::type_details & type ) );

	static void unregisterInputFileHandler ( const std::string & fileType );

	static std::unique_ptr < abstraction::OperationAbstraction > getAbstraction ( const std::string & fileType, const core::type_details & type );
};

} /* namespace abstraction */


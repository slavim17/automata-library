#pragma once

#include <abstraction/ValueHolderInterface.hpp>
#include <exception/CommonException.h>
#include <memory>
#include <registry/Registry.h>

namespace cli {

class ResultInterpret {
	template < class T >
	static std::optional < T > tryInterpretation ( std::shared_ptr < abstraction::Value > result ) {
		core::type_details type = core::type_details::get < T > ( );
		if ( result->getActualType ( ).compatible_with ( type ) && result->getDeclaredType ( ) != type ) {
			std::shared_ptr < abstraction::OperationAbstraction > denormalize = abstraction::Registry::getDenormalizeAbstraction ( type );
			denormalize->attachInput ( result, 0 );
			result = denormalize->eval ( );
			if ( ! result )
				throw std::invalid_argument ( "Internal error in denormalization." );
		}

		std::shared_ptr < abstraction::ValueHolderInterface < T > > ptr = std::dynamic_pointer_cast < abstraction::ValueHolderInterface < T > > ( result );
		if ( ptr )
			return ptr->getValue ( );
		else
			return std::nullopt;
	}

public:
	static int cli ( const std::shared_ptr < abstraction::Value > & result ) {
		if ( result ) {
			std::optional < int > res1 = tryInterpretation < int > ( result );
			if ( res1 )
				return res1.value ( );

			std::optional < bool > res2 = tryInterpretation < bool > ( result );
			if ( res2 )
				return static_cast < int > ( ! res2.value ( ) );

			std::optional < unsigned > res3 = tryInterpretation < unsigned > ( result );
			if ( res3 )
				return static_cast < int > ( res3.value ( ) );

			throw exception::CommonException ( "Invalid result type. Provided: " + ext::to_string ( result->getActualType ( ) ) );
		} else {
			return 0;
		}
	}

	template < class T >
	static T value ( std::shared_ptr < abstraction::Value > result ) {
		if ( result ) {
			std::optional < T > res = tryInterpretation < T > ( result );
			if ( res )
				return res.value ( );
		}

		throw exception::CommonException ( "Type " + ext::to_string < T > ( ) + " not provided, instead " + ext::to_string ( result->getActualType ( ) ) + " provided." );
	}
};

} /* namespace cli */

#include "Move.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto move = ext::Register < void > ( [ ] ( ) {
		abstraction::AlgorithmRegistry::registerRaw < cli::builtin::Move > ( cli::builtin::Move::move, ext::pair < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet > { core::type_details::universal_type ( ), abstraction::TypeQualifiers::TypeQualifierSet::RREF }, ext::vector < ext::tuple < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet, std::string > > { { core::type_details::universal_type ( ), abstraction::TypeQualifiers::TypeQualifierSet::LREF, "arg0" } } );
	}, [ ] ( ) {
		abstraction::AlgorithmRegistry::unregisterRaw < cli::builtin::Move > ( ext::vector < ext::pair < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet > > { { core::type_details::universal_type ( ), abstraction::TypeQualifiers::TypeQualifierSet::LREF } } );
	} );

} /* namespace */

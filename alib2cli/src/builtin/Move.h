#pragma once

#include <abstraction/Value.hpp>
#include <vector>

namespace cli {

namespace builtin {

/**
 * Builtin move on abstraction values.
 *
 */
class Move {
public:
	static std::shared_ptr < abstraction::Value > move ( const std::vector < std::shared_ptr < abstraction::Value > > & params ) {
		if ( params [ 0 ]->isTemporary ( ) ) {
			abstraction::TypeQualifiers::TypeQualifierSet typeQualifiers = abstraction::TypeQualifiers::TypeQualifierSet::RREF;
			if ( abstraction::TypeQualifiers::isConst ( params [ 0 ]->getTypeQualifiers ( ) ) )
				typeQualifiers = typeQualifiers | abstraction::TypeQualifiers::TypeQualifierSet::CONST;

			return params [ 0 ]->clone ( typeQualifiers, true );
		} else {
			abstraction::TypeQualifiers::TypeQualifierSet typeQualifiers = abstraction::TypeQualifiers::TypeQualifierSet::RREF;
			if ( abstraction::TypeQualifiers::isConst ( params [ 0 ]->getTypeQualifiers ( ) ) )
				typeQualifiers = typeQualifiers | abstraction::TypeQualifiers::TypeQualifierSet::CONST;

			return std::make_shared < abstraction::ValueReference > ( params [ 0 ], typeQualifiers, true );
		}
	}
};

} /* namespace builtin */

} /* namespace cli */


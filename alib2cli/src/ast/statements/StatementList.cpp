#include <ast/statements/StatementList.h>

namespace cli {

StatementList::StatementList ( ext::vector < std::shared_ptr < Statement > > statements ) : m_statements ( std::move ( statements ) ) {
}

std::shared_ptr < abstraction::Value > StatementList::translateAndEval ( const std::shared_ptr < abstraction::Value > &, Environment & environment ) const {
	if ( m_statements.empty ( ) )
		throw std::invalid_argument ( "Statement list can't be empty" );

	std::shared_ptr < abstraction::Value > next = m_statements [ 0 ]->translateAndEval ( nullptr, environment );
	for ( size_t i = 1; i < m_statements.size ( ); ++ i )
		next = m_statements [ i ]->translateAndEval ( next, environment );

	return next;
}

void StatementList::append ( std::shared_ptr < Statement > statement ) {
	m_statements.emplace_back ( std::move ( statement ) );
}

} /* namespace cli */

#pragma once

#include <ast/Statement.h>

namespace cli {

class StatementList final : public Statement {
	ext::vector < std::shared_ptr < Statement > > m_statements;

public:
	StatementList ( ext::vector < std::shared_ptr < Statement > > statements );

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > &, Environment & environment ) const override;

	void append ( std::shared_ptr < Statement > statement );

};

} /* namespace cli */

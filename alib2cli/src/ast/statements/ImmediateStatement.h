#pragma once

#include <ast/Statement.h>
#include <object/ObjectFactory.h>

namespace cli {

template < class Type >
class ImmediateStatement final : public Statement {
	Type m_value;

public:
	ImmediateStatement ( Type value ) : m_value ( std::move ( value ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > &, Environment & ) const override {
		return std::make_shared < abstraction::ValueHolder < object::Object > > ( object::ObjectFactory < >::construct ( m_value ), true );
	}

};

} /* namespace cli */

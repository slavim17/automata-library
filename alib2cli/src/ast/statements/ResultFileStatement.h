#pragma once

#include <ast/Statement.h>

#include <registry/OutputFileRegistry.hpp>

namespace cli {

class ResultFileStatement final : public Statement {
	std::unique_ptr < cli::Arg > m_file;
	std::unique_ptr < Arg > m_fileType;

public:
	ResultFileStatement ( std::unique_ptr < cli::Arg > file, std::unique_ptr < Arg > fileType ) : m_file ( std::move ( file ) ), m_fileType ( std::move ( fileType ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > & prev, Environment & environment ) const override {
		std::string filetype = "xml";
		if ( m_fileType )
			filetype = m_fileType->eval ( environment );

		std::unique_ptr < abstraction::OperationAbstraction > res = abstraction::OutputFileRegistry::getAbstraction ( filetype, prev->getDeclaredType ( ) );

		std::shared_ptr < abstraction::ValueHolder < std::string > > file = std::make_shared < abstraction::ValueHolder < std::string > > ( m_file->eval ( environment ), true );
		res->attachInput ( file, 0 );
		res->attachInput ( prev, 1 );
		return res->eval ( );
	}

};

} /* namespace cli */

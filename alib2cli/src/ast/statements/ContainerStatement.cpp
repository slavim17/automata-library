#include <exception>

#include <ext/iostream>

#include <ast/statements/ContainerStatement.h>
#include <ast/Option.h>
#include <ast/Arg.h>
#include <common/CastHelper.h>
#include <registry/Registry.h>

namespace cli {

ContainerStatement::ContainerStatement ( std::string container, ext::vector < std::shared_ptr < Statement > > params ) : m_container ( std::move ( container ) ), m_params ( std::move ( params ) ) {
}

std::shared_ptr < abstraction::Value > ContainerStatement::translateAndEval ( const std::shared_ptr < abstraction::Value > & prev, Environment & environment ) const {
	ext::vector < std::shared_ptr < abstraction::Value > > params;
	for ( const std::shared_ptr < Statement > & param : m_params ) {
		params.push_back ( param->translateAndEval ( prev, environment ) );
	}

	std::unique_ptr < abstraction::OperationAbstraction > algo = abstraction::Registry::getContainerAbstraction ( m_container );

	int i = 0;
	ext::vector < std::shared_ptr < abstraction::Value > > casted_params;
	for ( const std::shared_ptr < abstraction::Value > & param : params ) {
		casted_params.push_back ( abstraction::CastHelper::eval ( environment, param, algo->getParamType ( i ) ) );
		++ i;
	}

	i = 0;
	for ( const std::shared_ptr < abstraction::Value > & param : casted_params ) {
		algo->attachInput ( param, i );
		++ i;
	}

	std::shared_ptr < abstraction::Value > res = algo->eval ( );
	if ( ! res )
		throw std::invalid_argument ( "Eval of algorithm " + m_container + " failed." );

	return res;
}

} /* namespace cli */

#pragma once

#include <ast/Statement.h>
#include <ast/options/TypeOption.h>

namespace cli {

class FileStatement final : public Statement {
	std::unique_ptr < cli::Arg > m_file;
	std::unique_ptr < Arg > m_fileType;
	std::unique_ptr < TypeOption > m_type;

public:
	FileStatement ( std::unique_ptr < Arg > file, std::unique_ptr < Arg > fileType, std::unique_ptr < TypeOption > type );

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > &, Environment & environment ) const override;

};

} /* namespace cli */


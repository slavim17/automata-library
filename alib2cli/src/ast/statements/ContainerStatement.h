#pragma once

#include <ast/Statement.h>
#include <ast/options/TypeOption.h>

namespace cli {

class ContainerStatement final : public Statement {
	std::string m_container;
	ext::vector < std::shared_ptr < Statement > > m_params;

public:
	ContainerStatement ( std::string container, ext::vector < std::shared_ptr < Statement > > params );

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > & prev, Environment & environment ) const override;

};

} /* namespace cli */


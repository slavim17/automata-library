#pragma once

#include <ast/Expression.h>

#include <sstream>
#include <string>

namespace cli {

class ActualTypeExpression final : public Expression {
	std::unique_ptr < Expression > m_expression;

public:
	ActualTypeExpression ( std::unique_ptr < Expression > expression ) : m_expression ( std::move ( expression ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( Environment & environment ) const override {
		std::shared_ptr < abstraction::Value > translatedExpression = m_expression->translateAndEval ( environment );

		std::stringstream type;
		if ( abstraction::TypeQualifiers::isConst ( translatedExpression->getTypeQualifiers ( ) ) )
			type << "const ";
		type << translatedExpression->getActualType ( );
		if ( abstraction::TypeQualifiers::isRvalueRef ( translatedExpression->getTypeQualifiers ( ) ) )
			type << " &&";
		if ( abstraction::TypeQualifiers::isLvalueRef ( translatedExpression->getTypeQualifiers ( ) ) )
			type << " &";

		return std::make_shared < abstraction::ValueHolder < std::string > > ( type.str ( ), true );
	}

};

} /* namespace cli */

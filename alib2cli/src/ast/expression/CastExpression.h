#pragma once

#include <ast/Expression.h>
#include <common/CastHelper.h>

namespace cli {

class CastExpression final : public Expression {
	std::unique_ptr < cli::Arg > m_type;
	std::unique_ptr < Expression > m_expression;

public:
	CastExpression ( std::unique_ptr < cli::Arg > type, std::unique_ptr < Expression > expression ) : m_type ( std::move ( type ) ), m_expression ( std::move ( expression ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( Environment & environment ) const override {
		std::string type = m_type->eval ( environment );

		std::shared_ptr < abstraction::Value > translatedExpression = m_expression->translateAndEval ( environment );

		return abstraction::CastHelper::eval ( environment, translatedExpression, core::type_details::as_type ( type ) );
	}

};

} /* namespace cli */


#pragma once

#include <ast/Option.h>
#include <common/AlgorithmCategories.hpp>

namespace cli {

class CategoryOption final : public Option {
	std::string m_key;

public:
	CategoryOption ( std::string key ) : m_key ( std::move ( key ) ) {
	}

	abstraction::AlgorithmCategories::AlgorithmCategory getCategory ( ) const {
		return abstraction::AlgorithmCategories::algorithmCategory ( m_key );
	}

};

} /* namespace cli */


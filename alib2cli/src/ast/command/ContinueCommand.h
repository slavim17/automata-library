#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class ContinueCommand : public Command {
public:
	CommandResult run ( Environment & /* environment */ ) const override {
		return CommandResult::CONTINUE;
	}
};

} /* namespace cli */


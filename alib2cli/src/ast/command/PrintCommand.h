#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>
#include <ast/Expression.h>

namespace cli {

class PrintCommand : public Command {
	std::unique_ptr < Expression > m_expr;

public:
	PrintCommand ( std::unique_ptr < Expression > expr ) : m_expr ( std::move ( expr ) ) {
	}

	CommandResult run ( Environment & environment ) const override;
};

} /* namespace cli */


#include "PrintCommand.h"

#include <global/GlobalData.h>
#include <registry/Registry.h>

#include <core/type_details.hpp>

namespace cli {

CommandResult PrintCommand::run ( Environment & environment ) const {
	std::shared_ptr < abstraction::Value > value = m_expr->translateAndEval ( environment );

	if ( value->getDeclaredType ( ) == core::type_details::void_type ( ) )
		throw std::invalid_argument ( "Printing void is not allowed." );

	std::unique_ptr < abstraction::OperationAbstraction > res = abstraction::Registry::getValuePrinterAbstraction ( value->getDeclaredType ( ) );

	res->attachInput ( value, 0 );
	res->attachInput ( std::make_shared < abstraction::ValueHolder < ext::ostream & > > ( common::Streams::out, false ), 1 );
	std::shared_ptr < abstraction::Value > result = res->eval ( );
	if ( ! result )
		throw std::invalid_argument ( "Eval of result print statement failed." );

	return CommandResult::OK;
}

} /* namespace cli */

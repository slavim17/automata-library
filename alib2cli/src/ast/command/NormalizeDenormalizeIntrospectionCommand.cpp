#include "NormalizeDenormalizeIntrospectionCommand.h"

#include <registry/Registry.h>
#include <global/GlobalData.h>

namespace cli {

void NormalizeDenormalizeIntrospectionCommand::printTypes ( const ext::list < std::string > & types ) {
	for ( const std::string & type : types )
		common::Streams::out << type << std::endl;
}

CommandResult NormalizeDenormalizeIntrospectionCommand::run ( Environment & ) const {
	if ( m_what == What::NORMALIZE )
		printTypes ( abstraction::Registry::listNormalizations ( ) );

	if ( m_what == What::DENORMALIZE )
		printTypes ( abstraction::Registry::listDenormalizations ( ) );

	return CommandResult::OK;
}

} /* namespace cli */

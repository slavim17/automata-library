#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>
#include <ast/Statement.h>
#include <common/CastHelper.h>
#include <common/ResultInterpret.h>

namespace cli {

class IfCommand : public Command {
	std::unique_ptr < Expression > m_condition;
	std::unique_ptr < Command > m_thenBranch;
	std::unique_ptr < Command > m_elseBranch;

public:
	IfCommand ( std::unique_ptr < Expression > condition, std::unique_ptr < Command > thenBranch, std::unique_ptr < Command > elseBranch ) : m_condition ( std::move ( condition ) ), m_thenBranch ( std::move ( thenBranch ) ), m_elseBranch ( std::move ( elseBranch ) ) {
	}

	CommandResult run ( Environment & environment ) const override {
		std::shared_ptr < abstraction::Value > conditionResult = m_condition->translateAndEval ( environment );

		std::shared_ptr < abstraction::Value > castedResult = abstraction::CastHelper::eval ( environment, conditionResult, core::type_details::as_type ( "bool" ) );

		if ( cli::ResultInterpret::value < bool > ( castedResult ) )
			return m_thenBranch->run ( environment );
		else if ( m_elseBranch != nullptr )
			return m_elseBranch->run ( environment );

		return cli::CommandResult::OK;
	}
};

} /* namespace cli */


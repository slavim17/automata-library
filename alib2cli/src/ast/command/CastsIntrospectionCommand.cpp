#include "CastsIntrospectionCommand.h"

#include <registry/Registry.h>
#include <global/GlobalData.h>

namespace cli {

void CastsIntrospectionCommand::printTypes ( const ext::list < ext::pair < core::type_details, bool > > & types ) {
	for ( const ext::pair < core::type_details, bool > & type : types ) {
		common::Streams::out << type.first;
		if ( type.second )
			common::Streams::out << " explicit ";
		common::Streams::out << std::endl;
	}
}

void CastsIntrospectionCommand::printCasts ( const ext::list < ext::tuple < core::type_details, core::type_details, bool > > & casts ) {
	for ( const ext::tuple < core::type_details, core::type_details, bool > & cast : casts ) {
		common::Streams::out << std::get < 0 > ( cast ) << ", " << std::get < 1 > ( cast );
		if ( std::get < 2 > ( cast ) )
			common::Streams::out << " explicit";
		common::Streams::out << std::endl;
	}
}

CommandResult CastsIntrospectionCommand::run ( Environment & environment ) const {
	std::string param;
	if ( m_param != nullptr )
		param = m_param->eval ( environment );

	if ( m_from )
		printTypes ( abstraction::Registry::listCastsFrom ( core::type_details::as_type ( param ) ) );

	if ( m_to )
		printTypes ( abstraction::Registry::listCastsTo ( core::type_details::as_type ( param ) ) );

	if ( ! m_from && ! m_to )
		printCasts ( abstraction::Registry::listCasts ( ) );

	return CommandResult::OK;
}

} /* namespace cli */

#pragma once

#include <ast/Arg.h>
#include <ast/Command.h>
#include <environment/Environment.h>
#include <alib/list>
#include <alib/tuple>

namespace cli {

class CastsIntrospectionCommand : public Command {
	std::unique_ptr < cli::Arg > m_param;
	bool m_from;
	bool m_to;

	static void printTypes ( const ext::list < ext::pair < core::type_details, bool > > & types );

	static void printCasts ( const ext::list < ext::tuple < core::type_details, core::type_details, bool > > & casts );

public:
	CastsIntrospectionCommand ( std::unique_ptr < cli::Arg > param, bool from, bool to ) : m_param ( std::move ( param ) ), m_from ( from ), m_to ( to ) {
	}

	CommandResult run ( Environment & environment ) const override;
};

} /* namespace cli */

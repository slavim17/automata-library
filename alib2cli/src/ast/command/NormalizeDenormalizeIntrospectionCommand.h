#pragma once

#include <ast/Arg.h>
#include <ast/Command.h>
#include <environment/Environment.h>
#include <alib/list>
#include <alib/tuple>

namespace cli {

class NormalizeDenormalizeIntrospectionCommand : public Command {
public:
	enum class What {
		NORMALIZE,
		DENORMALIZE
	};

private:
	What m_what;

	static void printTypes ( const ext::list < std::string > & types );

public:
	NormalizeDenormalizeIntrospectionCommand ( What what ) : m_what ( what ) {
	}

	CommandResult run ( Environment & environment ) const override;
};

} /* namespace cli */

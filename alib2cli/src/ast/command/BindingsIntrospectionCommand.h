#pragma once

#include <ast/Arg.h>
#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class BindingsIntrospectionCommand : public Command {
	std::unique_ptr < cli::Arg > m_param;

public:
	BindingsIntrospectionCommand ( std::unique_ptr < cli::Arg > param ) : m_param ( std::move ( param ) ) {
	}

	CommandResult run ( Environment & environment ) const override;
};

} /* namespace cli */


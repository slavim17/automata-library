#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class BreakCommand : public Command {
public:
	CommandResult run ( Environment & /* environment */ ) const override {
		return CommandResult::BREAK;
	}
};

} /* namespace cli */


#include "SetCommand.h"

#include <global/GlobalData.h>
#include <ext/random>

namespace cli {

CommandResult SetCommand::run ( Environment & ) const {
	if ( m_param == "verbose" ) {
		common::GlobalData::verbose = ext::from_string < bool > ( m_value );
	} else if ( m_param == "measure" ) {
		common::GlobalData::measure = ext::from_string < bool > ( m_value );
	} else if ( m_param == "optimizeXml" ) {
		common::GlobalData::optimizeXml = ext::from_string < bool > ( m_value );
	} else if ( m_param == "seed" ) {
		ext::random_devices::semirandom.seed ( ext::from_string < unsigned > ( m_value ) );
	} else {
		common::Streams::out << "The set parameter " << m_param << " does not exist." << std::endl;
	}
	return CommandResult::OK;
}

} /* namespace cli */

#include "ShowMeasurements.h"

#include <global/GlobalData.h>

namespace cli {

CommandResult ShowMeasurements::run ( Environment & ) const {
	common::Streams::out << m_format << measurements::results ( ) << std::endl;
	return CommandResult::OK;
}

} /* namespace cli */

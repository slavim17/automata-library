#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>
#include <alib/list>
#include <registry/AlgorithmRegistryInfo.hpp>

namespace cli {

class OperatorsIntrospectionCommand : public Command {
	static void typePrint ( const ext::pair < core::type_details, abstraction::TypeQualifiers::TypeQualifierSet > & result, ext::ostream & os );

	template < class Operators > // INFO ok, only called from run inside OperatorsIntrospectionCommand.cpp
	static void printOperators ( const ext::list < ext::pair < Operators, abstraction::AlgorithmFullInfo > > & overloads );

public:
	OperatorsIntrospectionCommand ( ) = default;

	CommandResult run ( Environment & /* environment */ ) const override;
};

} /* namespace cli */

#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>
#include <ast/Expression.h>

namespace cli {

class CalcCommand : public Command {
	std::unique_ptr < cli::Expression > m_expr;

public:
	CalcCommand ( std::unique_ptr < cli::Expression > expr ) : m_expr ( std::move ( expr ) ) {
	}

	CommandResult run ( Environment & environment ) const override {
		m_expr->translateAndEval ( environment );

		return cli::CommandResult::OK;
	}
};

} /* namespace cli */

